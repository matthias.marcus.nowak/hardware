# House
The House represents a common residential prosumer, in which the power absorbed from the grid is mainly due to household appliances. The ambient can be thermo-regulated using a Peltier module in H-bridge configuration, allowing both heating and cooling. The current temperature is visualized on a 3-digit LED display, but it switches to target temperature when using the increment/decrement touch buttons. A PV-cell on the roof generates additional power, which can freely flow back into the grid if in surplus. The circuitry includes a power generation sub-block, a telemetry sub-block for monitoring the self power consumption in the 3.3 V bus, a telemetry sub-block for monitoring the external power generation from the PV cell, a thermo-regulation sub-block, a sub-block for the visulization of energy mix and active appliances and a sub-block for the LEDs display.

## Schematic
[<img src="docs/house_sch.png"  width="889" height="500">](docs/house_sch-main.png)

[<img src="docs/house_sch-sub_connectors.png"  width="145" height="100">](docs/house_sch-sub_connectors.png)
&nbsp;
[<img src="docs/house_sch-sub_power_slave.png"  width="145" height="100">](docs/house_sch-sub_power_slave.png)
&nbsp;
[<img src="docs/house_sch-sub_display.png"  width="145" height="100">](docs/house_sch-sub_display.png)
&nbsp;
[<img src="docs/house_sch-sub_load_electrical.png"  width="145" height="100">](docs/house_sch-sub_load_electrical.png)
&nbsp;
[<img src="docs/house_sch-sub_load_thermal.png"  width="145" height="100">](docs/house_sch-sub_load_thermal.png)
&nbsp;
[<img src="docs/house_sch-sub_telemetry.png"  width="145" height="100">](docs/house_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/house_sch-sub_telemetry-ext.png"  width="145" height="100">](docs/house_sch-sub_telemetry-ext.png)
&nbsp;
[<img src="docs/house_sch-sub_mechanical.png"  width="145" height="100">](docs/house_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/house_pcb.png"  width="569" height="500">](docs/house_pcb-brd.png)

[<img src="docs/house_pcb-F_Cu.png"  width="145" height="100">](docs/house_pcb-F_Cu.png)
&nbsp;
[<img src="docs/house_pcb-In1_Cu.png"  width="145" height="100">](docs/house_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/house_pcb-In2_Cu.png"  width="145" height="100">](docs/house_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/house_pcb-B_Cu.png"  width="145" height="100">](docs/house_pcb-B_Cu.png)

## Media

[<img src="docs/house_3d-top.png"  width="103" height="100">](docs/house_3d-top.png)
&nbsp;
[<img src="docs/house_3d-bottom.png"  width="102" height="100">](docs/house_3d-bottom.png)
&nbsp;
[<img src="docs/house_3d_side-top.png"  width="15" height="100">](docs/house_3d_side-top.png)
&nbsp;
[<img src="docs/house_3d_side-bottom.png"  width="15" height="100">](docs/house_3d_side-bottom.png)
&nbsp;
[<img src="docs/house_brd.png"  width="149" height="100">](docs/house_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/house/house.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/house/house.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/house/house.md)