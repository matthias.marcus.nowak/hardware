EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Power Supply"
Date "2020-07-20"
Rev "1.0"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6400 3300 6400 3400
$Comp
L Device:C_Small C?
U 1 1 5F48C62C
P 6400 3200
AR Path="/5F011FA8/5F48C62C" Ref="C?"  Part="1" 
AR Path="/5F4872E5/5F48C62C" Ref="C?"  Part="1" 
AR Path="/5F48C62C" Ref="C?"  Part="1" 
F 0 "C?" H 6350 3150 39  0000 R CNN
F 1 "4.7u" H 6400 3250 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6400 3200 50  0001 C CNN
F 3 "~" H 6400 3200 50  0001 C CNN
	1    6400 3200
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F48C632
P 6400 3400
AR Path="/5F011FA8/5F48C632" Ref="#PWR?"  Part="1" 
AR Path="/5F4872E5/5F48C632" Ref="#PWR?"  Part="1" 
AR Path="/5F48C632" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6400 3150 50  0001 C CNN
F 1 "GND" H 6405 3227 50  0000 C CNN
F 2 "" H 6400 3400 50  0001 C CNN
F 3 "" H 6400 3400 50  0001 C CNN
	1    6400 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F48C639
P 5700 3400
AR Path="/5F011FA8/5F48C639" Ref="#PWR?"  Part="1" 
AR Path="/5F4872E5/5F48C639" Ref="#PWR?"  Part="1" 
AR Path="/5F48C639" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5700 3150 50  0001 C CNN
F 1 "GND" H 5705 3227 50  0000 C CNN
F 2 "" H 5700 3400 50  0001 C CNN
F 3 "" H 5700 3400 50  0001 C CNN
	1    5700 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 3000 5000 3100
$Comp
L power:GND #PWR?
U 1 1 5F48C641
P 5000 3400
AR Path="/5F011FA8/5F48C641" Ref="#PWR?"  Part="1" 
AR Path="/5F4872E5/5F48C641" Ref="#PWR?"  Part="1" 
AR Path="/5F48C641" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5000 3150 50  0001 C CNN
F 1 "GND" H 5005 3227 50  0000 C CNN
F 2 "" H 5000 3400 50  0001 C CNN
F 3 "" H 5000 3400 50  0001 C CNN
	1    5000 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 3300 5000 3400
Wire Wire Line
	6400 3000 6400 3100
$Comp
L Device:C_Small C?
U 1 1 5F48C650
P 5000 3200
AR Path="/5F011FA8/5F48C650" Ref="C?"  Part="1" 
AR Path="/5F4872E5/5F48C650" Ref="C?"  Part="1" 
AR Path="/5F48C650" Ref="C?"  Part="1" 
F 0 "C?" H 4950 3150 39  0000 R CNN
F 1 "4.7u" H 5000 3250 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5000 3200 50  0001 C CNN
F 3 "~" H 5000 3200 50  0001 C CNN
	1    5000 3200
	1    0    0    1   
$EndComp
$Comp
L LEGOS:AP2114HA U?
U 1 1 5F48C656
P 5700 3100
AR Path="/5F011FA8/5F48C656" Ref="U?"  Part="1" 
AR Path="/5F4872E5/5F48C656" Ref="U?"  Part="1" 
AR Path="/5F48C656" Ref="U?"  Part="1" 
F 0 "U?" H 5700 3442 50  0000 C CNN
F 1 "AP2114HA" H 5700 3351 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5700 3425 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/115/AP2114-271472.pdf" H 5700 3200 50  0001 C CNN
	1    5700 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3000 5400 3000
Wire Wire Line
	6000 3000 6400 3000
Text HLabel 4800 3000 0    50   Input ~ 0
VIN
Text HLabel 6600 3000 2    50   Output ~ 0
VOUT
Wire Wire Line
	6600 3000 6400 3000
Connection ~ 6400 3000
Wire Wire Line
	5000 3000 4800 3000
Connection ~ 5000 3000
$EndSCHEMATC
