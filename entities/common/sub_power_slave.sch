EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Power Regulation - Slave"
Date "2020-08-20"
Rev "1.0"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7850 3825 7850 3975
$Comp
L power:GND #PWR?
U 1 1 5ED52544
P 7850 3975
F 0 "#PWR?" H 7850 3725 50  0001 C CNN
F 1 "GND" H 7855 3802 50  0000 C CNN
F 2 "" H 7850 3975 50  0001 C CNN
F 3 "" H 7850 3975 50  0001 C CNN
	1    7850 3975
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3625 7850 3325
$Comp
L LEGOS:AD8400ARZ1 U?
U 1 1 5ECF02F9
P 5400 4250
F 0 "U?" H 5050 3800 50  0000 C CNN
F 1 "AD8400ARZ10" H 5050 3700 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5400 3650 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD8400_8402_8403.pdf" H 4950 4650 50  0001 C CNN
	1    5400 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5ECF02FF
P 5400 4850
F 0 "#PWR?" H 5400 4600 50  0001 C CNN
F 1 "GND" H 5405 4677 50  0000 C CNN
F 2 "" H 5400 4850 50  0001 C CNN
F 3 "" H 5400 4850 50  0001 C CNN
	1    5400 4850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5400 4750 5400 4850
Text HLabel 4900 4450 0    50   Input ~ 0
~CS
NoConn ~ 5900 4050
Wire Wire Line
	4500 4350 4500 4450
$Comp
L power:GND #PWR?
U 1 1 5ECFCB30
P 4500 4450
F 0 "#PWR?" H 4500 4200 50  0001 C CNN
F 1 "GND" H 4505 4277 50  0000 C CNN
F 2 "" H 4500 4450 50  0001 C CNN
F 3 "" H 4500 4450 50  0001 C CNN
	1    4500 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5ECFCB29
P 4500 4250
F 0 "C?" H 4450 4200 39  0000 R CNN
F 1 "100n" H 4500 4300 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4500 4250 50  0001 C CNN
F 3 "~" H 4500 4250 50  0001 C CNN
	1    4500 4250
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F0D91D6
P 5400 3850
F 0 "#PWR?" H 5400 3700 50  0001 C CNN
F 1 "+5V" H 5415 4023 50  0000 C CNN
F 2 "" H 5400 3850 50  0001 C CNN
F 3 "" H 5400 3850 50  0001 C CNN
	1    5400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3850 4500 3850
Wire Wire Line
	4500 3850 4500 4150
Text GLabel 4900 4050 0    39   Input ~ 0
MOSI
Text GLabel 4900 4250 0    39   Input ~ 0
CLK
$Comp
L LEGOS:LT3081R U?
U 1 1 5F3F89C6
P 6250 3125
F 0 "U?" H 6250 3615 50  0000 C CNN
F 1 "LT3081R" H 6250 3524 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-7_TabPin4" H 6250 3125 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/3081fc.pdf" H 6250 3125 50  0001 C CNN
	1    6250 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3175 5300 3275
$Comp
L power:GND #PWR?
U 1 1 5F41E135
P 5300 3275
F 0 "#PWR?" H 5300 3025 50  0001 C CNN
F 1 "GND" H 5305 3102 50  0000 C CNN
F 2 "" H 5300 3275 50  0001 C CNN
F 3 "" H 5300 3275 50  0001 C CNN
	1    5300 3275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2875 5300 2975
$Comp
L Device:CP_Small C?
U 1 1 5F425B4E
P 5300 3075
F 0 "C?" H 5150 3125 39  0000 L CNN
F 1 "100u" H 5075 3050 39  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B" H 5300 3075 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/212/1/KEM_T2050_T491_AUTO-1093567.pdf" H 5300 3075 50  0001 C CNN
F 4 "T491B107M006ATAUTO" H 5300 3075 50  0001 C CNN "Mfr"
	1    5300 3075
	1    0    0    -1  
$EndComp
Connection ~ 5300 2875
Wire Wire Line
	5300 2875 5450 2875
$Comp
L Device:R_Small R?
U 1 1 5F42CDE5
P 7250 3325
F 0 "R?" V 7375 3300 39  0000 L CNN
F 1 "0.5-1W" V 7325 3200 39  0000 L CNN
F 2 "Resistor_SMD:R_2512_6332Metric" H 7250 3325 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/303/res_pcs-1665423.pdf" H 7250 3325 50  0001 C CNN
F 4 "PCS2512FR5000ET" V 7250 3325 50  0001 C CNN "Mfr"
	1    7250 3325
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7150 3325 7050 3325
NoConn ~ 6700 3525
Wire Wire Line
	5650 3525 5650 3600
Wire Wire Line
	5650 3600 5900 3600
Wire Wire Line
	5900 3600 5900 3525
Wire Wire Line
	5900 3600 7050 3600
Wire Wire Line
	7050 3600 7050 3325
Connection ~ 5900 3600
Wire Wire Line
	7500 4450 7500 4250
Wire Wire Line
	7500 3325 7350 3325
Wire Wire Line
	6150 3525 6150 4250
$Comp
L Device:C_Small C?
U 1 1 5ED52550
P 6825 4250
F 0 "C?" V 6675 4300 39  0000 R CNN
F 1 "30p" V 6725 4300 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6825 4250 50  0001 C CNN
F 3 "~" H 6825 4250 50  0001 C CNN
	1    6825 4250
	0    -1   1    0   
$EndComp
Wire Wire Line
	6925 4250 7500 4250
Connection ~ 7500 4250
Wire Wire Line
	7500 4250 7500 3325
Wire Wire Line
	6725 4250 6150 4250
$Comp
L Device:CP_Small C?
U 1 1 5F434D08
P 7850 3725
F 0 "C?" H 7700 3775 39  0000 L CNN
F 1 "100u" H 7625 3700 39  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B" H 7850 3725 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/212/1/KEM_T2050_T491_AUTO-1093567.pdf" H 7850 3725 50  0001 C CNN
F 4 "T491B107M006ATAUTO" H 7850 3725 50  0001 C CNN "Mfr"
	1    7850 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4250 5900 4250
Connection ~ 6150 4250
Wire Wire Line
	5900 4450 7500 4450
Text HLabel 8150 3325 2    50   Output ~ 0
VOUT
Text HLabel 4800 2725 2    50   Input ~ 0
VIN
$Sheet
S 4150 2475 550  500 
U 5F4BF641
F0 "TelemetryExt" 39
F1 "sub_telemetry.sch" 39
F2 "VCOM" I R 4700 2575 39 
F3 "V+" I R 4700 2725 39 
F4 "V-" I R 4700 2875 39 
F5 "~IRQ" O L 4150 2575 39 
F6 "A1" I L 4150 2725 39 
F7 "A0" I L 4150 2875 39 
$EndSheet
Wire Wire Line
	4700 2725 4800 2725
Wire Wire Line
	7850 3325 7500 3325
Connection ~ 7500 3325
$Comp
L power:GND #PWR?
U 1 1 5F4CE682
P 4050 2725
F 0 "#PWR?" H 4050 2475 50  0001 C CNN
F 1 "GND" V 4050 2525 50  0000 C CNN
F 2 "" H 4050 2725 50  0001 C CNN
F 3 "" H 4050 2725 50  0001 C CNN
	1    4050 2725
	0    1    -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F4D12AF
P 4050 2875
AR Path="/5F011FA8/5F128218/5F4D12AF" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5F4D12AF" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F4D12AF" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4D12AF" Ref="#PWR?"  Part="1" 
AR Path="/5F4D12AF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4050 2725 50  0001 C CNN
F 1 "+3.3V" V 4050 3125 50  0000 C CNN
F 2 "" H 4050 2875 50  0001 C CNN
F 3 "" H 4050 2875 50  0001 C CNN
	1    4050 2875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 2725 4150 2725
Wire Wire Line
	4050 2875 4150 2875
Wire Wire Line
	4050 2575 4150 2575
Wire Wire Line
	8150 3325 7850 3325
Connection ~ 7850 3325
Wire Wire Line
	4700 2875 5300 2875
Wire Wire Line
	4700 2575 7500 2575
Wire Wire Line
	7500 2575 7500 3325
Text HLabel 4050 2575 0    50   Input ~ 0
~IRQ
Connection ~ 5400 3850
Connection ~ 7050 3325
$EndSCHEMATC
