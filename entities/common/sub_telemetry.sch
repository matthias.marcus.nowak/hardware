EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Current/Voltage/Power Monitor"
Date "2020-08-20"
Rev "1.2"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5125 3450 5325 3450
Wire Wire Line
	5125 3250 5125 3450
$Comp
L power:+3.3V #PWR?
U 1 1 5EF551CA
P 5125 3250
AR Path="/5F011FA8/5F128218/5EF551CA" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5EF551CA" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551CA" Ref="#PWR?"  Part="1" 
AR Path="/5EF551CA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5125 3100 50  0001 C CNN
F 1 "+3.3V" H 5140 3423 39  0000 C CNN
F 2 "" H 5125 3250 50  0001 C CNN
F 3 "" H 5125 3250 50  0001 C CNN
	1    5125 3250
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF551D0
P 5325 3350
AR Path="/5F011FA8/5F128218/5EF551D0" Ref="C?"  Part="1" 
AR Path="/5F46DFE3/5EF551D0" Ref="C?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551D0" Ref="C?"  Part="1" 
AR Path="/5EF551D0" Ref="C?"  Part="1" 
F 0 "C?" H 5417 3396 39  0000 L CNN
F 1 "100n" H 5417 3305 39  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5325 3350 50  0001 C CNN
F 3 "~" H 5325 3350 50  0001 C CNN
	1    5325 3350
	1    0    0    -1  
$EndComp
$Comp
L Analog_ADC:INA233 U?
U 1 1 5EF551D6
P 5125 3950
AR Path="/5F011FA8/5F128218/5EF551D6" Ref="U?"  Part="1" 
AR Path="/5F46DFE3/5EF551D6" Ref="U?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551D6" Ref="U?"  Part="1" 
AR Path="/5EF551D6" Ref="U?"  Part="1" 
F 0 "U?" H 5175 3950 50  0000 R BNN
F 1 "INA233" H 4975 4050 50  0000 L BNN
F 2 "Package_SO:VSSOP-10_3x3mm_P0.5mm" H 5925 3500 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/ina233.pdf" H 5475 3850 50  0001 C CNN
	1    5125 3950
	1    0    0    -1  
$EndComp
Connection ~ 5125 3450
$Comp
L power:GND #PWR?
U 1 1 5EF551DD
P 5325 3250
AR Path="/5F011FA8/5F128218/5EF551DD" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5EF551DD" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551DD" Ref="#PWR?"  Part="1" 
AR Path="/5EF551DD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5325 3000 50  0001 C CNN
F 1 "GND" H 5330 3077 39  0000 C CNN
F 2 "" H 5325 3250 50  0001 C CNN
F 3 "" H 5325 3250 50  0001 C CNN
	1    5325 3250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF551E8
P 5125 4450
AR Path="/5F011FA8/5F128218/5EF551E8" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5EF551E8" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551E8" Ref="#PWR?"  Part="1" 
AR Path="/5EF551E8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5125 4200 50  0001 C CNN
F 1 "GND" H 5130 4277 39  0000 C CNN
F 2 "" H 5125 4450 50  0001 C CNN
F 3 "" H 5125 4450 50  0001 C CNN
	1    5125 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4375 4000 4525 4000
Text HLabel 4375 3650 0    39   Input ~ 0
VCOM
Wire Wire Line
	4725 3650 4375 3650
Text HLabel 4375 4000 0    39   Input ~ 0
V+
Text HLabel 4375 4200 0    39   Input ~ 0
V-
$Comp
L Device:R_Small R?
U 1 1 5F0D1D51
P 6175 3750
AR Path="/5F011FA8/5F128218/5F0D1D51" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F0D1D51" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D1D51" Ref="R?"  Part="1" 
AR Path="/5F0D1D51" Ref="R?"  Part="1" 
F 0 "R?" H 6234 3788 39  0000 L CNN
F 1 "4.7k" H 6234 3713 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6175 3750 50  0001 C CNN
F 3 "~" H 6175 3750 50  0001 C CNN
	1    6175 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F0D2A77
P 6475 3750
AR Path="/5F011FA8/5F128218/5F0D2A77" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F0D2A77" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D2A77" Ref="R?"  Part="1" 
AR Path="/5F0D2A77" Ref="R?"  Part="1" 
F 0 "R?" H 6534 3788 39  0000 L CNN
F 1 "4.7k" H 6534 3713 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6475 3750 50  0001 C CNN
F 3 "~" H 6475 3750 50  0001 C CNN
	1    6475 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F0D2C5D
P 6775 3750
AR Path="/5F011FA8/5F128218/5F0D2C5D" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F0D2C5D" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D2C5D" Ref="R?"  Part="1" 
AR Path="/5F0D2C5D" Ref="R?"  Part="1" 
F 0 "R?" H 6834 3788 39  0000 L CNN
F 1 "4.7k" H 6834 3713 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6775 3750 50  0001 C CNN
F 3 "~" H 6775 3750 50  0001 C CNN
	1    6775 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F0D2F61
P 6175 3650
AR Path="/5F011FA8/5F128218/5F0D2F61" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5F0D2F61" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D2F61" Ref="#PWR?"  Part="1" 
AR Path="/5F0D2F61" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6175 3500 50  0001 C CNN
F 1 "+3.3V" H 6190 3823 39  0000 C CNN
F 2 "" H 6175 3650 50  0001 C CNN
F 3 "" H 6175 3650 50  0001 C CNN
	1    6175 3650
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F0D3352
P 6475 3650
AR Path="/5F011FA8/5F128218/5F0D3352" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5F0D3352" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D3352" Ref="#PWR?"  Part="1" 
AR Path="/5F0D3352" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6475 3500 50  0001 C CNN
F 1 "+3.3V" H 6490 3823 39  0000 C CNN
F 2 "" H 6475 3650 50  0001 C CNN
F 3 "" H 6475 3650 50  0001 C CNN
	1    6475 3650
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F0D3606
P 6775 3650
AR Path="/5F011FA8/5F128218/5F0D3606" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5F0D3606" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D3606" Ref="#PWR?"  Part="1" 
AR Path="/5F0D3606" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6775 3500 50  0001 C CNN
F 1 "+3.3V" H 6790 3823 39  0000 C CNN
F 2 "" H 6775 3650 50  0001 C CNN
F 3 "" H 6775 3650 50  0001 C CNN
	1    6775 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5525 3950 6175 3950
Wire Wire Line
	6175 3850 6175 3950
Connection ~ 6175 3950
Wire Wire Line
	6175 3950 7075 3950
Wire Wire Line
	5525 4050 6475 4050
Wire Wire Line
	6475 3850 6475 4050
Connection ~ 6475 4050
Wire Wire Line
	6475 4050 7075 4050
Wire Wire Line
	5525 4250 6775 4250
Wire Wire Line
	6775 3850 6775 4250
Connection ~ 6775 4250
Wire Wire Line
	6775 4250 7075 4250
Wire Wire Line
	4375 4200 4525 4200
Text GLabel 7075 3950 2    39   Input ~ 0
SDA
Text GLabel 7075 4050 2    39   Input ~ 0
SCL
Text HLabel 7075 4250 2    39   Output ~ 0
~IRQ
$Comp
L Device:R_Small R?
U 1 1 5F12603D
P 4525 4100
AR Path="/5F011FA8/5F128218/5F12603D" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F12603D" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F12603D" Ref="R?"  Part="1" 
AR Path="/5F12603D" Ref="R?"  Part="1" 
F 0 "R?" H 4575 4050 39  0000 L CNN
F 1 "50m" H 4575 4100 39  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 4525 4100 50  0001 C CNN
F 3 "https://www.ohmite.com/assets/docs/res_lvk.pdf" H 4525 4100 50  0001 C CNN
F 4 "LVK12R050CER" V 4525 4100 50  0001 C CNN "Mfr"
	1    4525 4100
	1    0    0    1   
$EndComp
Text Notes 4350 4125 0    28   ~ 0
0.25%
Wire Wire Line
	4725 4000 4725 4050
Wire Wire Line
	4725 4150 4725 4200
Connection ~ 4525 4200
Wire Wire Line
	4525 4200 4725 4200
Connection ~ 4525 4000
Wire Wire Line
	4525 4000 4725 4000
Text HLabel 5625 3650 2    39   Input ~ 0
A1
Text HLabel 5625 3750 2    39   Input ~ 0
A0
Wire Wire Line
	5625 3650 5525 3650
Wire Wire Line
	5625 3750 5525 3750
Text Notes 4275 3950 0    39   ~ 0
Vmax = 82 mV
$EndSCHEMATC
