# Antenna
The Base Transceiver Station is a docking station for a RaspberryPi single-board computer, which is connected in vertical position using a side-entry connector. The circuitry includes a time delay startup circuit, a telemetry sub-block for monitoring the self power consumption in the 5V bus and a sub-block for programmable LEDs animations.
## Schematic
[<img src="docs/antenna_sch.png"  width="653" height="500">](docs/antenna_sch-main.png)

[<img src="docs/antenna_sch-sub_connectors.png"  width="145" height="100">](docs/antenna_sch-sub_connectors.png)
&nbsp;
[<img src="docs/antenna_sch-sub_led.png"  width="145" height="100">](docs/antenna_sch-sub_led.png)
&nbsp;
[<img src="docs/antenna_sch-sub_telemetry.png"  width="145" height="100">](docs/antenna_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/antenna_sch-sub_mechanical.png"  width="145" height="100">](docs/antenna_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/antenna_pcb.png"  width="531" height="500">](docs/antenna_pcb-brd.png)

[<img src="docs/antenna_pcb-F_Cu.png"  width="145" height="100">](docs/antenna_pcb-F_Cu.png)
&nbsp;
[<img src="docs/antenna_pcb-In1_Cu.png"  width="145" height="100">](docs/antenna_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/antenna_pcb-In2_Cu.png"  width="145" height="100">](docs/antenna_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/antenna_pcb-B_Cu.png"  width="145" height="100">](docs/antenna_pcb-B_Cu.png)

## Media

[<img src="docs/antenna_3d-top.png"  width="84" height="100">](docs/antenna_3d-top.png)
&nbsp;
[<img src="docs/antenna_3d-bottom.png"  width="84" height="100">](docs/antenna_3d-bottom.png)
&nbsp;
[<img src="docs/antenna_brd.png"  width="111" height="100">](docs/antenna_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/antenna/antenna.md)
* [Software]() 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/antenna/antenna.md)
