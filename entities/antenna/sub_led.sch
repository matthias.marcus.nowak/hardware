EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "LEDs"
Date "2020-10-06"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3725 3975 3725 4125
Text Notes 2875 4075 0    39   ~ 0
3xLED\nVf = 3 V @ If = 10 mA
$Comp
L Device:R_Small R?
U 1 1 5F77AD0E
P 3725 3475
AR Path="/5F2104BF/5F77AD0E" Ref="R?"  Part="1" 
AR Path="/5F77AD0E" Ref="R?"  Part="1" 
AR Path="/5F5F0002/5F77AD0E" Ref="R?"  Part="1" 
AR Path="/5EFE1C3B/5F77AD0E" Ref="R?"  Part="1" 
AR Path="/5F6BC8E7/5F77AD0E" Ref="R8"  Part="1" 
F 0 "R8" H 3784 3513 39  0000 L CNN
F 1 "68" H 3784 3438 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3725 3475 50  0001 C CNN
F 3 "~" H 3725 3475 50  0001 C CNN
	1    3725 3475
	1    0    0    -1  
$EndComp
Wire Wire Line
	3725 3575 3725 3675
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F560EC9
P 3625 4325
AR Path="/5F2104BF/5F560EC9" Ref="Q?"  Part="1" 
AR Path="/5F560EC9" Ref="Q?"  Part="1" 
AR Path="/5F5F0002/5F560EC9" Ref="Q?"  Part="1" 
AR Path="/5F496ED4/5F560EC9" Ref="Q?"  Part="1" 
AR Path="/5EFE1C3B/5F560EC9" Ref="Q?"  Part="1" 
AR Path="/5F6BC8E7/5F560EC9" Ref="Q3"  Part="1" 
F 0 "Q3" H 3830 4363 39  0000 L CNN
F 1 "Si2302CDS" H 3830 4288 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3825 4425 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 3625 4325 50  0001 C CNN
	1    3625 4325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F560ECF
P 3725 4525
AR Path="/5F2104BF/5F560ECF" Ref="#PWR?"  Part="1" 
AR Path="/5F560ECF" Ref="#PWR?"  Part="1" 
AR Path="/5F5F0002/5F560ECF" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F560ECF" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F560ECF" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 3725 4275 50  0001 C CNN
F 1 "GND" H 3730 4360 39  0000 C CNN
F 2 "" H 3725 4525 50  0001 C CNN
F 3 "" H 3725 4525 50  0001 C CNN
	1    3725 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 4325 3325 4325
Wire Wire Line
	3725 3375 3725 3275
$Comp
L power:+5V #PWR?
U 1 1 5F6F1624
P 3725 3275
AR Path="/5F102375/5F6F1624" Ref="#PWR?"  Part="1" 
AR Path="/5F2104BF/5F6F1624" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F6F1624" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F6F1624" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 3725 3125 50  0001 C CNN
F 1 "+5V" H 3725 3450 39  0000 C CNN
F 2 "" H 3725 3275 50  0001 C CNN
F 3 "" H 3725 3275 50  0001 C CNN
	1    3725 3275
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5F6FAD32
P 3725 3825
AR Path="/5F2104BF/5F6FAD32" Ref="D?"  Part="1" 
AR Path="/5F6FAD32" Ref="D?"  Part="1" 
AR Path="/5F5F0002/5F6FAD32" Ref="D?"  Part="1" 
AR Path="/5F496ED4/5F6FAD32" Ref="D?"  Part="1" 
AR Path="/5EFE1C3B/5F6FAD32" Ref="D?"  Part="1" 
AR Path="/5F6BC8E7/5F6FAD32" Ref="D1"  Part="1" 
F 0 "D1" V 3756 3707 39  0000 R CNN
F 1 "LED" V 3675 3675 39  0000 R CNN
F 2 "" H 3725 3825 50  0001 C CNN
F 3 "" H 3725 3825 50  0001 C CNN
	1    3725 3825
	0    1    -1   0   
$EndComp
Wire Wire Line
	4825 3975 4825 4125
Wire Wire Line
	4825 3575 4825 3675
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F72C266
P 4725 4325
AR Path="/5F2104BF/5F72C266" Ref="Q?"  Part="1" 
AR Path="/5F72C266" Ref="Q?"  Part="1" 
AR Path="/5F5F0002/5F72C266" Ref="Q?"  Part="1" 
AR Path="/5F496ED4/5F72C266" Ref="Q?"  Part="1" 
AR Path="/5EFE1C3B/5F72C266" Ref="Q?"  Part="1" 
AR Path="/5F6BC8E7/5F72C266" Ref="Q4"  Part="1" 
F 0 "Q4" H 4930 4363 39  0000 L CNN
F 1 "Si2302CDS" H 4930 4288 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4925 4425 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 4725 4325 50  0001 C CNN
	1    4725 4325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F72C26C
P 4825 4525
AR Path="/5F2104BF/5F72C26C" Ref="#PWR?"  Part="1" 
AR Path="/5F72C26C" Ref="#PWR?"  Part="1" 
AR Path="/5F5F0002/5F72C26C" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F72C26C" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F72C26C" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 4825 4275 50  0001 C CNN
F 1 "GND" H 4830 4360 39  0000 C CNN
F 2 "" H 4825 4525 50  0001 C CNN
F 3 "" H 4825 4525 50  0001 C CNN
	1    4825 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 4325 4425 4325
Wire Wire Line
	4825 3375 4825 3275
$Comp
L power:+5V #PWR?
U 1 1 5F72C274
P 4825 3275
AR Path="/5F102375/5F72C274" Ref="#PWR?"  Part="1" 
AR Path="/5F2104BF/5F72C274" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F72C274" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F72C274" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 4825 3125 50  0001 C CNN
F 1 "+5V" H 4825 3450 39  0000 C CNN
F 2 "" H 4825 3275 50  0001 C CNN
F 3 "" H 4825 3275 50  0001 C CNN
	1    4825 3275
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5F72C27B
P 4825 3825
AR Path="/5F2104BF/5F72C27B" Ref="D?"  Part="1" 
AR Path="/5F72C27B" Ref="D?"  Part="1" 
AR Path="/5F5F0002/5F72C27B" Ref="D?"  Part="1" 
AR Path="/5F496ED4/5F72C27B" Ref="D?"  Part="1" 
AR Path="/5EFE1C3B/5F72C27B" Ref="D?"  Part="1" 
AR Path="/5F6BC8E7/5F72C27B" Ref="D2"  Part="1" 
F 0 "D2" V 4856 3707 39  0000 R CNN
F 1 "LED" V 4775 3675 39  0000 R CNN
F 2 "" H 4825 3825 50  0001 C CNN
F 3 "" H 4825 3825 50  0001 C CNN
	1    4825 3825
	0    1    -1   0   
$EndComp
Wire Wire Line
	5925 3975 5925 4125
Wire Wire Line
	5925 3575 5925 3675
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F72FC86
P 5825 4325
AR Path="/5F2104BF/5F72FC86" Ref="Q?"  Part="1" 
AR Path="/5F72FC86" Ref="Q?"  Part="1" 
AR Path="/5F5F0002/5F72FC86" Ref="Q?"  Part="1" 
AR Path="/5F496ED4/5F72FC86" Ref="Q?"  Part="1" 
AR Path="/5EFE1C3B/5F72FC86" Ref="Q?"  Part="1" 
AR Path="/5F6BC8E7/5F72FC86" Ref="Q5"  Part="1" 
F 0 "Q5" H 6030 4363 39  0000 L CNN
F 1 "Si2302CDS" H 6030 4288 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6025 4425 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 5825 4325 50  0001 C CNN
	1    5825 4325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F72FC8C
P 5925 4525
AR Path="/5F2104BF/5F72FC8C" Ref="#PWR?"  Part="1" 
AR Path="/5F72FC8C" Ref="#PWR?"  Part="1" 
AR Path="/5F5F0002/5F72FC8C" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F72FC8C" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F72FC8C" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 5925 4275 50  0001 C CNN
F 1 "GND" H 5930 4360 39  0000 C CNN
F 2 "" H 5925 4525 50  0001 C CNN
F 3 "" H 5925 4525 50  0001 C CNN
	1    5925 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5625 4325 5525 4325
Wire Wire Line
	5925 3375 5925 3275
$Comp
L power:+5V #PWR?
U 1 1 5F72FC94
P 5925 3275
AR Path="/5F102375/5F72FC94" Ref="#PWR?"  Part="1" 
AR Path="/5F2104BF/5F72FC94" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F72FC94" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F72FC94" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 5925 3125 50  0001 C CNN
F 1 "+5V" H 5925 3450 39  0000 C CNN
F 2 "" H 5925 3275 50  0001 C CNN
F 3 "" H 5925 3275 50  0001 C CNN
	1    5925 3275
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5F72FC9B
P 5925 3825
AR Path="/5F2104BF/5F72FC9B" Ref="D?"  Part="1" 
AR Path="/5F72FC9B" Ref="D?"  Part="1" 
AR Path="/5F5F0002/5F72FC9B" Ref="D?"  Part="1" 
AR Path="/5F496ED4/5F72FC9B" Ref="D?"  Part="1" 
AR Path="/5EFE1C3B/5F72FC9B" Ref="D?"  Part="1" 
AR Path="/5F6BC8E7/5F72FC9B" Ref="D3"  Part="1" 
F 0 "D3" V 5956 3707 39  0000 R CNN
F 1 "LED" V 5875 3675 39  0000 R CNN
F 2 "" H 5925 3825 50  0001 C CNN
F 3 "" H 5925 3825 50  0001 C CNN
	1    5925 3825
	0    1    -1   0   
$EndComp
Wire Wire Line
	7025 3975 7025 4125
Wire Wire Line
	7025 3575 7025 3675
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F730F41
P 6925 4325
AR Path="/5F2104BF/5F730F41" Ref="Q?"  Part="1" 
AR Path="/5F730F41" Ref="Q?"  Part="1" 
AR Path="/5F5F0002/5F730F41" Ref="Q?"  Part="1" 
AR Path="/5F496ED4/5F730F41" Ref="Q?"  Part="1" 
AR Path="/5EFE1C3B/5F730F41" Ref="Q?"  Part="1" 
AR Path="/5F6BC8E7/5F730F41" Ref="Q6"  Part="1" 
F 0 "Q6" H 7130 4363 39  0000 L CNN
F 1 "Si2302CDS" H 7130 4288 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7125 4425 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 6925 4325 50  0001 C CNN
	1    6925 4325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F730F47
P 7025 4525
AR Path="/5F2104BF/5F730F47" Ref="#PWR?"  Part="1" 
AR Path="/5F730F47" Ref="#PWR?"  Part="1" 
AR Path="/5F5F0002/5F730F47" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F730F47" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F730F47" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 7025 4275 50  0001 C CNN
F 1 "GND" H 7030 4360 39  0000 C CNN
F 2 "" H 7025 4525 50  0001 C CNN
F 3 "" H 7025 4525 50  0001 C CNN
	1    7025 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	6725 4325 6625 4325
Wire Wire Line
	7025 3375 7025 3275
$Comp
L power:+5V #PWR?
U 1 1 5F730F4F
P 7025 3275
AR Path="/5F102375/5F730F4F" Ref="#PWR?"  Part="1" 
AR Path="/5F2104BF/5F730F4F" Ref="#PWR?"  Part="1" 
AR Path="/5EFE1C3B/5F730F4F" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC8E7/5F730F4F" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 7025 3125 50  0001 C CNN
F 1 "+5V" H 7025 3450 39  0000 C CNN
F 2 "" H 7025 3275 50  0001 C CNN
F 3 "" H 7025 3275 50  0001 C CNN
	1    7025 3275
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5F730F56
P 7025 3825
AR Path="/5F2104BF/5F730F56" Ref="D?"  Part="1" 
AR Path="/5F730F56" Ref="D?"  Part="1" 
AR Path="/5F5F0002/5F730F56" Ref="D?"  Part="1" 
AR Path="/5F496ED4/5F730F56" Ref="D?"  Part="1" 
AR Path="/5EFE1C3B/5F730F56" Ref="D?"  Part="1" 
AR Path="/5F6BC8E7/5F730F56" Ref="D4"  Part="1" 
F 0 "D4" V 7056 3707 39  0000 R CNN
F 1 "LED" V 6975 3675 39  0000 R CNN
F 2 "" H 7025 3825 50  0001 C CNN
F 3 "" H 7025 3825 50  0001 C CNN
	1    7025 3825
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F751AD2
P 4825 3475
AR Path="/5F2104BF/5F751AD2" Ref="R?"  Part="1" 
AR Path="/5F751AD2" Ref="R?"  Part="1" 
AR Path="/5F5F0002/5F751AD2" Ref="R?"  Part="1" 
AR Path="/5EFE1C3B/5F751AD2" Ref="R?"  Part="1" 
AR Path="/5F6BC8E7/5F751AD2" Ref="R9"  Part="1" 
F 0 "R9" H 4884 3513 39  0000 L CNN
F 1 "68" H 4884 3438 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4825 3475 50  0001 C CNN
F 3 "~" H 4825 3475 50  0001 C CNN
	1    4825 3475
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F751EE0
P 5925 3475
AR Path="/5F2104BF/5F751EE0" Ref="R?"  Part="1" 
AR Path="/5F751EE0" Ref="R?"  Part="1" 
AR Path="/5F5F0002/5F751EE0" Ref="R?"  Part="1" 
AR Path="/5EFE1C3B/5F751EE0" Ref="R?"  Part="1" 
AR Path="/5F6BC8E7/5F751EE0" Ref="R10"  Part="1" 
F 0 "R10" H 5984 3513 39  0000 L CNN
F 1 "68" H 5984 3438 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5925 3475 50  0001 C CNN
F 3 "~" H 5925 3475 50  0001 C CNN
	1    5925 3475
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F7523BA
P 7025 3475
AR Path="/5F2104BF/5F7523BA" Ref="R?"  Part="1" 
AR Path="/5F7523BA" Ref="R?"  Part="1" 
AR Path="/5F5F0002/5F7523BA" Ref="R?"  Part="1" 
AR Path="/5EFE1C3B/5F7523BA" Ref="R?"  Part="1" 
AR Path="/5F6BC8E7/5F7523BA" Ref="R11"  Part="1" 
F 0 "R11" H 7084 3513 39  0000 L CNN
F 1 "68" H 7084 3438 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7025 3475 50  0001 C CNN
F 3 "~" H 7025 3475 50  0001 C CNN
	1    7025 3475
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5F6F5CB8
P 9075 3975
F 0 "J3" H 9155 3967 50  0000 L CNN
F 1 "Conn_01x08" H 9155 3876 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x04_P2.54mm_Vertical_SMD" H 9075 3975 50  0001 C CNN
F 3 "https://www.samtec.com/products/ssm-104-f-dv" H 9075 3975 50  0001 C CNN
F 4 "SSM-104-F-DV" H 9075 3975 50  0001 C CNN "Mfr"
	1    9075 3975
	1    0    0    -1  
$EndComp
Wire Wire Line
	8775 3675 8875 3675
Wire Wire Line
	8775 3775 8875 3775
Wire Wire Line
	8775 3875 8875 3875
Wire Wire Line
	8775 3975 8875 3975
Wire Wire Line
	8775 4075 8875 4075
Wire Wire Line
	8775 4175 8875 4175
Wire Wire Line
	8775 4275 8875 4275
Wire Wire Line
	8775 4375 8875 4375
Text Label 3725 3675 0    39   ~ 0
1+
Text Label 3725 3975 0    39   ~ 0
1-
Text Label 4825 3675 0    39   ~ 0
2+
Text Label 4825 3975 0    39   ~ 0
2-
Text Label 5925 3675 0    39   ~ 0
3+
Text Label 7025 3675 0    39   ~ 0
4+
Text Label 5925 3975 0    39   ~ 0
3-
Text Label 7025 3975 0    39   ~ 0
4-
Text Label 8775 4275 2    39   ~ 0
4+
Text Label 8775 4375 2    39   ~ 0
4-
Text Label 8775 4075 2    39   ~ 0
3+
Text Label 8775 4175 2    39   ~ 0
3-
Text Label 8775 3875 2    39   ~ 0
2+
Text Label 8775 3975 2    39   ~ 0
2-
Text Label 8775 3775 2    39   ~ 0
1-
Text Label 8775 3675 2    39   ~ 0
1+
Text GLabel 3325 4325 0    39   Input ~ 0
IO1
Text GLabel 4425 4325 0    39   Input ~ 0
IO2
Text GLabel 5525 4325 0    39   Input ~ 0
IO3
Text GLabel 6625 4325 0    39   Input ~ 0
IO4
$EndSCHEMATC
