EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "Current/Voltage/Power Monitor"
Date "2020-08-20"
Rev "1.2"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5425 3400 5625 3400
Wire Wire Line
	5425 3200 5425 3400
$Comp
L power:+3.3V #PWR?
U 1 1 5EF551CA
P 5425 3200
AR Path="/5F011FA8/5F128218/5EF551CA" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5EF551CA" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551CA" Ref="#PWR?"  Part="1" 
AR Path="/5F524582/5EF551CA" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 5425 3050 50  0001 C CNN
F 1 "+3.3V" H 5440 3373 39  0000 C CNN
F 2 "" H 5425 3200 50  0001 C CNN
F 3 "" H 5425 3200 50  0001 C CNN
	1    5425 3200
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF551D0
P 5625 3300
AR Path="/5F011FA8/5F128218/5EF551D0" Ref="C?"  Part="1" 
AR Path="/5F46DFE3/5EF551D0" Ref="C?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551D0" Ref="C?"  Part="1" 
AR Path="/5F524582/5EF551D0" Ref="C2"  Part="1" 
F 0 "C2" H 5717 3346 39  0000 L CNN
F 1 "100n" H 5717 3255 39  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5625 3300 50  0001 C CNN
F 3 "~" H 5625 3300 50  0001 C CNN
	1    5625 3300
	1    0    0    -1  
$EndComp
$Comp
L Analog_ADC:INA233 U?
U 1 1 5EF551D6
P 5425 3900
AR Path="/5F011FA8/5F128218/5EF551D6" Ref="U?"  Part="1" 
AR Path="/5F46DFE3/5EF551D6" Ref="U?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551D6" Ref="U?"  Part="1" 
AR Path="/5F524582/5EF551D6" Ref="U1"  Part="1" 
F 0 "U1" H 5475 3900 50  0000 R BNN
F 1 "INA233" H 5275 4000 50  0000 L BNN
F 2 "Package_SO:VSSOP-10_3x3mm_P0.5mm" H 6225 3450 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/ina233.pdf" H 5775 3800 50  0001 C CNN
	1    5425 3900
	1    0    0    -1  
$EndComp
Connection ~ 5425 3400
$Comp
L power:GND #PWR?
U 1 1 5EF551DD
P 5625 3200
AR Path="/5F011FA8/5F128218/5EF551DD" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5EF551DD" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551DD" Ref="#PWR?"  Part="1" 
AR Path="/5F524582/5EF551DD" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 5625 2950 50  0001 C CNN
F 1 "GND" H 5630 3027 39  0000 C CNN
F 2 "" H 5625 3200 50  0001 C CNN
F 3 "" H 5625 3200 50  0001 C CNN
	1    5625 3200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF551E8
P 5425 4400
AR Path="/5F011FA8/5F128218/5EF551E8" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5EF551E8" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5EF551E8" Ref="#PWR?"  Part="1" 
AR Path="/5F524582/5EF551E8" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 5425 4150 50  0001 C CNN
F 1 "GND" H 5430 4227 39  0000 C CNN
F 2 "" H 5425 4400 50  0001 C CNN
F 3 "" H 5425 4400 50  0001 C CNN
	1    5425 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4675 3950 4825 3950
Text HLabel 4675 3600 0    39   Input ~ 0
VCOM
Wire Wire Line
	5025 3600 4675 3600
Text HLabel 4675 3950 0    39   Input ~ 0
V+
Text HLabel 4675 4150 0    39   Input ~ 0
V-
$Comp
L Device:R_Small R?
U 1 1 5F0D1D51
P 6475 3700
AR Path="/5F011FA8/5F128218/5F0D1D51" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F0D1D51" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D1D51" Ref="R?"  Part="1" 
AR Path="/5F524582/5F0D1D51" Ref="R4"  Part="1" 
F 0 "R4" H 6534 3738 39  0000 L CNN
F 1 "4.7k" H 6534 3663 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6475 3700 50  0001 C CNN
F 3 "~" H 6475 3700 50  0001 C CNN
	1    6475 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F0D2A77
P 6775 3700
AR Path="/5F011FA8/5F128218/5F0D2A77" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F0D2A77" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D2A77" Ref="R?"  Part="1" 
AR Path="/5F524582/5F0D2A77" Ref="R5"  Part="1" 
F 0 "R5" H 6834 3738 39  0000 L CNN
F 1 "4.7k" H 6834 3663 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6775 3700 50  0001 C CNN
F 3 "~" H 6775 3700 50  0001 C CNN
	1    6775 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F0D2C5D
P 7075 3700
AR Path="/5F011FA8/5F128218/5F0D2C5D" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F0D2C5D" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D2C5D" Ref="R?"  Part="1" 
AR Path="/5F524582/5F0D2C5D" Ref="R6"  Part="1" 
F 0 "R6" H 7134 3738 39  0000 L CNN
F 1 "4.7k" H 7134 3663 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7075 3700 50  0001 C CNN
F 3 "~" H 7075 3700 50  0001 C CNN
	1    7075 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F0D2F61
P 6475 3600
AR Path="/5F011FA8/5F128218/5F0D2F61" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5F0D2F61" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D2F61" Ref="#PWR?"  Part="1" 
AR Path="/5F524582/5F0D2F61" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 6475 3450 50  0001 C CNN
F 1 "+3.3V" H 6490 3773 39  0000 C CNN
F 2 "" H 6475 3600 50  0001 C CNN
F 3 "" H 6475 3600 50  0001 C CNN
	1    6475 3600
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F0D3352
P 6775 3600
AR Path="/5F011FA8/5F128218/5F0D3352" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5F0D3352" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D3352" Ref="#PWR?"  Part="1" 
AR Path="/5F524582/5F0D3352" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 6775 3450 50  0001 C CNN
F 1 "+3.3V" H 6790 3773 39  0000 C CNN
F 2 "" H 6775 3600 50  0001 C CNN
F 3 "" H 6775 3600 50  0001 C CNN
	1    6775 3600
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F0D3606
P 7075 3600
AR Path="/5F011FA8/5F128218/5F0D3606" Ref="#PWR?"  Part="1" 
AR Path="/5F46DFE3/5F0D3606" Ref="#PWR?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F0D3606" Ref="#PWR?"  Part="1" 
AR Path="/5F524582/5F0D3606" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 7075 3450 50  0001 C CNN
F 1 "+3.3V" H 7090 3773 39  0000 C CNN
F 2 "" H 7075 3600 50  0001 C CNN
F 3 "" H 7075 3600 50  0001 C CNN
	1    7075 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5825 3900 6475 3900
Wire Wire Line
	6475 3800 6475 3900
Connection ~ 6475 3900
Wire Wire Line
	6475 3900 7375 3900
Wire Wire Line
	5825 4000 6775 4000
Wire Wire Line
	6775 3800 6775 4000
Connection ~ 6775 4000
Wire Wire Line
	6775 4000 7375 4000
Wire Wire Line
	5825 4200 7075 4200
Wire Wire Line
	7075 3800 7075 4200
Connection ~ 7075 4200
Wire Wire Line
	7075 4200 7375 4200
Wire Wire Line
	4675 4150 4825 4150
Text GLabel 7375 3900 2    39   Input ~ 0
SDA
Text GLabel 7375 4000 2    39   Input ~ 0
SCL
Text HLabel 7375 4200 2    39   Output ~ 0
~IRQ
$Comp
L Device:R_Small R?
U 1 1 5F12603D
P 4825 4050
AR Path="/5F011FA8/5F128218/5F12603D" Ref="R?"  Part="1" 
AR Path="/5F46DFE3/5F12603D" Ref="R?"  Part="1" 
AR Path="/5F011FA8/5F4BF641/5F12603D" Ref="R?"  Part="1" 
AR Path="/5F524582/5F12603D" Ref="R3"  Part="1" 
F 0 "R3" H 4875 4000 39  0000 L CNN
F 1 "50m" H 4875 4050 39  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 4825 4050 50  0001 C CNN
F 3 "https://www.ohmite.com/assets/docs/res_lvk.pdf" H 4825 4050 50  0001 C CNN
F 4 "LVK12R050CER" V 4825 4050 50  0001 C CNN "Mfr"
	1    4825 4050
	1    0    0    1   
$EndComp
Text Notes 4650 4075 0    28   ~ 0
0.25%
Wire Wire Line
	5025 3950 5025 4000
Wire Wire Line
	5025 4100 5025 4150
Connection ~ 4825 4150
Wire Wire Line
	4825 4150 5025 4150
Connection ~ 4825 3950
Wire Wire Line
	4825 3950 5025 3950
Text HLabel 5925 3600 2    39   Input ~ 0
A1
Text HLabel 5925 3700 2    39   Input ~ 0
A0
Wire Wire Line
	5925 3600 5825 3600
Wire Wire Line
	5925 3700 5825 3700
Text Notes 4575 3900 0    39   ~ 0
Vmax = 82 mV
$EndSCHEMATC
