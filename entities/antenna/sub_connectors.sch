EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Sub Connectors"
Date "2020-10-06"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3700 2950 0    39   Input ~ 0
RXD
Wire Notes Line
	5850 4400 7850 4400
Wire Notes Line
	7850 4400 7850 5500
Text Notes 5850 5600 0    50   ~ 0
Main Socket\n
$Comp
L Device:R_Small R7
U 1 1 5F2C646C
P 7500 5100
AR Path="/5ED6690B/5F2C646C" Ref="R7"  Part="1" 
AR Path="/5F570D59/5F2C646C" Ref="R?"  Part="1" 
F 0 "R7" H 7550 5050 39  0000 L CNN
F 1 "4.7k" H 7550 5100 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7500 5100 50  0001 C CNN
F 3 "~" H 7500 5100 50  0001 C CNN
	1    7500 5100
	1    0    0    -1  
$EndComp
Text HLabel 7700 5300 2    39   BiDi ~ 0
ID
$Comp
L LEGOS:PogoConn J2
U 1 1 5F2C647A
P 6600 5000
AR Path="/5ED6690B/5F2C647A" Ref="J2"  Part="1" 
AR Path="/5F570D59/5F2C647A" Ref="J?"  Part="1" 
F 0 "J2" H 6650 5517 50  0000 C CNN
F 1 "PogoConn" H 6650 5426 50  0000 C CNN
F 2 "LEGOS:PogoConn_Standard" H 6600 5000 50  0001 C CNN
F 3 "~" H 6600 5000 50  0001 C CNN
	1    6600 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6150 4700 6300 4700
Wire Wire Line
	6150 4800 6300 4800
Wire Wire Line
	6150 4900 6300 4900
Wire Wire Line
	6150 5000 6300 5000
Wire Wire Line
	6150 5100 6300 5100
Wire Wire Line
	6150 5200 6300 5200
$Comp
L LEGOS:VBUS #PWR019
U 1 1 5F2C6486
P 6300 5300
AR Path="/5ED6690B/5F2C6486" Ref="#PWR019"  Part="1" 
AR Path="/5F570D59/5F2C6486" Ref="#PWR?"  Part="1" 
F 0 "#PWR019" H 6300 5150 50  0001 C CNN
F 1 "VBUS" V 6300 5450 39  0000 L CNN
F 2 "" H 6300 5300 50  0001 C CNN
F 3 "" H 6300 5300 50  0001 C CNN
	1    6300 5300
	0    -1   1    0   
$EndComp
$Comp
L power:+5V #PWR020
U 1 1 5F2C648C
P 6300 5400
AR Path="/5ED6690B/5F2C648C" Ref="#PWR020"  Part="1" 
AR Path="/5F570D59/5F2C648C" Ref="#PWR?"  Part="1" 
F 0 "#PWR020" H 6300 5250 50  0001 C CNN
F 1 "+5V" V 6300 5600 39  0000 C CNN
F 2 "" H 6300 5400 50  0001 C CNN
F 3 "" H 6300 5400 50  0001 C CNN
	1    6300 5400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5F2C6492
P 6800 5400
AR Path="/5ED6690B/5F2C6492" Ref="#PWR021"  Part="1" 
AR Path="/5F570D59/5F2C6492" Ref="#PWR?"  Part="1" 
F 0 "#PWR021" H 6800 5150 50  0001 C CNN
F 1 "GND" V 6800 5200 39  0000 C CNN
F 2 "" H 6800 5400 50  0001 C CNN
F 3 "" H 6800 5400 50  0001 C CNN
	1    6800 5400
	0    -1   1    0   
$EndComp
Wire Wire Line
	6950 4700 6800 4700
Wire Wire Line
	6950 4800 6800 4800
Wire Wire Line
	6950 5000 6800 5000
Wire Wire Line
	6950 4900 6800 4900
Wire Wire Line
	6950 5100 6800 5100
Wire Wire Line
	6950 5200 6800 5200
Wire Wire Line
	6800 5300 7500 5300
Wire Wire Line
	7500 5200 7500 5300
Connection ~ 7500 5300
Wire Wire Line
	7500 5300 7700 5300
$Comp
L power:+3.3V #PWR022
U 1 1 5F2C64A2
P 7500 5000
AR Path="/5ED6690B/5F2C64A2" Ref="#PWR022"  Part="1" 
AR Path="/5F570D59/5F2C64A2" Ref="#PWR?"  Part="1" 
F 0 "#PWR022" H 7500 4850 50  0001 C CNN
F 1 "+3.3V" H 7515 5165 39  0000 C CNN
F 2 "" H 7500 5000 50  0001 C CNN
F 3 "" H 7500 5000 50  0001 C CNN
	1    7500 5000
	1    0    0    -1  
$EndComp
Text HLabel 6950 4700 2    39   Input ~ 0
~IRQ_1
Text HLabel 6950 4800 2    39   Input ~ 0
~IRQ_2
Text HLabel 6950 4900 2    39   Input ~ 0
~IRQ_3
Text HLabel 6950 5000 2    39   Input ~ 0
~IRQ_4
Text HLabel 6950 5100 2    39   Input ~ 0
~IRQ_5
Text HLabel 6950 5200 2    39   Input ~ 0
~IRQ_6
Text HLabel 6150 5200 0    39   Output ~ 0
~EN_6
Text HLabel 6150 5100 0    39   Output ~ 0
~EN_5
Text HLabel 6150 5000 0    39   Output ~ 0
~EN_4
Text HLabel 6150 4900 0    39   Output ~ 0
~EN_3
Text HLabel 6150 4800 0    39   Output ~ 0
~EN_2
Text HLabel 6150 4700 0    39   Output ~ 0
~EN_1
$Comp
L power:+3.3V #PWR014
U 1 1 5F681C7B
P 4750 2300
AR Path="/5ED6690B/5F681C7B" Ref="#PWR014"  Part="1" 
AR Path="/5F570D59/5F681C7B" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 4750 2150 50  0001 C CNN
F 1 "+3.3V" V 4750 2500 39  0000 C CNN
F 2 "" H 4750 2300 50  0001 C CNN
F 3 "" H 4750 2300 50  0001 C CNN
	1    4750 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5F682009
P 4250 5200
AR Path="/5ED6690B/5F682009" Ref="#PWR015"  Part="1" 
AR Path="/5F570D59/5F682009" Ref="#PWR?"  Part="1" 
F 0 "#PWR015" H 4250 4950 50  0001 C CNN
F 1 "GND" V 4250 5000 39  0000 C CNN
F 2 "" H 4250 5200 50  0001 C CNN
F 3 "" H 4250 5200 50  0001 C CNN
	1    4250 5200
	-1   0    0    -1  
$EndComp
Text GLabel 5600 3150 2    39   Input ~ 0
SDA
Text GLabel 5600 3250 2    39   Input ~ 0
SCL
Text HLabel 5600 3450 2    39   BiDi ~ 0
ID
Wire Wire Line
	4450 2450 4450 2300
Wire Wire Line
	4550 2450 4550 2300
Wire Wire Line
	3850 2850 3700 2850
Wire Wire Line
	3850 2950 3700 2950
Text HLabel 3700 2850 0    39   Output ~ 0
TXD
$Comp
L power:+5VD #PWR016
U 1 1 5F697331
P 4450 2300
AR Path="/5ED6690B/5F697331" Ref="#PWR016"  Part="1" 
AR Path="/5F570D59/5F697331" Ref="#PWR?"  Part="1" 
F 0 "#PWR016" H 4450 2150 50  0001 C CNN
F 1 "+5VD" V 4450 2450 39  0000 L CNN
F 2 "" H 4450 2300 50  0001 C CNN
F 3 "" H 4450 2300 50  0001 C CNN
	1    4450 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+5VD #PWR017
U 1 1 5F698E8F
P 4550 2300
AR Path="/5ED6690B/5F698E8F" Ref="#PWR017"  Part="1" 
AR Path="/5F570D59/5F698E8F" Ref="#PWR?"  Part="1" 
F 0 "#PWR017" H 4550 2150 50  0001 C CNN
F 1 "+5VD" V 4550 2450 39  0000 L CNN
F 2 "" H 4550 2300 50  0001 C CNN
F 3 "" H 4550 2300 50  0001 C CNN
	1    4550 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3450 5600 3450
Wire Wire Line
	5450 3250 5600 3250
Wire Wire Line
	5450 3150 5600 3150
Wire Wire Line
	4750 2450 4750 2300
Wire Wire Line
	4250 5050 4250 5200
$Comp
L Connector:Raspberry_Pi_2_3 J?
U 1 1 5F7D0DB9
P 4650 3750
AR Path="/5F7D0DB9" Ref="J?"  Part="1" 
AR Path="/5ED6690B/5F7D0DB9" Ref="J1"  Part="1" 
F 0 "J1" H 4650 5231 50  0000 C CNN
F 1 "Raspberry_Pi_3B+" H 4000 5050 50  0000 C CNN
F 2 "LEGOS:RaspberryPi_vertical" H 4650 3750 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 4650 3750 50  0001 C CNN
F 4 "SSM-120-L-DH" H 4650 3750 50  0001 C CNN "Mfr"
	1    4650 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 5F7E6A3F
P 4850 2300
AR Path="/5ED6690B/5F7E6A3F" Ref="#PWR0101"  Part="1" 
AR Path="/5F570D59/5F7E6A3F" Ref="#PWR?"  Part="1" 
F 0 "#PWR0101" H 4850 2150 50  0001 C CNN
F 1 "+3.3V" V 4850 2500 39  0000 C CNN
F 2 "" H 4850 2300 50  0001 C CNN
F 3 "" H 4850 2300 50  0001 C CNN
	1    4850 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2450 4850 2300
Wire Wire Line
	4950 5050 4850 5050
Connection ~ 4250 5050
Connection ~ 4350 5050
Wire Wire Line
	4350 5050 4250 5050
Connection ~ 4450 5050
Wire Wire Line
	4450 5050 4350 5050
Connection ~ 4550 5050
Wire Wire Line
	4550 5050 4450 5050
Connection ~ 4650 5050
Wire Wire Line
	4650 5050 4550 5050
Connection ~ 4750 5050
Wire Wire Line
	4750 5050 4650 5050
Connection ~ 4850 5050
Wire Wire Line
	4850 5050 4750 5050
Text Notes 3500 5600 0    50   ~ 0
SBC Interface
Wire Notes Line
	3500 1850 5850 1850
Wire Notes Line
	3500 5500 3500 1850
Wire Notes Line
	3500 5500 7850 5500
Wire Notes Line
	5850 1850 5850 5500
Text GLabel 3700 4350 0    39   Input ~ 0
IO1
Wire Wire Line
	3850 3550 3700 3550
Wire Wire Line
	3850 3150 3700 3150
Text GLabel 3700 3650 0    39   Input ~ 0
IO2
Text GLabel 3700 3550 0    39   Input ~ 0
IO3
Wire Wire Line
	3850 4350 3700 4350
Wire Wire Line
	3850 3650 3700 3650
Text GLabel 3700 3150 0    39   Input ~ 0
IO4
$EndSCHEMATC
