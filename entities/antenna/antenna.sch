EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Antenna"
Date "2020-10-06"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6950 2825 6850 2825
Wire Wire Line
	6950 2925 6850 2925
Text Label 6850 2825 2    39   ~ 0
TXD
Text Label 6850 2925 2    39   ~ 0
RXD
Text Label 6850 3025 2    39   ~ 0
ID
Wire Wire Line
	6850 3525 6950 3525
$Comp
L power:+5V #PWR?
U 1 1 5F5322D9
P 6850 3525
AR Path="/5ED6690B/5F5322D9" Ref="#PWR?"  Part="1" 
AR Path="/5F5322D9" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 6850 3375 50  0001 C CNN
F 1 "+5V" V 6850 3725 39  0000 C CNN
F 2 "" H 6850 3525 50  0001 C CNN
F 3 "" H 6850 3525 50  0001 C CNN
	1    6850 3525
	0    -1   -1   0   
$EndComp
$Sheet
S 6950 3425 550  500 
U 5F524582
F0 "Telemetry" 39
F1 "sub_telemetry.sch" 39
F2 "VCOM" I L 6950 3525 39 
F3 "V+" I L 6950 3675 39 
F4 "V-" I L 6950 3825 39 
F5 "~IRQ" O R 7500 3525 39 
F6 "A1" I R 7500 3675 39 
F7 "A0" I R 7500 3825 39 
$EndSheet
Wire Wire Line
	6850 3025 6950 3025
$Sheet
S 6950 2725 550  400 
U 5ED6690B
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "RXD" I L 6950 2925 39 
F3 "TXD" O L 6950 2825 39 
F4 "ID" B L 6950 3025 39 
$EndSheet
$Comp
L power:+5VD #PWR?
U 1 1 5F699EF5
P 6850 3675
AR Path="/5ED6690B/5F699EF5" Ref="#PWR?"  Part="1" 
AR Path="/5F699EF5" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 6850 3525 50  0001 C CNN
F 1 "+5VD" V 6850 3825 39  0000 L CNN
F 2 "" H 6850 3675 50  0001 C CNN
F 3 "" H 6850 3675 50  0001 C CNN
	1    6850 3675
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 3675 6950 3675
$Comp
L power:GND #PWR?
U 1 1 5F69B1F4
P 7600 3825
AR Path="/5ED6690B/5F69B1F4" Ref="#PWR?"  Part="1" 
AR Path="/5F69B1F4" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 7600 3575 50  0001 C CNN
F 1 "GND" V 7600 3625 39  0000 C CNN
F 2 "" H 7600 3825 50  0001 C CNN
F 3 "" H 7600 3825 50  0001 C CNN
	1    7600 3825
	0    -1   1    0   
$EndComp
Wire Wire Line
	7600 3825 7500 3825
Wire Wire Line
	7500 3675 7600 3675
Wire Wire Line
	7600 3675 7600 3825
Connection ~ 7600 3825
Wire Wire Line
	7600 3525 7500 3525
Text Label 7600 3525 0    39   ~ 0
~IRQ
$Comp
L power:GND #PWR?
U 1 1 5F6B4A30
P 5300 5075
AR Path="/5F6ADF41/5F6B4A30" Ref="#PWR?"  Part="1" 
AR Path="/5F6B4A30" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 5300 4825 50  0001 C CNN
F 1 "GND" H 5305 4902 39  0000 C CNN
F 2 "" H 5300 5075 50  0001 C CNN
F 3 "" H 5300 5075 50  0001 C CNN
	1    5300 5075
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F6B4A37
P 4300 4875
AR Path="/5F6ADF41/5F6B4A37" Ref="C?"  Part="1" 
AR Path="/5F6B4A37" Ref="C1"  Part="1" 
F 0 "C1" H 4250 4825 39  0000 R CNN
F 1 "22u" H 4250 4925 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4300 4875 50  0001 C CNN
F 3 "~" H 4300 4875 50  0001 C CNN
	1    4300 4875
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F6B4A3D
P 4300 4225
AR Path="/5F6ADF41/5F6B4A3D" Ref="R?"  Part="1" 
AR Path="/5F6B4A3D" Ref="R1"  Part="1" 
F 0 "R1" H 4350 4175 39  0000 L CNN
F 1 "100k" H 4350 4225 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4300 4225 50  0001 C CNN
F 3 "~" H 4300 4225 50  0001 C CNN
	1    4300 4225
	-1   0    0    1   
$EndComp
Wire Wire Line
	5300 5075 5300 4825
$Comp
L Switch:SW_Push SW?
U 1 1 5F6B4A60
P 3850 4875
AR Path="/5F6ADF41/5F6B4A60" Ref="SW?"  Part="1" 
AR Path="/5F6B4A60" Ref="SW1"  Part="1" 
F 0 "SW1" V 3800 4675 39  0000 L CNN
F 1 "PTS815" V 3850 4575 39  0000 L CNN
F 2 "LEGOS:PTS815" H 3850 5075 50  0001 C CNN
F 3 "~" H 3850 5075 50  0001 C CNN
	1    3850 4875
	0    1    -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F6BC2C1
P 4300 3825
AR Path="/5ED6690B/5F6BC2C1" Ref="#PWR?"  Part="1" 
AR Path="/5F6BC2C1" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 4300 3675 50  0001 C CNN
F 1 "+5V" H 4300 3975 39  0000 C CNN
F 2 "" H 4300 3825 50  0001 C CNN
F 3 "" H 4300 3825 50  0001 C CNN
	1    4300 3825
	1    0    0    -1  
$EndComp
$Sheet
S 6950 4225 550  450 
U 5F6BC8E7
F0 "LEDs" 39
F1 "sub_led.sch" 39
$EndSheet
$Comp
L Device:Q_PMOS_GSD Q?
U 1 1 5F6C4ACB
P 5800 3925
AR Path="/5F496ED4/5F6C4ACB" Ref="Q?"  Part="1" 
AR Path="/5F6C4ACB" Ref="Q2"  Part="1" 
AR Path="/5F5F0002/5F6C4ACB" Ref="Q?"  Part="1" 
AR Path="/5F2104BF/5F6C4ACB" Ref="Q?"  Part="1" 
AR Path="/5EFE1C3B/5F6C4ACB" Ref="Q?"  Part="1" 
AR Path="/5F6BC8E7/5F6C4ACB" Ref="Q?"  Part="1" 
F 0 "Q2" V 6050 3975 39  0000 L CNN
F 1 "Si2301BDS" V 6000 3975 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6000 4025 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 5800 3925 50  0001 C CNN
	1    5800 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	5800 4125 5800 4325
Wire Wire Line
	6000 3825 6950 3825
$Comp
L Device:R_Small R?
U 1 1 5F73C48F
P 5300 4075
AR Path="/5F6ADF41/5F73C48F" Ref="R?"  Part="1" 
AR Path="/5F73C48F" Ref="R2"  Part="1" 
F 0 "R2" H 5350 4025 39  0000 L CNN
F 1 "1k" H 5350 4075 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5300 4075 50  0001 C CNN
F 3 "~" H 5300 4075 50  0001 C CNN
	1    5300 4075
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 3825 5300 3825
Wire Wire Line
	5300 3825 5300 3975
Wire Wire Line
	5300 4175 5300 4325
Wire Wire Line
	5300 4325 5800 4325
Wire Wire Line
	5300 4325 5300 4425
Connection ~ 5300 4325
Wire Wire Line
	4300 4625 4300 4775
$Comp
L power:GND #PWR?
U 1 1 5F7459EC
P 4300 5075
AR Path="/5F6ADF41/5F7459EC" Ref="#PWR?"  Part="1" 
AR Path="/5F7459EC" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 4300 4825 50  0001 C CNN
F 1 "GND" H 4305 4902 39  0000 C CNN
F 2 "" H 4300 5075 50  0001 C CNN
F 3 "" H 4300 5075 50  0001 C CNN
	1    4300 5075
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 4975 4300 5075
Wire Wire Line
	4300 4325 4300 4625
Connection ~ 4300 4625
Wire Wire Line
	4300 3825 4300 4125
Connection ~ 4300 3825
Wire Wire Line
	3850 4625 4300 4625
$Comp
L power:GND #PWR?
U 1 1 5F74F53E
P 3850 5075
AR Path="/5F6ADF41/5F74F53E" Ref="#PWR?"  Part="1" 
AR Path="/5F74F53E" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 3850 4825 50  0001 C CNN
F 1 "GND" H 3855 4902 39  0000 C CNN
F 2 "" H 3850 5075 50  0001 C CNN
F 3 "" H 3850 5075 50  0001 C CNN
	1    3850 5075
	1    0    0    -1  
$EndComp
Text Notes 5900 4375 0    39   ~ 0
Startup delay 0.5 s
$Sheet
S 6950 2025 550  400 
U 5F570D59
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
$Comp
L Transistor_BJT:MMBT3904 Q7
U 1 1 5F6F2D2B
P 5200 4625
F 0 "Q7" H 5390 4663 39  0000 L CNN
F 1 "MMBT3904" H 5390 4588 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5400 4550 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 5200 4625 50  0001 L CNN
	1    5200 4625
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:MMBT3906 Q1
U 1 1 5F6F3BAD
P 5000 4325
F 0 "Q1" H 5191 4287 39  0000 L CNN
F 1 "MMBT3906" H 5191 4362 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5200 4250 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3906.pdf" H 5000 4325 50  0001 L CNN
	1    5000 4325
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 3825 4900 3825
Connection ~ 5300 3825
Wire Wire Line
	4900 4125 4900 3825
Connection ~ 4900 3825
Wire Wire Line
	4900 3825 5300 3825
Wire Wire Line
	4300 4625 4900 4625
Wire Wire Line
	4900 4525 4900 4625
Connection ~ 4900 4625
Wire Wire Line
	4900 4625 5000 4625
Wire Wire Line
	5300 4325 5200 4325
Wire Wire Line
	3850 4625 3850 4675
$EndSCHEMATC
