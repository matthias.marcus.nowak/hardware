EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title "Solar Farm"
Date "2020-10-09"
Rev "1.2"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5EF48CAB
P 3150 5450
AR Path="/5EF40FCD/5EF48CAB" Ref="#PWR?"  Part="1" 
AR Path="/5EF48CAB" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 3150 5200 50  0001 C CNN
F 1 "GND" H 3155 5277 39  0000 C CNN
F 2 "" H 3150 5450 50  0001 C CNN
F 3 "" H 3150 5450 50  0001 C CNN
	1    3150 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5EF48CB1
P 3150 2450
AR Path="/5EF40FCD/5EF48CB1" Ref="C?"  Part="1" 
AR Path="/5EF48CB1" Ref="C3"  Part="1" 
F 0 "C3" H 3300 2400 39  0000 R CNN
F 1 "10u" H 3300 2500 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3150 2450 50  0001 C CNN
F 3 "~" H 3150 2450 50  0001 C CNN
	1    3150 2450
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF48CB7
P 2900 2450
AR Path="/5EF40FCD/5EF48CB7" Ref="C?"  Part="1" 
AR Path="/5EF48CB7" Ref="C2"  Part="1" 
F 0 "C2" H 2850 2400 39  0000 R CNN
F 1 "100n" H 2900 2500 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2900 2450 50  0001 C CNN
F 3 "~" H 2900 2450 50  0001 C CNN
	1    2900 2450
	1    0    0    1   
$EndComp
Wire Wire Line
	3150 2550 3150 2650
Wire Wire Line
	2900 2550 2900 2650
$Comp
L power:GND #PWR?
U 1 1 5EF48CBF
P 2900 2350
AR Path="/5EF40FCD/5EF48CBF" Ref="#PWR?"  Part="1" 
AR Path="/5EF48CBF" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 2900 2100 50  0001 C CNN
F 1 "GND" H 2905 2177 39  0000 C CNN
F 2 "" H 2900 2350 50  0001 C CNN
F 3 "" H 2900 2350 50  0001 C CNN
	1    2900 2350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF48CC5
P 3150 2350
AR Path="/5EF40FCD/5EF48CC5" Ref="#PWR?"  Part="1" 
AR Path="/5EF48CC5" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 3150 2100 50  0001 C CNN
F 1 "GND" H 3155 2177 39  0000 C CNN
F 2 "" H 3150 2350 50  0001 C CNN
F 3 "" H 3150 2350 50  0001 C CNN
	1    3150 2350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF48CCB
P 2050 3250
AR Path="/5EF40FCD/5EF48CCB" Ref="#PWR?"  Part="1" 
AR Path="/5EF48CCB" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2050 3000 50  0001 C CNN
F 1 "GND" H 2055 3077 39  0000 C CNN
F 2 "" H 2050 3250 50  0001 C CNN
F 3 "" H 2050 3250 50  0001 C CNN
	1    2050 3250
	1    0    0    -1  
$EndComp
Connection ~ 2050 2850
$Comp
L Device:C_Small C?
U 1 1 5EF48CD2
P 2050 3150
AR Path="/5EF40FCD/5EF48CD2" Ref="C?"  Part="1" 
AR Path="/5EF48CD2" Ref="C1"  Part="1" 
F 0 "C1" H 2000 3100 39  0000 R CNN
F 1 "1u" H 2000 3200 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2050 3150 50  0001 C CNN
F 3 "~" H 2050 3150 50  0001 C CNN
	1    2050 3150
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EF48CD8
P 2050 2600
AR Path="/5EF40FCD/5EF48CD8" Ref="R?"  Part="1" 
AR Path="/5EF48CD8" Ref="R1"  Part="1" 
F 0 "R1" H 2100 2550 39  0000 L CNN
F 1 "10k" H 2100 2600 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2050 2600 50  0001 C CNN
F 3 "~" H 2050 2600 50  0001 C CNN
	1    2050 2600
	-1   0    0    1   
$EndComp
Text Notes 4200 2850 0    39   ~ 0
UP during boot
Text Notes 4200 3450 0    39   ~ 0
DOWN during boot
Text Notes 4200 3050 0    39   ~ 0
DOWN during prog
Text Notes 4200 3750 0    39   ~ 0
UP during boot for log
Text Notes 2500 4800 1    39   ~ 0
Internally connected to the flash
Text Notes 4200 3350 0    39   ~ 0
UP during boot
Text Notes 3750 2850 0    39   ~ 0
IO0
Wire Wire Line
	2050 2850 2050 3050
Text Notes 4050 5050 2    39   ~ 0
only input
Text Notes 4050 5150 2    39   ~ 0
only input
Text Notes 2500 3050 2    39   ~ 0
only input
Text Notes 2500 3150 2    39   ~ 0
only input
Wire Wire Line
	2050 2850 2550 2850
Wire Wire Line
	2650 2350 2650 2650
Wire Wire Line
	2050 2350 2050 2500
Wire Wire Line
	2050 2700 2050 2850
Text Label 3950 3150 0    39   ~ 0
RXD
Text Label 3950 2950 0    39   ~ 0
TXD
Text Label 2150 2850 0    39   ~ 0
EN
Text Label 3950 2850 0    39   ~ 0
ID
$Comp
L RF_Module:ESP32-WROOM-32D U?
U 1 1 5EF48D0C
P 3150 4050
AR Path="/5EF40FCD/5EF48D0C" Ref="U?"  Part="1" 
AR Path="/5EF48D0C" Ref="U1"  Part="1" 
F 0 "U1" H 2600 4550 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 2250 4450 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 3150 2550 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 2850 4100 50  0001 C CNN
	1    3150 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2650 2900 2650
Connection ~ 3150 2650
Connection ~ 2900 2650
Wire Wire Line
	2900 2650 2650 2650
NoConn ~ 3750 5150
NoConn ~ 3750 5050
NoConn ~ 2550 3150
NoConn ~ 2550 3050
NoConn ~ 2550 4050
NoConn ~ 2550 4150
NoConn ~ 2550 4250
NoConn ~ 2550 4350
NoConn ~ 2550 4450
NoConn ~ 2550 4550
Text Label 3950 4350 0    39   ~ 0
LIGHT_R
Text Label 3950 4250 0    39   ~ 0
SERVO
Text Notes 3750 4550 0    39   ~ 0
DAC
Text Notes 3750 4650 0    39   ~ 0
DAC
Wire Wire Line
	3750 4950 3950 4950
Wire Wire Line
	3750 4850 3950 4850
Wire Wire Line
	3750 4550 3950 4550
Wire Wire Line
	3750 3850 3950 3850
Wire Wire Line
	3750 3250 3950 3250
Wire Wire Line
	3750 3750 3950 3750
Wire Wire Line
	3750 4650 3950 4650
Wire Wire Line
	3750 4750 3950 4750
Wire Wire Line
	3750 3350 3950 3350
Wire Wire Line
	3750 3150 3950 3150
Wire Wire Line
	3750 2950 3950 2950
Wire Wire Line
	3750 2850 3950 2850
Wire Wire Line
	6300 3950 6200 3950
Wire Wire Line
	6300 4075 6200 4075
Wire Wire Line
	6300 4200 6200 4200
Wire Wire Line
	6300 4325 6200 4325
Wire Wire Line
	6300 4450 6200 4450
Text Label 6200 3950 2    39   ~ 0
20%
Text Label 6200 4075 2    39   ~ 0
40%
Text Label 6200 4200 2    39   ~ 0
60%
Text Label 6200 4325 2    39   ~ 0
80%
Text Label 6200 4450 2    39   ~ 0
100%
Wire Wire Line
	3750 4050 3950 4050
Wire Wire Line
	3750 4150 3950 4150
Wire Wire Line
	3750 3950 3950 3950
Wire Wire Line
	3750 4350 3950 4350
Wire Wire Line
	3750 4450 3950 4450
$Comp
L power:+3.3V #PWR02
U 1 1 5F179592
P 2050 2350
F 0 "#PWR02" H 2050 2200 50  0001 C CNN
F 1 "+3.3V" H 2065 2515 39  0000 C CNN
F 2 "" H 2050 2350 50  0001 C CNN
F 3 "" H 2050 2350 50  0001 C CNN
	1    2050 2350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 5F179FF8
P 2650 2350
F 0 "#PWR04" H 2650 2200 50  0001 C CNN
F 1 "+3.3V" H 2665 2515 39  0000 C CNN
F 2 "" H 2650 2350 50  0001 C CNN
F 3 "" H 2650 2350 50  0001 C CNN
	1    2650 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2000 6300 2000
Wire Wire Line
	6250 2100 6250 2000
Wire Wire Line
	6300 2100 6250 2100
Text Label 6200 2000 2    39   ~ 0
ID
Text Label 6200 1900 2    39   ~ 0
RXD
Text Label 6200 1800 2    39   ~ 0
TXD
Text Label 6200 1700 2    39   ~ 0
EN
Wire Wire Line
	6300 1900 6200 1900
Wire Wire Line
	6300 1800 6200 1800
Wire Wire Line
	6300 1700 6200 1700
Text GLabel 3950 3750 2    39   Input ~ 0
SDA
Text GLabel 3950 3550 2    39   Input ~ 0
SCL
Text GLabel 3950 3650 2    39   Input ~ 0
CLK
Text GLabel 3950 4750 2    39   Input ~ 0
MOSI
Wire Wire Line
	3950 3650 3750 3650
$Sheet
S 6300 1600 550  600 
U 5F105D17
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "EN" O L 6300 1700 39 
F3 "TXD" O L 6300 1800 39 
F4 "RXD" I L 6300 1900 39 
F5 "IO0" B L 6300 2000 39 
F6 "ID" B L 6300 2100 39 
$EndSheet
$Sheet
S 6300 3850 550  700 
U 5F0C5806
F0 "Light Indicator" 39
F1 "sub_indicator.sch" 39
F2 "20%" I L 6300 3950 39 
F3 "40%" I L 6300 4075 39 
F4 "60%" I L 6300 4200 39 
F5 "80%" I L 6300 4325 39 
F6 "100%" I L 6300 4450 39 
$EndSheet
Wire Wire Line
	6250 2000 6200 2000
Connection ~ 6250 2000
$Sheet
S 6300 2500 550  400 
U 5F437552
F0 "Auto Power" 39
F1 "sub_power_auto.sch" 39
F2 "VIN" I L 6300 2600 39 
F3 "VOUT" O L 6300 2800 39 
$EndSheet
Wire Wire Line
	6200 2600 6300 2600
Wire Wire Line
	6200 2800 6300 2800
$Comp
L power:+3.3V #PWR09
U 1 1 5F439CAC
P 6200 2800
F 0 "#PWR09" H 6200 2650 50  0001 C CNN
F 1 "+3.3V" V 6200 3000 39  0000 C CNN
F 2 "" H 6200 2800 50  0001 C CNN
F 3 "" H 6200 2800 50  0001 C CNN
	1    6200 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F43C531
P 6200 2600
AR Path="/5F105D17/5F43C531" Ref="#PWR?"  Part="1" 
AR Path="/5F43C531" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 6200 2450 50  0001 C CNN
F 1 "+5V" V 6200 2800 39  0000 C CNN
F 2 "" H 6200 2600 50  0001 C CNN
F 3 "" H 6200 2600 50  0001 C CNN
	1    6200 2600
	0    -1   -1   0   
$EndComp
$Sheet
S 6300 3200 550  350 
U 5F43D67A
F0 "Solar Power" 39
F1 "sub_power_slave.sch" 39
F2 "~CS" I R 6850 3450 39 
F3 "VOUT" O L 6300 3450 39 
F4 "VIN" I L 6300 3300 39 
F5 "~IRQ" I R 6850 3300 39 
$EndSheet
Wire Wire Line
	6200 3300 6300 3300
Wire Wire Line
	6200 3450 6300 3450
Wire Wire Line
	6950 3300 6850 3300
Wire Wire Line
	6950 3450 6850 3450
$Comp
L power:+5V #PWR?
U 1 1 5F443871
P 6200 3300
AR Path="/5F105D17/5F443871" Ref="#PWR?"  Part="1" 
AR Path="/5F443871" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 6200 3150 50  0001 C CNN
F 1 "+5V" V 6200 3500 39  0000 C CNN
F 2 "" H 6200 3300 50  0001 C CNN
F 3 "" H 6200 3300 50  0001 C CNN
	1    6200 3300
	0    -1   -1   0   
$EndComp
$Comp
L LEGOS:VBUS #PWR?
U 1 1 5F445187
P 6200 3450
AR Path="/5F105D17/5F445187" Ref="#PWR?"  Part="1" 
AR Path="/5F445187" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 6200 3300 50  0001 C CNN
F 1 "VBUS" V 6200 3600 39  0000 L CNN
F 2 "" H 6200 3450 50  0001 C CNN
F 3 "" H 6200 3450 50  0001 C CNN
	1    6200 3450
	0    -1   1    0   
$EndComp
Text Label 6950 3300 0    39   ~ 0
~IRQ
Text Label 6950 3450 0    39   ~ 0
~CS
$Comp
L Switch:SW_Push SW1
U 1 1 5F4632A0
P 1650 3050
F 0 "SW1" V 1550 2800 39  0000 L CNN
F 1 "PTS815" V 1650 2700 39  0000 L CNN
F 2 "LEGOS:PTS815" H 1650 3250 50  0001 C CNN
F 3 "~" H 1650 3250 50  0001 C CNN
	1    1650 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 2850 2050 2850
$Comp
L power:GND #PWR?
U 1 1 5F466FFD
P 1650 3250
AR Path="/5EF40FCD/5F466FFD" Ref="#PWR?"  Part="1" 
AR Path="/5F466FFD" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1650 3000 50  0001 C CNN
F 1 "GND" H 1655 3077 39  0000 C CNN
F 2 "" H 1650 3250 50  0001 C CNN
F 3 "" H 1650 3250 50  0001 C CNN
	1    1650 3250
	1    0    0    -1  
$EndComp
Text Label 3950 4650 0    39   ~ 0
~CS
Text Label 3950 3250 0    39   ~ 0
20%
Text Label 3950 3850 0    39   ~ 0
40%
Text Label 3950 3950 0    39   ~ 0
60%
Text Label 3950 4050 0    39   ~ 0
80%
Text Label 3950 4150 0    39   ~ 0
100%
Text Label 3950 3350 0    39   ~ 0
~IRQ
$Sheet
S 6300 4850 550  825 
U 5F47F17B
F0 "Solar Tracker" 39
F1 "sub_solar_tracker.sch" 39
F2 "SERVO" I L 6300 4950 39 
F3 "LIGHT_R" I L 6300 5100 39 
F4 "LIGHT_L" I L 6300 5250 39 
F5 "LIGHT" I L 6300 5550 39 
F6 "LIGHT_LR" I L 6300 5400 39 
$EndSheet
Wire Wire Line
	6300 4950 6200 4950
Wire Wire Line
	6300 5100 6200 5100
Wire Wire Line
	6300 5250 6200 5250
Text Label 3950 4450 0    39   ~ 0
LIGHT_L
Text Label 6200 4950 2    39   ~ 0
SERVO
Text Label 6200 5250 2    39   ~ 0
LIGHT_L
Text Label 6200 5100 2    39   ~ 0
LIGHT_R
$Sheet
S 6300 950  550  350 
U 5F561856
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
Wire Wire Line
	6300 5550 6200 5550
Text Label 6200 5550 2    39   ~ 0
LIGHT
Wire Wire Line
	3950 4250 3750 4250
Text Label 3950 4950 0    39   ~ 0
LIGHT
Wire Wire Line
	3950 3550 3750 3550
Text Label 3950 4850 0    39   ~ 0
LIGHT_LR
Wire Wire Line
	6300 5400 6200 5400
Text Label 6200 5400 2    39   ~ 0
LIGHT_LR
$EndSCHEMATC
