# Hospital
The Hospital represents one of the critical infrastructure for health service, which must provide uninterrupted operation even during a power outage. The workload is defined by the emergency status, which is triggered by the presence of an emergency vehicle on the roof or at the entrance, detected using a magnetic field sensor. The circuitry includes a uninterruptible power supply UPS sub-block, a telemetry sub-block for monitoring the self power consumption in the 3.3 V bus and a sub-block for LED emergency status indicators.

## Schematic
[<img src="docs/hospital_sch.png"  width="1109" height="500">](docs/hospital_sch-main.png)

[<img src="docs/hospital_sch-sub_connectors.png"  width="145" height="100">](docs/hospital_sch-sub_connectors.png)
&nbsp;
[<img src="docs/hospital_sch-sub_power_ups.png"  width="145" height="100">](docs/hospital_sch-sub_power_ups.png)
&nbsp;
[<img src="docs/hospital_sch-sub_emergency_auto.png"  width="145" height="100">](docs/hospital_sch-sub_emergency_auto.png)
&nbsp;
[<img src="docs/hospital_sch-sub_emergency_heli.png"  width="145" height="100">](docs/hospital_sch-sub_emergency_heli.png)
&nbsp;
[<img src="docs/hospital_sch-sub_telemetry.png"  width="145" height="100">](docs/hospital_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/hospital_sch-sub_mechanical.png"  width="145" height="100">](docs/hospital_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/hospital_pcb.png"  width="726" height="500">](docs/hospital_pcb-brd.png)

[<img src="docs/hospital_pcb-F_Cu.png"  width="145" height="100">](docs/hospital_pcb-F_Cu.png)
&nbsp;
[<img src="docs/hospital_pcb-In1_Cu.png"  width="145" height="100">](docs/hospital_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/hospital_pcb-In2_Cu.png"  width="145" height="100">](docs/hospital_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/hospital_pcb-B_Cu.png"  width="145" height="100">](docs/hospital_pcb-B_Cu.png)

## Media

[<img src="docs/hospital_3d-top.png"  width="103" height="100">](docs/hospital_3d-top.png)
&nbsp;
[<img src="docs/hospital_3d-bottom.png"  width="102" height="100">](docs/hospital_3d-bottom.png)
&nbsp;
[<img src="docs/hospital_3d_roof-top.png"  width="74" height="100">](docs/hospital_3d_roof-top.png)
&nbsp;
[<img src="docs/hospital_3d_roof-bottom.png"  width="75" height="100">](docs/hospital_3d_roof-bottom.png)
&nbsp;
[<img src="docs/hospital_brd.png"  width="149" height="100">](docs/hospital_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/hospital/hospital.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/hospital/hospital.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/hospital/hospital.md)