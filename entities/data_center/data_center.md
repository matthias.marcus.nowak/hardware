# Data Center
The Data Center represents one of the critical infrastructure for IT operations, which must provide uninterrupted operation even during a power outage. The circuitry includes a uninterruptible power supply UPS sub-block, a telemetry sub-block for monitoring the self power consumption in the 3.3 V bus and a sub-block for programmable LEDs animations.

## Schematic
[<img src="docs/data_center_sch.png"  width="1157" height="500">](docs/data_center_sch-main.png)

[<img src="docs/data_center_sch-sub_connectors.png"  width="145" height="100">](docs/data_center_sch-sub_connectors.png)
&nbsp;
[<img src="docs/data_center_sch-sub_power_ups.png"  width="145" height="100">](docs/data_center_sch-sub_power_ups.png)
&nbsp;
[<img src="docs/data_center_sch-sub_servers.png"  width="145" height="100">](docs/data_center_sch-sub_servers.png)
&nbsp;
[<img src="docs/data_center_sch-sub_telemetry.png"  width="145" height="100">](docs/data_center_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/data_center_sch-sub_mechanical.png"  width="145" height="100">](docs/data_center_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/data_center_pcb.png"  width="531" height="500">](docs/data_center_pcb-brd.png)

[<img src="docs/data_center_pcb-F_Cu.png"  width="145" height="100">](docs/data_center_pcb-F_Cu.png)
&nbsp;
[<img src="docs/data_center_pcb-In1_Cu.png"  width="145" height="100">](docs/data_center_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/data_center_pcb-In2_Cu.png"  width="145" height="100">](docs/data_center_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/data_center_pcb-B_Cu.png"  width="145" height="100">](docs/data_center_pcb-B_Cu.png)

## Media

[<img src="docs/data_center_3d-top.png"  width="93" height="100">](docs/data_center_3d-top.png)
&nbsp;
[<img src="docs/data_center_3d-bottom.png"  width="91" height="100">](docs/data_center_3d-bottom.png)
&nbsp;
[<img src="docs/data_center_brd.png"  width="149" height="100">](docs/data_center_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/data_center/data_center.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/data_center/data_center.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/data_center/data_center.md)
