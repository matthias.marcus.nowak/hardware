# Factory
The Factory represents a typical industrial consumer with load due to heavy machinery or assembly lines. The power absorbed from the grid is determined by the current working load, variable via touch buttons. The circuitry includes an assembly line sub-block with a stepper motor used to drive a robotic arm and programmable LEDs, a telemetry sub-block for monitoring the self power consumption in the 3.3 V bus and a sub-block for LED indicators of the current workload.

## Schematic
[<img src="docs/factory_sch.png"  width="698" height="500">](docs/factory_sch-main.png)

[<img src="docs/factory_sch-sub_connectors.png"  width="145" height="100">](docs/factory_sch-sub_connectors.png)
&nbsp;
[<img src="docs/factory_sch-sub_assembly_line.png"  width="145" height="100">](docs/factory_sch-sub_assembly_line.png)
&nbsp;
[<img src="docs/factory_sch-sub_indicator.png"  width="145" height="100">](docs/factory_sch-sub_indicator.png)
&nbsp;
[<img src="docs/factory_sch-sub_telemetry.png"  width="145" height="100">](docs/factory_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/factory_sch-sub_mechanical.png"  width="145" height="100">](docs/factory_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/factory_pcb.png"  width="470" height="500">](docs/factory_pcb-brd.png)

[<img src="docs/factory_pcb-F_Cu.png"  width="145" height="100">](docs/factory_pcb-F_Cu.png)
&nbsp;
[<img src="docs/factory_pcb-In1_Cu.png"  width="145" height="100">](docs/factory_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/factory_pcb-In2_Cu.png"  width="145" height="100">](docs/factory_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/factory_pcb-B_Cu.png"  width="145" height="100">](docs/factory_pcb-B_Cu.png)

## Media

[<img src="docs/factory_3d-top.png"  width="103" height="100">](docs/factory_3d-top.png)
&nbsp;
[<img src="docs/factory_3d-bottom.png"  width="103" height="100">](docs/factory_3d-bottom.png)
&nbsp;
[<img src="docs/factory_brd.png"  width="133" height="100">](docs/factory_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/factory/factory.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/factory/factory.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/factory/factory.md)