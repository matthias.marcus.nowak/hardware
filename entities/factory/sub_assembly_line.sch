EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "Assembly Line"
Date "2020-08-10"
Rev "1.0"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Motor:Motor_Servo M?
U 1 1 5F428139
P 4300 2500
AR Path="/5F428139" Ref="M?"  Part="1" 
AR Path="/5F0C6676/5F428139" Ref="M1"  Part="1" 
AR Path="/5F55C724/5F428139" Ref="M?"  Part="1" 
F 0 "M1" H 4632 2565 50  0000 L CNN
F 1 "SG51R" H 4632 2474 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical_SMD_Pin1Right" H 4300 2310 50  0001 C CNN
F 3 "https://www.adafruit.com/product/2201" H 4300 2310 50  0001 C CNN
F 4 "TSM-103-01-F-SV" H 4300 2500 50  0001 C CNN "Mfr"
	1    4300 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F42813F
P 4000 2600
AR Path="/5F42813F" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F42813F" Ref="#PWR030"  Part="1" 
AR Path="/5F55C724/5F42813F" Ref="#PWR?"  Part="1" 
F 0 "#PWR030" H 4000 2350 50  0001 C CNN
F 1 "GND" H 4005 2427 39  0000 C CNN
F 2 "" H 4000 2600 50  0001 C CNN
F 3 "" H 4000 2600 50  0001 C CNN
	1    4000 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F428145
P 3200 2300
AR Path="/5F0DB548/5F428145" Ref="Q?"  Part="1" 
AR Path="/5F428145" Ref="Q?"  Part="1" 
AR Path="/5F0C6676/5F428145" Ref="Q2"  Part="1" 
AR Path="/5F55C724/5F428145" Ref="Q?"  Part="1" 
F 0 "Q2" V 3450 2350 39  0000 L CNN
F 1 "Si2302CDS" V 3550 2350 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3400 2400 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 3200 2300 50  0001 C CNN
	1    3200 2300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F42814B
P 2850 2200
AR Path="/5F42814B" Ref="R?"  Part="1" 
AR Path="/5F0C6676/5F42814B" Ref="R8"  Part="1" 
AR Path="/5F55C724/5F42814B" Ref="R?"  Part="1" 
F 0 "R8" H 2900 2150 39  0000 L CNN
F 1 "10k" H 2900 2200 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2850 2200 50  0001 C CNN
F 3 "~" H 2850 2200 50  0001 C CNN
	1    2850 2200
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F428151
P 3550 2200
AR Path="/5F428151" Ref="R?"  Part="1" 
AR Path="/5F0C6676/5F428151" Ref="R9"  Part="1" 
AR Path="/5F55C724/5F428151" Ref="R?"  Part="1" 
F 0 "R9" H 3600 2150 39  0000 L CNN
F 1 "10k" H 3600 2200 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3550 2200 50  0001 C CNN
F 3 "~" H 3550 2200 50  0001 C CNN
	1    3550 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 2300 2850 2400
Wire Wire Line
	2850 2400 3000 2400
Wire Wire Line
	3400 2400 3550 2400
Wire Wire Line
	3550 2400 3550 2300
Wire Wire Line
	3200 2000 2850 2000
Wire Wire Line
	2850 2000 2850 2100
Wire Wire Line
	3200 2000 3200 2100
Wire Wire Line
	3550 2000 3550 2100
$Comp
L power:+5V #PWR?
U 1 1 5F42815F
P 3550 2000
AR Path="/5F0DB548/5F42815F" Ref="#PWR?"  Part="1" 
AR Path="/5F42815F" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F42815F" Ref="#PWR027"  Part="1" 
AR Path="/5F55C724/5F42815F" Ref="#PWR?"  Part="1" 
F 0 "#PWR027" H 3550 1850 50  0001 C CNN
F 1 "+5V" H 3550 2173 39  0000 C CNN
F 2 "" H 3550 2000 50  0001 C CNN
F 3 "" H 3550 2000 50  0001 C CNN
	1    3550 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F428165
P 2850 2000
AR Path="/5F428165" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F428165" Ref="#PWR026"  Part="1" 
AR Path="/5F55C724/5F428165" Ref="#PWR?"  Part="1" 
F 0 "#PWR026" H 2850 1850 50  0001 C CNN
F 1 "+3.3V" H 2865 2173 39  0000 C CNN
F 2 "" H 2850 2000 50  0001 C CNN
F 3 "" H 2850 2000 50  0001 C CNN
	1    2850 2000
	1    0    0    -1  
$EndComp
Connection ~ 2850 2000
Wire Wire Line
	4000 2400 3550 2400
Connection ~ 3550 2400
Wire Wire Line
	4000 2500 3850 2500
Wire Wire Line
	3850 2500 3850 2000
$Comp
L power:+5V #PWR?
U 1 1 5F428170
P 3850 2000
AR Path="/5F0DB548/5F428170" Ref="#PWR?"  Part="1" 
AR Path="/5F428170" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F428170" Ref="#PWR029"  Part="1" 
AR Path="/5F55C724/5F428170" Ref="#PWR?"  Part="1" 
F 0 "#PWR029" H 3850 1850 50  0001 C CNN
F 1 "+5V" H 3850 2173 39  0000 C CNN
F 2 "" H 3850 2000 50  0001 C CNN
F 3 "" H 3850 2000 50  0001 C CNN
	1    3850 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2400 2850 2400
Connection ~ 2850 2400
$Comp
L power:GND #PWR?
U 1 1 5F42ECC8
P 2800 5300
AR Path="/5F0DB548/5F42ECC8" Ref="#PWR?"  Part="1" 
AR Path="/5F42ECC8" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F42ECC8" Ref="#PWR025"  Part="1" 
AR Path="/5F55C724/5F42ECC8" Ref="#PWR?"  Part="1" 
F 0 "#PWR025" H 2800 5050 50  0001 C CNN
F 1 "GND" H 2805 5127 39  0000 C CNN
F 2 "" H 2800 5300 50  0001 C CNN
F 3 "" H 2800 5300 50  0001 C CNN
	1    2800 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F42ECD4
P 2700 5000
AR Path="/5F0DB548/5F42ECD4" Ref="Q?"  Part="1" 
AR Path="/5F42ECD4" Ref="Q?"  Part="1" 
AR Path="/5F0C6676/5F42ECD4" Ref="Q1"  Part="1" 
AR Path="/5F55C724/5F42ECD4" Ref="Q?"  Part="1" 
F 0 "Q1" H 2905 5046 39  0000 L CNN
F 1 "Si2302CDS" H 2905 4955 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2900 5100 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 2700 5000 50  0001 C CNN
	1    2700 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F42ECDA
P 2300 5200
AR Path="/5F0DB548/5F42ECDA" Ref="R?"  Part="1" 
AR Path="/5F42ECDA" Ref="R?"  Part="1" 
AR Path="/5F0C6676/5F42ECDA" Ref="R7"  Part="1" 
AR Path="/5F55C724/5F42ECDA" Ref="R?"  Part="1" 
F 0 "R7" H 2359 5238 39  0000 L CNN
F 1 "10k" H 2359 5163 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2300 5200 50  0001 C CNN
F 3 "~" H 2300 5200 50  0001 C CNN
	1    2300 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F42ECE0
P 2300 5300
AR Path="/5F0DB548/5F42ECE0" Ref="#PWR?"  Part="1" 
AR Path="/5F42ECE0" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F42ECE0" Ref="#PWR023"  Part="1" 
AR Path="/5F55C724/5F42ECE0" Ref="#PWR?"  Part="1" 
F 0 "#PWR023" H 2300 5050 50  0001 C CNN
F 1 "GND" H 2305 5127 39  0000 C CNN
F 2 "" H 2300 5300 50  0001 C CNN
F 3 "" H 2300 5300 50  0001 C CNN
	1    2300 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5F42ECEC
P 2800 4500
AR Path="/5F0DB548/5F42ECEC" Ref="D?"  Part="1" 
AR Path="/5F42ECEC" Ref="D?"  Part="1" 
AR Path="/5F0C6676/5F42ECEC" Ref="D2"  Part="1" 
AR Path="/5F55C724/5F42ECEC" Ref="D?"  Part="1" 
F 0 "D2" V 2831 4382 39  0000 R CNN
F 1 "L128" V 2756 4382 39  0000 R CNN
F 2 "LEGOS:LED_LUXEON_2835_Handsoldering" H 2800 4500 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/602/DS236-luxeon-2835-color-line-datasheet-1596094.pdf" H 2800 4500 50  0001 C CNN
	1    2800 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2800 4650 2800 4800
Wire Wire Line
	2500 5000 2300 5000
Wire Wire Line
	2300 5000 2300 5100
Wire Wire Line
	2800 5200 2800 5300
Wire Wire Line
	2200 5000 2300 5000
Connection ~ 2300 5000
$Comp
L power:GND #PWR?
U 1 1 5F42ECFA
P 4300 5300
AR Path="/5F0DB548/5F42ECFA" Ref="#PWR?"  Part="1" 
AR Path="/5F42ECFA" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F42ECFA" Ref="#PWR032"  Part="1" 
AR Path="/5F55C724/5F42ECFA" Ref="#PWR?"  Part="1" 
F 0 "#PWR032" H 4300 5050 50  0001 C CNN
F 1 "GND" H 4305 5127 39  0000 C CNN
F 2 "" H 4300 5300 50  0001 C CNN
F 3 "" H 4300 5300 50  0001 C CNN
	1    4300 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F42ED06
P 3800 5200
AR Path="/5F0DB548/5F42ED06" Ref="R?"  Part="1" 
AR Path="/5F42ED06" Ref="R?"  Part="1" 
AR Path="/5F0C6676/5F42ED06" Ref="R10"  Part="1" 
AR Path="/5F55C724/5F42ED06" Ref="R?"  Part="1" 
F 0 "R10" H 3859 5238 39  0000 L CNN
F 1 "10k" H 3859 5163 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3800 5200 50  0001 C CNN
F 3 "~" H 3800 5200 50  0001 C CNN
	1    3800 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F42ED0C
P 3800 5300
AR Path="/5F0DB548/5F42ED0C" Ref="#PWR?"  Part="1" 
AR Path="/5F42ED0C" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F42ED0C" Ref="#PWR028"  Part="1" 
AR Path="/5F55C724/5F42ED0C" Ref="#PWR?"  Part="1" 
F 0 "#PWR028" H 3800 5050 50  0001 C CNN
F 1 "GND" H 3805 5127 39  0000 C CNN
F 2 "" H 3800 5300 50  0001 C CNN
F 3 "" H 3800 5300 50  0001 C CNN
	1    3800 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5F42ED18
P 4300 4500
AR Path="/5F0DB548/5F42ED18" Ref="D?"  Part="1" 
AR Path="/5F42ED18" Ref="D?"  Part="1" 
AR Path="/5F0C6676/5F42ED18" Ref="D3"  Part="1" 
AR Path="/5F55C724/5F42ED18" Ref="D?"  Part="1" 
F 0 "D3" V 4331 4382 39  0000 R CNN
F 1 "L128" V 4256 4382 39  0000 R CNN
F 2 "LEGOS:LED_LUXEON_2835_Handsoldering" H 4300 4500 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/602/DS236-luxeon-2835-color-line-datasheet-1596094.pdf" H 4300 4500 50  0001 C CNN
	1    4300 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4300 4650 4300 4800
Wire Wire Line
	4000 5000 3800 5000
Wire Wire Line
	3800 5000 3800 5100
Wire Wire Line
	4300 5200 4300 5300
Wire Wire Line
	3700 5000 3800 5000
Connection ~ 3800 5000
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F42ED28
P 4200 5000
AR Path="/5F0DB548/5F42ED28" Ref="Q?"  Part="1" 
AR Path="/5F42ED28" Ref="Q?"  Part="1" 
AR Path="/5F0C6676/5F42ED28" Ref="Q3"  Part="1" 
AR Path="/5F55C724/5F42ED28" Ref="Q?"  Part="1" 
F 0 "Q3" H 4405 5046 39  0000 L CNN
F 1 "Si2302CDS" H 4405 4955 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4400 5100 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 4200 5000 50  0001 C CNN
	1    4200 5000
	1    0    0    -1  
$EndComp
Text HLabel 2700 2400 0    39   Input ~ 0
SERVO
Text HLabel 2200 5000 0    39   Input ~ 0
SPARK1
Text HLabel 3700 5000 0    39   Input ~ 0
SPARK2
Text Notes 4450 4800 0    39   ~ 0
L128-5770003500000\nCold White 5700K\nIf 125 mA @ Vf = 2.85
Text Notes 2950 4800 0    39   ~ 0
L128-RYL1003500000\nRoyal Blue 440nm-460nm\nIf 125 mA @ Vf = 3
$Comp
L power:+3.3V #PWR?
U 1 1 5F435EBF
P 2800 3925
AR Path="/5F435EBF" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F435EBF" Ref="#PWR024"  Part="1" 
AR Path="/5F55C724/5F435EBF" Ref="#PWR?"  Part="1" 
F 0 "#PWR024" H 2800 3775 50  0001 C CNN
F 1 "+3.3V" H 2815 4098 39  0000 C CNN
F 2 "" H 2800 3925 50  0001 C CNN
F 3 "" H 2800 3925 50  0001 C CNN
	1    2800 3925
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F436435
P 4300 3925
AR Path="/5F436435" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F436435" Ref="#PWR031"  Part="1" 
AR Path="/5F55C724/5F436435" Ref="#PWR?"  Part="1" 
F 0 "#PWR031" H 4300 3775 50  0001 C CNN
F 1 "+3.3V" H 4315 4098 39  0000 C CNN
F 2 "" H 4300 3925 50  0001 C CNN
F 3 "" H 4300 3925 50  0001 C CNN
	1    4300 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3925 4300 4000
$Comp
L Device:R_Small R?
U 1 1 5F406309
P 4300 4150
AR Path="/5F406309" Ref="R?"  Part="1" 
AR Path="/5F0C6676/5F406309" Ref="R22"  Part="1" 
AR Path="/5F55C724/5F406309" Ref="R?"  Part="1" 
F 0 "R22" H 4350 4100 39  0000 L CNN
F 1 "3.6 - 1/4 W" H 4350 4150 39  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4300 4150 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/219/RK73B-1825463.pdf" H 4300 4150 50  0001 C CNN
F 4 "RK73B2ATTD3R6J " H 4300 4150 50  0001 C CNN "Mfr"
	1    4300 4150
	1    0    0    1   
$EndComp
$Comp
L Device:CP_Small C6
U 1 1 5F407107
P 3950 4500
AR Path="/5F0C6676/5F407107" Ref="C6"  Part="1" 
AR Path="/5F55C724/5F407107" Ref="C?"  Part="1" 
F 0 "C6" H 3862 4538 39  0000 R CNN
F 1 "100u" H 3862 4463 39  0000 R CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 3950 4500 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/40/tlj-776102.pdf" H 3950 4500 50  0001 C CNN
F 4 "TLJA107M004R0500 " H 3950 4500 50  0001 C CNN "Mfr"
	1    3950 4500
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F4085F5
P 3950 4600
AR Path="/5F0DB548/5F4085F5" Ref="#PWR?"  Part="1" 
AR Path="/5F4085F5" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F4085F5" Ref="#PWR049"  Part="1" 
AR Path="/5F55C724/5F4085F5" Ref="#PWR?"  Part="1" 
F 0 "#PWR049" H 3950 4350 50  0001 C CNN
F 1 "GND" H 3955 4427 39  0000 C CNN
F 2 "" H 3950 4600 50  0001 C CNN
F 3 "" H 3950 4600 50  0001 C CNN
	1    3950 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C5
U 1 1 5F40C92F
P 2450 4500
AR Path="/5F0C6676/5F40C92F" Ref="C5"  Part="1" 
AR Path="/5F55C724/5F40C92F" Ref="C?"  Part="1" 
F 0 "C5" H 2362 4538 39  0000 R CNN
F 1 "100u" H 2362 4463 39  0000 R CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 2450 4500 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/40/tlj-776102.pdf" H 2450 4500 50  0001 C CNN
F 4 "TLJA107M004R0500 " H 2450 4500 50  0001 C CNN "Mfr"
	1    2450 4500
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F40C935
P 2450 4600
AR Path="/5F0DB548/5F40C935" Ref="#PWR?"  Part="1" 
AR Path="/5F40C935" Ref="#PWR?"  Part="1" 
AR Path="/5F0C6676/5F40C935" Ref="#PWR048"  Part="1" 
AR Path="/5F55C724/5F40C935" Ref="#PWR?"  Part="1" 
F 0 "#PWR048" H 2450 4350 50  0001 C CNN
F 1 "GND" H 2455 4427 39  0000 C CNN
F 2 "" H 2450 4600 50  0001 C CNN
F 3 "" H 2450 4600 50  0001 C CNN
	1    2450 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F40E567
P 2800 4150
AR Path="/5F40E567" Ref="R?"  Part="1" 
AR Path="/5F0C6676/5F40E567" Ref="R21"  Part="1" 
AR Path="/5F55C724/5F40E567" Ref="R?"  Part="1" 
F 0 "R21" H 2850 4100 39  0000 L CNN
F 1 "2.4 - 1/4 W" H 2850 4150 39  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2800 4150 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/219/RK73B-1825463.pdf" H 2800 4150 50  0001 C CNN
F 4 "RK73B2ATTD2R4J " H 2800 4150 50  0001 C CNN "Mfr"
	1    2800 4150
	1    0    0    1   
$EndComp
Wire Wire Line
	2800 4250 2800 4350
Wire Wire Line
	2800 3925 2800 4000
Wire Wire Line
	2800 4000 2450 4000
Wire Wire Line
	2450 4000 2450 4400
Connection ~ 2800 4000
Wire Wire Line
	2800 4000 2800 4050
Wire Wire Line
	4300 4250 4300 4350
Wire Wire Line
	4300 4000 3950 4000
Wire Wire Line
	3950 4000 3950 4400
Connection ~ 4300 4000
Wire Wire Line
	4300 4000 4300 4050
$EndSCHEMATC
