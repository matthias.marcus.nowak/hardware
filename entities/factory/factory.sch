EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Factory"
Date "2020-10-02"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR07
U 1 1 5E680CF4
P 4550 5250
F 0 "#PWR07" H 4550 5000 50  0001 C CNN
F 1 "GND" H 4550 5000 50  0000 C CNN
F 2 "" H 4550 5250 50  0001 C CNN
F 3 "" H 4550 5250 50  0001 C CNN
	1    4550 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C3
U 1 1 5E8CA2CF
P 4550 2250
F 0 "C3" H 4700 2200 39  0000 R CNN
F 1 "10u" H 4700 2300 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4550 2250 50  0001 C CNN
F 3 "~" H 4550 2250 50  0001 C CNN
	1    4550 2250
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5E8CB08F
P 4250 2250
F 0 "C2" H 4200 2200 39  0000 R CNN
F 1 "100n" H 4250 2300 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4250 2250 50  0001 C CNN
F 3 "~" H 4250 2250 50  0001 C CNN
	1    4250 2250
	1    0    0    1   
$EndComp
Wire Wire Line
	4550 2350 4550 2450
Wire Wire Line
	4250 2350 4250 2450
$Comp
L power:GND #PWR05
U 1 1 5E8F3CB2
P 4250 2150
F 0 "#PWR05" H 4250 1900 50  0001 C CNN
F 1 "GND" H 4255 1977 39  0000 C CNN
F 2 "" H 4250 2150 50  0001 C CNN
F 3 "" H 4250 2150 50  0001 C CNN
	1    4250 2150
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5E8F4451
P 4550 2150
F 0 "#PWR06" H 4550 1900 50  0001 C CNN
F 1 "GND" H 4555 1977 39  0000 C CNN
F 2 "" H 4550 2150 50  0001 C CNN
F 3 "" H 4550 2150 50  0001 C CNN
	1    4550 2150
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E8F840A
P 3450 3050
F 0 "#PWR03" H 3450 2800 50  0001 C CNN
F 1 "GND" H 3455 2877 39  0000 C CNN
F 2 "" H 3450 3050 50  0001 C CNN
F 3 "" H 3450 3050 50  0001 C CNN
	1    3450 3050
	1    0    0    -1  
$EndComp
Connection ~ 3450 2650
$Comp
L Device:C_Small C1
U 1 1 5E8F73C1
P 3450 2850
F 0 "C1" H 3400 2800 39  0000 R CNN
F 1 "10u" H 3400 2900 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3450 2850 50  0001 C CNN
F 3 "~" H 3450 2850 50  0001 C CNN
	1    3450 2850
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5E8F6D03
P 3450 2400
F 0 "R1" H 3500 2350 39  0000 L CNN
F 1 "10k" H 3500 2400 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3450 2400 50  0001 C CNN
F 3 "~" H 3450 2400 50  0001 C CNN
	1    3450 2400
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 5EA426E4
P 3900 2150
F 0 "#PWR04" H 3900 2000 50  0001 C CNN
F 1 "+3.3V" H 3915 2323 39  0000 C CNN
F 2 "" H 3900 2150 50  0001 C CNN
F 3 "" H 3900 2150 50  0001 C CNN
	1    3900 2150
	1    0    0    -1  
$EndComp
Text Label 3600 2650 0    39   ~ 0
EN
Text Label 5350 2650 0    39   ~ 0
ID
Text Notes 5500 2650 0    28   ~ 0
UP during boot
Text Notes 5500 3250 0    28   ~ 0
DOWN during boot
Text Label 5350 2750 0    39   ~ 0
TXD
Text Label 5350 2950 0    39   ~ 0
RXD
Text Notes 5500 2850 0    28   ~ 0
DOWN during prog
Text Notes 5550 3550 0    28   ~ 0
UP during boot for log
Text Notes 3750 4400 1    28   ~ 0
Internally connected to the flash
Text Notes 5500 3150 0    28   ~ 0
UP during boot
Text Notes 5250 2650 0    39   ~ 0
IO0
Wire Wire Line
	3450 2650 3450 2750
Text Notes 3800 2850 2    28   ~ 0
only input
Text Notes 3800 2950 2    28   ~ 0
only input
Wire Wire Line
	3450 2650 3950 2650
Wire Wire Line
	3900 2150 3900 2450
$Comp
L power:+3.3V #PWR02
U 1 1 5EBD636F
P 3450 2150
F 0 "#PWR02" H 3450 2000 50  0001 C CNN
F 1 "+3.3V" H 3465 2323 39  0000 C CNN
F 2 "" H 3450 2150 50  0001 C CNN
F 3 "" H 3450 2150 50  0001 C CNN
	1    3450 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2150 3450 2300
Wire Wire Line
	3450 2500 3450 2650
Wire Wire Line
	5150 3150 5350 3150
Wire Wire Line
	5150 3650 5350 3650
Wire Wire Line
	5150 2950 5350 2950
Wire Wire Line
	5150 2750 5350 2750
Wire Wire Line
	5150 2650 5350 2650
Wire Wire Line
	3900 2450 4250 2450
Connection ~ 4250 2450
Wire Wire Line
	4250 2450 4550 2450
NoConn ~ 3950 3850
NoConn ~ 3950 3950
NoConn ~ 3950 4050
NoConn ~ 3950 4150
NoConn ~ 3950 4250
NoConn ~ 3950 4350
NoConn ~ 3950 2850
NoConn ~ 3950 2950
Text Notes 5150 4350 0    39   ~ 0
DAC\n
Text Notes 5150 4450 0    39   ~ 0
DAC\n
Text Label 7975 1850 2    39   ~ 0
EN
Text Label 7975 1950 2    39   ~ 0
TXD
Text Label 7975 2050 2    39   ~ 0
RXD
Wire Wire Line
	7900 4600 8075 4600
Wire Wire Line
	8075 4700 7900 4700
Wire Wire Line
	8075 4800 7900 4800
Wire Wire Line
	8075 4900 7900 4900
Wire Wire Line
	8075 5000 7900 5000
Text Label 7900 4600 2    39   ~ 0
20%
Text Label 7900 4700 2    39   ~ 0
40%
Text Label 7900 4800 2    39   ~ 0
60%
Text Label 7900 4900 2    39   ~ 0
80%
Text Label 7900 5000 2    39   ~ 0
100%
Text Notes 5200 4875 0    39   ~ 0
only input\n
Text Notes 5200 4975 0    39   ~ 0
only input\n
Text Label 5350 4350 0    39   ~ 0
20%
Text Label 5350 4450 0    39   ~ 0
40%
Text Label 5350 4550 0    39   ~ 0
60%
Text Label 5350 3250 0    39   ~ 0
100%
Wire Wire Line
	5150 3950 5350 3950
Wire Wire Line
	5150 4050 5350 4050
Wire Wire Line
	8075 1850 7975 1850
Wire Wire Line
	7975 1950 8075 1950
Wire Wire Line
	7975 2050 8075 2050
Wire Wire Line
	7975 2150 8025 2150
Text Label 7975 2150 2    39   ~ 0
ID
Wire Wire Line
	7975 2850 8075 2850
Text Label 5350 4050 0    39   ~ 0
SPARK1
Wire Wire Line
	5150 4350 5350 4350
$Comp
L LEGOS:VBUS #PWR08
U 1 1 5F0EA274
P 7825 3750
F 0 "#PWR08" H 7825 3600 50  0001 C CNN
F 1 "VBUS" H 7840 3923 39  0000 C CNN
F 2 "" H 7825 3750 50  0001 C CNN
F 3 "" H 7825 3750 50  0001 C CNN
	1    7825 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8075 3750 7825 3750
Wire Wire Line
	7825 3750 7825 3900
Wire Wire Line
	7825 3900 8075 3900
Connection ~ 7825 3750
Wire Wire Line
	8075 4100 7825 4100
$Comp
L power:+3.3V #PWR09
U 1 1 5F0F68DA
P 7825 4100
F 0 "#PWR09" H 7825 3950 50  0001 C CNN
F 1 "+3.3V" H 7840 4273 39  0000 C CNN
F 2 "" H 7825 4100 50  0001 C CNN
F 3 "" H 7825 4100 50  0001 C CNN
	1    7825 4100
	-1   0    0    1   
$EndComp
Text GLabel 5350 3750 2    39   Input ~ 0
SDA
Text GLabel 5350 3150 2    39   Input ~ 0
SCL
$Comp
L LEGOS:TouchButton BT1
U 1 1 5F155C04
P 8220 5295
AR Path="/5F155C04" Ref="BT1"  Part="1" 
AR Path="/5F110284/5F155C04" Ref="BT?"  Part="1" 
F 0 "BT1" H 8471 4936 50  0000 L CNN
F 1 "TouchButton" H 8471 4845 50  0000 L CNN
F 2 "LEGOS:Touch_Button" H 8775 5035 50  0001 C CNN
F 3 "" H 8220 5295 50  0001 C CNN
	1    8220 5295
	1    0    0    -1  
$EndComp
$Comp
L LEGOS:TouchButton BT2
U 1 1 5F155C0A
P 8220 5645
AR Path="/5F155C0A" Ref="BT2"  Part="1" 
AR Path="/5F110284/5F155C0A" Ref="BT?"  Part="1" 
F 0 "BT2" H 8471 5286 50  0000 L CNN
F 1 "TouchButton" H 8471 5195 50  0000 L CNN
F 2 "LEGOS:Touch_Button" H 8775 5385 50  0001 C CNN
F 3 "" H 8220 5645 50  0001 C CNN
	1    8220 5645
	1    0    0    -1  
$EndComp
Text Label 7925 6050 2    39   ~ 0
LOAD+
Text Label 7925 5700 2    39   ~ 0
LOAD-
Wire Wire Line
	8025 6050 7925 6050
Wire Wire Line
	7925 5700 8025 5700
Text Notes 3050 4750 0    50   ~ 0
Touch GPIO:\nT0 - IO4\nT1 - IO0\nT2 - IO2\nT3 - IO15\nT4 - IO13\nT5 - IO12\nT6 - IO14\nT7 - IO27\nT8 - IO33\nT9 - IO32
Text Label 5350 4750 0    39   ~ 0
LOAD-
Text Label 5350 4650 0    39   ~ 0
LOAD+
Wire Wire Line
	5150 4650 5350 4650
Wire Wire Line
	5350 4550 5150 4550
$Comp
L Switch:SW_Push SW1
U 1 1 5F318984
P 3050 2850
F 0 "SW1" V 2950 2650 39  0000 L CNN
F 1 "PTS815" V 3050 2550 39  0000 L CNN
F 2 "LEGOS:PTS815" H 3050 3050 50  0001 C CNN
F 3 "~" H 3050 3050 50  0001 C CNN
	1    3050 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	3050 2650 3450 2650
$Comp
L power:GND #PWR01
U 1 1 5F31D8A7
P 3050 3050
F 0 "#PWR01" H 3050 2800 50  0001 C CNN
F 1 "GND" H 3055 2877 39  0000 C CNN
F 2 "" H 3050 3050 50  0001 C CNN
F 3 "" H 3050 3050 50  0001 C CNN
	1    3050 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2950 3450 3050
$Sheet
S 8075 1750 550  650 
U 5F0C767B
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "EN" I L 8075 1850 39 
F3 "TXD" O L 8075 1950 39 
F4 "RXD" I L 8075 2050 39 
F5 "IO0" B L 8075 2150 39 
F6 "ID" B L 8075 2250 39 
$EndSheet
Wire Wire Line
	8025 2150 8025 2250
Wire Wire Line
	8025 2250 8075 2250
Connection ~ 8025 2150
Wire Wire Line
	8025 2150 8075 2150
$Sheet
S 8075 3650 525  575 
U 5F0E69AF
F0 "Telemetry" 39
F1 "sub_telemetry.sch" 39
F2 "V+" I L 8075 3900 39 
F3 "V-" I L 8075 4100 39 
F4 "VCOM" I L 8075 3750 39 
F5 "~IRQ" O R 8600 3750 39 
F6 "A1" I R 8600 3900 39 
F7 "A0" I R 8600 4100 39 
$EndSheet
Text Label 5350 3550 0    39   ~ 0
SERVO
Wire Wire Line
	5350 3250 5150 3250
$Sheet
S 8075 2750 550  550 
U 5F0C6676
F0 "Assembly Line" 39
F1 "sub_assembly_line.sch" 39
F2 "SERVO" I L 8075 2850 39 
F3 "SPARK1" I L 8075 3050 39 
F4 "SPARK2" I L 8075 3150 39 
$EndSheet
Text Label 7975 2850 2    39   ~ 0
SERVO
Text Label 5350 3950 0    39   ~ 0
SPARK2
Wire Wire Line
	5150 4450 5350 4450
Text Label 7975 3050 2    39   ~ 0
SPARK1
Wire Wire Line
	8075 3050 7975 3050
Text Label 7975 3150 2    39   ~ 0
SPARK2
Wire Wire Line
	8075 3150 7975 3150
$Sheet
S 8075 4500 525  600 
U 5F0DB548
F0 "Load Indicator" 39
F1 "sub_indicator.sch" 39
F2 "20%" I L 8075 4600 39 
F3 "40%" I L 8075 4700 39 
F4 "60%" I L 8075 4800 39 
F5 "80%" I L 8075 4900 39 
F6 "100%" I L 8075 5000 39 
$EndSheet
$Comp
L Power_Supervisor:LM809 U3
U 1 1 5F3CBCFB
P 2300 2650
F 0 "U3" H 2071 2688 39  0000 R CNN
F 1 "TLV803E" H 2071 2613 39  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2600 2750 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv803e.pdf" H 2600 2750 50  0001 C CNN
	1    2300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2650 2600 2650
Connection ~ 3050 2650
$Comp
L power:GND #PWR0101
U 1 1 5F3CD209
P 2300 3050
F 0 "#PWR0101" H 2300 2800 50  0001 C CNN
F 1 "GND" H 2305 2877 39  0000 C CNN
F 2 "" H 2300 3050 50  0001 C CNN
F 3 "" H 2300 3050 50  0001 C CNN
	1    2300 3050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5F3CD545
P 2300 2150
F 0 "#PWR0102" H 2300 2000 50  0001 C CNN
F 1 "+3.3V" H 2315 2323 39  0000 C CNN
F 2 "" H 2300 2150 50  0001 C CNN
F 3 "" H 2300 2150 50  0001 C CNN
	1    2300 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2150 2300 2250
$Comp
L power:GND #PWR0103
U 1 1 5F3FB387
P 8700 4100
F 0 "#PWR0103" H 8700 3850 50  0001 C CNN
F 1 "GND" V 8700 3900 39  0000 C CNN
F 2 "" H 8700 4100 50  0001 C CNN
F 3 "" H 8700 4100 50  0001 C CNN
	1    8700 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8700 4100 8600 4100
Wire Wire Line
	8600 3900 8700 3900
Wire Wire Line
	8700 3900 8700 4100
Connection ~ 8700 4100
Wire Wire Line
	8700 3750 8600 3750
Text Label 8700 3750 0    39   ~ 0
~IRQ
Text Label 5350 3650 0    39   ~ 0
~IRQ
Connection ~ 4550 2450
Wire Wire Line
	5350 4750 5150 4750
Wire Wire Line
	5150 3550 5350 3550
Wire Wire Line
	5150 3750 5350 3750
Wire Wire Line
	5150 3450 5350 3450
Text Label 5350 3450 0    39   ~ 0
80%
$Comp
L RF_Module:ESP32-WROOM-32D U1
U 1 1 5EF4E292
P 4550 3850
F 0 "U1" H 3700 4400 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 3650 4250 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 4550 2350 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 4250 3900 50  0001 C CNN
	1    4550 3850
	1    0    0    -1  
$EndComp
$Sheet
S 8050 1150 550  300 
U 5F55C724
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
$Comp
L Device:R_Small R23
U 1 1 5F5B52F5
P 4950 2250
F 0 "R23" H 5000 2200 39  0000 L CNN
F 1 "10k" H 5000 2250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4950 2250 50  0001 C CNN
F 3 "~" H 4950 2250 50  0001 C CNN
	1    4950 2250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5F5B73F8
P 4950 2150
F 0 "#PWR0110" H 4950 1900 50  0001 C CNN
F 1 "GND" H 4955 1977 39  0000 C CNN
F 2 "" H 4950 2150 50  0001 C CNN
F 3 "" H 4950 2150 50  0001 C CNN
	1    4950 2150
	-1   0    0    1   
$EndComp
Text Label 5350 2850 0    39   ~ 0
IO2
Wire Wire Line
	5150 2850 5350 2850
Text Label 4950 2450 0    39   ~ 0
IO2
Wire Wire Line
	4950 2350 4950 2450
$EndSCHEMATC
