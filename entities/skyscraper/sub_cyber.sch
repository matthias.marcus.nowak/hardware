EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title "Sub Cyber Security"
Date "2020-10-17"
Rev "1.0"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7675 4550 7675 4400
Wire Wire Line
	7275 4550 7275 4500
$Comp
L power:GND #PWR?
U 1 1 5F8D9910
P 7275 4550
AR Path="/5F1D940F/5F8D9910" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D9910" Ref="#PWR?"  Part="1" 
AR Path="/5F8D9910" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D9910" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D9910" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D9910" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8D9910" Ref="#PWR079"  Part="1" 
F 0 "#PWR079" H 7275 4300 50  0001 C CNN
F 1 "GND" H 7280 4377 50  0000 C CNN
F 2 "" H 7275 4550 50  0001 C CNN
F 3 "" H 7275 4550 50  0001 C CNN
	1    7275 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8DB7C7
P 7675 4550
AR Path="/5F1D940F/5F8DB7C7" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7C7" Ref="#PWR?"  Part="1" 
AR Path="/5F8DB7C7" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7C7" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7C7" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7C7" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8DB7C7" Ref="#PWR081"  Part="1" 
F 0 "#PWR081" H 7675 4300 50  0001 C CNN
F 1 "GND" H 7680 4377 50  0000 C CNN
F 2 "" H 7675 4550 50  0001 C CNN
F 3 "" H 7675 4550 50  0001 C CNN
	1    7675 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8DB7C8
P 7275 4400
AR Path="/5F8DB7C8" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8DB7C8" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7C8" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8DB7C8" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7C8" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8DB7C8" Ref="R?"  Part="1" 
AR Path="/5F8B6860/5F8DB7C8" Ref="R48"  Part="1" 
F 0 "R48" H 7325 4350 39  0000 L CNN
F 1 "10k" H 7325 4400 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7275 4400 50  0001 C CNN
F 3 "~" H 7275 4400 50  0001 C CNN
	1    7275 4400
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8D9922
P 7575 4200
AR Path="/5F1D940F/5F8D9922" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8D9922" Ref="Q?"  Part="1" 
AR Path="/5F8D9922" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8D9922" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8D9922" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F8D9922" Ref="Q?"  Part="1" 
AR Path="/5F8B6860/5F8D9922" Ref="Q15"  Part="1" 
F 0 "Q15" H 7780 4246 39  0000 L CNN
F 1 "Si2302CDS" H 7780 4155 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7775 4300 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 7575 4200 50  0001 C CNN
	1    7575 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7275 4200 7275 4300
Connection ~ 7275 4200
Wire Wire Line
	7275 4200 7375 4200
Wire Wire Line
	7275 4200 7175 4200
Wire Wire Line
	7675 3925 7675 4000
Text HLabel 7175 4200 0    39   Input ~ 0
DATA
Wire Wire Line
	5675 4550 5675 4400
Wire Wire Line
	5275 4550 5275 4500
$Comp
L power:GND #PWR?
U 1 1 5F8DB7CA
P 5275 4550
AR Path="/5F1D940F/5F8DB7CA" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7CA" Ref="#PWR?"  Part="1" 
AR Path="/5F8DB7CA" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7CA" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7CA" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7CA" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8DB7CA" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 5275 4300 50  0001 C CNN
F 1 "GND" H 5280 4377 50  0000 C CNN
F 2 "" H 5275 4550 50  0001 C CNN
F 3 "" H 5275 4550 50  0001 C CNN
	1    5275 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8DB7CB
P 5675 4550
AR Path="/5F1D940F/5F8DB7CB" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7CB" Ref="#PWR?"  Part="1" 
AR Path="/5F8DB7CB" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7CB" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7CB" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7CB" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8DB7CB" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 5675 4300 50  0001 C CNN
F 1 "GND" H 5680 4377 50  0000 C CNN
F 2 "" H 5675 4550 50  0001 C CNN
F 3 "" H 5675 4550 50  0001 C CNN
	1    5675 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8DB7CC
P 5275 4400
AR Path="/5F8DB7CC" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8DB7CC" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7CC" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8DB7CC" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7CC" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8DB7CC" Ref="R?"  Part="1" 
AR Path="/5F8B6860/5F8DB7CC" Ref="R46"  Part="1" 
F 0 "R46" H 5325 4350 39  0000 L CNN
F 1 "10k" H 5325 4400 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5275 4400 50  0001 C CNN
F 3 "~" H 5275 4400 50  0001 C CNN
	1    5275 4400
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8D994F
P 5575 4200
AR Path="/5F1D940F/5F8D994F" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8D994F" Ref="Q?"  Part="1" 
AR Path="/5F8D994F" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8D994F" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8D994F" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F8D994F" Ref="Q?"  Part="1" 
AR Path="/5F8B6860/5F8D994F" Ref="Q14"  Part="1" 
F 0 "Q14" H 5780 4246 39  0000 L CNN
F 1 "Si2302CDS" H 5780 4155 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5775 4300 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 5575 4200 50  0001 C CNN
	1    5575 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5275 4200 5275 4300
Connection ~ 5275 4200
Wire Wire Line
	5275 4200 5375 4200
Wire Wire Line
	5275 4200 5175 4200
Wire Wire Line
	5675 3925 5675 4000
Text HLabel 5175 4200 0    39   Input ~ 0
STATUS
Wire Wire Line
	3675 3225 3675 3325
$Comp
L Device:LED D24
U 1 1 5F8D9962
P 3675 3475
F 0 "D24" V 3706 3357 39  0000 R CNN
F 1 "WP1503SRD " V 3631 3357 39  0000 R CNN
F 2 "" H 3675 3475 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP1503SRD-60174.pdf" H 3675 3475 50  0001 C CNN
	1    3675 3475
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3675 4550 3675 4400
Wire Wire Line
	3275 4550 3275 4500
$Comp
L power:GND #PWR?
U 1 1 5F8DB7CF
P 3275 4550
AR Path="/5F1D940F/5F8DB7CF" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7CF" Ref="#PWR?"  Part="1" 
AR Path="/5F8DB7CF" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7CF" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7CF" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7CF" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8DB7CF" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 3275 4300 50  0001 C CNN
F 1 "GND" H 3280 4377 50  0000 C CNN
F 2 "" H 3275 4550 50  0001 C CNN
F 3 "" H 3275 4550 50  0001 C CNN
	1    3275 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8DB7D0
P 3675 4550
AR Path="/5F1D940F/5F8DB7D0" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7D0" Ref="#PWR?"  Part="1" 
AR Path="/5F8DB7D0" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7D0" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7D0" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7D0" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8DB7D0" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 3675 4300 50  0001 C CNN
F 1 "GND" H 3680 4377 50  0000 C CNN
F 2 "" H 3675 4550 50  0001 C CNN
F 3 "" H 3675 4550 50  0001 C CNN
	1    3675 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8DB7D1
P 3275 4400
AR Path="/5F8DB7D1" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8DB7D1" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7D1" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8DB7D1" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7D1" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8DB7D1" Ref="R?"  Part="1" 
AR Path="/5F8B6860/5F8DB7D1" Ref="R44"  Part="1" 
F 0 "R44" H 3325 4350 39  0000 L CNN
F 1 "10k" H 3325 4400 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3275 4400 50  0001 C CNN
F 3 "~" H 3275 4400 50  0001 C CNN
	1    3275 4400
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8D997C
P 3575 4200
AR Path="/5F1D940F/5F8D997C" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8D997C" Ref="Q?"  Part="1" 
AR Path="/5F8D997C" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8D997C" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8D997C" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F8D997C" Ref="Q?"  Part="1" 
AR Path="/5F8B6860/5F8D997C" Ref="Q13"  Part="1" 
F 0 "Q13" H 3780 4246 39  0000 L CNN
F 1 "Si2302CDS" H 3780 4155 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3775 4300 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 3575 4200 50  0001 C CNN
	1    3575 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3275 4200 3275 4300
Connection ~ 3275 4200
Wire Wire Line
	3275 4200 3375 4200
Wire Wire Line
	3275 4200 3175 4200
Wire Wire Line
	3675 3925 3675 4000
Text HLabel 3175 4200 0    39   Input ~ 0
ATTACK
$Comp
L power:+3.3V #PWR?
U 1 1 5F8DB7D3
P 3675 3225
AR Path="/5F8DB7D3" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7D3" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7D3" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8DB7D3" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7D3" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7D3" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8DB7D3" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 3675 3075 50  0001 C CNN
F 1 "+3.3V" H 3690 3398 39  0000 C CNN
F 2 "" H 3675 3225 50  0001 C CNN
F 3 "" H 3675 3225 50  0001 C CNN
	1    3675 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	3675 3625 3675 3675
Wire Wire Line
	5675 3225 5675 3325
$Comp
L Device:LED D25
U 1 1 5F8DB7D4
P 5675 3475
F 0 "D25" V 5706 3357 39  0000 R CNN
F 1 "VAOL-5701SBY4" V 5631 3357 39  0000 R CNN
F 2 "" H 5675 3475 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/423/VAOL-5701SBY4-1064867.pdf" H 5675 3475 50  0001 C CNN
	1    5675 3475
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8DB7D5
P 5675 3825
AR Path="/5F8DB7D5" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8DB7D5" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7D5" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8DB7D5" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7D5" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8DB7D5" Ref="R?"  Part="1" 
AR Path="/5F8B6860/5F8DB7D5" Ref="R47"  Part="1" 
F 0 "R47" H 5725 3775 39  0000 L CNN
F 1 "75" H 5725 3825 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5675 3825 50  0001 C CNN
F 3 "~" H 5675 3825 50  0001 C CNN
	1    5675 3825
	-1   0    0    1   
$EndComp
Wire Wire Line
	5675 3625 5675 3675
Wire Wire Line
	7675 3225 7675 3325
$Comp
L power:+3.3V #PWR?
U 1 1 5F8E9616
P 7675 3225
AR Path="/5F8E9616" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8E9616" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8E9616" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8E9616" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8E9616" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8E9616" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8E9616" Ref="#PWR080"  Part="1" 
F 0 "#PWR080" H 7675 3075 50  0001 C CNN
F 1 "+3.3V" H 7690 3398 39  0000 C CNN
F 2 "" H 7675 3225 50  0001 C CNN
F 3 "" H 7675 3225 50  0001 C CNN
	1    7675 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	7675 3625 7675 3675
$Comp
L power:+5V #PWR?
U 1 1 5F8C723C
P 5675 3225
AR Path="/5F108703/5F8C723C" Ref="#PWR?"  Part="1" 
AR Path="/5F87D3D1/5F8C723C" Ref="#PWR?"  Part="1" 
AR Path="/5F8B6860/5F8C723C" Ref="#PWR077"  Part="1" 
F 0 "#PWR077" H 5675 3075 50  0001 C CNN
F 1 "+5V" H 5675 3400 39  0000 C CNN
F 2 "" H 5675 3225 50  0001 C CNN
F 3 "" H 5675 3225 50  0001 C CNN
	1    5675 3225
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D26
U 1 1 5F8CB5EF
P 7675 3475
F 0 "D26" V 7706 3357 39  0000 R CNN
F 1 "WP1503SRD " V 7631 3357 39  0000 R CNN
F 2 "" H 7675 3475 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP1503SRD-60174.pdf" H 7675 3475 50  0001 C CNN
	1    7675 3475
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8CBF9E
P 3675 3825
AR Path="/5F8CBF9E" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8CBF9E" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8CBF9E" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8CBF9E" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8CBF9E" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8CBF9E" Ref="R?"  Part="1" 
AR Path="/5F8B6860/5F8CBF9E" Ref="R45"  Part="1" 
F 0 "R45" H 3725 3775 39  0000 L CNN
F 1 "75" H 3725 3825 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3675 3825 50  0001 C CNN
F 3 "~" H 3675 3825 50  0001 C CNN
	1    3675 3825
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8CC3CD
P 7675 3825
AR Path="/5F8CC3CD" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8CC3CD" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8CC3CD" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8CC3CD" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8CC3CD" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8CC3CD" Ref="R?"  Part="1" 
AR Path="/5F8B6860/5F8CC3CD" Ref="R49"  Part="1" 
F 0 "R49" H 7725 3775 39  0000 L CNN
F 1 "75" H 7725 3825 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7675 3825 50  0001 C CNN
F 3 "~" H 7675 3825 50  0001 C CNN
	1    7675 3825
	-1   0    0    1   
$EndComp
Wire Wire Line
	3675 3675 3575 3675
Text HLabel 3575 3675 0    39   Input ~ 0
CS_ATK
Wire Wire Line
	5675 3675 5575 3675
Text HLabel 5575 3675 0    39   Input ~ 0
CS_STA
Wire Wire Line
	7675 3675 7575 3675
Text HLabel 7575 3675 0    39   Input ~ 0
CS_DAT
Connection ~ 7675 3675
Wire Wire Line
	7675 3675 7675 3725
Connection ~ 5675 3675
Wire Wire Line
	5675 3675 5675 3725
Connection ~ 3675 3675
Wire Wire Line
	3675 3675 3675 3725
$EndSCHEMATC
