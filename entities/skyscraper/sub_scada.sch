EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 8
Title "Sub SCADA Control"
Date "2020-10-15"
Rev "1.0"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4250 4550 4125 4550
Text HLabel 4250 4550 2    39   Input ~ 0
LOAD
Wire Wire Line
	4125 4850 4125 4950
$Comp
L power:GND #PWR?
U 1 1 5F94475F
P 4125 4950
AR Path="/5F1D940F/5F94475F" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F94475F" Ref="#PWR?"  Part="1" 
AR Path="/5F94475F" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F94475F" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F94475F" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F94475F" Ref="#PWR044"  Part="1" 
AR Path="/5F8B6860/5F94475F" Ref="#PWR?"  Part="1" 
F 0 "#PWR044" H 4125 4700 50  0001 C CNN
F 1 "GND" H 4130 4777 50  0000 C CNN
F 2 "" H 4125 4950 50  0001 C CNN
F 3 "" H 4125 4950 50  0001 C CNN
	1    4125 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4125 4550 4125 4650
Connection ~ 4125 4550
Wire Wire Line
	3975 4550 4125 4550
$Comp
L Device:R_Small R?
U 1 1 5F942289
P 4125 4750
AR Path="/5F942289" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F942289" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F942289" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F942289" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F942289" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F942289" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F942289" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F942289" Ref="R13"  Part="1" 
AR Path="/5F8B6860/5F942289" Ref="R?"  Part="1" 
F 0 "R13" H 4175 4700 39  0000 L CNN
F 1 "10k" H 4175 4750 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4125 4750 50  0001 C CNN
F 3 "~" H 4125 4750 50  0001 C CNN
	1    4125 4750
	1    0    0    1   
$EndComp
Text Notes 3725 5025 0    39   ~ 0
GREEN
$Comp
L power:+3.3V #PWR?
U 1 1 5F92A4E1
P 3675 4350
AR Path="/5F92A4E1" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F92A4E1" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F92A4E1" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F92A4E1" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F92A4E1" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F92A4E1" Ref="#PWR042"  Part="1" 
AR Path="/5F8B6860/5F92A4E1" Ref="#PWR?"  Part="1" 
F 0 "#PWR042" H 3675 4200 50  0001 C CNN
F 1 "+3.3V" H 3690 4523 39  0000 C CNN
F 2 "" H 3675 4350 50  0001 C CNN
F 3 "" H 3675 4350 50  0001 C CNN
	1    3675 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q4
U 1 1 5F925A41
P 3775 4550
AR Path="/5F8F350D/5F925A41" Ref="Q4"  Part="1" 
AR Path="/5F8B6860/5F925A41" Ref="Q?"  Part="1" 
F 0 "Q4" H 3425 4375 39  0000 L CNN
F 1 "Si2301BDS" H 3425 4450 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3975 4650 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 3775 4550 50  0001 C CNN
	1    3775 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3675 4950 3675 5050
$Comp
L Device:R_Small R?
U 1 1 5F9202A7
P 3675 4850
AR Path="/5F9202A7" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F9202A7" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F9202A7" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F9202A7" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F9202A7" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F9202A7" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F9202A7" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F9202A7" Ref="R12"  Part="1" 
AR Path="/5F8B6860/5F9202A7" Ref="R?"  Part="1" 
F 0 "R12" H 3725 4800 39  0000 L CNN
F 1 "60" H 3725 4850 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3675 4850 50  0001 C CNN
F 3 "~" H 3675 4850 50  0001 C CNN
	1    3675 4850
	1    0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F8FDCEC
P 3475 4950
AR Path="/5F8FDCEC" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8FDCEC" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8FDCEC" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8FDCEC" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8FDCEC" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8FDCEC" Ref="#PWR040"  Part="1" 
AR Path="/5F8B6860/5F8FDCEC" Ref="#PWR?"  Part="1" 
F 0 "#PWR040" H 3475 4800 50  0001 C CNN
F 1 "+3.3V" H 3490 5123 39  0000 C CNN
F 2 "" H 3475 4950 50  0001 C CNN
F 3 "" H 3475 4950 50  0001 C CNN
	1    3475 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5F8D98F4
P 1900 1800
AR Path="/5F0506E3/5F8D98F4" Ref="C?"  Part="1" 
AR Path="/5F8D98F4" Ref="C?"  Part="1" 
AR Path="/5F8F350D/5F8D98F4" Ref="C5"  Part="1" 
AR Path="/5F8B6860/5F8D98F4" Ref="C?"  Part="1" 
F 0 "C5" H 2050 1750 39  0000 R CNN
F 1 "10u" H 2050 1850 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1900 1800 50  0001 C CNN
F 3 "~" H 1900 1800 50  0001 C CNN
	1    1900 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 1700 1900 1600
$Comp
L power:GND #PWR?
U 1 1 5F8D98FB
P 1900 1900
AR Path="/5F0506E3/5F8D98FB" Ref="#PWR?"  Part="1" 
AR Path="/5F8D98FB" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D98FB" Ref="#PWR036"  Part="1" 
AR Path="/5F8B6860/5F8D98FB" Ref="#PWR?"  Part="1" 
F 0 "#PWR036" H 1900 1650 50  0001 C CNN
F 1 "GND" H 1905 1727 39  0000 C CNN
F 2 "" H 1900 1900 50  0001 C CNN
F 3 "" H 1900 1900 50  0001 C CNN
	1    1900 1900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F8D9901
P 1900 1600
AR Path="/5F8D9901" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D9901" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D9901" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8D9901" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D9901" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D9901" Ref="#PWR035"  Part="1" 
AR Path="/5F8B6860/5F8D9901" Ref="#PWR?"  Part="1" 
F 0 "#PWR035" H 1900 1450 50  0001 C CNN
F 1 "+3.3V" H 1915 1773 39  0000 C CNN
F 2 "" H 1900 1600 50  0001 C CNN
F 3 "" H 1900 1600 50  0001 C CNN
	1    1900 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 2950 7400 2800
Wire Wire Line
	7000 2950 7000 2900
$Comp
L power:GND #PWR?
U 1 1 5F8DB7C6
P 7000 2950
AR Path="/5F1D940F/5F8DB7C6" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7C6" Ref="#PWR?"  Part="1" 
AR Path="/5F8DB7C6" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7C6" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7C6" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7C6" Ref="#PWR048"  Part="1" 
AR Path="/5F8B6860/5F8DB7C6" Ref="#PWR?"  Part="1" 
F 0 "#PWR048" H 7000 2700 50  0001 C CNN
F 1 "GND" H 7005 2777 50  0000 C CNN
F 2 "" H 7000 2950 50  0001 C CNN
F 3 "" H 7000 2950 50  0001 C CNN
	1    7000 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8D9916
P 7400 2950
AR Path="/5F1D940F/5F8D9916" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D9916" Ref="#PWR?"  Part="1" 
AR Path="/5F8D9916" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D9916" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D9916" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D9916" Ref="#PWR050"  Part="1" 
AR Path="/5F8B6860/5F8D9916" Ref="#PWR?"  Part="1" 
F 0 "#PWR050" H 7400 2700 50  0001 C CNN
F 1 "GND" H 7405 2777 50  0000 C CNN
F 2 "" H 7400 2950 50  0001 C CNN
F 3 "" H 7400 2950 50  0001 C CNN
	1    7400 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8D991C
P 7000 2800
AR Path="/5F8D991C" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8D991C" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8D991C" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8D991C" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8D991C" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8D991C" Ref="R16"  Part="1" 
AR Path="/5F8B6860/5F8D991C" Ref="R?"  Part="1" 
F 0 "R16" H 7050 2750 39  0000 L CNN
F 1 "10k" H 7050 2800 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7000 2800 50  0001 C CNN
F 3 "~" H 7000 2800 50  0001 C CNN
	1    7000 2800
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8DB7C9
P 7300 2600
AR Path="/5F1D940F/5F8DB7C9" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7C9" Ref="Q?"  Part="1" 
AR Path="/5F8DB7C9" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8DB7C9" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7C9" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F8DB7C9" Ref="Q6"  Part="1" 
AR Path="/5F8B6860/5F8DB7C9" Ref="Q?"  Part="1" 
F 0 "Q6" H 7505 2646 39  0000 L CNN
F 1 "Si2302CDS" H 7505 2555 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7500 2700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 7300 2600 50  0001 C CNN
	1    7300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2600 7000 2700
Connection ~ 7000 2600
Wire Wire Line
	7000 2600 7100 2600
Wire Wire Line
	7000 2600 6900 2600
Wire Wire Line
	7400 2325 7400 2400
Text HLabel 6900 2600 0    39   Input ~ 0
SOLAR
Wire Wire Line
	5400 2950 5400 2800
Wire Wire Line
	5000 2950 5000 2900
$Comp
L power:GND #PWR?
U 1 1 5F8D993D
P 5000 2950
AR Path="/5F1D940F/5F8D993D" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D993D" Ref="#PWR?"  Part="1" 
AR Path="/5F8D993D" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D993D" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D993D" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D993D" Ref="#PWR045"  Part="1" 
AR Path="/5F8B6860/5F8D993D" Ref="#PWR?"  Part="1" 
F 0 "#PWR045" H 5000 2700 50  0001 C CNN
F 1 "GND" H 5005 2777 50  0000 C CNN
F 2 "" H 5000 2950 50  0001 C CNN
F 3 "" H 5000 2950 50  0001 C CNN
	1    5000 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8D9943
P 5400 2950
AR Path="/5F1D940F/5F8D9943" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D9943" Ref="#PWR?"  Part="1" 
AR Path="/5F8D9943" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D9943" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D9943" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D9943" Ref="#PWR047"  Part="1" 
AR Path="/5F8B6860/5F8D9943" Ref="#PWR?"  Part="1" 
F 0 "#PWR047" H 5400 2700 50  0001 C CNN
F 1 "GND" H 5405 2777 50  0000 C CNN
F 2 "" H 5400 2950 50  0001 C CNN
F 3 "" H 5400 2950 50  0001 C CNN
	1    5400 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8D9949
P 5000 2800
AR Path="/5F8D9949" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8D9949" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8D9949" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8D9949" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8D9949" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8D9949" Ref="R14"  Part="1" 
AR Path="/5F8B6860/5F8D9949" Ref="R?"  Part="1" 
F 0 "R14" H 5050 2750 39  0000 L CNN
F 1 "10k" H 5050 2800 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5000 2800 50  0001 C CNN
F 3 "~" H 5000 2800 50  0001 C CNN
	1    5000 2800
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8DB7CD
P 5300 2600
AR Path="/5F1D940F/5F8DB7CD" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7CD" Ref="Q?"  Part="1" 
AR Path="/5F8DB7CD" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8DB7CD" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7CD" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F8DB7CD" Ref="Q5"  Part="1" 
AR Path="/5F8B6860/5F8DB7CD" Ref="Q?"  Part="1" 
F 0 "Q5" H 5505 2646 39  0000 L CNN
F 1 "Si2302CDS" H 5505 2555 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5500 2700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 5300 2600 50  0001 C CNN
	1    5300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2600 5000 2700
Connection ~ 5000 2600
Wire Wire Line
	5000 2600 5100 2600
Wire Wire Line
	5000 2600 4900 2600
Wire Wire Line
	5400 2325 5400 2400
Text HLabel 4900 2600 0    39   Input ~ 0
WIND
Wire Wire Line
	3400 1625 3400 1725
$Comp
L Device:LED D3
U 1 1 5F8DB7CE
P 3400 1875
AR Path="/5F8F350D/5F8DB7CE" Ref="D3"  Part="1" 
AR Path="/5F8B6860/5F8DB7CE" Ref="D?"  Part="1" 
F 0 "D3" V 3431 1757 39  0000 R CNN
F 1 "IN-S124ARB" V 3356 1757 39  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" H 3400 1875 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" H 3400 1875 50  0001 C CNN
	1    3400 1875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 2950 3400 2800
Wire Wire Line
	3000 2950 3000 2900
$Comp
L power:GND #PWR?
U 1 1 5F8D996A
P 3000 2950
AR Path="/5F1D940F/5F8D996A" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D996A" Ref="#PWR?"  Part="1" 
AR Path="/5F8D996A" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D996A" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D996A" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D996A" Ref="#PWR037"  Part="1" 
AR Path="/5F8B6860/5F8D996A" Ref="#PWR?"  Part="1" 
F 0 "#PWR037" H 3000 2700 50  0001 C CNN
F 1 "GND" H 3005 2777 50  0000 C CNN
F 2 "" H 3000 2950 50  0001 C CNN
F 3 "" H 3000 2950 50  0001 C CNN
	1    3000 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8D9970
P 3400 2950
AR Path="/5F1D940F/5F8D9970" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D9970" Ref="#PWR?"  Part="1" 
AR Path="/5F8D9970" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D9970" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D9970" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D9970" Ref="#PWR039"  Part="1" 
AR Path="/5F8B6860/5F8D9970" Ref="#PWR?"  Part="1" 
F 0 "#PWR039" H 3400 2700 50  0001 C CNN
F 1 "GND" H 3405 2777 50  0000 C CNN
F 2 "" H 3400 2950 50  0001 C CNN
F 3 "" H 3400 2950 50  0001 C CNN
	1    3400 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8D9976
P 3000 2800
AR Path="/5F8D9976" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8D9976" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8D9976" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8D9976" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8D9976" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8D9976" Ref="R9"  Part="1" 
AR Path="/5F8B6860/5F8D9976" Ref="R?"  Part="1" 
F 0 "R9" H 3050 2750 39  0000 L CNN
F 1 "10k" H 3050 2800 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3000 2800 50  0001 C CNN
F 3 "~" H 3000 2800 50  0001 C CNN
	1    3000 2800
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8DB7D2
P 3300 2600
AR Path="/5F1D940F/5F8DB7D2" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7D2" Ref="Q?"  Part="1" 
AR Path="/5F8DB7D2" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8DB7D2" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7D2" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F8DB7D2" Ref="Q2"  Part="1" 
AR Path="/5F8B6860/5F8DB7D2" Ref="Q?"  Part="1" 
F 0 "Q2" H 3505 2646 39  0000 L CNN
F 1 "Si2302CDS" H 3505 2555 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3500 2700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 3300 2600 50  0001 C CNN
	1    3300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2600 3000 2700
Connection ~ 3000 2600
Wire Wire Line
	3000 2600 3100 2600
Wire Wire Line
	3000 2600 2900 2600
Wire Wire Line
	3400 2325 3400 2400
Text HLabel 2900 2600 0    39   Input ~ 0
FUEL
$Comp
L power:+3.3V #PWR?
U 1 1 5F8D9988
P 3400 1625
AR Path="/5F8D9988" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D9988" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D9988" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8D9988" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D9988" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D9988" Ref="#PWR038"  Part="1" 
AR Path="/5F8B6860/5F8D9988" Ref="#PWR?"  Part="1" 
F 0 "#PWR038" H 3400 1475 50  0001 C CNN
F 1 "+3.3V" H 3415 1798 39  0000 C CNN
F 2 "" H 3400 1625 50  0001 C CNN
F 3 "" H 3400 1625 50  0001 C CNN
	1    3400 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 2950 9400 2800
Wire Wire Line
	9000 2950 9000 2900
$Comp
L power:GND #PWR?
U 1 1 5F8D9997
P 9000 2950
AR Path="/5F1D940F/5F8D9997" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D9997" Ref="#PWR?"  Part="1" 
AR Path="/5F8D9997" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D9997" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D9997" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D9997" Ref="#PWR051"  Part="1" 
AR Path="/5F8B6860/5F8D9997" Ref="#PWR?"  Part="1" 
F 0 "#PWR051" H 9000 2700 50  0001 C CNN
F 1 "GND" H 9005 2777 50  0000 C CNN
F 2 "" H 9000 2950 50  0001 C CNN
F 3 "" H 9000 2950 50  0001 C CNN
	1    9000 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8D999D
P 9400 2950
AR Path="/5F1D940F/5F8D999D" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8D999D" Ref="#PWR?"  Part="1" 
AR Path="/5F8D999D" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8D999D" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8D999D" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8D999D" Ref="#PWR053"  Part="1" 
AR Path="/5F8B6860/5F8D999D" Ref="#PWR?"  Part="1" 
F 0 "#PWR053" H 9400 2700 50  0001 C CNN
F 1 "GND" H 9405 2777 50  0000 C CNN
F 2 "" H 9400 2950 50  0001 C CNN
F 3 "" H 9400 2950 50  0001 C CNN
	1    9400 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8D99A3
P 9000 2800
AR Path="/5F8D99A3" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8D99A3" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8D99A3" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8D99A3" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8D99A3" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8D99A3" Ref="R18"  Part="1" 
AR Path="/5F8B6860/5F8D99A3" Ref="R?"  Part="1" 
F 0 "R18" H 9050 2750 39  0000 L CNN
F 1 "10k" H 9050 2800 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9000 2800 50  0001 C CNN
F 3 "~" H 9000 2800 50  0001 C CNN
	1    9000 2800
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8D99A9
P 9300 2600
AR Path="/5F1D940F/5F8D99A9" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8D99A9" Ref="Q?"  Part="1" 
AR Path="/5F8D99A9" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8D99A9" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8D99A9" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F8D99A9" Ref="Q7"  Part="1" 
AR Path="/5F8B6860/5F8D99A9" Ref="Q?"  Part="1" 
F 0 "Q7" H 9505 2646 39  0000 L CNN
F 1 "Si2302CDS" H 9505 2555 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9500 2700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 9300 2600 50  0001 C CNN
	1    9300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 2600 9000 2700
Connection ~ 9000 2600
Wire Wire Line
	9000 2600 9100 2600
Wire Wire Line
	9000 2600 8900 2600
Wire Wire Line
	9400 2325 9400 2400
Text HLabel 8900 2600 0    39   Input ~ 0
FAULT
$Comp
L Device:R_Small R?
U 1 1 5F8DDE0C
P 3400 2225
AR Path="/5F8DDE0C" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8DDE0C" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8DDE0C" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8DDE0C" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8DDE0C" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8DDE0C" Ref="R10"  Part="1" 
AR Path="/5F8B6860/5F8DDE0C" Ref="R?"  Part="1" 
F 0 "R10" H 3450 2175 39  0000 L CNN
F 1 "20" H 3450 2225 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3400 2225 50  0001 C CNN
F 3 "~" H 3400 2225 50  0001 C CNN
	1    3400 2225
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 2025 3400 2125
Wire Wire Line
	5400 1625 5400 1725
$Comp
L Device:LED D5
U 1 1 5F8E7A19
P 5400 1875
AR Path="/5F8F350D/5F8E7A19" Ref="D5"  Part="1" 
AR Path="/5F8B6860/5F8E7A19" Ref="D?"  Part="1" 
F 0 "D5" V 5431 1757 39  0000 R CNN
F 1 "IN-S124ARB" V 5356 1757 39  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" H 5400 1875 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" H 5400 1875 50  0001 C CNN
	1    5400 1875
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F8E7A1F
P 5400 1625
AR Path="/5F8E7A1F" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8E7A1F" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8E7A1F" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8E7A1F" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8E7A1F" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8E7A1F" Ref="#PWR046"  Part="1" 
AR Path="/5F8B6860/5F8E7A1F" Ref="#PWR?"  Part="1" 
F 0 "#PWR046" H 5400 1475 50  0001 C CNN
F 1 "+3.3V" H 5415 1798 39  0000 C CNN
F 2 "" H 5400 1625 50  0001 C CNN
F 3 "" H 5400 1625 50  0001 C CNN
	1    5400 1625
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E7A25
P 5400 2225
AR Path="/5F8E7A25" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E7A25" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E7A25" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E7A25" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E7A25" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8E7A25" Ref="R15"  Part="1" 
AR Path="/5F8B6860/5F8E7A25" Ref="R?"  Part="1" 
F 0 "R15" H 5450 2175 39  0000 L CNN
F 1 "20" H 5450 2225 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5400 2225 50  0001 C CNN
F 3 "~" H 5400 2225 50  0001 C CNN
	1    5400 2225
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 2025 5400 2125
Wire Wire Line
	7400 1625 7400 1725
$Comp
L Device:LED D6
U 1 1 5F8E9610
P 7400 1875
AR Path="/5F8F350D/5F8E9610" Ref="D6"  Part="1" 
AR Path="/5F8B6860/5F8E9610" Ref="D?"  Part="1" 
F 0 "D6" V 7431 1757 39  0000 R CNN
F 1 "IN-S124ARB" V 7356 1757 39  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" H 7400 1875 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" H 7400 1875 50  0001 C CNN
	1    7400 1875
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F8DB7D6
P 7400 1625
AR Path="/5F8DB7D6" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8DB7D6" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8DB7D6" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8DB7D6" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8DB7D6" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8DB7D6" Ref="#PWR049"  Part="1" 
AR Path="/5F8B6860/5F8DB7D6" Ref="#PWR?"  Part="1" 
F 0 "#PWR049" H 7400 1475 50  0001 C CNN
F 1 "+3.3V" H 7415 1798 39  0000 C CNN
F 2 "" H 7400 1625 50  0001 C CNN
F 3 "" H 7400 1625 50  0001 C CNN
	1    7400 1625
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E961C
P 7400 2225
AR Path="/5F8E961C" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E961C" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E961C" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E961C" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E961C" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8E961C" Ref="R17"  Part="1" 
AR Path="/5F8B6860/5F8E961C" Ref="R?"  Part="1" 
F 0 "R17" H 7450 2175 39  0000 L CNN
F 1 "20" H 7450 2225 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7400 2225 50  0001 C CNN
F 3 "~" H 7400 2225 50  0001 C CNN
	1    7400 2225
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 2025 7400 2125
Wire Wire Line
	9400 1625 9400 1725
$Comp
L Device:LED D7
U 1 1 5F8EC775
P 9400 1875
AR Path="/5F8F350D/5F8EC775" Ref="D7"  Part="1" 
AR Path="/5F8B6860/5F8EC775" Ref="D?"  Part="1" 
F 0 "D7" V 9431 1757 39  0000 R CNN
F 1 "IN-S124ARUR" V 9356 1757 39  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" H 9400 1875 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" H 9400 1875 50  0001 C CNN
	1    9400 1875
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F8EC77B
P 9400 1625
AR Path="/5F8EC77B" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8EC77B" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8EC77B" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8EC77B" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8EC77B" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F8EC77B" Ref="#PWR052"  Part="1" 
AR Path="/5F8B6860/5F8EC77B" Ref="#PWR?"  Part="1" 
F 0 "#PWR052" H 9400 1475 50  0001 C CNN
F 1 "+3.3V" H 9415 1798 39  0000 C CNN
F 2 "" H 9400 1625 50  0001 C CNN
F 3 "" H 9400 1625 50  0001 C CNN
	1    9400 1625
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8EC781
P 9400 2225
AR Path="/5F8EC781" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8EC781" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8EC781" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8EC781" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8EC781" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F8EC781" Ref="R19"  Part="1" 
AR Path="/5F8B6860/5F8EC781" Ref="R?"  Part="1" 
F 0 "R19" H 9450 2175 39  0000 L CNN
F 1 "125" H 9450 2225 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9400 2225 50  0001 C CNN
F 3 "~" H 9400 2225 50  0001 C CNN
	1    9400 2225
	-1   0    0    1   
$EndComp
Wire Wire Line
	9400 2025 9400 2125
$Comp
L LEGOS:LED_Dual_CCAA D4
U 1 1 5F8F41A8
P 3575 5350
AR Path="/5F8F350D/5F8F41A8" Ref="D4"  Part="1" 
AR Path="/5F8B6860/5F8F41A8" Ref="D?"  Part="1" 
F 0 "D4" V 3537 5638 39  0000 L CNN
F 1 "156125RV73000" V 3612 5638 39  0000 L CNN
F 2 "LEGOS:WL-SBRW_Wuerth_156125RV73000" H 3605 5350 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/445/156125RV73000-1716231.pdf" H 3605 5350 50  0001 C CNN
	1    3575 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	3475 6550 3475 6400
$Comp
L power:GND #PWR?
U 1 1 5F907A5E
P 3475 6550
AR Path="/5F1D940F/5F907A5E" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F907A5E" Ref="#PWR?"  Part="1" 
AR Path="/5F907A5E" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F907A5E" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F907A5E" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F907A5E" Ref="#PWR041"  Part="1" 
AR Path="/5F8B6860/5F907A5E" Ref="#PWR?"  Part="1" 
F 0 "#PWR041" H 3475 6300 50  0001 C CNN
F 1 "GND" H 3480 6377 50  0000 C CNN
F 2 "" H 3475 6550 50  0001 C CNN
F 3 "" H 3475 6550 50  0001 C CNN
	1    3475 6550
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F907A6A
P 3375 6200
AR Path="/5F1D940F/5F907A6A" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F907A6A" Ref="Q?"  Part="1" 
AR Path="/5F907A6A" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F907A6A" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F907A6A" Ref="Q?"  Part="1" 
AR Path="/5F8F350D/5F907A6A" Ref="Q3"  Part="1" 
AR Path="/5F8B6860/5F907A6A" Ref="Q?"  Part="1" 
F 0 "Q3" H 3580 6246 39  0000 L CNN
F 1 "Si2302CDS" H 3580 6155 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3575 6300 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 3375 6200 50  0001 C CNN
	1    3375 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3475 5925 3475 6000
Text HLabel 2975 6200 0    39   Input ~ 0
LOAD
$Comp
L Device:R_Small R?
U 1 1 5F907A76
P 3475 5825
AR Path="/5F907A76" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F907A76" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F907A76" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F907A76" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F907A76" Ref="R?"  Part="1" 
AR Path="/5F8F350D/5F907A76" Ref="R11"  Part="1" 
AR Path="/5F8B6860/5F907A76" Ref="R?"  Part="1" 
F 0 "R11" H 3525 5775 39  0000 L CNN
F 1 "20" H 3525 5825 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3475 5825 50  0001 C CNN
F 3 "~" H 3475 5825 50  0001 C CNN
	1    3475 5825
	-1   0    0    1   
$EndComp
Wire Wire Line
	3475 5650 3475 5725
Wire Wire Line
	3475 4950 3475 5050
$Comp
L power:GND #PWR?
U 1 1 5F914CD7
P 3675 5725
AR Path="/5F1D940F/5F914CD7" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F914CD7" Ref="#PWR?"  Part="1" 
AR Path="/5F914CD7" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F914CD7" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F914CD7" Ref="#PWR?"  Part="1" 
AR Path="/5F8F350D/5F914CD7" Ref="#PWR043"  Part="1" 
AR Path="/5F8B6860/5F914CD7" Ref="#PWR?"  Part="1" 
F 0 "#PWR043" H 3675 5475 50  0001 C CNN
F 1 "GND" H 3680 5552 50  0000 C CNN
F 2 "" H 3675 5725 50  0001 C CNN
F 3 "" H 3675 5725 50  0001 C CNN
	1    3675 5725
	1    0    0    -1  
$EndComp
Wire Wire Line
	3675 5650 3675 5725
Wire Wire Line
	2975 6200 3175 6200
$EndSCHEMATC
