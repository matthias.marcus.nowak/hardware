# Skyscraper
The Skyscraper represents a common aggregated residential consumer. The building hosts a SCADA center for monitoring the overall status of the grid, a cyber-security center for monitoring the status of cyber-attacks and data-breach affecting the infrastructure. The building is provided with a HVAC system that takes of air recirculation, heating and monitors the indoor air quality (IAQ). The circuitry includes a telemetry sub-block for monitoring the self power consumption in the 3.3 V bus, a sub-block for internal and external lights, and a sub-block for SCADA, Cyber-Security and HVAC each.

## Schematic
[<img src="docs/skyscraper_sch.png"  width="1261" height="500">](docs/skyscraper_sch-main.png)

[<img src="docs/skyscraper_sch-sub_connectors.png"  width="145" height="100">](docs/skyscraper_sch-sub_connectors.png)
&nbsp;
[<img src="docs/skyscraper_sch-sub_lights.png"  width="145" height="100">](docs/skyscraper_sch-sub_lights.png)
&nbsp;
[<img src="docs/skyscraper_sch-sub_scada.png"  width="145" height="100">](docs/skyscraper_sch-sub_scada.png)
&nbsp;
[<img src="docs/skyscraper_sch-sub_cyber.png"  width="145" height="100">](docs/skyscraper_sch-sub_cyber.png)
&nbsp;
[<img src="docs/skyscraper_sch-sub_hvac.png"  width="145" height="100">](docs/skyscraper_sch-sub_hvac.png)
&nbsp;
[<img src="docs/skyscraper_sch-sub_telemetry.png"  width="145" height="100">](docs/skyscraper_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/skyscraper_sch-sub_mechanical.png"  width="145" height="100">](docs/skyscraper_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/skyscraper_pcb.png"  width="531" height="500">](docs/skyscraper_pcb-brd.png)

[<img src="docs/skyscraper_pcb-F_Cu.png"  width="145" height="100">](docs/skyscraper_pcb-F_Cu.png)
&nbsp;
[<img src="docs/skyscraper_pcb-In1_Cu.png"  width="145" height="100">](docs/skyscraper_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/skyscraper_pcb-In2_Cu.png"  width="145" height="100">](docs/skyscraper_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/skyscraper_pcb-B_Cu.png"  width="145" height="100">](docs/skyscraper_pcb-B_Cu.png)

## Media

[<img src="docs/skyscraper_3d-top.png"  width="100" height="100">](docs/skyscraper_3d-top.png)
&nbsp;
[<img src="docs/skyscraper_3d-bottom.png"  width="100" height="100">](docs/skyscraper_3d-bottom.png)
&nbsp;
[<img src="docs/skyscraper_3d_scada-top.png"  width="31" height="100">](docs/skyscraper_3d_scada-top.png)
&nbsp;
[<img src="docs/skyscraper_3d_scada-bottom.png"  width="31" height="100">](docs/skyscraper_3d_scada-bottom.png)
&nbsp;
[<img src="docs/skyscraper_brd.png"  width="150" height="100">](docs/skyscraper_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/skyscraper/skyscraper.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/skyscraper/skyscraper.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/skyscraper/skyscraper.md)