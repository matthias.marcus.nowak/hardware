EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 8
Title "Sub Building Lights"
Date "2020-10-15"
Rev "1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3775 4900 3775 4750
Wire Wire Line
	3375 4900 3375 4850
$Comp
L power:GND #PWR?
U 1 1 5F8EA9CE
P 3375 4900
AR Path="/5F1D940F/5F8EA9CE" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8EA9CE" Ref="#PWR?"  Part="1" 
AR Path="/5F8EA9CE" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8EA9CE" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8EA9CE" Ref="#PWR?"  Part="1" 
AR Path="/5F8AB3A0/5F8EA9CE" Ref="#PWR054"  Part="1" 
AR Path="/5F8F350D/5F8EA9CE" Ref="#PWR?"  Part="1" 
F 0 "#PWR054" H 3375 4650 50  0001 C CNN
F 1 "GND" H 3380 4727 50  0000 C CNN
F 2 "" H 3375 4900 50  0001 C CNN
F 3 "" H 3375 4900 50  0001 C CNN
	1    3375 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8EA9D4
P 3775 4900
AR Path="/5F1D940F/5F8EA9D4" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8EA9D4" Ref="#PWR?"  Part="1" 
AR Path="/5F8EA9D4" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8EA9D4" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8EA9D4" Ref="#PWR?"  Part="1" 
AR Path="/5F8AB3A0/5F8EA9D4" Ref="#PWR056"  Part="1" 
AR Path="/5F8F350D/5F8EA9D4" Ref="#PWR?"  Part="1" 
F 0 "#PWR056" H 3775 4650 50  0001 C CNN
F 1 "GND" H 3780 4727 50  0000 C CNN
F 2 "" H 3775 4900 50  0001 C CNN
F 3 "" H 3775 4900 50  0001 C CNN
	1    3775 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8EA9DA
P 3375 4750
AR Path="/5F8EA9DA" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8EA9DA" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8EA9DA" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8EA9DA" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8EA9DA" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8EA9DA" Ref="R20"  Part="1" 
AR Path="/5F8F350D/5F8EA9DA" Ref="R?"  Part="1" 
F 0 "R20" H 3425 4700 39  0000 L CNN
F 1 "10k" H 3425 4750 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3375 4750 50  0001 C CNN
F 3 "~" H 3375 4750 50  0001 C CNN
	1    3375 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F8EA9E0
P 3675 4550
AR Path="/5F1D940F/5F8EA9E0" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F8EA9E0" Ref="Q?"  Part="1" 
AR Path="/5F8EA9E0" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F8EA9E0" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F8EA9E0" Ref="Q?"  Part="1" 
AR Path="/5F8AB3A0/5F8EA9E0" Ref="Q8"  Part="1" 
AR Path="/5F8F350D/5F8EA9E0" Ref="Q?"  Part="1" 
F 0 "Q8" H 3880 4596 39  0000 L CNN
F 1 "Si2302CDS" H 3880 4505 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3875 4650 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 3675 4550 50  0001 C CNN
	1    3675 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3375 4550 3375 4650
Wire Wire Line
	3375 4550 3475 4550
Wire Wire Line
	3375 4550 3275 4550
Connection ~ 3375 4550
Text HLabel 3275 4550 0    39   Input ~ 0
EXT
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB18
P 3775 3725
AR Path="/5F1D940F/5F7EAB18" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB18" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB18" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB18" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F7EAB18" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F7EAB18" Ref="D8"  Part="1" 
AR Path="/5F8F350D/5F7EAB18" Ref="D?"  Part="1" 
F 0 "D8" V 3825 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 3775 3625 31  0000 R CNN
F 2 "" V 3775 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 3775 3725 50  0001 C CNN
	1    3775 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	3775 3475 3775 3625
$Comp
L Device:LED_Small D?
U 1 1 5F8B6597
P 4175 3725
AR Path="/5F1D940F/5F8B6597" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8B6597" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8B6597" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8B6597" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8B6597" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8B6597" Ref="D9"  Part="1" 
AR Path="/5F8F350D/5F8B6597" Ref="D?"  Part="1" 
F 0 "D9" V 4225 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 4175 3625 31  0000 R CNN
F 2 "" V 4175 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 4175 3725 50  0001 C CNN
	1    4175 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	4175 3475 4175 3625
Wire Wire Line
	3775 3475 4175 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8B6DCC
P 4575 3725
AR Path="/5F1D940F/5F8B6DCC" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8B6DCC" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8B6DCC" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8B6DCC" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8B6DCC" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8B6DCC" Ref="D10"  Part="1" 
AR Path="/5F8F350D/5F8B6DCC" Ref="D?"  Part="1" 
F 0 "D10" V 4625 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 4575 3625 31  0000 R CNN
F 2 "" V 4575 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 4575 3725 50  0001 C CNN
	1    4575 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	4575 3475 4575 3625
Wire Wire Line
	4175 3475 4575 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8B750A
P 4975 3725
AR Path="/5F1D940F/5F8B750A" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8B750A" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8B750A" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8B750A" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8B750A" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8B750A" Ref="D11"  Part="1" 
AR Path="/5F8F350D/5F8B750A" Ref="D?"  Part="1" 
F 0 "D11" V 5025 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 4975 3625 31  0000 R CNN
F 2 "" V 4975 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 4975 3725 50  0001 C CNN
	1    4975 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	4975 3475 4975 3625
Wire Wire Line
	4575 3475 4975 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8B94C0
P 5375 3725
AR Path="/5F1D940F/5F8B94C0" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8B94C0" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8B94C0" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8B94C0" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8B94C0" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8B94C0" Ref="D12"  Part="1" 
AR Path="/5F8F350D/5F8B94C0" Ref="D?"  Part="1" 
F 0 "D12" V 5425 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 5375 3625 31  0000 R CNN
F 2 "" V 5375 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 5375 3725 50  0001 C CNN
	1    5375 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	5375 3475 5375 3625
Wire Wire Line
	4975 3475 5375 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8B94D1
P 5775 3725
AR Path="/5F1D940F/5F8B94D1" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8B94D1" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8B94D1" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8B94D1" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8B94D1" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8B94D1" Ref="D13"  Part="1" 
AR Path="/5F8F350D/5F8B94D1" Ref="D?"  Part="1" 
F 0 "D13" V 5825 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 5775 3625 31  0000 R CNN
F 2 "" V 5775 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 5775 3725 50  0001 C CNN
	1    5775 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	5775 3475 5775 3625
Wire Wire Line
	5375 3475 5775 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8B94E2
P 6175 3725
AR Path="/5F1D940F/5F8B94E2" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8B94E2" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8B94E2" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8B94E2" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8B94E2" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8B94E2" Ref="D14"  Part="1" 
AR Path="/5F8F350D/5F8B94E2" Ref="D?"  Part="1" 
F 0 "D14" V 6225 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 6175 3625 31  0000 R CNN
F 2 "" V 6175 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 6175 3725 50  0001 C CNN
	1    6175 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	6175 3475 6175 3625
Wire Wire Line
	5775 3475 6175 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8B94F3
P 6575 3725
AR Path="/5F1D940F/5F8B94F3" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8B94F3" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8B94F3" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8B94F3" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8B94F3" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8B94F3" Ref="D15"  Part="1" 
AR Path="/5F8F350D/5F8B94F3" Ref="D?"  Part="1" 
F 0 "D15" V 6625 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 6575 3625 31  0000 R CNN
F 2 "" V 6575 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 6575 3725 50  0001 C CNN
	1    6575 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	6575 3475 6575 3625
Wire Wire Line
	6175 3475 6575 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8BBB7F
P 6975 3725
AR Path="/5F1D940F/5F8BBB7F" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8BBB7F" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8BBB7F" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8BBB7F" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8BBB7F" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8BBB7F" Ref="D16"  Part="1" 
AR Path="/5F8F350D/5F8BBB7F" Ref="D?"  Part="1" 
F 0 "D16" V 7025 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 6975 3625 31  0000 R CNN
F 2 "" V 6975 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 6975 3725 50  0001 C CNN
	1    6975 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	6975 3475 6975 3625
Wire Wire Line
	6575 3475 6975 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8BBB90
P 7375 3725
AR Path="/5F1D940F/5F8BBB90" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8BBB90" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8BBB90" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8BBB90" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8BBB90" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8BBB90" Ref="D17"  Part="1" 
AR Path="/5F8F350D/5F8BBB90" Ref="D?"  Part="1" 
F 0 "D17" V 7425 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 7375 3625 31  0000 R CNN
F 2 "" V 7375 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 7375 3725 50  0001 C CNN
	1    7375 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	7375 3475 7375 3625
Wire Wire Line
	6975 3475 7375 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8BBBA1
P 7775 3725
AR Path="/5F1D940F/5F8BBBA1" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8BBBA1" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8BBBA1" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8BBBA1" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8BBBA1" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8BBBA1" Ref="D18"  Part="1" 
AR Path="/5F8F350D/5F8BBBA1" Ref="D?"  Part="1" 
F 0 "D18" V 7825 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 7775 3625 31  0000 R CNN
F 2 "" V 7775 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 7775 3725 50  0001 C CNN
	1    7775 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	7775 3475 7775 3625
Wire Wire Line
	7375 3475 7775 3475
$Comp
L Device:LED_Small D?
U 1 1 5F8BBBB2
P 8175 3725
AR Path="/5F1D940F/5F8BBBB2" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F8BBBB2" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F8BBBB2" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F8BBBB2" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F8BBBB2" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F8BBBB2" Ref="D19"  Part="1" 
AR Path="/5F8F350D/5F8BBBB2" Ref="D?"  Part="1" 
F 0 "D19" V 8225 3625 39  0000 R CNN
F 1 "OVLLY8C7" V 8175 3625 31  0000 R CNN
F 2 "" V 8175 3725 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Optoelectronics/Datasheets/OVLL.pdf" V 8175 3725 50  0001 C CNN
	1    8175 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	8175 3475 8175 3625
Wire Wire Line
	7775 3475 8175 3475
Connection ~ 3775 3475
Connection ~ 4175 3475
Connection ~ 4575 3475
Connection ~ 4975 3475
Connection ~ 5375 3475
Connection ~ 5775 3475
Connection ~ 6175 3475
Connection ~ 6575 3475
Connection ~ 6975 3475
Connection ~ 7375 3475
Connection ~ 7775 3475
$Comp
L power:+3.3V #PWR?
U 1 1 5F8EA9EC
P 3775 3475
AR Path="/5F8EA9EC" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F8EA9EC" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F8EA9EC" Ref="#PWR?"  Part="1" 
AR Path="/5F1D940F/5F8EA9EC" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F8EA9EC" Ref="#PWR?"  Part="1" 
AR Path="/5F8AB3A0/5F8EA9EC" Ref="#PWR055"  Part="1" 
AR Path="/5F8F350D/5F8EA9EC" Ref="#PWR?"  Part="1" 
F 0 "#PWR055" H 3775 3325 50  0001 C CNN
F 1 "+3.3V" H 3790 3648 39  0000 C CNN
F 2 "" H 3775 3475 50  0001 C CNN
F 3 "" H 3775 3475 50  0001 C CNN
	1    3775 3475
	1    0    0    -1  
$EndComp
Text HLabel 3600 4300 0    39   Input ~ 0
LAMP_EXT
Text Notes 4325 3425 0    50   ~ 0
If = (12+3) x 20 mA = 300 mA
Wire Wire Line
	8925 4300 8925 4175
Connection ~ 8175 3475
Wire Wire Line
	8925 3475 8925 3625
Wire Wire Line
	8925 3825 8925 3975
$Comp
L Device:LED_Small D?
U 1 1 5F93E45A
P 8925 3725
AR Path="/5F1D940F/5F93E45A" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F93E45A" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F93E45A" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F93E45A" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F93E45A" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F93E45A" Ref="D20"  Part="1" 
AR Path="/5F8F350D/5F93E45A" Ref="D?"  Part="1" 
F 0 "D20" V 8975 3625 39  0000 R CNN
F 1 "C513A-WSS-CW0Z0151" V 8925 3625 31  0000 R CNN
F 2 "" V 8925 3725 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/90/C513A_WSN_WSS_MSN_MSS_1042-268537.pdf" V 8925 3725 50  0001 C CNN
	1    8925 3725
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F9739F0
P 8925 4075
AR Path="/5F9739F0" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F9739F0" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F9739F0" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F9739F0" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F9739F0" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F9739F0" Ref="R34"  Part="1" 
AR Path="/5F8F350D/5F9739F0" Ref="R?"  Part="1" 
F 0 "R34" H 8975 4025 39  0000 L CNN
F 1 "5" H 8975 4075 39  0000 L CNN
F 2 "" H 8925 4075 50  0001 C CNN
F 3 "~" H 8925 4075 50  0001 C CNN
F 4 "RN55D5R00FB14" H 8925 4075 50  0001 C CNN "Mfr"
	1    8925 4075
	-1   0    0    1   
$EndComp
Wire Wire Line
	9300 4300 9300 4175
Wire Wire Line
	9300 3475 9300 3625
Wire Wire Line
	9300 3825 9300 3975
$Comp
L Device:LED_Small D?
U 1 1 5F97F31E
P 9300 3725
AR Path="/5F1D940F/5F97F31E" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F97F31E" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F97F31E" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F97F31E" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F97F31E" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F97F31E" Ref="D21"  Part="1" 
AR Path="/5F8F350D/5F97F31E" Ref="D?"  Part="1" 
F 0 "D21" V 9350 3625 39  0000 R CNN
F 1 "C513A-WSS-CW0Z0151" V 9300 3625 31  0001 R CNN
F 2 "" V 9300 3725 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/90/C513A_WSN_WSS_MSN_MSS_1042-268537.pdf" V 9300 3725 50  0001 C CNN
	1    9300 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	9675 4300 9675 4175
Wire Wire Line
	9675 3475 9675 3625
Wire Wire Line
	9675 3825 9675 3975
$Comp
L Device:LED_Small D?
U 1 1 5F981CF2
P 9675 3725
AR Path="/5F1D940F/5F981CF2" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F981CF2" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F981CF2" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F981CF2" Ref="D?"  Part="1" 
AR Path="/5F89ACEF/5F7C0AF0/5F981CF2" Ref="D?"  Part="1" 
AR Path="/5F8AB3A0/5F981CF2" Ref="D22"  Part="1" 
AR Path="/5F8F350D/5F981CF2" Ref="D?"  Part="1" 
F 0 "D22" V 9725 3625 39  0000 R CNN
F 1 "C513A-WSS-CW0Z0151" V 9675 3625 31  0001 R CNN
F 2 "" V 9675 3725 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/90/C513A_WSN_WSS_MSN_MSS_1042-268537.pdf" V 9675 3725 50  0001 C CNN
	1    9675 3725
	0    1    -1   0   
$EndComp
Wire Wire Line
	8175 3475 8925 3475
Wire Wire Line
	9675 4300 9300 4300
Connection ~ 9300 4300
Wire Wire Line
	9300 4300 8925 4300
Connection ~ 9300 3475
Wire Wire Line
	9300 3475 9675 3475
Connection ~ 8925 3475
Wire Wire Line
	8925 3475 9300 3475
Wire Wire Line
	8175 4300 8175 4175
Wire Wire Line
	8175 3825 8175 3975
Wire Wire Line
	7775 4300 7775 4175
Wire Wire Line
	7775 3825 7775 3975
Wire Wire Line
	7375 4300 7375 4175
Wire Wire Line
	7375 3825 7375 3975
Wire Wire Line
	6975 4300 6975 4175
Wire Wire Line
	6975 3825 6975 3975
Wire Wire Line
	6575 4300 6575 4175
Wire Wire Line
	6575 3825 6575 3975
Wire Wire Line
	6175 4300 6175 4175
Wire Wire Line
	6175 3825 6175 3975
Wire Wire Line
	5775 4300 5775 4175
Wire Wire Line
	5775 3825 5775 3975
Wire Wire Line
	5375 4300 5375 4175
Wire Wire Line
	5375 3825 5375 3975
Wire Wire Line
	4975 4300 4975 4175
Wire Wire Line
	4975 3825 4975 3975
Wire Wire Line
	4575 4300 4575 4175
Wire Wire Line
	4575 3825 4575 3975
Wire Wire Line
	4175 3825 4175 3975
Wire Wire Line
	3775 3825 3775 3975
$Comp
L Device:R_Small R?
U 1 1 5F9A32D0
P 3775 4075
AR Path="/5F9A32D0" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F9A32D0" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F9A32D0" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F9A32D0" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F9A32D0" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F9A32D0" Ref="R21"  Part="1" 
AR Path="/5F8F350D/5F9A32D0" Ref="R?"  Part="1" 
F 0 "R21" H 3825 4025 39  0000 L CNN
F 1 "60" H 3825 4075 39  0000 L CNN
F 2 "" H 3775 4075 50  0001 C CNN
F 3 "~" H 3775 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 3775 4075 50  0001 C CNN "Mfr"
	1    3775 4075
	-1   0    0    1   
$EndComp
Wire Wire Line
	3775 4175 3775 4300
Wire Wire Line
	8175 4300 7775 4300
Connection ~ 4175 4300
Wire Wire Line
	4175 4300 3775 4300
Connection ~ 4575 4300
Wire Wire Line
	4575 4300 4175 4300
Connection ~ 4975 4300
Wire Wire Line
	4975 4300 4575 4300
Connection ~ 5375 4300
Wire Wire Line
	5375 4300 4975 4300
Connection ~ 5775 4300
Wire Wire Line
	5775 4300 5375 4300
Connection ~ 6175 4300
Wire Wire Line
	6175 4300 5775 4300
Connection ~ 6575 4300
Wire Wire Line
	6575 4300 6175 4300
Connection ~ 6975 4300
Wire Wire Line
	6975 4300 6575 4300
Connection ~ 7375 4300
Wire Wire Line
	7375 4300 6975 4300
Connection ~ 7775 4300
Wire Wire Line
	7775 4300 7375 4300
Connection ~ 3775 4300
Wire Wire Line
	3775 4300 3775 4350
Wire Wire Line
	3775 4300 3600 4300
Connection ~ 8925 4300
$Comp
L Device:R_Small R?
U 1 1 5F9C33B8
P 9300 4075
AR Path="/5F9C33B8" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F9C33B8" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F9C33B8" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F9C33B8" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F9C33B8" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F9C33B8" Ref="R35"  Part="1" 
AR Path="/5F8F350D/5F9C33B8" Ref="R?"  Part="1" 
F 0 "R35" H 9350 4025 39  0000 L CNN
F 1 "5" H 9350 4075 39  0000 L CNN
F 2 "" H 9300 4075 50  0001 C CNN
F 3 "~" H 9300 4075 50  0001 C CNN
F 4 "RN55D5R00FB14" H 9300 4075 50  0001 C CNN "Mfr"
	1    9300 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F9C367D
P 9675 4075
AR Path="/5F9C367D" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F9C367D" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F9C367D" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F9C367D" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F9C367D" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F9C367D" Ref="R36"  Part="1" 
AR Path="/5F8F350D/5F9C367D" Ref="R?"  Part="1" 
F 0 "R36" H 9725 4025 39  0000 L CNN
F 1 "5" H 9725 4075 39  0000 L CNN
F 2 "" H 9675 4075 50  0001 C CNN
F 3 "~" H 9675 4075 50  0001 C CNN
F 4 "RN55D5R00FB14" H 9675 4075 50  0001 C CNN "Mfr"
	1    9675 4075
	-1   0    0    1   
$EndComp
Wire Wire Line
	8925 4900 8925 4750
Wire Wire Line
	8525 4900 8525 4850
$Comp
L power:GND #PWR?
U 1 1 5F9DA69A
P 8525 4900
AR Path="/5F1D940F/5F9DA69A" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F9DA69A" Ref="#PWR?"  Part="1" 
AR Path="/5F9DA69A" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F9DA69A" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F9DA69A" Ref="#PWR?"  Part="1" 
AR Path="/5F8AB3A0/5F9DA69A" Ref="#PWR057"  Part="1" 
AR Path="/5F8F350D/5F9DA69A" Ref="#PWR?"  Part="1" 
F 0 "#PWR057" H 8525 4650 50  0001 C CNN
F 1 "GND" H 8530 4727 50  0000 C CNN
F 2 "" H 8525 4900 50  0001 C CNN
F 3 "" H 8525 4900 50  0001 C CNN
	1    8525 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F9DA6A0
P 8925 4900
AR Path="/5F1D940F/5F9DA6A0" Ref="#PWR?"  Part="1" 
AR Path="/5F1D49F5/5F9DA6A0" Ref="#PWR?"  Part="1" 
AR Path="/5F9DA6A0" Ref="#PWR?"  Part="1" 
AR Path="/5F202C6C/5F9DA6A0" Ref="#PWR?"  Part="1" 
AR Path="/5F89ACEF/5F9DA6A0" Ref="#PWR?"  Part="1" 
AR Path="/5F8AB3A0/5F9DA6A0" Ref="#PWR058"  Part="1" 
AR Path="/5F8F350D/5F9DA6A0" Ref="#PWR?"  Part="1" 
F 0 "#PWR058" H 8925 4650 50  0001 C CNN
F 1 "GND" H 8930 4727 50  0000 C CNN
F 2 "" H 8925 4900 50  0001 C CNN
F 3 "" H 8925 4900 50  0001 C CNN
	1    8925 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F9DA6A6
P 8525 4750
AR Path="/5F9DA6A6" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F9DA6A6" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F9DA6A6" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F9DA6A6" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F9DA6A6" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F9DA6A6" Ref="R33"  Part="1" 
AR Path="/5F8F350D/5F9DA6A6" Ref="R?"  Part="1" 
F 0 "R33" H 8575 4700 39  0000 L CNN
F 1 "10k" H 8575 4750 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8525 4750 50  0001 C CNN
F 3 "~" H 8525 4750 50  0001 C CNN
	1    8525 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5F9DA6AC
P 8825 4550
AR Path="/5F1D940F/5F9DA6AC" Ref="Q?"  Part="1" 
AR Path="/5F1D49F5/5F9DA6AC" Ref="Q?"  Part="1" 
AR Path="/5F9DA6AC" Ref="Q?"  Part="1" 
AR Path="/5F202C6C/5F9DA6AC" Ref="Q?"  Part="1" 
AR Path="/5F89ACEF/5F9DA6AC" Ref="Q?"  Part="1" 
AR Path="/5F8AB3A0/5F9DA6AC" Ref="Q9"  Part="1" 
AR Path="/5F8F350D/5F9DA6AC" Ref="Q?"  Part="1" 
F 0 "Q9" H 9030 4596 39  0000 L CNN
F 1 "Si2302CDS" H 9030 4505 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9025 4650 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 8825 4550 50  0001 C CNN
	1    8825 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8525 4550 8525 4650
Wire Wire Line
	8525 4550 8625 4550
Wire Wire Line
	8525 4550 8425 4550
Connection ~ 8525 4550
Text HLabel 8425 4550 0    39   Input ~ 0
INT
Wire Wire Line
	8925 4300 8925 4350
Text HLabel 8750 4300 0    39   Input ~ 0
LAMP_INT
Wire Wire Line
	8925 4300 8750 4300
Wire Wire Line
	4175 4300 4175 4175
$Comp
L Device:R_Small R?
U 1 1 5F8E514F
P 4175 4075
AR Path="/5F8E514F" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E514F" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E514F" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E514F" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E514F" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8E514F" Ref="R22"  Part="1" 
AR Path="/5F8F350D/5F8E514F" Ref="R?"  Part="1" 
F 0 "R22" H 4225 4025 39  0000 L CNN
F 1 "60" H 4225 4075 39  0000 L CNN
F 2 "" H 4175 4075 50  0001 C CNN
F 3 "~" H 4175 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 4175 4075 50  0001 C CNN "Mfr"
	1    4175 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E56C0
P 4575 4075
AR Path="/5F8E56C0" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E56C0" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E56C0" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E56C0" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E56C0" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8E56C0" Ref="R23"  Part="1" 
AR Path="/5F8F350D/5F8E56C0" Ref="R?"  Part="1" 
F 0 "R23" H 4625 4025 39  0000 L CNN
F 1 "60" H 4625 4075 39  0000 L CNN
F 2 "" H 4575 4075 50  0001 C CNN
F 3 "~" H 4575 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 4575 4075 50  0001 C CNN "Mfr"
	1    4575 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E5C7B
P 4975 4075
AR Path="/5F8E5C7B" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E5C7B" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E5C7B" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E5C7B" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E5C7B" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8E5C7B" Ref="R24"  Part="1" 
AR Path="/5F8F350D/5F8E5C7B" Ref="R?"  Part="1" 
F 0 "R24" H 5025 4025 39  0000 L CNN
F 1 "60" H 5025 4075 39  0000 L CNN
F 2 "" H 4975 4075 50  0001 C CNN
F 3 "~" H 4975 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 4975 4075 50  0001 C CNN "Mfr"
	1    4975 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E8D6E
P 5375 4075
AR Path="/5F8E8D6E" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E8D6E" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E8D6E" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E8D6E" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E8D6E" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8E8D6E" Ref="R25"  Part="1" 
AR Path="/5F8F350D/5F8E8D6E" Ref="R?"  Part="1" 
F 0 "R25" H 5425 4025 39  0000 L CNN
F 1 "60" H 5425 4075 39  0000 L CNN
F 2 "" H 5375 4075 50  0001 C CNN
F 3 "~" H 5375 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 5375 4075 50  0001 C CNN "Mfr"
	1    5375 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E8D75
P 5775 4075
AR Path="/5F8E8D75" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E8D75" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E8D75" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E8D75" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E8D75" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8E8D75" Ref="R26"  Part="1" 
AR Path="/5F8F350D/5F8E8D75" Ref="R?"  Part="1" 
F 0 "R26" H 5825 4025 39  0000 L CNN
F 1 "60" H 5825 4075 39  0000 L CNN
F 2 "" H 5775 4075 50  0001 C CNN
F 3 "~" H 5775 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 5775 4075 50  0001 C CNN "Mfr"
	1    5775 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E8D7C
P 6175 4075
AR Path="/5F8E8D7C" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E8D7C" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E8D7C" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E8D7C" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E8D7C" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8E8D7C" Ref="R27"  Part="1" 
AR Path="/5F8F350D/5F8E8D7C" Ref="R?"  Part="1" 
F 0 "R27" H 6225 4025 39  0000 L CNN
F 1 "60" H 6225 4075 39  0000 L CNN
F 2 "" H 6175 4075 50  0001 C CNN
F 3 "~" H 6175 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 6175 4075 50  0001 C CNN "Mfr"
	1    6175 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8E8D83
P 6575 4075
AR Path="/5F8E8D83" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8E8D83" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8E8D83" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8E8D83" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8E8D83" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8E8D83" Ref="R28"  Part="1" 
AR Path="/5F8F350D/5F8E8D83" Ref="R?"  Part="1" 
F 0 "R28" H 6625 4025 39  0000 L CNN
F 1 "60" H 6625 4075 39  0000 L CNN
F 2 "" H 6575 4075 50  0001 C CNN
F 3 "~" H 6575 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 6575 4075 50  0001 C CNN "Mfr"
	1    6575 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8EBC7F
P 6975 4075
AR Path="/5F8EBC7F" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8EBC7F" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8EBC7F" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8EBC7F" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8EBC7F" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8EBC7F" Ref="R29"  Part="1" 
AR Path="/5F8F350D/5F8EBC7F" Ref="R?"  Part="1" 
F 0 "R29" H 7025 4025 39  0000 L CNN
F 1 "60" H 7025 4075 39  0000 L CNN
F 2 "" H 6975 4075 50  0001 C CNN
F 3 "~" H 6975 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 6975 4075 50  0001 C CNN "Mfr"
	1    6975 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8EBC86
P 7375 4075
AR Path="/5F8EBC86" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8EBC86" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8EBC86" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8EBC86" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8EBC86" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8EBC86" Ref="R30"  Part="1" 
AR Path="/5F8F350D/5F8EBC86" Ref="R?"  Part="1" 
F 0 "R30" H 7425 4025 39  0000 L CNN
F 1 "60" H 7425 4075 39  0000 L CNN
F 2 "" H 7375 4075 50  0001 C CNN
F 3 "~" H 7375 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 7375 4075 50  0001 C CNN "Mfr"
	1    7375 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8EBC8D
P 7775 4075
AR Path="/5F8EBC8D" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8EBC8D" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8EBC8D" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8EBC8D" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8EBC8D" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8EBC8D" Ref="R31"  Part="1" 
AR Path="/5F8F350D/5F8EBC8D" Ref="R?"  Part="1" 
F 0 "R31" H 7825 4025 39  0000 L CNN
F 1 "60" H 7825 4075 39  0000 L CNN
F 2 "" H 7775 4075 50  0001 C CNN
F 3 "~" H 7775 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 7775 4075 50  0001 C CNN "Mfr"
	1    7775 4075
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F8EBC94
P 8175 4075
AR Path="/5F8EBC94" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F8EBC94" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F8EBC94" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F8EBC94" Ref="R?"  Part="1" 
AR Path="/5F89ACEF/5F8EBC94" Ref="R?"  Part="1" 
AR Path="/5F8AB3A0/5F8EBC94" Ref="R32"  Part="1" 
AR Path="/5F8F350D/5F8EBC94" Ref="R?"  Part="1" 
F 0 "R32" H 8225 4025 39  0000 L CNN
F 1 "60" H 8225 4075 39  0000 L CNN
F 2 "" H 8175 4075 50  0001 C CNN
F 3 "~" H 8175 4075 50  0001 C CNN
F 4 "RN50C57R6FB14 " H 8175 4075 50  0001 C CNN "Mfr"
	1    8175 4075
	-1   0    0    1   
$EndComp
$EndSCHEMATC
