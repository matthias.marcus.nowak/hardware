EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title "Power Plant"
Date "2020-10-02"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR07
U 1 1 5E680CF4
P 3600 5600
F 0 "#PWR07" H 3600 5350 50  0001 C CNN
F 1 "GND" H 3605 5427 50  0000 C CNN
F 2 "" H 3600 5600 50  0001 C CNN
F 3 "" H 3600 5600 50  0001 C CNN
	1    3600 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C3
U 1 1 5E8CA2CF
P 3600 2600
F 0 "C3" H 3750 2550 39  0000 R CNN
F 1 "10u" H 3750 2650 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3600 2600 50  0001 C CNN
F 3 "~" H 3600 2600 50  0001 C CNN
	1    3600 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5E8CB08F
P 3350 2600
F 0 "C2" H 3300 2550 39  0000 R CNN
F 1 "100n" H 3350 2650 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 2600 50  0001 C CNN
F 3 "~" H 3350 2600 50  0001 C CNN
	1    3350 2600
	1    0    0    1   
$EndComp
Wire Wire Line
	3600 2700 3600 2800
Wire Wire Line
	3350 2700 3350 2800
$Comp
L power:GND #PWR05
U 1 1 5E8F3CB2
P 3350 2500
F 0 "#PWR05" H 3350 2250 50  0001 C CNN
F 1 "GND" H 3355 2327 39  0000 C CNN
F 2 "" H 3350 2500 50  0001 C CNN
F 3 "" H 3350 2500 50  0001 C CNN
	1    3350 2500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5E8F4451
P 3600 2500
F 0 "#PWR06" H 3600 2250 50  0001 C CNN
F 1 "GND" H 3605 2327 39  0000 C CNN
F 2 "" H 3600 2500 50  0001 C CNN
F 3 "" H 3600 2500 50  0001 C CNN
	1    3600 2500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E8F840A
P 2500 3400
F 0 "#PWR03" H 2500 3150 50  0001 C CNN
F 1 "GND" H 2505 3227 39  0000 C CNN
F 2 "" H 2500 3400 50  0001 C CNN
F 3 "" H 2500 3400 50  0001 C CNN
	1    2500 3400
	1    0    0    -1  
$EndComp
Connection ~ 2500 3000
$Comp
L Device:C_Small C1
U 1 1 5E8F73C1
P 2500 3300
F 0 "C1" H 2450 3250 39  0000 R CNN
F 1 "100n" H 2500 3350 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2500 3300 50  0001 C CNN
F 3 "~" H 2500 3300 50  0001 C CNN
	1    2500 3300
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5E8F6D03
P 2500 2750
F 0 "R1" H 2550 2700 39  0000 L CNN
F 1 "10k" H 2550 2750 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2500 2750 50  0001 C CNN
F 3 "~" H 2500 2750 50  0001 C CNN
	1    2500 2750
	-1   0    0    1   
$EndComp
Text Notes 4650 3000 0    28   ~ 0
UP during boot
Text Notes 4650 3600 0    28   ~ 0
DOWN during boot
Wire Wire Line
	4200 3100 4300 3100
Wire Wire Line
	4200 3300 4300 3300
Text Notes 4650 3200 0    28   ~ 0
DOWN during prog
Text Notes 4650 3900 0    28   ~ 0
UP during boot for log
Text Notes 2950 4750 1    28   ~ 0
Internally connected to the flash
Text Notes 4650 3500 0    28   ~ 0
UP during boot
Wire Wire Line
	4200 3000 4300 3000
Text Notes 4200 3000 0    39   ~ 0
IO0
Wire Wire Line
	2500 3000 2500 3200
Text Notes 4750 5200 2    28   ~ 0
only input
Text Notes 4450 5300 2    28   ~ 0
only input
Text Notes 2950 3200 2    28   ~ 0
only input
Text Notes 2950 3300 2    28   ~ 0
only input
Wire Wire Line
	2500 3000 3000 3000
Wire Wire Line
	3100 2500 3100 2800
Wire Wire Line
	2500 2500 2500 2650
Wire Wire Line
	2500 2850 2500 3000
Text Label 4300 4200 0    39   ~ 0
~CS_I
Wire Wire Line
	4300 3800 4200 3800
Wire Wire Line
	4300 3700 4200 3700
Text Label 4300 4300 0    39   ~ 0
~CS_V
Text Label 4300 3300 0    39   ~ 0
RXD
Text Label 4300 3100 0    39   ~ 0
TXD
Text Label 2200 3000 2    39   ~ 0
EN
Wire Wire Line
	7050 2250 6900 2250
Wire Wire Line
	7050 2350 6900 2350
Wire Wire Line
	7050 2450 6900 2450
Text Label 6900 2250 2    39   ~ 0
EN
Text Label 6900 2350 2    39   ~ 0
TXD
Text Label 6900 2450 2    39   ~ 0
RXD
Text Label 4300 3000 0    39   ~ 0
ID
Text Label 6900 2550 2    39   ~ 0
ID
$Comp
L RF_Module:ESP32-WROOM-32D U1
U 1 1 5ED8F80E
P 3600 4200
F 0 "U1" H 3050 4700 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 2700 4600 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 3600 2700 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 3300 4250 50  0001 C CNN
	1    3600 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2800 3350 2800
Connection ~ 3600 2800
Connection ~ 3350 2800
Wire Wire Line
	3350 2800 3100 2800
NoConn ~ 4200 5300
NoConn ~ 3000 3300
NoConn ~ 3000 3200
NoConn ~ 3000 4200
NoConn ~ 3000 4300
NoConn ~ 3000 4400
NoConn ~ 3000 4500
NoConn ~ 3000 4600
NoConn ~ 3000 4700
Wire Wire Line
	4200 4400 4300 4400
Wire Wire Line
	4200 4300 4300 4300
Wire Wire Line
	4200 4100 4300 4100
Wire Wire Line
	4200 5200 4300 5200
Wire Wire Line
	7050 3950 6950 3950
Text Label 7700 3800 0    39   ~ 0
~IRQ
Wire Wire Line
	4200 4900 4300 4900
$Comp
L power:+3.3V #PWR?
U 1 1 5F0E8348
P 3100 2500
AR Path="/5ECB7EC7/5F0E8348" Ref="#PWR?"  Part="1" 
AR Path="/5F0E8348" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 3100 2350 50  0001 C CNN
F 1 "+3.3V" H 3115 2673 39  0000 C CNN
F 2 "" H 3100 2500 50  0001 C CNN
F 3 "" H 3100 2500 50  0001 C CNN
	1    3100 2500
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F0EA483
P 2500 2500
AR Path="/5ECB7EC7/5F0EA483" Ref="#PWR?"  Part="1" 
AR Path="/5F0EA483" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 2500 2350 50  0001 C CNN
F 1 "+3.3V" H 2515 2673 39  0000 C CNN
F 2 "" H 2500 2500 50  0001 C CNN
F 3 "" H 2500 2500 50  0001 C CNN
	1    2500 2500
	-1   0    0    -1  
$EndComp
Text GLabel 4300 3400 2    39   Input ~ 0
MOSI
Text GLabel 4300 4100 2    39   Input ~ 0
SDA
Wire Wire Line
	7050 3800 6950 3800
Wire Wire Line
	6950 2650 7050 2650
Wire Wire Line
	6950 2550 7050 2550
Wire Wire Line
	6900 2550 6950 2550
Connection ~ 6950 2550
Wire Wire Line
	6950 2650 6950 2550
Text Notes 2300 4750 0    39   ~ 0
Touch GPIO:\nT0 - IO4\nT1 - IO0\nT2 - IO2\nT3 - IO15\nT4 - IO13\nT5 - IO12\nT6 - IO14\nT7 - IO27\nT8 - IO33\nT9 - IO32
Text Notes 4200 4700 0    28   ~ 0
DAC
Text Notes 4200 4800 0    28   ~ 0
DAC
Wire Wire Line
	4300 3600 4200 3600
Wire Wire Line
	4300 4600 4200 4600
Text Label 4300 3800 0    39   ~ 0
Slider_3
Text Label 4300 4900 0    39   ~ 0
Slider_4
$Comp
L LEGOS:TouchButton U2
U 1 1 5F16C460
P 9195 3595
F 0 "U2" H 9446 3236 50  0000 L CNN
F 1 "TouchButton" H 9446 3145 50  0000 L CNN
F 2 "LEGOS:Touch_Button_Square" H 9750 3335 50  0001 C CNN
F 3 "" H 9195 3595 50  0001 C CNN
	1    9195 3595
	1    0    0    -1  
$EndComp
Text Label 9000 4000 2    39   ~ 0
FUNC
$Comp
L Switch:SW_Push SW1
U 1 1 5F51958E
P 2050 3200
F 0 "SW1" V 2000 3000 39  0000 L CNN
F 1 "PTS815" V 2100 2900 39  0000 L CNN
F 2 "LEGOS:PTS815" H 2050 3400 50  0001 C CNN
F 3 "~" H 2050 3400 50  0001 C CNN
	1    2050 3200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5F51A2D4
P 2050 3400
F 0 "#PWR01" H 2050 3150 50  0001 C CNN
F 1 "GND" H 2055 3227 39  0000 C CNN
F 2 "" H 2050 3400 50  0001 C CNN
F 3 "" H 2050 3400 50  0001 C CNN
	1    2050 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3000 2500 3000
$Sheet
S 7050 2150 550  600 
U 5ED6690B
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "EN" O L 7050 2250 39 
F3 "RXD" I L 7050 2450 39 
F4 "TXD" O L 7050 2350 39 
F5 "IO0" B L 7050 2550 39 
F6 "ID" B L 7050 2650 39 
$EndSheet
$Sheet
S 7050 3050 550  350 
U 5F524582
F0 "Auto Power" 39
F1 "sub_power_auto.sch" 39
F2 "VOUT" I L 7050 3300 39 
F3 "VIN" I L 7050 3150 39 
$EndSheet
$Sheet
S 7050 3700 550  750 
U 5ECB7EC7
F0 "Master Power" 39
F1 "sub_power_master.sch" 39
F2 "~IRQ" I R 7600 3800 39 
F3 "VOUT" I L 7050 3950 39 
F4 "VIN" I L 7050 3800 39 
F5 "~CS_V" I R 7600 3950 39 
F6 "~CS_I" I R 7600 4100 39 
F7 "TEMP" O L 7050 4200 39 
F8 "MON" O L 7050 4350 39 
$EndSheet
Wire Wire Line
	7700 3800 7600 3800
$Comp
L LEGOS:VBUS #PWR?
U 1 1 5F5322D3
P 6950 3950
AR Path="/5ED6690B/5F5322D3" Ref="#PWR?"  Part="1" 
AR Path="/5F5322D3" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 6950 3800 50  0001 C CNN
F 1 "VBUS" V 6950 4100 39  0000 L CNN
F 2 "" H 6950 3950 50  0001 C CNN
F 3 "" H 6950 3950 50  0001 C CNN
	1    6950 3950
	0    -1   1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F5322D9
P 6950 3150
AR Path="/5ED6690B/5F5322D9" Ref="#PWR?"  Part="1" 
AR Path="/5F5322D9" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 6950 3000 50  0001 C CNN
F 1 "+5V" V 6950 3350 39  0000 C CNN
F 2 "" H 6950 3150 50  0001 C CNN
F 3 "" H 6950 3150 50  0001 C CNN
	1    6950 3150
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F5335BD
P 6950 3300
AR Path="/5ECB7EC7/5F5335BD" Ref="#PWR?"  Part="1" 
AR Path="/5F5335BD" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 6950 3150 50  0001 C CNN
F 1 "+3.3V" V 6950 3500 39  0000 C CNN
F 2 "" H 6950 3300 50  0001 C CNN
F 3 "" H 6950 3300 50  0001 C CNN
	1    6950 3300
	0    -1   1    0   
$EndComp
Wire Wire Line
	6950 3150 7050 3150
Wire Wire Line
	6950 3300 7050 3300
$Comp
L power:+5V #PWR?
U 1 1 5F538A42
P 6950 3800
AR Path="/5ED6690B/5F538A42" Ref="#PWR?"  Part="1" 
AR Path="/5F538A42" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 6950 3650 50  0001 C CNN
F 1 "+5V" V 6950 4000 39  0000 C CNN
F 2 "" H 6950 3800 50  0001 C CNN
F 3 "" H 6950 3800 50  0001 C CNN
	1    6950 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7700 3950 7600 3950
Wire Wire Line
	7700 4100 7600 4100
Text Label 7700 3950 0    39   ~ 0
~CS_V
Text Label 7700 4100 0    39   ~ 0
~CS_I
Wire Wire Line
	6950 4350 7050 4350
$Comp
L LEGOS:TouchSlider U?
U 1 1 5F5CFF55
P 9480 2865
AR Path="/5F249020/5F5CFF55" Ref="U?"  Part="1" 
AR Path="/5F5CFF55" Ref="U3"  Part="1" 
F 0 "U3" V 10130 2565 50  0000 L CNN
F 1 "TouchSlider" V 10030 2415 50  0000 L CNN
F 2 "LEGOS:Touch_Slider" H 10310 2635 50  0001 C CNN
F 3 "" H 9475 2805 50  0001 C CNN
	1    9480 2865
	0    1    -1   0   
$EndComp
Wire Wire Line
	8900 2495 9000 2495
Wire Wire Line
	8900 2655 9000 2655
Wire Wire Line
	8900 2815 9000 2815
Wire Wire Line
	8900 2975 9000 2975
Wire Wire Line
	8900 3135 9000 3135
Wire Wire Line
	8900 3295 9000 3295
Text Label 8900 2495 2    39   ~ 0
Slider_1
Text Label 8900 2655 2    39   ~ 0
Slider_2
Text Label 8900 2815 2    39   ~ 0
Slider_3
Text Label 8900 2975 2    39   ~ 0
Slider_4
Text Label 8900 3135 2    39   ~ 0
Slider_5
Text Label 8900 3295 2    39   ~ 0
Slider_6
$Sheet
S 7050 4750 550  650 
U 5F51B236
F0 "Monitor" 39
F1 "sub_monitor.sch" 39
F2 "MON" I L 7050 4850 39 
F3 "TEMP" I L 7050 5000 39 
F4 "PIEZO" I L 7050 5150 39 
F5 "ALTERNATOR" I L 7050 5300 39 
$EndSheet
Wire Wire Line
	6950 4350 6950 4850
Wire Wire Line
	6950 4850 7050 4850
Wire Wire Line
	6900 4200 6900 5000
Wire Wire Line
	6900 5000 7050 5000
Wire Wire Line
	6900 4200 7050 4200
Text Label 4300 3600 0    39   ~ 0
Slider_2
Text Label 4300 3900 0    39   ~ 0
Slider_1
Wire Wire Line
	6950 5150 7050 5150
Text Label 4300 4600 0    39   ~ 0
SMOKE
Text Label 6950 5150 2    39   ~ 0
SMOKE
Wire Wire Line
	6950 5300 7050 5300
Text Label 4300 3200 0    39   ~ 0
TURBO
Text Label 6950 5300 2    39   ~ 0
TURBO
Text Label 4300 3700 0    39   ~ 0
FUNC
Text Label 4300 4000 0    39   ~ 0
~IRQ
Wire Wire Line
	4200 5100 4300 5100
Text Label 4300 5100 0    39   ~ 0
Slider_5
Wire Wire Line
	4200 5000 4300 5000
Text Label 4300 5000 0    39   ~ 0
Slider_6
$Sheet
S 7050 1500 550  350 
U 5F59E278
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
Wire Wire Line
	4200 4200 4300 4200
Text GLabel 4300 3500 2    39   Input ~ 0
SCL
Wire Wire Line
	4300 4000 4200 4000
Text GLabel 4300 4400 2    39   Input ~ 0
CLK
Wire Wire Line
	4300 3900 4200 3900
Wire Wire Line
	4300 3200 4200 3200
Wire Wire Line
	4300 3400 4200 3400
Wire Wire Line
	4300 3500 4200 3500
Text Label 4300 5200 0    39   ~ 0
TEMP
Text Label 6900 5000 2    39   ~ 0
TEMP
$EndSCHEMATC
