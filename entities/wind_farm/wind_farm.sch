EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title "Wind Farm"
Date "2020-10-03"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2050 5500 0    50   ~ 0
https://dl.espressif.com/dl/schematics/esp32-pico-kit-v4.1_schematic.pdf
$Comp
L power:GND #PWR?
U 1 1 5EFD965F
P 3850 5100
AR Path="/5EF40FCD/5EFD965F" Ref="#PWR?"  Part="1" 
AR Path="/5EFD965F" Ref="#PWR07"  Part="1" 
AR Path="/5EFB83EE/5EFD965F" Ref="#PWR?"  Part="1" 
F 0 "#PWR07" H 3850 4850 50  0001 C CNN
F 1 "GND" H 3855 4927 50  0000 C CNN
F 2 "" H 3850 5100 50  0001 C CNN
F 3 "" H 3850 5100 50  0001 C CNN
	1    3850 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5EFD9665
P 3850 2100
AR Path="/5EF40FCD/5EFD9665" Ref="C?"  Part="1" 
AR Path="/5EFD9665" Ref="C3"  Part="1" 
AR Path="/5EFB83EE/5EFD9665" Ref="C?"  Part="1" 
F 0 "C3" H 4000 2050 39  0000 R CNN
F 1 "10u" H 4000 2150 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3850 2100 50  0001 C CNN
F 3 "~" H 3850 2100 50  0001 C CNN
	1    3850 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFD966B
P 3600 2100
AR Path="/5EF40FCD/5EFD966B" Ref="C?"  Part="1" 
AR Path="/5EFD966B" Ref="C2"  Part="1" 
AR Path="/5EFB83EE/5EFD966B" Ref="C?"  Part="1" 
F 0 "C2" H 3550 2050 39  0000 R CNN
F 1 "100n" H 3600 2150 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3600 2100 50  0001 C CNN
F 3 "~" H 3600 2100 50  0001 C CNN
	1    3600 2100
	1    0    0    1   
$EndComp
Wire Wire Line
	3850 2200 3850 2300
Wire Wire Line
	3600 2200 3600 2300
$Comp
L power:GND #PWR?
U 1 1 5EFD9673
P 3600 2000
AR Path="/5EF40FCD/5EFD9673" Ref="#PWR?"  Part="1" 
AR Path="/5EFD9673" Ref="#PWR05"  Part="1" 
AR Path="/5EFB83EE/5EFD9673" Ref="#PWR?"  Part="1" 
F 0 "#PWR05" H 3600 1750 50  0001 C CNN
F 1 "GND" H 3605 1827 39  0000 C CNN
F 2 "" H 3600 2000 50  0001 C CNN
F 3 "" H 3600 2000 50  0001 C CNN
	1    3600 2000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFD9679
P 3850 2000
AR Path="/5EF40FCD/5EFD9679" Ref="#PWR?"  Part="1" 
AR Path="/5EFD9679" Ref="#PWR06"  Part="1" 
AR Path="/5EFB83EE/5EFD9679" Ref="#PWR?"  Part="1" 
F 0 "#PWR06" H 3850 1750 50  0001 C CNN
F 1 "GND" H 3855 1827 39  0000 C CNN
F 2 "" H 3850 2000 50  0001 C CNN
F 3 "" H 3850 2000 50  0001 C CNN
	1    3850 2000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFD967F
P 2750 2900
AR Path="/5EF40FCD/5EFD967F" Ref="#PWR?"  Part="1" 
AR Path="/5EFD967F" Ref="#PWR03"  Part="1" 
AR Path="/5EFB83EE/5EFD967F" Ref="#PWR?"  Part="1" 
F 0 "#PWR03" H 2750 2650 50  0001 C CNN
F 1 "GND" H 2755 2727 39  0000 C CNN
F 2 "" H 2750 2900 50  0001 C CNN
F 3 "" H 2750 2900 50  0001 C CNN
	1    2750 2900
	1    0    0    -1  
$EndComp
Connection ~ 2750 2500
$Comp
L Device:C_Small C?
U 1 1 5EFD9686
P 2750 2800
AR Path="/5EF40FCD/5EFD9686" Ref="C?"  Part="1" 
AR Path="/5EFD9686" Ref="C1"  Part="1" 
AR Path="/5EFB83EE/5EFD9686" Ref="C?"  Part="1" 
F 0 "C1" H 2700 2750 39  0000 R CNN
F 1 "1u" H 2700 2850 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2750 2800 50  0001 C CNN
F 3 "~" H 2750 2800 50  0001 C CNN
	1    2750 2800
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EFD968C
P 2750 2250
AR Path="/5EF40FCD/5EFD968C" Ref="R?"  Part="1" 
AR Path="/5EFD968C" Ref="R1"  Part="1" 
AR Path="/5EFB83EE/5EFD968C" Ref="R?"  Part="1" 
F 0 "R1" H 2800 2200 39  0000 L CNN
F 1 "10k" H 2800 2250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2750 2250 50  0001 C CNN
F 3 "~" H 2750 2250 50  0001 C CNN
	1    2750 2250
	-1   0    0    1   
$EndComp
Text Notes 4900 2500 0    28   ~ 0
UP during boot
Text Notes 4900 3100 0    28   ~ 0
DOWN during boot
Text Notes 4900 2700 0    28   ~ 0
DOWN during prog
Text Notes 4900 3400 0    28   ~ 0
UP during boot for log
Text Notes 3200 4250 1    28   ~ 0
Internally connected to the flash
Text Notes 4900 3000 0    28   ~ 0
UP during boot
Text Notes 4450 2500 0    39   ~ 0
IO0
Wire Wire Line
	2750 2500 2750 2700
Text Notes 3200 2700 2    28   ~ 0
only input
Text Notes 3200 2800 2    28   ~ 0
only input
Wire Wire Line
	2750 2500 3250 2500
Wire Wire Line
	3350 2000 3350 2300
Wire Wire Line
	2750 2000 2750 2150
Wire Wire Line
	2750 2350 2750 2500
Text Label 4650 2800 0    39   ~ 0
RXD
Text Label 4650 2600 0    39   ~ 0
TXD
Text Label 2875 2500 0    39   ~ 0
EN
Text Label 4650 2500 0    39   ~ 0
ID
$Comp
L RF_Module:ESP32-WROOM-32D U?
U 1 1 5EFD96B3
P 3850 3700
AR Path="/5EF40FCD/5EFD96B3" Ref="U?"  Part="1" 
AR Path="/5EFD96B3" Ref="U1"  Part="1" 
AR Path="/5EFB83EE/5EFD96B3" Ref="U?"  Part="1" 
F 0 "U1" H 3300 4200 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 2950 4100 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 3850 2200 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 3550 3750 50  0001 C CNN
	1    3850 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2300 3600 2300
Connection ~ 3850 2300
Connection ~ 3600 2300
Wire Wire Line
	3600 2300 3350 2300
NoConn ~ 3250 2800
NoConn ~ 3250 2700
NoConn ~ 3250 3700
NoConn ~ 3250 3800
NoConn ~ 3250 3900
NoConn ~ 3250 4000
NoConn ~ 3250 4100
NoConn ~ 3250 4200
Wire Wire Line
	4450 4000 4650 4000
Wire Wire Line
	4450 3700 4650 3700
Wire Wire Line
	4450 3300 4650 3300
Wire Wire Line
	4450 3200 4650 3200
Wire Wire Line
	4450 2800 4650 2800
Wire Wire Line
	4450 2600 4650 2600
Wire Wire Line
	4450 2500 4650 2500
Wire Wire Line
	6900 3900 6800 3900
Text Label 7550 4100 0    39   ~ 0
~CS
Wire Wire Line
	4450 4600 4650 4600
Wire Wire Line
	4450 2700 4650 2700
Wire Wire Line
	9495 3305 9385 3305
Wire Wire Line
	4450 4200 4650 4200
Wire Wire Line
	4450 4300 4650 4300
Wire Wire Line
	4450 4100 4650 4100
Text Label 6800 2450 2    39   ~ 0
TXD
Text Label 6800 2550 2    39   ~ 0
RXD
Text Label 6800 2650 2    39   ~ 0
ID
Wire Wire Line
	6800 2650 6850 2650
Wire Wire Line
	6900 2550 6800 2550
Wire Wire Line
	6800 2450 6900 2450
Wire Wire Line
	6900 2750 6850 2750
Wire Wire Line
	6850 2750 6850 2650
Connection ~ 6850 2650
Wire Wire Line
	6850 2650 6900 2650
Text Label 6800 2350 2    39   ~ 0
EN
Wire Wire Line
	6800 2350 6900 2350
Wire Wire Line
	4650 3500 4450 3500
Wire Wire Line
	6800 4600 6900 4600
Wire Wire Line
	6800 4700 6900 4700
Wire Wire Line
	6800 4800 6900 4800
Text Notes 2500 4300 0    39   ~ 0
Touch GPIO:\nT0 - IO4\nT1 - IO0\nT2 - IO2\nT3 - IO15\nT4 - IO13\nT5 - IO12\nT6 - IO14\nT7 - IO27\nT8 - IO33\nT9 - IO32
Text Label 4650 4500 0    39   ~ 0
Slider_1
Text Label 4650 4600 0    39   ~ 0
Slider_2
Text Label 4650 4400 0    39   ~ 0
Slider_3
Text Label 4650 3300 0    39   ~ 0
Slider_4
Text Label 4650 3100 0    39   ~ 0
Slider_5
Text Label 4650 3200 0    39   ~ 0
Slider_6
Wire Wire Line
	4450 3400 4650 3400
Wire Wire Line
	4650 3100 4450 3100
Text Label 4650 4000 0    39   ~ 0
~CS
Wire Wire Line
	4650 3600 4450 3600
Wire Wire Line
	4450 3900 4650 3900
Wire Wire Line
	4650 2900 4450 2900
Text GLabel 4650 3800 2    39   Input ~ 0
SDA
Text GLabel 4650 3700 2    39   Input ~ 0
SCL
Text GLabel 4650 4100 2    39   Input ~ 0
MOSI
Text GLabel 4650 3900 2    39   Input ~ 0
CLK
Text Notes 4450 4800 0    28   ~ 0
only input
Text Notes 4450 4700 0    28   ~ 0
only input
Text Notes 4450 4200 0    28   ~ 0
DAC
Text Notes 4450 4300 0    28   ~ 0
DAC
$Sheet
S 6900 2250 550  600 
U 5F107976
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "EN" O L 6900 2350 39 
F3 "TXD" O L 6900 2450 39 
F4 "RXD" I L 6900 2550 39 
F5 "IO0" B L 6900 2650 39 
F6 "ID" B L 6900 2750 39 
$EndSheet
$Comp
L Switch:SW_Push SW1
U 1 1 5F3D1690
P 2350 2700
F 0 "SW1" V 2275 2950 39  0000 R CNN
F 1 "PTS815" V 2350 3050 39  0000 R CNN
F 2 "LEGOS:PTS815" H 2350 2900 50  0001 C CNN
F 3 "~" H 2350 2900 50  0001 C CNN
	1    2350 2700
	0    -1   1    0   
$EndComp
Wire Wire Line
	2350 2500 2750 2500
$Comp
L power:GND #PWR?
U 1 1 5F3D61DD
P 2350 2900
AR Path="/5EF40FCD/5F3D61DD" Ref="#PWR?"  Part="1" 
AR Path="/5F3D61DD" Ref="#PWR01"  Part="1" 
AR Path="/5EFB83EE/5F3D61DD" Ref="#PWR?"  Part="1" 
F 0 "#PWR01" H 2350 2650 50  0001 C CNN
F 1 "GND" H 2355 2727 39  0000 C CNN
F 2 "" H 2350 2900 50  0001 C CNN
F 3 "" H 2350 2900 50  0001 C CNN
	1    2350 2900
	1    0    0    -1  
$EndComp
$Sheet
S 6900 4500 550  600 
U 5EFF6F82
F0 "Wind Indicator" 39
F1 "sub_indicator.sch" 39
F2 "100%" I L 6900 5000 39 
F3 "80%" I L 6900 4900 39 
F4 "60%" I L 6900 4800 39 
F5 "40%" I L 6900 4700 39 
F6 "20%" I L 6900 4600 39 
$EndSheet
Text Label 9385 3305 2    39   ~ 0
Slider_6
Text Label 9385 3145 2    39   ~ 0
Slider_5
Text Label 9385 2985 2    39   ~ 0
Slider_4
Text Label 9385 2825 2    39   ~ 0
Slider_3
Text Label 9385 2665 2    39   ~ 0
Slider_2
Text Label 9385 2505 2    39   ~ 0
Slider_1
Wire Wire Line
	9495 3145 9385 3145
Wire Wire Line
	9495 2985 9385 2985
Wire Wire Line
	9495 2825 9385 2825
Wire Wire Line
	9495 2665 9385 2665
Wire Wire Line
	9495 2505 9385 2505
$Comp
L LEGOS:TouchSlider U2
U 1 1 5F027F29
P 9975 2875
F 0 "U2" V 10575 2575 50  0000 L CNN
F 1 "TouchSlider" V 10475 2425 50  0000 L CNN
F 2 "LEGOS:Touch_Slider_Round" H 10805 2645 50  0001 C CNN
F 3 "" H 9970 2815 50  0001 C CNN
	1    9975 2875
	0    1    -1   0   
$EndComp
Wire Wire Line
	6800 4900 6900 4900
Text Label 6800 4600 2    39   ~ 0
20%
Text Label 6800 4700 2    39   ~ 0
40%
Text Label 6800 4800 2    39   ~ 0
60%
Text Label 6800 4900 2    39   ~ 0
80%
Text Label 4650 4300 0    39   ~ 0
20%
Text Label 4650 4200 0    39   ~ 0
40%
Text Label 4650 3600 0    39   ~ 0
60%
Text Label 4650 3500 0    39   ~ 0
80%
Text Label 4650 2900 0    39   ~ 0
100%
Wire Wire Line
	4650 4400 4450 4400
Wire Wire Line
	4650 4500 4450 4500
Wire Wire Line
	4650 3800 4450 3800
Text Label 4650 2700 0    39   ~ 0
FAN
$Comp
L power:+3.3V #PWR02
U 1 1 5F46273E
P 2750 2000
F 0 "#PWR02" H 2750 1850 50  0001 C CNN
F 1 "+3.3V" H 2765 2173 39  0000 C CNN
F 2 "" H 2750 2000 50  0001 C CNN
F 3 "" H 2750 2000 50  0001 C CNN
	1    2750 2000
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 5F463C0C
P 3350 2000
F 0 "#PWR04" H 3350 1850 50  0001 C CNN
F 1 "+3.3V" H 3365 2173 39  0000 C CNN
F 2 "" H 3350 2000 50  0001 C CNN
F 3 "" H 3350 2000 50  0001 C CNN
	1    3350 2000
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q1
U 1 1 5F46DC84
P 9650 5000
F 0 "Q1" H 9854 5046 39  0000 L CNN
F 1 "Si2302CDS" H 9854 4955 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9850 5100 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2302cds-1764899.pdf" H 9650 5000 50  0001 C CNN
	1    9650 5000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR013
U 1 1 5F4720C3
P 9750 3850
F 0 "#PWR013" H 9750 3700 50  0001 C CNN
F 1 "+5V" H 9765 4015 39  0000 C CNN
F 2 "" H 9750 3850 50  0001 C CNN
F 3 "" H 9750 3850 50  0001 C CNN
	1    9750 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F4769C6
P 9750 5350
AR Path="/5EF40FCD/5F4769C6" Ref="#PWR?"  Part="1" 
AR Path="/5F4769C6" Ref="#PWR014"  Part="1" 
AR Path="/5EFB83EE/5F4769C6" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 9750 5100 50  0001 C CNN
F 1 "GND" H 9755 5177 39  0000 C CNN
F 2 "" H 9750 5350 50  0001 C CNN
F 3 "" H 9750 5350 50  0001 C CNN
	1    9750 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5200 9750 5350
Text Label 9150 5000 2    39   ~ 0
FAN
$Sheet
S 6900 3150 550  350 
U 5F4872E5
F0 "Auto Power" 39
F1 "sub_power_auto.sch" 39
F2 "VIN" I L 6900 3250 39 
F3 "VOUT" O L 6900 3400 39 
$EndSheet
$Comp
L power:+3.3V #PWR09
U 1 1 5F499837
P 6800 3400
F 0 "#PWR09" H 6800 3250 50  0001 C CNN
F 1 "+3.3V" V 6800 3600 39  0000 C CNN
F 2 "" H 6800 3400 50  0001 C CNN
F 3 "" H 6800 3400 50  0001 C CNN
	1    6800 3400
	0    -1   1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F49B682
P 6800 3250
AR Path="/5F107976/5F49B682" Ref="#PWR?"  Part="1" 
AR Path="/5F49B682" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 6800 3100 50  0001 C CNN
F 1 "+5V" V 6800 3450 39  0000 C CNN
F 2 "" H 6800 3250 50  0001 C CNN
F 3 "" H 6800 3250 50  0001 C CNN
	1    6800 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6900 3250 6800 3250
Wire Wire Line
	6900 3400 6800 3400
Wire Wire Line
	6800 4100 6900 4100
$Comp
L power:+5V #PWR?
U 1 1 5F4B753A
P 6800 3900
AR Path="/5F107976/5F4B753A" Ref="#PWR?"  Part="1" 
AR Path="/5F4B753A" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 6800 3750 50  0001 C CNN
F 1 "+5V" V 6800 4100 39  0000 C CNN
F 2 "" H 6800 3900 50  0001 C CNN
F 3 "" H 6800 3900 50  0001 C CNN
	1    6800 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 4100 7450 4100
Wire Wire Line
	7550 3900 7450 3900
Text Label 7550 3900 0    39   ~ 0
~IRQ
$Comp
L LEGOS:VBUS #PWR?
U 1 1 5F4FC2D4
P 6800 4100
AR Path="/5F107976/5F4FC2D4" Ref="#PWR?"  Part="1" 
AR Path="/5F4FC2D4" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 6800 3950 50  0001 C CNN
F 1 "VBUS" V 6800 4250 39  0000 L CNN
F 2 "" H 6800 4100 50  0001 C CNN
F 3 "" H 6800 4100 50  0001 C CNN
	1    6800 4100
	0    -1   1    0   
$EndComp
Wire Wire Line
	6800 5000 6900 5000
Text Label 6800 5000 2    39   ~ 0
100%
$Sheet
S 6900 3800 550  400 
U 5F011FA8
F0 "Wind Power" 39
F1 "sub_power_slave.sch" 39
F2 "~CS" I R 7450 4100 39 
F3 "VOUT" O L 6900 4100 39 
F4 "VIN" I L 6900 3900 39 
F5 "~IRQ" I R 7450 3900 39 
$EndSheet
$Comp
L Device:R_Small R?
U 1 1 5F52FA58
P 9300 5200
AR Path="/5EF40FCD/5F52FA58" Ref="R?"  Part="1" 
AR Path="/5F52FA58" Ref="R2"  Part="1" 
AR Path="/5EFB83EE/5F52FA58" Ref="R?"  Part="1" 
F 0 "R2" H 9350 5150 39  0000 L CNN
F 1 "10k" H 9350 5200 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9300 5200 50  0001 C CNN
F 3 "~" H 9300 5200 50  0001 C CNN
	1    9300 5200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F5301E2
P 9300 5350
AR Path="/5EF40FCD/5F5301E2" Ref="#PWR?"  Part="1" 
AR Path="/5F5301E2" Ref="#PWR012"  Part="1" 
AR Path="/5EFB83EE/5F5301E2" Ref="#PWR?"  Part="1" 
F 0 "#PWR012" H 9300 5100 50  0001 C CNN
F 1 "GND" H 9305 5177 39  0000 C CNN
F 2 "" H 9300 5350 50  0001 C CNN
F 3 "" H 9300 5350 50  0001 C CNN
	1    9300 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5300 9300 5350
Wire Wire Line
	9300 5100 9300 5000
Wire Wire Line
	9300 5000 9450 5000
Wire Wire Line
	9300 5000 9150 5000
Connection ~ 9300 5000
Wire Wire Line
	9450 4750 9750 4750
$Comp
L Motor:Motor_DC M1
U 1 1 5F5464B3
P 9750 4150
F 0 "M1" H 9908 4138 39  0000 L CNN
F 1 "Motor_DC" H 9908 4063 39  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical_SMD_Pin1Left" H 9750 4060 50  0001 C CNN
F 3 "~" H 9750 4060 50  0001 C CNN
F 4 "SSM-102-L-SV-K-TR" H 9750 4150 50  0001 C CNN "Mfr"
	1    9750 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 3900 9450 3900
Connection ~ 9750 4750
Wire Wire Line
	9750 4750 9750 4800
Wire Wire Line
	9450 4350 9450 4750
Wire Wire Line
	9450 3900 9450 4050
Wire Wire Line
	9750 3850 9750 3900
Connection ~ 9750 3900
Wire Wire Line
	9750 3900 9750 3950
$Comp
L Diode:BAT60A D?
U 1 1 5F56FF82
P 9450 4200
AR Path="/5F107976/5F56FF82" Ref="D?"  Part="1" 
AR Path="/5F56FF82" Ref="D1"  Part="1" 
F 0 "D1" V 9400 4350 39  0000 C CNN
F 1 "BAT60A" V 9450 4400 39  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 9450 4025 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/Infineon-BAT60ASERIES-DS-v01_01-en.pdf?fileId=db3a304313d846880113def70c9304a9" H 9450 4200 50  0001 C CNN
	1    9450 4200
	0    -1   1    0   
$EndComp
Text Label 4650 3000 0    39   ~ 0
~IRQ
$Sheet
S 6900 1600 550  350 
U 5F55C1D7
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
Wire Wire Line
	4650 3000 4450 3000
$Comp
L Device:R_Small R?
U 1 1 5F673437
P 9750 4600
AR Path="/5EF40FCD/5F673437" Ref="R?"  Part="1" 
AR Path="/5F673437" Ref="R19"  Part="1" 
AR Path="/5EFB83EE/5F673437" Ref="R?"  Part="1" 
F 0 "R19" H 9800 4550 39  0000 L CNN
F 1 "100" H 9800 4600 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9750 4600 50  0001 C CNN
F 3 "~" H 9750 4600 50  0001 C CNN
	1    9750 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	9750 4450 9750 4500
Wire Wire Line
	9750 4700 9750 4750
$EndSCHEMATC
