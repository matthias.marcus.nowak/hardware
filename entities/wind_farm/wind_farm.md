# Wind Farm
The Wind Farm is a power producer from renewable energy, which converts the wind to electrical energy. It injects current in the grid using a current source configuration, able to erogate up to 1 A. The wind speed can be changed using the touch slider, the LED indicators and the wind turbine speed will change accordingly. The circuitry includes a power generation sub-block, a telemetry sub-block for monitoring the power production in the 3.3 V bus and a sub-block for the visulization of the current output load.

## Schematic
[<img src="docs/wind_farm_sch.png"  width="1023" height="500">](docs/wind_farm_sch-main.png)

[<img src="docs/wind_farm_sch-sub_connectors.png"  width="145" height="100">](docs/wind_farm_sch-sub_connectors.png)
&nbsp;
[<img src="docs/wind_farm_sch-sub_power_slave.png"  width="145" height="100">](docs/wind_farm_sch-sub_power_slave.png)
&nbsp;
[<img src="docs/wind_farm_sch-sub_power_auto.png"  width="145" height="100">](docs/wind_farm_sch-sub_power_auto.png)
&nbsp;
[<img src="docs/wind_farm_sch-sub_indicator.png"  width="145" height="100">](docs/wind_farm_sch-sub_indicator.png)
&nbsp;
[<img src="docs/wind_farm_sch-sub_telemetry.png"  width="145" height="100">](docs/wind_farm_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/wind_farm_sch-sub_mechanical.png"  width="145" height="100">](docs/wind_farm_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/wind_farm_pcb.png"  width="531" height="500">](docs/wind_farm_pcb-brd.png)

[<img src="docs/wind_farm_pcb-F_Cu.png"  width="145" height="100">](docs/wind_farm_pcb-F_Cu.png)
&nbsp;
[<img src="docs/wind_farm_pcb-In1_Cu.png"  width="145" height="100">](docs/wind_farm_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/wind_farm_pcb-In2_Cu.png"  width="145" height="100">](docs/wind_farm_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/wind_farm_pcb-B_Cu.png"  width="145" height="100">](docs/wind_farm_pcb-B_Cu.png)

## Media

[<img src="docs/wind_farm_3d-top.png"  width="100" height="100">](docs/wind_farm_3d-top.png)
&nbsp;
[<img src="docs/wind_farm_3d-bottom.png"  width="100" height="100">](docs/wind_farm_3d-bottom.png)
&nbsp;
[<img src="docs/wind_farm_brd.png"  width="151" height="100">](docs/wind_farm_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/wind_farm/wind_farm.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/wind_farm/wind_farm.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/wind_farm/wind_farm.md)