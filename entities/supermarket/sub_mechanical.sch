EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 9
Title "Sub Mechanical"
Date "2020-10-02"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 5F54DCAB
P 4000 2500
F 0 "H1" H 4100 2546 50  0000 L CNN
F 1 "MountingHole" H 4100 2455 50  0000 L CNN
F 2 "LEGOS:WA-SMSI_Wuerth_97730356332" H 4000 2500 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/en/SMSI_SMT_STEEL_SPACER_M1_6_INTERNAL_BOTTOM_CLOSED_W_PIN/" H 4000 2500 50  0001 C CNN
	1    4000 2500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F54E3EE
P 4000 3000
F 0 "H2" H 4100 3046 50  0000 L CNN
F 1 "MountingHole" H 4100 2955 50  0000 L CNN
F 2 "LEGOS:WA-SMSI_Wuerth_97730356332" H 4000 3000 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/en/SMSI_SMT_STEEL_SPACER_M1_6_INTERNAL_BOTTOM_CLOSED_W_PIN/" H 4000 3000 50  0001 C CNN
	1    4000 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F54E664
P 4000 3500
F 0 "H3" H 4100 3546 50  0000 L CNN
F 1 "MountingHole" H 4100 3455 50  0000 L CNN
F 2 "LEGOS:WA-SMSI_Wuerth_97730356332" H 4000 3500 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/en/SMSI_SMT_STEEL_SPACER_M1_6_INTERNAL_BOTTOM_CLOSED_W_PIN/" H 4000 3500 50  0001 C CNN
	1    4000 3500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F54FB8F
P 5000 2500
F 0 "H4" H 5100 2546 50  0000 L CNN
F 1 "MountingHole" H 5100 2455 50  0000 L CNN
F 2 "LEGOS:WA-SMSI_Wuerth_97730356332" H 5000 2500 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/en/SMSI_SMT_STEEL_SPACER_M1_6_INTERNAL_BOTTOM_CLOSED_W_PIN/" H 5000 2500 50  0001 C CNN
	1    5000 2500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5F54FB95
P 5000 3000
F 0 "H5" H 5100 3046 50  0000 L CNN
F 1 "MountingHole" H 5100 2955 50  0000 L CNN
F 2 "LEGOS:WA-SMSI_Wuerth_97730356332" H 5000 3000 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/en/SMSI_SMT_STEEL_SPACER_M1_6_INTERNAL_BOTTOM_CLOSED_W_PIN/" H 5000 3000 50  0001 C CNN
	1    5000 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 5F54FB9B
P 5000 3500
F 0 "H6" H 5100 3546 50  0000 L CNN
F 1 "MountingHole" H 5100 3455 50  0000 L CNN
F 2 "LEGOS:WA-SMSI_Wuerth_97730356332" H 5000 3500 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/en/SMSI_SMT_STEEL_SPACER_M1_6_INTERNAL_BOTTOM_CLOSED_W_PIN/" H 5000 3500 50  0001 C CNN
	1    5000 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
