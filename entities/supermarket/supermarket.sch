EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 9
Title "Supermarket"
Date "2020-10-03"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5EF3C20D
P 3600 5100
AR Path="/5EF37CE1/5EF3C20D" Ref="#PWR?"  Part="1" 
AR Path="/5EF3C20D" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3600 4850 50  0001 C CNN
F 1 "GND" H 3605 4927 39  0000 C CNN
F 2 "" H 3600 5100 50  0001 C CNN
F 3 "" H 3600 5100 50  0001 C CNN
	1    3600 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5EF3C213
P 3600 2100
AR Path="/5EF37CE1/5EF3C213" Ref="C?"  Part="1" 
AR Path="/5EF3C213" Ref="C3"  Part="1" 
F 0 "C3" H 3750 2050 39  0000 R CNN
F 1 "10u" H 3750 2150 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3600 2100 50  0001 C CNN
F 3 "~" H 3600 2100 50  0001 C CNN
	1    3600 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF3C219
P 3350 2100
AR Path="/5EF37CE1/5EF3C219" Ref="C?"  Part="1" 
AR Path="/5EF3C219" Ref="C2"  Part="1" 
F 0 "C2" H 3300 2050 39  0000 R CNN
F 1 "100n" H 3350 2150 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 2100 50  0001 C CNN
F 3 "~" H 3350 2100 50  0001 C CNN
	1    3350 2100
	1    0    0    1   
$EndComp
Wire Wire Line
	3600 2200 3600 2300
Wire Wire Line
	3350 2200 3350 2300
$Comp
L power:GND #PWR?
U 1 1 5EF3C221
P 3350 2000
AR Path="/5EF37CE1/5EF3C221" Ref="#PWR?"  Part="1" 
AR Path="/5EF3C221" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 3350 1750 50  0001 C CNN
F 1 "GND" H 3355 1827 39  0000 C CNN
F 2 "" H 3350 2000 50  0001 C CNN
F 3 "" H 3350 2000 50  0001 C CNN
	1    3350 2000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF3C227
P 3600 2000
AR Path="/5EF37CE1/5EF3C227" Ref="#PWR?"  Part="1" 
AR Path="/5EF3C227" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 3600 1750 50  0001 C CNN
F 1 "GND" H 3605 1827 39  0000 C CNN
F 2 "" H 3600 2000 50  0001 C CNN
F 3 "" H 3600 2000 50  0001 C CNN
	1    3600 2000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF3C22D
P 2500 2900
AR Path="/5EF37CE1/5EF3C22D" Ref="#PWR?"  Part="1" 
AR Path="/5EF3C22D" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 2500 2650 50  0001 C CNN
F 1 "GND" H 2505 2727 39  0000 C CNN
F 2 "" H 2500 2900 50  0001 C CNN
F 3 "" H 2500 2900 50  0001 C CNN
	1    2500 2900
	1    0    0    -1  
$EndComp
Connection ~ 2500 2500
$Comp
L Device:C_Small C?
U 1 1 5EF3C234
P 2500 2700
AR Path="/5EF37CE1/5EF3C234" Ref="C?"  Part="1" 
AR Path="/5EF3C234" Ref="C1"  Part="1" 
F 0 "C1" H 2450 2650 39  0000 R CNN
F 1 "10u" H 2450 2750 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2500 2700 50  0001 C CNN
F 3 "~" H 2500 2700 50  0001 C CNN
	1    2500 2700
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EF3C23A
P 2500 2250
AR Path="/5EF37CE1/5EF3C23A" Ref="R?"  Part="1" 
AR Path="/5EF3C23A" Ref="R1"  Part="1" 
F 0 "R1" H 2550 2200 39  0000 L CNN
F 1 "10k" H 2550 2250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2500 2250 50  0001 C CNN
F 3 "~" H 2500 2250 50  0001 C CNN
	1    2500 2250
	-1   0    0    1   
$EndComp
Text Notes 4750 2500 0    28   ~ 0
UP during boot
Text Notes 4750 3100 0    28   ~ 0
DOWN during boot
Text Notes 4750 2700 0    28   ~ 0
DOWN during prog
Text Notes 4750 3400 0    28   ~ 0
UP during boot for log
Text Notes 2950 4250 1    28   ~ 0
Internally connected to the flash
Text Notes 4750 3000 0    28   ~ 0
UP during boot
Text Notes 4200 2500 0    39   ~ 0
IO0
Wire Wire Line
	2500 2500 2500 2600
Text Notes 4850 4700 2    28   ~ 0
only input
Text Notes 4850 4800 2    28   ~ 0
only input
Text Notes 2950 2700 2    28   ~ 0
only input
Wire Wire Line
	2500 2500 3000 2500
Wire Wire Line
	3100 2000 3100 2300
Wire Wire Line
	2500 2000 2500 2150
Wire Wire Line
	2500 2350 2500 2500
Text Label 4350 2800 0    39   ~ 0
RXD
Text Label 4350 2600 0    39   ~ 0
TXD
Text Label 2650 2500 0    39   ~ 0
EN
Text Label 4350 2500 0    39   ~ 0
ID
Wire Wire Line
	3600 2300 3350 2300
Connection ~ 3350 2300
Wire Wire Line
	3350 2300 3100 2300
NoConn ~ 3000 3700
NoConn ~ 3000 3800
NoConn ~ 3000 3900
NoConn ~ 3000 4000
NoConn ~ 3000 4100
NoConn ~ 3000 4200
Wire Wire Line
	6950 1500 6850 1500
Wire Wire Line
	6950 1600 6850 1600
Wire Wire Line
	6950 1700 6850 1700
Text Label 6850 1500 2    39   ~ 0
EN
Text Label 6850 1600 2    39   ~ 0
TXD
Text Label 6850 1700 2    39   ~ 0
RXD
Text Label 6850 1800 2    39   ~ 0
ID
Text Notes 4225 4300 0    39   ~ 0
DAC
Text Notes 4225 4200 0    39   ~ 0
DAC
$Comp
L power:+3.3V #PWR05
U 1 1 5F108B24
P 3100 2000
F 0 "#PWR05" H 3100 1850 50  0001 C CNN
F 1 "+3.3V" H 3115 2173 39  0000 C CNN
F 2 "" H 3100 2000 50  0001 C CNN
F 3 "" H 3100 2000 50  0001 C CNN
	1    3100 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR03
U 1 1 5F1092FA
P 2500 2000
F 0 "#PWR03" H 2500 1850 50  0001 C CNN
F 1 "+3.3V" H 2515 2173 39  0000 C CNN
F 2 "" H 2500 2000 50  0001 C CNN
F 3 "" H 2500 2000 50  0001 C CNN
	1    2500 2000
	1    0    0    -1  
$EndComp
Text Notes 2200 4250 0    39   ~ 0
Touch GPIO:\nT0 - IO4\nT1 - IO0\nT2 - IO2\nT3 - IO15\nT4 - IO13\nT5 - IO12\nT6 - IO14\nT7 - IO27\nT8 - IO33\nT9 - IO32
Wire Wire Line
	6950 1800 6900 1800
Wire Wire Line
	6950 1900 6900 1900
Wire Wire Line
	6900 1900 6900 1800
Connection ~ 6900 1800
Wire Wire Line
	6900 1800 6850 1800
Wire Wire Line
	6950 2400 6900 2400
Wire Wire Line
	6950 2500 6900 2500
Wire Wire Line
	6900 2500 6900 2400
Wire Wire Line
	6850 4750 6950 4750
Wire Wire Line
	4200 4300 4350 4300
Wire Wire Line
	4350 4400 4200 4400
Wire Wire Line
	4200 4500 4350 4500
Wire Wire Line
	4350 4600 4200 4600
Text Label 6850 4750 2    39   ~ 0
LOAD_EL
Wire Wire Line
	6850 5550 6950 5550
Wire Wire Line
	4350 4100 4200 4100
Wire Wire Line
	4200 2500 4350 2500
Wire Wire Line
	4200 2600 4350 2600
Wire Wire Line
	4200 2800 4350 2800
Wire Wire Line
	4200 3300 4350 3300
Wire Wire Line
	4200 3400 4350 3400
Wire Wire Line
	4200 3600 4350 3600
Wire Wire Line
	4200 3700 4350 3700
Wire Wire Line
	4350 3800 4200 3800
Wire Wire Line
	4350 4000 4200 4000
Wire Wire Line
	4350 4200 4200 4200
Connection ~ 6900 2400
Wire Wire Line
	6950 2600 6850 2600
Wire Wire Line
	6900 2400 6850 2400
$Comp
L power:+3.3V #PWR010
U 1 1 5F1072B2
P 6850 2600
F 0 "#PWR010" H 6850 2450 50  0001 C CNN
F 1 "+3.3V" V 6850 2800 39  0000 C CNN
F 2 "" H 6850 2600 50  0001 C CNN
F 3 "" H 6850 2600 50  0001 C CNN
	1    6850 2600
	0    -1   -1   0   
$EndComp
$Comp
L LEGOS:VBUS #PWR09
U 1 1 5F106A67
P 6850 2400
F 0 "#PWR09" H 6850 2250 50  0001 C CNN
F 1 "VBUS" V 6850 2600 39  0000 C CNN
F 2 "" H 6850 2400 50  0001 C CNN
F 3 "" H 6850 2400 50  0001 C CNN
	1    6850 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4200 3900 4350 3900
Wire Wire Line
	4350 3500 4200 3500
Wire Wire Line
	4200 2900 4350 2900
Wire Wire Line
	4350 2700 4200 2700
$Comp
L Switch:SW_Push SW1
U 1 1 5F49537E
P 2050 2700
F 0 "SW1" V 2000 2500 39  0000 L CNN
F 1 "PTS815" V 2050 2400 39  0000 L CNN
F 2 "LEGOS:PTS815" H 2050 2900 50  0001 C CNN
F 3 "~" H 2050 2900 50  0001 C CNN
	1    2050 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 2500 2500 2500
Wire Wire Line
	2050 2900 2500 2900
Connection ~ 2500 2900
$Sheet
S 6950 1400 550  600 
U 5F102375
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "EN" O L 6950 1500 39 
F3 "TXD" O L 6950 1600 39 
F4 "RXD" I L 6950 1700 39 
F5 "IO0" B L 6950 1800 39 
F6 "ID" B L 6950 1900 39 
$EndSheet
$Sheet
S 6950 2300 550  400 
U 5F10521C
F0 "Telemetry" 39
F1 "sub_telemetry.sch" 39
F2 "V+" I L 6950 2500 39 
F3 "V-" I L 6950 2600 39 
F4 "VCOM" I L 6950 2400 39 
F5 "~IRQ" O R 7500 2400 39 
F6 "A1" I R 7500 2500 39 
F7 "A0" I R 7500 2600 39 
$EndSheet
Wire Wire Line
	7600 2500 7500 2500
Wire Wire Line
	7600 2500 7600 2600
Wire Wire Line
	7600 2600 7500 2600
Wire Wire Line
	7600 2400 7500 2400
Wire Wire Line
	2500 2800 2500 2900
$Comp
L power:GND #PWR?
U 1 1 5F4A927F
P 7600 2600
AR Path="/5EF37CE1/5F4A927F" Ref="#PWR?"  Part="1" 
AR Path="/5F4A927F" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 7600 2350 50  0001 C CNN
F 1 "GND" V 7600 2400 39  0000 C CNN
F 2 "" H 7600 2600 50  0001 C CNN
F 3 "" H 7600 2600 50  0001 C CNN
	1    7600 2600
	0    -1   -1   0   
$EndComp
Connection ~ 7600 2600
$Sheet
S 6950 3000 550  400 
U 5F4B756B
F0 "Gas Power" 39
F1 "sub_power_slave.sch" 39
F2 "~CS" I R 7500 3100 39 
F3 "VOUT" O L 6950 3300 39 
F4 "VIN" I L 6950 3100 39 
F5 "~IRQ" I R 7500 3300 39 
$EndSheet
$Comp
L power:+3.3V #PWR012
U 1 1 5F4B8F6D
P 6850 3300
F 0 "#PWR012" H 6850 3150 50  0001 C CNN
F 1 "+3.3V" V 6850 3500 39  0000 C CNN
F 2 "" H 6850 3300 50  0001 C CNN
F 3 "" H 6850 3300 50  0001 C CNN
	1    6850 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 3300 6950 3300
Wire Wire Line
	6850 3100 6950 3100
Wire Wire Line
	7600 3100 7500 3100
Wire Wire Line
	7600 3300 7500 3300
$Comp
L power:+5V #PWR?
U 1 1 5F4C38BA
P 6850 3100
AR Path="/5F102375/5F4C38BA" Ref="#PWR?"  Part="1" 
AR Path="/5F4C38BA" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 6850 2950 50  0001 C CNN
F 1 "+5V" V 6850 3300 39  0000 C CNN
F 2 "" H 6850 3100 50  0001 C CNN
F 3 "" H 6850 3100 50  0001 C CNN
	1    6850 3100
	0    -1   -1   0   
$EndComp
Text Label 7600 3300 0    39   ~ 0
~IRQEXT
Text Label 7600 3100 0    39   ~ 0
~CS
Text Label 4350 4000 0    39   ~ 0
~CS
Wire Wire Line
	4200 3200 4350 3200
Wire Wire Line
	4350 3000 4200 3000
Wire Wire Line
	4350 3100 4200 3100
Text Label 7600 2400 0    39   ~ 0
~IRQBUS
Text Label 4350 4500 0    39   ~ 0
PRICE-
Text Label 4350 2700 0    39   ~ 0
PRICE_IND
Text Notes 2000 6000 0    50   ~ 0
https://www.bioenergyconsult.com/biomass-cogeneration/
$Sheet
S 6950 4650 550  350 
U 5F2104BF
F0 "Electrical Load" 39
F1 "sub_load_electrical.sch" 39
F2 "LOAD" I L 6950 4750 39 
F3 "SOURCE" I L 6950 4900 39 
$EndSheet
$Sheet
S 6950 5300 550  500 
U 5F5F0002
F0 "Thermal Load" 39
F1 "sub_load_thermal.sch" 39
F2 "SOURCE" I L 6950 5550 39 
F3 "LEVEL" I L 6950 5700 39 
F4 "LOAD" I L 6950 5400 39 
$EndSheet
Text Label 6850 5550 2    39   ~ 0
SOURCE
$Comp
L Power_Supervisor:LM809 U1
U 1 1 5F4CF789
P 1400 2500
F 0 "U1" H 1171 2538 39  0000 R CNN
F 1 "TLV803E" H 1171 2463 39  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1700 2600 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv803e.pdf" H 1700 2600 50  0001 C CNN
	1    1400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2500 1700 2500
Connection ~ 2050 2500
$Comp
L power:+3.3V #PWR01
U 1 1 5F4D2E6F
P 1400 2000
F 0 "#PWR01" H 1400 1850 50  0001 C CNN
F 1 "+3.3V" H 1415 2173 39  0000 C CNN
F 2 "" H 1400 2000 50  0001 C CNN
F 3 "" H 1400 2000 50  0001 C CNN
	1    1400 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F4D3ED7
P 1400 2900
AR Path="/5EF37CE1/5F4D3ED7" Ref="#PWR?"  Part="1" 
AR Path="/5F4D3ED7" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 1400 2650 50  0001 C CNN
F 1 "GND" H 1405 2727 39  0000 C CNN
F 2 "" H 1400 2900 50  0001 C CNN
F 3 "" H 1400 2900 50  0001 C CNN
	1    1400 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 2000 1400 2100
Wire Wire Line
	6950 3800 6850 3800
Wire Wire Line
	6850 3900 6950 3900
Wire Wire Line
	6950 4000 6850 4000
Wire Wire Line
	6850 4100 6950 4100
Text Label 6850 4100 2    39   ~ 0
BCD_D
Text Label 6850 4000 2    39   ~ 0
BCD_C
Text Label 6850 3900 2    39   ~ 0
BCD_B
Text Label 6850 3800 2    39   ~ 0
BCD_A
Wire Wire Line
	7500 3800 7600 3800
Wire Wire Line
	7600 3900 7500 3900
Wire Wire Line
	7500 4000 7600 4000
Wire Wire Line
	7600 4100 7500 4100
Text Label 7600 4100 0    39   ~ 0
DB
Text Label 7600 4000 0    39   ~ 0
D2
Text Label 7600 3900 0    39   ~ 0
D1
Text Label 7600 3800 0    39   ~ 0
D0
$Comp
L LEGOS:TouchButton BT?
U 1 1 5F6996AC
P 9545 1645
AR Path="/5F496ED4/5F6996AC" Ref="BT?"  Part="1" 
AR Path="/5F6996AC" Ref="BT1"  Part="1" 
F 0 "BT1" H 9796 1286 50  0000 L CNN
F 1 "TouchButton" H 9796 1195 50  0000 L CNN
F 2 "LEGOS:Touch_Button" H 10100 1385 50  0001 C CNN
F 3 "" H 9545 1645 50  0001 C CNN
	1    9545 1645
	1    0    0    -1  
$EndComp
$Comp
L LEGOS:TouchButton BT?
U 1 1 5F6996B2
P 9545 1995
AR Path="/5F496ED4/5F6996B2" Ref="BT?"  Part="1" 
AR Path="/5F6996B2" Ref="BT2"  Part="1" 
F 0 "BT2" H 9796 1636 50  0000 L CNN
F 1 "TouchButton" H 9796 1545 50  0000 L CNN
F 2 "LEGOS:Touch_Button" H 10100 1735 50  0001 C CNN
F 3 "" H 9545 1995 50  0001 C CNN
	1    9545 1995
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 2400 9350 2400
Wire Wire Line
	9350 2750 9250 2750
Wire Wire Line
	9350 2050 9250 2050
$Comp
L LEGOS:TouchButton BT?
U 1 1 5F6996BB
P 9545 2345
AR Path="/5F496ED4/5F6996BB" Ref="BT?"  Part="1" 
AR Path="/5F6996BB" Ref="BT3"  Part="1" 
F 0 "BT3" H 9796 1986 50  0000 L CNN
F 1 "TouchButton" H 9796 1895 50  0000 L CNN
F 2 "LEGOS:Touch_Button" H 10100 2085 50  0001 C CNN
F 3 "" H 9545 2345 50  0001 C CNN
	1    9545 2345
	1    0    0    -1  
$EndComp
$Sheet
S 6950 3700 550  650 
U 5F3DC4D2
F0 "Display" 39
F1 "sub_display.sch" 39
F2 "BCD0" I L 6950 3800 39 
F3 "BCD1" I L 6950 3900 39 
F4 "BCD2" I L 6950 4000 39 
F5 "BCD3" I L 6950 4100 39 
F6 "D2" I R 7500 4000 39 
F7 "D1" I R 7500 3900 39 
F8 "D0" I R 7500 3800 39 
F9 "BRIGHT" I R 7500 4100 39 
F10 "SOURCE" I L 6950 4250 39 
$EndSheet
Text Label 4350 4300 0    39   ~ 0
BCD_D
Text Label 4350 4200 0    39   ~ 0
BCD_C
Text Label 4350 4600 0    39   ~ 0
BCD_B
Text Label 4350 4400 0    39   ~ 0
BCD_A
Text Label 4350 2900 0    39   ~ 0
D0
Text Label 4350 3100 0    39   ~ 0
SOURCE
Wire Wire Line
	6850 4250 6950 4250
Text Label 6850 4250 2    39   ~ 0
PRICE_IND
Text Label 9250 2750 2    39   ~ 0
PRICE_SEL
Text Label 9250 2400 2    39   ~ 0
PRICE-
Wire Wire Line
	6850 4900 6950 4900
Text Label 6850 4900 2    39   ~ 0
SOURCE
Text GLabel 4350 3900 2    39   Input ~ 0
CLK
Wire Wire Line
	6850 5700 6950 5700
Text Label 6850 5700 2    39   ~ 0
STORAGE
Text Label 2900 2800 2    39   ~ 0
STORAGE
Wire Wire Line
	4200 4700 4350 4700
Text Label 2900 2700 2    39   ~ 0
~IRQBUS
Wire Wire Line
	2900 2700 3000 2700
Text Notes 2950 2800 2    28   ~ 0
only input
Wire Wire Line
	2900 2800 3000 2800
$Sheet
S 6950 750  550  350 
U 5F5772D0
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
Text Label 4350 3300 0    39   ~ 0
DB
Text GLabel 4350 4100 2    39   Input ~ 0
MOSI
Text GLabel 4350 3000 2    39   Input ~ 0
SCL
Text Label 4350 3200 0    39   ~ 0
PRICE_SEL
Text Label 4350 3800 0    39   ~ 0
LOAD_EL
Text Label 4350 3500 0    39   ~ 0
D1
Text GLabel 4350 3700 2    39   Input ~ 0
SDA
Text Label 4350 3600 0    39   ~ 0
D2
Connection ~ 3600 2300
$Comp
L RF_Module:ESP32-WROOM-32D U?
U 1 1 5EF3C26E
P 3600 3700
AR Path="/5EF37CE1/5EF3C26E" Ref="U?"  Part="1" 
AR Path="/5EF3C26E" Ref="U2"  Part="1" 
F 0 "U2" H 3050 4200 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 2700 4100 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 3600 2200 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 3300 3750 50  0001 C CNN
	1    3600 3700
	1    0    0    -1  
$EndComp
Text Label 4350 4700 0    39   ~ 0
PRICE+
Wire Wire Line
	4350 4800 4200 4800
Wire Wire Line
	6850 5400 6950 5400
Text Label 6850 5400 2    39   ~ 0
LOAD_TH
Text Label 4350 3400 0    39   ~ 0
LOAD_TH
$Comp
L LEGOS:TTP223 U8
U 1 1 606A53F7
P 9750 3800
F 0 "U8" H 9450 4175 39  0000 C CNN
F 1 "TTP223" H 9525 4100 39  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 9800 3550 50  0001 L CNN
F 3 "https://files.seeedstudio.com/wiki/Grove-Touch_Sensor/res/TTP223.pdf" H 10020 4350 50  0001 C CNN
	1    9750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3900 9250 3900
Text Label 9250 2050 2    39   ~ 0
TPRICE+
Text Label 9250 3900 2    39   ~ 0
TPRICE+
Wire Wire Line
	10150 3900 10250 3900
Text Label 10250 3900 0    39   ~ 0
PRICE+
$Comp
L power:GND #PWR?
U 1 1 606B0AC3
P 9750 4100
AR Path="/5EF37CE1/606B0AC3" Ref="#PWR?"  Part="1" 
AR Path="/606B0AC3" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 9750 3850 50  0001 C CNN
F 1 "GND" H 9755 3927 39  0000 C CNN
F 2 "" H 9750 4100 50  0001 C CNN
F 3 "" H 9750 4100 50  0001 C CNN
	1    9750 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0125
U 1 1 606B1C2F
P 9750 3500
F 0 "#PWR0125" H 9750 3350 50  0001 C CNN
F 1 "+3.3V" H 9765 3673 39  0000 C CNN
F 2 "" H 9750 3500 50  0001 C CNN
F 3 "" H 9750 3500 50  0001 C CNN
	1    9750 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 606B885A
P 10250 3700
AR Path="/5EF37CE1/606B885A" Ref="#PWR?"  Part="1" 
AR Path="/606B885A" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 10250 3450 50  0001 C CNN
F 1 "GND" V 10250 3500 39  0000 C CNN
F 2 "" H 10250 3700 50  0001 C CNN
F 3 "" H 10250 3700 50  0001 C CNN
	1    10250 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10250 3700 10150 3700
$Comp
L power:GND #PWR?
U 1 1 606BBF05
P 9250 3700
AR Path="/5EF37CE1/606BBF05" Ref="#PWR?"  Part="1" 
AR Path="/606BBF05" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 9250 3450 50  0001 C CNN
F 1 "GND" V 9250 3500 39  0000 C CNN
F 2 "" H 9250 3700 50  0001 C CNN
F 3 "" H 9250 3700 50  0001 C CNN
	1    9250 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 3700 9350 3700
$EndSCHEMATC
