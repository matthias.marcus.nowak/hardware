# Supermarket
The supermarket represents a common commercial prosumer, in which the power absorbed from the grid is mainly used for lighting and thermo-regulation, in which an emulated thermal system fills a storage element of hot water and use it when needed. A combined heat and power (CHP) system allows to extract the required energy from burning gas as alternative to using energy from the grid. The mixing ratio of the two sources is defined by an algorithm based on their market cost in €/kWh. The cost of the selected source is visualized on a 3-digit LED display by toggling the selection touch button, whereas the increment/decrement touch buttons can be used to change the cost. If more power than required is generated, it can freely flow back into the grid. The circuitry includes a power generation sub-block, a telemetry sub-block for monitoring the self-power consumption in the 3.3 V bus, a telemetry sub-block for monitoring the external power generation from gas combustion, a thermo-regulation sub-block, a sub-block for the visualization of energy mix and active appliances and a sub-block for the LEDs display.

## Schematic
[<img src="docs/supermarket_sch.png"  width="931" height="500">](docs/supermarket_sch-main.png)

[<img src="docs/supermarket_sch-sub_connectors.png"  width="145" height="100">](docs/supermarket_sch-sub_connectors.png)
&nbsp;
[<img src="docs/supermarket_sch-sub_power_slave.png"  width="145" height="100">](docs/supermarket_sch-sub_power_slave.png)
&nbsp;
[<img src="docs/supermarket_sch-sub_display.png"  width="145" height="100">](docs/supermarket_sch-sub_display.png)
&nbsp;
[<img src="docs/supermarket_sch-sub_load_electrical.png"  width="145" height="100">](docs/supermarket_sch-sub_load_electrical.png)
&nbsp;
[<img src="docs/supermarket_sch-sub_load_thermal.png"  width="145" height="100">](docs/supermarket_sch-sub_load_thermal.png)
&nbsp;
[<img src="docs/supermarket_sch-sub_telemetry.png"  width="145" height="100">](docs/supermarket_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/supermarket_sch-sub_telemetry-ext.png"  width="145" height="100">](docs/supermarket_sch-sub_telemetry-ext.png)
&nbsp;
[<img src="docs/supermarket_sch-sub_mechanical.png"  width="145" height="100">](docs/supermarket_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/supermarket_pcb.png"  width="490" height="500">](docs/supermarket_pcb-brd.png)

[<img src="docs/supermarket_pcb-F_Cu.png"  width="145" height="100">](docs/supermarket_pcb-F_Cu.png)
&nbsp;
[<img src="docs/supermarket_pcb-In1_Cu.png"  width="145" height="100">](docs/supermarket_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/supermarket_pcb-In2_Cu.png"  width="145" height="100">](docs/supermarket_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/supermarket_pcb-B_Cu.png"  width="145" height="100">](docs/supermarket_pcb-B_Cu.png)

## Media

[<img src="docs/supermarket_3d-top.png"  width="91" height="100">](docs/supermarket_3d-top.png)
&nbsp;
[<img src="docs/supermarket_3d-bottom.png"  width="92" height="100">](docs/supermarket_3d-bottom.png)
&nbsp;
[<img src="docs/supermarket_brd.png"  width="149" height="100">](docs/supermarket_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/supermarket/supermarket.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/supermarket/supermarket.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/supermarket/supermarket.md)