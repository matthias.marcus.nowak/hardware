%% Thermal storage discharge

clear, close all, clc
NTC = @(T)680*exp(3910*(1./(T+273.15)-1/(25+273.15)));
t = (0:1e-4:40-1e-4)';

T = 15:5:35;
R = NTC(T);
C = 1.5e-3;

V = exp(-t./(R*C))*0.96+0.04;
V(V<0.05)=nan;
plot(t,V*100)
xlabel('Time (s)')
ylabel('Storage (%)'), ylim([0 100])
title('Discharge [Temperature]')
legend(cellstr([num2str(T'),repmat(' �C',5,1)]))

%% Thermal storage charge

R = [4.12,3.64,3.16,2.68,2.2]*1e3;
V = 1-exp(-t./(R*C))*0.96;

V(V>0.95)=nan;
figure
plot(t,V*100)
xlabel('Time (s)')
ylabel('Storage (%)'), ylim([0 100])
title('Charge [Grid + Gas]')
legend('100% + 0%','75% + 25%','50% + 50%','25% + 75%','0% + 100%','location','southeast')