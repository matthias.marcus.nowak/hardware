EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title "Sub Grandstand"
Date "2020-07-23"
Rev "1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED_Small D?
U 1 1 5F19A6D5
P 2650 3550
AR Path="/5F1D940F/5F19A6D5" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F19A6D5" Ref="D17"  Part="1" 
AR Path="/5F202C6C/5F19A6D5" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F19A6D5" Ref="D14"  Part="1" 
AR Path="/5F7C0AF0/5F19A6D5" Ref="D?"  Part="1" 
F 0 "D14" V 2700 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 2650 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 2650 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 2650 3550 50  0001 C CNN
	1    2650 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	2650 3650 2650 3800
Wire Wire Line
	2650 3800 2250 3800
Connection ~ 2650 3000
Wire Wire Line
	2650 3000 2650 3150
Connection ~ 2650 3800
Wire Wire Line
	2650 3800 3200 3800
$Comp
L Device:LED_Small D?
U 1 1 5F7EB350
P 3200 3550
AR Path="/5F1D940F/5F7EB350" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EB350" Ref="D18"  Part="1" 
AR Path="/5F202C6C/5F7EB350" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F7EB350" Ref="D15"  Part="1" 
AR Path="/5F7C0AF0/5F7EB350" Ref="D?"  Part="1" 
F 0 "D15" V 3250 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 3200 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 3200 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 3200 3550 50  0001 C CNN
	1    3200 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 3650 3200 3800
Wire Wire Line
	3200 3000 3200 3150
$Comp
L Device:LED_Small D?
U 1 1 5F7EB351
P 3750 3550
AR Path="/5F1D940F/5F7EB351" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EB351" Ref="D19"  Part="1" 
AR Path="/5F202C6C/5F7EB351" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F7EB351" Ref="D16"  Part="1" 
AR Path="/5F7C0AF0/5F7EB351" Ref="D?"  Part="1" 
F 0 "D16" V 3800 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 3750 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 3750 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 3750 3550 50  0001 C CNN
	1    3750 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	3750 3650 3750 3800
Wire Wire Line
	3750 3000 3750 3150
$Comp
L Device:LED_Small D?
U 1 1 5F32F1E9
P 4300 3550
AR Path="/5F1D940F/5F32F1E9" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F32F1E9" Ref="D20"  Part="1" 
AR Path="/5F202C6C/5F32F1E9" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F32F1E9" Ref="D17"  Part="1" 
AR Path="/5F7C0AF0/5F32F1E9" Ref="D?"  Part="1" 
F 0 "D17" V 4350 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 4300 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 4300 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 4300 3550 50  0001 C CNN
	1    4300 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	4300 3650 4300 3800
Wire Wire Line
	4300 3000 4300 3150
$Comp
L Device:LED_Small D?
U 1 1 5F80A6C6
P 4850 3550
AR Path="/5F1D940F/5F80A6C6" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F80A6C6" Ref="D21"  Part="1" 
AR Path="/5F202C6C/5F80A6C6" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F80A6C6" Ref="D18"  Part="1" 
AR Path="/5F7C0AF0/5F80A6C6" Ref="D?"  Part="1" 
F 0 "D18" V 4900 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 4850 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 4850 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 4850 3550 50  0001 C CNN
	1    4850 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	4850 3650 4850 3800
Wire Wire Line
	4850 3000 4850 3150
$Comp
L Device:LED_Small D?
U 1 1 5F333037
P 5400 3550
AR Path="/5F1D940F/5F333037" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F333037" Ref="D22"  Part="1" 
AR Path="/5F202C6C/5F333037" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F333037" Ref="D19"  Part="1" 
AR Path="/5F7C0AF0/5F333037" Ref="D?"  Part="1" 
F 0 "D19" V 5450 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 5400 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 5400 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 5400 3550 50  0001 C CNN
	1    5400 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	5400 3650 5400 3800
Wire Wire Line
	5400 3000 5400 3150
$Comp
L Device:LED_Small D?
U 1 1 5F80A6C7
P 5950 3550
AR Path="/5F1D940F/5F80A6C7" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F80A6C7" Ref="D23"  Part="1" 
AR Path="/5F202C6C/5F80A6C7" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F80A6C7" Ref="D20"  Part="1" 
AR Path="/5F7C0AF0/5F80A6C7" Ref="D?"  Part="1" 
F 0 "D20" V 6000 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 5950 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 5950 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 5950 3550 50  0001 C CNN
	1    5950 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	5950 3650 5950 3800
Wire Wire Line
	5950 3000 5950 3150
$Comp
L Device:LED_Small D?
U 1 1 5F333047
P 6500 3550
AR Path="/5F1D940F/5F333047" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F333047" Ref="D24"  Part="1" 
AR Path="/5F202C6C/5F333047" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F333047" Ref="D21"  Part="1" 
AR Path="/5F7C0AF0/5F333047" Ref="D?"  Part="1" 
F 0 "D21" V 6550 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 6500 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 6500 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 6500 3550 50  0001 C CNN
	1    6500 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	6500 3650 6500 3800
Wire Wire Line
	6500 3000 6500 3150
$Comp
L Device:LED_Small D?
U 1 1 5F33304F
P 7050 3550
AR Path="/5F1D940F/5F33304F" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F33304F" Ref="D25"  Part="1" 
AR Path="/5F202C6C/5F33304F" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F33304F" Ref="D22"  Part="1" 
AR Path="/5F7C0AF0/5F33304F" Ref="D?"  Part="1" 
F 0 "D22" V 7100 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 7050 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 7050 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 7050 3550 50  0001 C CNN
	1    7050 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	7050 3650 7050 3800
Wire Wire Line
	7050 3000 7050 3150
$Comp
L Device:LED_Small D?
U 1 1 5F7EB356
P 7600 3550
AR Path="/5F1D940F/5F7EB356" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EB356" Ref="D26"  Part="1" 
AR Path="/5F202C6C/5F7EB356" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F7EB356" Ref="D23"  Part="1" 
AR Path="/5F7C0AF0/5F7EB356" Ref="D?"  Part="1" 
F 0 "D23" V 7650 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 7600 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 7600 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 7600 3550 50  0001 C CNN
	1    7600 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	7600 3650 7600 3800
Wire Wire Line
	7600 3000 7600 3150
$Comp
L Device:LED_Small D?
U 1 1 5F7EB357
P 8150 3550
AR Path="/5F1D940F/5F7EB357" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EB357" Ref="D27"  Part="1" 
AR Path="/5F202C6C/5F7EB357" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F7EB357" Ref="D24"  Part="1" 
AR Path="/5F7C0AF0/5F7EB357" Ref="D?"  Part="1" 
F 0 "D24" V 8200 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 8150 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 8150 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 8150 3550 50  0001 C CNN
	1    8150 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	8150 3650 8150 3800
Wire Wire Line
	8150 3000 8150 3150
$Comp
L Device:LED_Small D?
U 1 1 5F335AED
P 8700 3550
AR Path="/5F1D940F/5F335AED" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F335AED" Ref="D28"  Part="1" 
AR Path="/5F202C6C/5F335AED" Ref="D?"  Part="1" 
AR Path="/5F7B83A2/5F335AED" Ref="D25"  Part="1" 
AR Path="/5F7C0AF0/5F335AED" Ref="D?"  Part="1" 
F 0 "D25" V 8750 3450 39  0000 R CNN
F 1 "IN-S124ARG" V 8700 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 8700 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 8700 3550 50  0001 C CNN
	1    8700 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	8700 3650 8700 3800
Wire Wire Line
	8700 3000 8700 3150
Text Notes 3200 2950 0    50   ~ 0
If = 12x 10 mA = 120 mA
Wire Wire Line
	2650 3000 3200 3000
Connection ~ 3200 3000
Wire Wire Line
	3200 3000 3750 3000
Connection ~ 3200 3800
Wire Wire Line
	3200 3800 3750 3800
Connection ~ 3750 3800
Wire Wire Line
	3750 3800 4300 3800
Connection ~ 3750 3000
Wire Wire Line
	3750 3000 4300 3000
Connection ~ 4300 3000
Wire Wire Line
	4300 3000 4850 3000
Connection ~ 4300 3800
Wire Wire Line
	4300 3800 4850 3800
Connection ~ 4850 3000
Wire Wire Line
	4850 3000 5400 3000
Connection ~ 4850 3800
Wire Wire Line
	4850 3800 5400 3800
Connection ~ 7600 3000
Wire Wire Line
	7600 3000 8150 3000
Connection ~ 7600 3800
Wire Wire Line
	7600 3800 8150 3800
Connection ~ 8150 3800
Wire Wire Line
	8150 3800 8700 3800
Connection ~ 8150 3000
Wire Wire Line
	8150 3000 8700 3000
Connection ~ 7050 3000
Wire Wire Line
	7050 3000 7600 3000
Connection ~ 6500 3000
Wire Wire Line
	6500 3000 7050 3000
Connection ~ 6500 3800
Wire Wire Line
	6500 3800 7050 3800
Connection ~ 7050 3800
Wire Wire Line
	7050 3800 7600 3800
Connection ~ 5400 3000
Wire Wire Line
	5400 3000 5950 3000
Connection ~ 5950 3000
Wire Wire Line
	5950 3000 6500 3000
Connection ~ 5950 3800
Wire Wire Line
	5950 3800 6500 3800
Connection ~ 5400 3800
Wire Wire Line
	5400 3800 5950 3800
Wire Wire Line
	2250 3000 2650 3000
Wire Wire Line
	2250 3350 2000 3350
Wire Wire Line
	2250 3450 2000 3450
Text HLabel 2000 3350 0    50   Input ~ 0
V+
Text HLabel 2000 3450 0    50   Input ~ 0
V-
$Comp
L Device:R_Small R?
U 1 1 5F80A6CC
P 2650 3250
AR Path="/5F80A6CC" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6CC" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6CC" Ref="R27"  Part="1" 
AR Path="/5F202C6C/5F80A6CC" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6CC" Ref="R24"  Part="1" 
AR Path="/5F7C0AF0/5F80A6CC" Ref="R?"  Part="1" 
F 0 "R24" H 2700 3200 39  0000 L CNN
F 1 "20" H 2700 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2650 3250 50  0001 C CNN
F 3 "~" H 2650 3250 50  0001 C CNN
	1    2650 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 3350 2650 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6CD
P 3200 3250
AR Path="/5F80A6CD" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6CD" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6CD" Ref="R28"  Part="1" 
AR Path="/5F202C6C/5F80A6CD" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6CD" Ref="R25"  Part="1" 
AR Path="/5F7C0AF0/5F80A6CD" Ref="R?"  Part="1" 
F 0 "R25" H 3250 3200 39  0000 L CNN
F 1 "20" H 3250 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3200 3250 50  0001 C CNN
F 3 "~" H 3200 3250 50  0001 C CNN
	1    3200 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 3350 3200 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6CE
P 3750 3250
AR Path="/5F80A6CE" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6CE" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6CE" Ref="R29"  Part="1" 
AR Path="/5F202C6C/5F80A6CE" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6CE" Ref="R26"  Part="1" 
AR Path="/5F7C0AF0/5F80A6CE" Ref="R?"  Part="1" 
F 0 "R26" H 3800 3200 39  0000 L CNN
F 1 "20" H 3800 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3750 3250 50  0001 C CNN
F 3 "~" H 3750 3250 50  0001 C CNN
	1    3750 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3350 3750 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6CF
P 4300 3250
AR Path="/5F80A6CF" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6CF" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6CF" Ref="R30"  Part="1" 
AR Path="/5F202C6C/5F80A6CF" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6CF" Ref="R27"  Part="1" 
AR Path="/5F7C0AF0/5F80A6CF" Ref="R?"  Part="1" 
F 0 "R27" H 4350 3200 39  0000 L CNN
F 1 "20" H 4350 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4300 3250 50  0001 C CNN
F 3 "~" H 4300 3250 50  0001 C CNN
	1    4300 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 3350 4300 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6D0
P 4850 3250
AR Path="/5F80A6D0" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D0" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D0" Ref="R31"  Part="1" 
AR Path="/5F202C6C/5F80A6D0" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6D0" Ref="R28"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D0" Ref="R?"  Part="1" 
F 0 "R28" H 4900 3200 39  0000 L CNN
F 1 "20" H 4900 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4850 3250 50  0001 C CNN
F 3 "~" H 4850 3250 50  0001 C CNN
	1    4850 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4850 3350 4850 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6D1
P 5400 3250
AR Path="/5F80A6D1" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D1" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D1" Ref="R32"  Part="1" 
AR Path="/5F202C6C/5F80A6D1" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6D1" Ref="R29"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D1" Ref="R?"  Part="1" 
F 0 "R29" H 5450 3200 39  0000 L CNN
F 1 "20" H 5450 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5400 3250 50  0001 C CNN
F 3 "~" H 5400 3250 50  0001 C CNN
	1    5400 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 3350 5400 3450
$Comp
L Device:R_Small R?
U 1 1 5F7AD1CD
P 5950 3250
AR Path="/5F7AD1CD" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AD1CD" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AD1CD" Ref="R33"  Part="1" 
AR Path="/5F202C6C/5F7AD1CD" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F7AD1CD" Ref="R30"  Part="1" 
AR Path="/5F7C0AF0/5F7AD1CD" Ref="R?"  Part="1" 
F 0 "R30" H 6000 3200 39  0000 L CNN
F 1 "20" H 6000 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5950 3250 50  0001 C CNN
F 3 "~" H 5950 3250 50  0001 C CNN
	1    5950 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 3350 5950 3450
$Comp
L Device:R_Small R?
U 1 1 5F7AD1D4
P 6500 3250
AR Path="/5F7AD1D4" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AD1D4" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AD1D4" Ref="R34"  Part="1" 
AR Path="/5F202C6C/5F7AD1D4" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F7AD1D4" Ref="R31"  Part="1" 
AR Path="/5F7C0AF0/5F7AD1D4" Ref="R?"  Part="1" 
F 0 "R31" H 6550 3200 39  0000 L CNN
F 1 "20" H 6550 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6500 3250 50  0001 C CNN
F 3 "~" H 6500 3250 50  0001 C CNN
	1    6500 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 3350 6500 3450
$Comp
L Device:R_Small R?
U 1 1 5F7AFBFE
P 7050 3250
AR Path="/5F7AFBFE" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AFBFE" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AFBFE" Ref="R35"  Part="1" 
AR Path="/5F202C6C/5F7AFBFE" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F7AFBFE" Ref="R32"  Part="1" 
AR Path="/5F7C0AF0/5F7AFBFE" Ref="R?"  Part="1" 
F 0 "R32" H 7100 3200 39  0000 L CNN
F 1 "20" H 7100 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7050 3250 50  0001 C CNN
F 3 "~" H 7050 3250 50  0001 C CNN
	1    7050 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7050 3350 7050 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6D5
P 7600 3250
AR Path="/5F80A6D5" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D5" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D5" Ref="R36"  Part="1" 
AR Path="/5F202C6C/5F80A6D5" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6D5" Ref="R33"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D5" Ref="R?"  Part="1" 
F 0 "R33" H 7650 3200 39  0000 L CNN
F 1 "20" H 7650 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7600 3250 50  0001 C CNN
F 3 "~" H 7600 3250 50  0001 C CNN
	1    7600 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7600 3350 7600 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6D6
P 8150 3250
AR Path="/5F80A6D6" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D6" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D6" Ref="R37"  Part="1" 
AR Path="/5F202C6C/5F80A6D6" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6D6" Ref="R34"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D6" Ref="R?"  Part="1" 
F 0 "R34" H 8200 3200 39  0000 L CNN
F 1 "20" H 8200 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8150 3250 50  0001 C CNN
F 3 "~" H 8150 3250 50  0001 C CNN
	1    8150 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8150 3350 8150 3450
$Comp
L Device:R_Small R?
U 1 1 5F80A6D7
P 8700 3250
AR Path="/5F80A6D7" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D7" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D7" Ref="R38"  Part="1" 
AR Path="/5F202C6C/5F80A6D7" Ref="R?"  Part="1" 
AR Path="/5F7B83A2/5F80A6D7" Ref="R35"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D7" Ref="R?"  Part="1" 
F 0 "R35" H 8750 3200 39  0000 L CNN
F 1 "20" H 8750 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8700 3250 50  0001 C CNN
F 3 "~" H 8700 3250 50  0001 C CNN
	1    8700 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8700 3350 8700 3450
Wire Wire Line
	2250 3450 2250 3800
Wire Wire Line
	2250 3000 2250 3350
$EndSCHEMATC
