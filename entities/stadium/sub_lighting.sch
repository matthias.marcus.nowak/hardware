EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title "Sub Grandstand"
Date "2020-07-23"
Rev "1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB18
P 1750 3925
AR Path="/5F1D940F/5F7EAB18" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB18" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB18" Ref="D32"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB18" Ref="D26"  Part="1" 
F 0 "D26" V 1800 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 1750 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 1750 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 1750 3925 50  0001 C CNN
	1    1750 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	1750 4025 1750 4175
Wire Wire Line
	1750 4175 1350 4175
Connection ~ 1750 3375
Wire Wire Line
	1750 3375 1750 3525
Connection ~ 1750 4175
Wire Wire Line
	1750 4175 2300 4175
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB19
P 2300 3925
AR Path="/5F1D940F/5F7EAB19" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB19" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB19" Ref="D33"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB19" Ref="D27"  Part="1" 
F 0 "D27" V 2350 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 2300 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 2300 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 2300 3925 50  0001 C CNN
	1    2300 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	2300 4025 2300 4175
Wire Wire Line
	2300 3375 2300 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB1A
P 2850 3925
AR Path="/5F1D940F/5F7EAB1A" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB1A" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB1A" Ref="D34"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB1A" Ref="D28"  Part="1" 
F 0 "D28" V 2900 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 2850 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 2850 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 2850 3925 50  0001 C CNN
	1    2850 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	2850 4025 2850 4175
Wire Wire Line
	2850 3375 2850 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB1B
P 3400 3925
AR Path="/5F1D940F/5F7EAB1B" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB1B" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB1B" Ref="D35"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB1B" Ref="D29"  Part="1" 
F 0 "D29" V 3450 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 3400 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 3400 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 3400 3925 50  0001 C CNN
	1    3400 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	3400 4025 3400 4175
Wire Wire Line
	3400 3375 3400 3525
$Comp
L Device:LED_Small D?
U 1 1 5F32F7AB
P 3950 3925
AR Path="/5F1D940F/5F32F7AB" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F32F7AB" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F32F7AB" Ref="D36"  Part="1" 
AR Path="/5F7C0AF0/5F32F7AB" Ref="D30"  Part="1" 
F 0 "D30" V 4000 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 3950 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 3950 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 3950 3925 50  0001 C CNN
	1    3950 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	3950 4025 3950 4175
Wire Wire Line
	3950 3375 3950 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB1D
P 4500 3925
AR Path="/5F1D940F/5F7EAB1D" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB1D" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB1D" Ref="D37"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB1D" Ref="D31"  Part="1" 
F 0 "D31" V 4550 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 4500 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 4500 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 4500 3925 50  0001 C CNN
	1    4500 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	4500 4025 4500 4175
Wire Wire Line
	4500 3375 4500 3525
$Comp
L Device:LED_Small D?
U 1 1 5F33303F
P 5050 3925
AR Path="/5F1D940F/5F33303F" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F33303F" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F33303F" Ref="D38"  Part="1" 
AR Path="/5F7C0AF0/5F33303F" Ref="D32"  Part="1" 
F 0 "D32" V 5100 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 5050 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 5050 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 5050 3925 50  0001 C CNN
	1    5050 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	5050 4025 5050 4175
Wire Wire Line
	5050 3375 5050 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB1F
P 5600 3925
AR Path="/5F1D940F/5F7EAB1F" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB1F" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB1F" Ref="D39"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB1F" Ref="D33"  Part="1" 
F 0 "D33" V 5650 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 5600 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 5600 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 5600 3925 50  0001 C CNN
	1    5600 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	5600 4025 5600 4175
Wire Wire Line
	5600 3375 5600 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB20
P 6150 3925
AR Path="/5F1D940F/5F7EAB20" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB20" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB20" Ref="D40"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB20" Ref="D34"  Part="1" 
F 0 "D34" V 6200 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 6150 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 6150 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 6150 3925 50  0001 C CNN
	1    6150 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	6150 4025 6150 4175
Wire Wire Line
	6150 3375 6150 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB21
P 6700 3925
AR Path="/5F1D940F/5F7EAB21" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB21" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB21" Ref="D41"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB21" Ref="D35"  Part="1" 
F 0 "D35" V 6750 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 6700 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 6700 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 6700 3925 50  0001 C CNN
	1    6700 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	6700 4025 6700 4175
Wire Wire Line
	6700 3375 6700 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7EAB22
P 7250 3925
AR Path="/5F1D940F/5F7EAB22" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7EAB22" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB22" Ref="D42"  Part="1" 
AR Path="/5F7C0AF0/5F7EAB22" Ref="D36"  Part="1" 
F 0 "D36" V 7300 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 7250 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 7250 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 7250 3925 50  0001 C CNN
	1    7250 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	7250 4025 7250 4175
Wire Wire Line
	7250 3375 7250 3525
$Comp
L Device:LED_Small D?
U 1 1 5F80A6C8
P 7800 3925
AR Path="/5F1D940F/5F80A6C8" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F80A6C8" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F80A6C8" Ref="D43"  Part="1" 
AR Path="/5F7C0AF0/5F80A6C8" Ref="D37"  Part="1" 
F 0 "D37" V 7850 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 7800 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 7800 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 7800 3925 50  0001 C CNN
	1    7800 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	7800 4025 7800 4175
Wire Wire Line
	7800 3375 7800 3525
$Comp
L Device:LED_Small D?
U 1 1 5F80A6C9
P 8350 3925
AR Path="/5F1D940F/5F80A6C9" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F80A6C9" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F80A6C9" Ref="D44"  Part="1" 
AR Path="/5F7C0AF0/5F80A6C9" Ref="D38"  Part="1" 
F 0 "D38" V 8400 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 8350 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 8350 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 8350 3925 50  0001 C CNN
	1    8350 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	8350 4025 8350 4175
Wire Wire Line
	8350 3375 8350 3525
$Comp
L Device:LED_Small D?
U 1 1 5F80A6CA
P 8900 3925
AR Path="/5F1D940F/5F80A6CA" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F80A6CA" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F80A6CA" Ref="D45"  Part="1" 
AR Path="/5F7C0AF0/5F80A6CA" Ref="D39"  Part="1" 
F 0 "D39" V 8950 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 8900 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 8900 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 8900 3925 50  0001 C CNN
	1    8900 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	8900 4025 8900 4175
Wire Wire Line
	8900 3375 8900 3525
$Comp
L Device:LED_Small D?
U 1 1 5F335B05
P 9450 3925
AR Path="/5F1D940F/5F335B05" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F335B05" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F335B05" Ref="D46"  Part="1" 
AR Path="/5F7C0AF0/5F335B05" Ref="D40"  Part="1" 
F 0 "D40" V 9500 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 9450 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 9450 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 9450 3925 50  0001 C CNN
	1    9450 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	9450 4025 9450 4175
Wire Wire Line
	9450 3375 9450 3525
Text Notes 2300 3325 0    50   ~ 0
If = 18x 10 mA = 180 mA
Wire Wire Line
	1750 3375 2300 3375
Connection ~ 2300 3375
Wire Wire Line
	2300 3375 2850 3375
Connection ~ 2300 4175
Wire Wire Line
	2300 4175 2850 4175
Connection ~ 2850 4175
Wire Wire Line
	2850 4175 3400 4175
Connection ~ 2850 3375
Wire Wire Line
	2850 3375 3400 3375
Connection ~ 3400 3375
Wire Wire Line
	3400 3375 3950 3375
Connection ~ 3400 4175
Wire Wire Line
	3400 4175 3950 4175
Connection ~ 3950 3375
Wire Wire Line
	3950 3375 4500 3375
Connection ~ 3950 4175
Wire Wire Line
	3950 4175 4500 4175
Connection ~ 6700 3375
Wire Wire Line
	6700 3375 7250 3375
Connection ~ 6700 4175
Wire Wire Line
	6700 4175 7250 4175
Connection ~ 7250 4175
Wire Wire Line
	7250 4175 7800 4175
Connection ~ 7800 3375
Wire Wire Line
	7800 3375 8350 3375
Connection ~ 7250 3375
Wire Wire Line
	7250 3375 7800 3375
Connection ~ 7800 4175
Wire Wire Line
	7800 4175 8350 4175
Connection ~ 6150 3375
Wire Wire Line
	6150 3375 6700 3375
Connection ~ 5600 3375
Wire Wire Line
	5600 3375 6150 3375
Connection ~ 5600 4175
Wire Wire Line
	5600 4175 6150 4175
Connection ~ 6150 4175
Wire Wire Line
	6150 4175 6700 4175
Connection ~ 8350 3375
Wire Wire Line
	8350 3375 8900 3375
Connection ~ 8350 4175
Wire Wire Line
	8350 4175 8900 4175
Connection ~ 8900 3375
Wire Wire Line
	8900 3375 9450 3375
Connection ~ 8900 4175
Wire Wire Line
	8900 4175 9450 4175
Connection ~ 4500 3375
Wire Wire Line
	4500 3375 5050 3375
Connection ~ 5050 3375
Wire Wire Line
	5050 3375 5600 3375
Connection ~ 5050 4175
Wire Wire Line
	5050 4175 5600 4175
Connection ~ 4500 4175
Wire Wire Line
	4500 4175 5050 4175
Wire Wire Line
	1350 3375 1750 3375
Wire Wire Line
	1350 3725 1100 3725
Wire Wire Line
	1350 3825 1100 3825
Text HLabel 1100 3725 0    50   Input ~ 0
V+
Text HLabel 1100 3825 0    50   Input ~ 0
V-
$Comp
L Device:R_Small R?
U 1 1 5F7A3E0D
P 1750 3625
AR Path="/5F7A3E0D" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7A3E0D" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7A3E0D" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7A3E0D" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7A3E0D" Ref="R36"  Part="1" 
F 0 "R36" H 1800 3575 39  0000 L CNN
F 1 "20" H 1800 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1750 3625 50  0001 C CNN
F 3 "~" H 1750 3625 50  0001 C CNN
	1    1750 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	1750 3725 1750 3825
$Comp
L Device:R_Small R?
U 1 1 5F7A7EF8
P 2300 3625
AR Path="/5F7A7EF8" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7A7EF8" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7A7EF8" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7A7EF8" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7A7EF8" Ref="R37"  Part="1" 
F 0 "R37" H 2350 3575 39  0000 L CNN
F 1 "20" H 2350 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2300 3625 50  0001 C CNN
F 3 "~" H 2300 3625 50  0001 C CNN
	1    2300 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	2300 3725 2300 3825
$Comp
L Device:R_Small R?
U 1 1 5F7A90BC
P 2850 3625
AR Path="/5F7A90BC" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7A90BC" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7A90BC" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7A90BC" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7A90BC" Ref="R38"  Part="1" 
F 0 "R38" H 2900 3575 39  0000 L CNN
F 1 "20" H 2900 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2850 3625 50  0001 C CNN
F 3 "~" H 2850 3625 50  0001 C CNN
	1    2850 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 3725 2850 3825
$Comp
L Device:R_Small R?
U 1 1 5F7AA37D
P 3400 3625
AR Path="/5F7AA37D" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AA37D" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AA37D" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7AA37D" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7AA37D" Ref="R39"  Part="1" 
F 0 "R39" H 3450 3575 39  0000 L CNN
F 1 "20" H 3450 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3400 3625 50  0001 C CNN
F 3 "~" H 3400 3625 50  0001 C CNN
	1    3400 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 3725 3400 3825
$Comp
L Device:R_Small R?
U 1 1 5F7AD1BF
P 3950 3625
AR Path="/5F7AD1BF" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AD1BF" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AD1BF" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7AD1BF" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7AD1BF" Ref="R40"  Part="1" 
F 0 "R40" H 4000 3575 39  0000 L CNN
F 1 "20" H 4000 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3950 3625 50  0001 C CNN
F 3 "~" H 3950 3625 50  0001 C CNN
	1    3950 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	3950 3725 3950 3825
$Comp
L Device:R_Small R?
U 1 1 5F7AD1C6
P 4500 3625
AR Path="/5F7AD1C6" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AD1C6" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AD1C6" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7AD1C6" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7AD1C6" Ref="R41"  Part="1" 
F 0 "R41" H 4550 3575 39  0000 L CNN
F 1 "20" H 4550 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4500 3625 50  0001 C CNN
F 3 "~" H 4500 3625 50  0001 C CNN
	1    4500 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 3725 4500 3825
$Comp
L Device:R_Small R?
U 1 1 5F80A6D2
P 5050 3625
AR Path="/5F80A6D2" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D2" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D2" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F80A6D2" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D2" Ref="R42"  Part="1" 
F 0 "R42" H 5100 3575 39  0000 L CNN
F 1 "20" H 5100 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5050 3625 50  0001 C CNN
F 3 "~" H 5050 3625 50  0001 C CNN
	1    5050 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	5050 3725 5050 3825
$Comp
L Device:R_Small R?
U 1 1 5F80A6D3
P 5600 3625
AR Path="/5F80A6D3" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D3" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D3" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F80A6D3" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D3" Ref="R43"  Part="1" 
F 0 "R43" H 5650 3575 39  0000 L CNN
F 1 "20" H 5650 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5600 3625 50  0001 C CNN
F 3 "~" H 5600 3625 50  0001 C CNN
	1    5600 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 3725 5600 3825
$Comp
L Device:R_Small R?
U 1 1 5F80A6D4
P 6150 3625
AR Path="/5F80A6D4" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F80A6D4" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F80A6D4" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F80A6D4" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F80A6D4" Ref="R44"  Part="1" 
F 0 "R44" H 6200 3575 39  0000 L CNN
F 1 "20" H 6200 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6150 3625 50  0001 C CNN
F 3 "~" H 6150 3625 50  0001 C CNN
	1    6150 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	6150 3725 6150 3825
$Comp
L Device:R_Small R?
U 1 1 5F7AFC05
P 6700 3625
AR Path="/5F7AFC05" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AFC05" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AFC05" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7AFC05" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7AFC05" Ref="R45"  Part="1" 
F 0 "R45" H 6750 3575 39  0000 L CNN
F 1 "20" H 6750 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6700 3625 50  0001 C CNN
F 3 "~" H 6700 3625 50  0001 C CNN
	1    6700 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 3725 6700 3825
$Comp
L Device:R_Small R?
U 1 1 5F7AFC0C
P 7250 3625
AR Path="/5F7AFC0C" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AFC0C" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AFC0C" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7AFC0C" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7AFC0C" Ref="R46"  Part="1" 
F 0 "R46" H 7300 3575 39  0000 L CNN
F 1 "20" H 7300 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7250 3625 50  0001 C CNN
F 3 "~" H 7250 3625 50  0001 C CNN
	1    7250 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 3725 7250 3825
$Comp
L Device:R_Small R?
U 1 1 5F7AFC13
P 7800 3625
AR Path="/5F7AFC13" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7AFC13" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7AFC13" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7AFC13" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7AFC13" Ref="R47"  Part="1" 
F 0 "R47" H 7850 3575 39  0000 L CNN
F 1 "20" H 7850 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7800 3625 50  0001 C CNN
F 3 "~" H 7800 3625 50  0001 C CNN
	1    7800 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	7800 3725 7800 3825
$Comp
L Device:R_Small R?
U 1 1 5F7B1DDB
P 8350 3625
AR Path="/5F7B1DDB" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7B1DDB" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7B1DDB" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7B1DDB" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7B1DDB" Ref="R48"  Part="1" 
F 0 "R48" H 8400 3575 39  0000 L CNN
F 1 "20" H 8400 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8350 3625 50  0001 C CNN
F 3 "~" H 8350 3625 50  0001 C CNN
	1    8350 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	8350 3725 8350 3825
$Comp
L Device:R_Small R?
U 1 1 5F7B1DE2
P 8900 3625
AR Path="/5F7B1DE2" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7B1DE2" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7B1DE2" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7B1DE2" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7B1DE2" Ref="R49"  Part="1" 
F 0 "R49" H 8950 3575 39  0000 L CNN
F 1 "20" H 8950 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8900 3625 50  0001 C CNN
F 3 "~" H 8900 3625 50  0001 C CNN
	1    8900 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	8900 3725 8900 3825
Wire Wire Line
	9450 3725 9450 3825
Wire Wire Line
	1350 3825 1350 4175
Wire Wire Line
	1350 3375 1350 3725
$Comp
L Device:LED_Small D?
U 1 1 5F7ABA2D
P 10000 3925
AR Path="/5F1D940F/5F7ABA2D" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7ABA2D" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7ABA2D" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F7ABA2D" Ref="D41"  Part="1" 
F 0 "D41" V 10050 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 10000 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 10000 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 10000 3925 50  0001 C CNN
	1    10000 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	10000 4025 10000 4175
Wire Wire Line
	10000 3375 10000 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7ABA35
P 10550 3925
AR Path="/5F1D940F/5F7ABA35" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7ABA35" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7ABA35" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F7ABA35" Ref="D42"  Part="1" 
F 0 "D42" V 10600 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 10550 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 10550 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 10550 3925 50  0001 C CNN
	1    10550 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	10550 4025 10550 4175
Wire Wire Line
	10550 3375 10550 3525
$Comp
L Device:LED_Small D?
U 1 1 5F7ABA3D
P 11100 3925
AR Path="/5F1D940F/5F7ABA3D" Ref="D?"  Part="1" 
AR Path="/5F1D49F5/5F7ABA3D" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7ABA3D" Ref="D?"  Part="1" 
AR Path="/5F7C0AF0/5F7ABA3D" Ref="D43"  Part="1" 
F 0 "D43" V 11150 3825 39  0000 R CNN
F 1 "IN-S126AT5UW" V 11100 3825 28  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" V 11100 3925 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Top%20View/IN-S126AT%20Series_V1.0.pdf" V 11100 3925 50  0001 C CNN
	1    11100 3925
	0    1    -1   0   
$EndComp
Wire Wire Line
	11100 4025 11100 4175
Wire Wire Line
	11100 3375 11100 3525
Wire Wire Line
	9450 3375 10000 3375
Wire Wire Line
	9450 4175 10000 4175
Connection ~ 10000 3375
Wire Wire Line
	10000 3375 10550 3375
Connection ~ 10000 4175
Wire Wire Line
	10000 4175 10550 4175
Connection ~ 10550 3375
Wire Wire Line
	10550 3375 11100 3375
Connection ~ 10550 4175
Wire Wire Line
	10550 4175 11100 4175
$Comp
L Device:R_Small R?
U 1 1 5F7ABA4F
P 10000 3625
AR Path="/5F7ABA4F" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7ABA4F" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7ABA4F" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7ABA4F" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7ABA4F" Ref="R51"  Part="1" 
F 0 "R51" H 10050 3575 39  0000 L CNN
F 1 "20" H 10050 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10000 3625 50  0001 C CNN
F 3 "~" H 10000 3625 50  0001 C CNN
	1    10000 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	10000 3725 10000 3825
$Comp
L Device:R_Small R?
U 1 1 5F7ABA56
P 10550 3625
AR Path="/5F7ABA56" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7ABA56" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7ABA56" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7ABA56" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7ABA56" Ref="R52"  Part="1" 
F 0 "R52" H 10600 3575 39  0000 L CNN
F 1 "20" H 10600 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10550 3625 50  0001 C CNN
F 3 "~" H 10550 3625 50  0001 C CNN
	1    10550 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	10550 3725 10550 3825
$Comp
L Device:R_Small R?
U 1 1 5F7ABA5D
P 11100 3625
AR Path="/5F7ABA5D" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7ABA5D" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7ABA5D" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7ABA5D" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7ABA5D" Ref="R53"  Part="1" 
F 0 "R53" H 11150 3575 39  0000 L CNN
F 1 "20" H 11150 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 11100 3625 50  0001 C CNN
F 3 "~" H 11100 3625 50  0001 C CNN
	1    11100 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	11100 3725 11100 3825
$Comp
L Device:R_Small R?
U 1 1 5F7B1DE9
P 9450 3625
AR Path="/5F7B1DE9" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7B1DE9" Ref="R?"  Part="1" 
AR Path="/5F1D49F5/5F7B1DE9" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7B1DE9" Ref="R?"  Part="1" 
AR Path="/5F7C0AF0/5F7B1DE9" Ref="R50"  Part="1" 
F 0 "R50" H 9500 3575 39  0000 L CNN
F 1 "20" H 9500 3625 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9450 3625 50  0001 C CNN
F 3 "~" H 9450 3625 50  0001 C CNN
	1    9450 3625
	-1   0    0    1   
$EndComp
$EndSCHEMATC
