# Stadium
The Stadium represents a power-hungry event-based consumer: has a moderate consumption during the training session, but during the a match it becomes significant. The event status can be changed via touch buttons. The circuitry includes an sub-block for LEDs lighting, a telemetry sub-block for monitoring the self power consumption in the 3.3 V bus and a sub-block for speaker control.

## Schematic
[<img src="docs/stadium_sch.png"  width="891" height="500">](docs/stadium_sch-main.png)

[<img src="docs/stadium_sch-sub_connectors.png"  width="145" height="100">](docs/stadium_sch-sub_connectors.png)
&nbsp;
[<img src="docs/stadium_sch-sub_granstand_a.png"  width="145" height="100">](docs/stadium_sch-sub_granstand_a.png)
&nbsp;
[<img src="docs/stadium_sch-sub_granstand_b.png"  width="145" height="100">](docs/stadium_sch-sub_granstand_b.png)
&nbsp;
[<img src="docs/stadium_sch-sub_lighting.png"  width="145" height="100">](docs/stadium_sch-sub_lighting.png)
&nbsp;
[<img src="docs/stadium_sch-sub_speaker.png"  width="145" height="100">](docs/stadium_sch-sub_speaker.png)
&nbsp;
[<img src="docs/stadium_sch-sub_telemetry.png"  width="145" height="100">](docs/stadium_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/stadium_sch-sub_mechanical.png"  width="145" height="100">](docs/stadium_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/stadium_pcb.png"  width="719" height="500">](docs/stadium_pcb-brd.png)

[<img src="docs/stadium_pcb-F_Cu.png"  width="145" height="100">](docs/stadium_pcb-F_Cu.png)
&nbsp;
[<img src="docs/stadium_pcb-In1_Cu.png"  width="145" height="100">](docs/stadium_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/stadium_pcb-In2_Cu.png"  width="145" height="100">](docs/stadium_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/stadium_pcb-B_Cu.png"  width="145" height="100">](docs/stadium_pcb-B_Cu.png)
&nbsp;
[<img src="docs/stadium_pcb-tech_drawing.png"  width="145" height="100">](docs/stadium_pcb-tech_drawing.png)

## Media

[<img src="docs/stadium_3d-top.png"  width="121" height="100">](docs/stadium_3d-top.png)
&nbsp;
[<img src="docs/stadium_3d-bottom.png"  width="120" height="100">](docs/stadium_3d-bottom.png)
&nbsp;
[<img src="docs/stadium_3d_lights-top.png"  width="115" height="100">](docs/stadium_3d_lights-top.png)
&nbsp;
[<img src="docs/stadium_3d_stand-top.png"  width="63" height="100">](docs/stadium_3d_stand-top.png)
&nbsp;
[<img src="docs/stadium_3d_stand-bottom.png"  width="63" height="100">](docs/stadium_3d_stand-bottom.png)
&nbsp;
[<img src="docs/stadium_brd.png"  width="193" height="100">](docs/stadium_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/stadium/stadium.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/stadium/stadium.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/stadium/stadium.md)