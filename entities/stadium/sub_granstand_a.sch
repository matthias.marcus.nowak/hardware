EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 8
Title "Sub Grandstand"
Date "2020-07-23"
Rev "1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED_Small D2
U 1 1 5F7EB34F
P 2650 3550
AR Path="/5F1D940F/5F7EB34F" Ref="D2"  Part="1" 
AR Path="/5F1D49F5/5F7EB34F" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EB34F" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EB34F" Ref="D2"  Part="1" 
AR Path="/5F7B83A2/5F7EB34F" Ref="D?"  Part="1" 
F 0 "D2" V 2700 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 2650 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 2650 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 2650 3550 50  0001 C CNN
	1    2650 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	2650 3650 2650 3800
Wire Wire Line
	2650 3800 2250 3800
Connection ~ 2650 3000
Wire Wire Line
	2650 3000 2650 3150
Connection ~ 2650 3800
Wire Wire Line
	2650 3800 3200 3800
$Comp
L Device:LED_Small D3
U 1 1 5F32E7D8
P 3200 3550
AR Path="/5F1D940F/5F32E7D8" Ref="D3"  Part="1" 
AR Path="/5F1D49F5/5F32E7D8" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F32E7D8" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F32E7D8" Ref="D3"  Part="1" 
AR Path="/5F7B83A2/5F32E7D8" Ref="D?"  Part="1" 
F 0 "D3" V 3250 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 3200 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 3200 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 3200 3550 50  0001 C CNN
	1    3200 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 3650 3200 3800
Wire Wire Line
	3200 3000 3200 3150
$Comp
L Device:LED_Small D4
U 1 1 5F32ECC6
P 3750 3550
AR Path="/5F1D940F/5F32ECC6" Ref="D4"  Part="1" 
AR Path="/5F1D49F5/5F32ECC6" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F32ECC6" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F32ECC6" Ref="D4"  Part="1" 
AR Path="/5F7B83A2/5F32ECC6" Ref="D?"  Part="1" 
F 0 "D4" V 3800 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 3750 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 3750 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 3750 3550 50  0001 C CNN
	1    3750 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	3750 3650 3750 3800
Wire Wire Line
	3750 3000 3750 3150
$Comp
L Device:LED_Small D5
U 1 1 5F7EB352
P 4300 3550
AR Path="/5F1D940F/5F7EB352" Ref="D5"  Part="1" 
AR Path="/5F1D49F5/5F7EB352" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EB352" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EB352" Ref="D5"  Part="1" 
AR Path="/5F7B83A2/5F7EB352" Ref="D?"  Part="1" 
F 0 "D5" V 4350 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 4300 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 4300 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 4300 3550 50  0001 C CNN
	1    4300 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	4300 3650 4300 3800
Wire Wire Line
	4300 3000 4300 3150
$Comp
L Device:LED_Small D6
U 1 1 5F7EAB1C
P 4850 3550
AR Path="/5F1D940F/5F7EAB1C" Ref="D6"  Part="1" 
AR Path="/5F1D49F5/5F7EAB1C" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB1C" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB1C" Ref="D6"  Part="1" 
AR Path="/5F7B83A2/5F7EAB1C" Ref="D?"  Part="1" 
F 0 "D6" V 4900 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 4850 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 4850 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 4850 3550 50  0001 C CNN
	1    4850 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	4850 3650 4850 3800
Wire Wire Line
	4850 3000 4850 3150
$Comp
L Device:LED_Small D7
U 1 1 5F7EB353
P 5400 3550
AR Path="/5F1D940F/5F7EB353" Ref="D7"  Part="1" 
AR Path="/5F1D49F5/5F7EB353" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EB353" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EB353" Ref="D7"  Part="1" 
AR Path="/5F7B83A2/5F7EB353" Ref="D?"  Part="1" 
F 0 "D7" V 5450 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 5400 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 5400 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 5400 3550 50  0001 C CNN
	1    5400 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	5400 3650 5400 3800
Wire Wire Line
	5400 3000 5400 3150
$Comp
L Device:LED_Small D8
U 1 1 5F7EAB1E
P 5950 3550
AR Path="/5F1D940F/5F7EAB1E" Ref="D8"  Part="1" 
AR Path="/5F1D49F5/5F7EAB1E" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB1E" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB1E" Ref="D8"  Part="1" 
AR Path="/5F7B83A2/5F7EAB1E" Ref="D?"  Part="1" 
F 0 "D8" V 6000 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 5950 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 5950 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 5950 3550 50  0001 C CNN
	1    5950 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	5950 3650 5950 3800
Wire Wire Line
	5950 3000 5950 3150
$Comp
L Device:LED_Small D9
U 1 1 5F7EB354
P 6500 3550
AR Path="/5F1D940F/5F7EB354" Ref="D9"  Part="1" 
AR Path="/5F1D49F5/5F7EB354" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EB354" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EB354" Ref="D9"  Part="1" 
AR Path="/5F7B83A2/5F7EB354" Ref="D?"  Part="1" 
F 0 "D9" V 6550 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 6500 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 6500 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 6500 3550 50  0001 C CNN
	1    6500 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	6500 3650 6500 3800
Wire Wire Line
	6500 3000 6500 3150
$Comp
L Device:LED_Small D10
U 1 1 5F7EB355
P 7050 3550
AR Path="/5F1D940F/5F7EB355" Ref="D10"  Part="1" 
AR Path="/5F1D49F5/5F7EB355" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EB355" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EB355" Ref="D10"  Part="1" 
AR Path="/5F7B83A2/5F7EB355" Ref="D?"  Part="1" 
F 0 "D10" V 7100 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 7050 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 7050 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 7050 3550 50  0001 C CNN
	1    7050 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	7050 3650 7050 3800
Wire Wire Line
	7050 3000 7050 3150
$Comp
L Device:LED_Small D11
U 1 1 5F333057
P 7600 3550
AR Path="/5F1D940F/5F333057" Ref="D11"  Part="1" 
AR Path="/5F1D49F5/5F333057" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F333057" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F333057" Ref="D11"  Part="1" 
AR Path="/5F7B83A2/5F333057" Ref="D?"  Part="1" 
F 0 "D11" V 7650 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 7600 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 7600 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 7600 3550 50  0001 C CNN
	1    7600 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	7600 3650 7600 3800
Wire Wire Line
	7600 3000 7600 3150
$Comp
L Device:LED_Small D12
U 1 1 5F335AE5
P 8150 3550
AR Path="/5F1D940F/5F335AE5" Ref="D12"  Part="1" 
AR Path="/5F1D49F5/5F335AE5" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F335AE5" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F335AE5" Ref="D12"  Part="1" 
AR Path="/5F7B83A2/5F335AE5" Ref="D?"  Part="1" 
F 0 "D12" V 8200 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 8150 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 8150 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 8150 3550 50  0001 C CNN
	1    8150 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	8150 3650 8150 3800
Wire Wire Line
	8150 3000 8150 3150
$Comp
L Device:LED_Small D13
U 1 1 5F7EAB23
P 8700 3550
AR Path="/5F1D940F/5F7EAB23" Ref="D13"  Part="1" 
AR Path="/5F1D49F5/5F7EAB23" Ref="D?"  Part="1" 
AR Path="/5F202C6C/5F7EAB23" Ref="D?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB23" Ref="D13"  Part="1" 
AR Path="/5F7B83A2/5F7EAB23" Ref="D?"  Part="1" 
F 0 "D13" V 8750 3450 39  0000 R CNN
F 1 "IN-S124ARUR" V 8700 3450 28  0000 R CNN
F 2 "LEGOS:LED_1204_3212Metric_ReverseMount_Hole1.8x2.4mm" V 8700 3550 50  0001 C CNN
F 3 "http://inolux-corp.com/datasheet/SMDLED/Mono%20Color%20Reverse%20Mount/IN-S124AR%20Series_V1.0.pdf" V 8700 3550 50  0001 C CNN
	1    8700 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	8700 3650 8700 3800
Wire Wire Line
	8700 3000 8700 3150
Text Notes 3200 2950 0    50   ~ 0
If = 12x 10 mA = 120 mA
Wire Wire Line
	2650 3000 3200 3000
Connection ~ 3200 3000
Wire Wire Line
	3200 3000 3750 3000
Connection ~ 3200 3800
Wire Wire Line
	3200 3800 3750 3800
Connection ~ 3750 3800
Wire Wire Line
	3750 3800 4300 3800
Connection ~ 3750 3000
Wire Wire Line
	3750 3000 4300 3000
Connection ~ 4300 3000
Wire Wire Line
	4300 3000 4850 3000
Connection ~ 4300 3800
Wire Wire Line
	4300 3800 4850 3800
Connection ~ 4850 3000
Wire Wire Line
	4850 3000 5400 3000
Connection ~ 4850 3800
Wire Wire Line
	4850 3800 5400 3800
Connection ~ 7600 3000
Wire Wire Line
	7600 3000 8150 3000
Connection ~ 7600 3800
Wire Wire Line
	7600 3800 8150 3800
Connection ~ 8150 3800
Wire Wire Line
	8150 3800 8700 3800
Connection ~ 8150 3000
Wire Wire Line
	8150 3000 8700 3000
Connection ~ 7050 3000
Wire Wire Line
	7050 3000 7600 3000
Connection ~ 6500 3000
Wire Wire Line
	6500 3000 7050 3000
Connection ~ 6500 3800
Wire Wire Line
	6500 3800 7050 3800
Connection ~ 7050 3800
Wire Wire Line
	7050 3800 7600 3800
Connection ~ 5400 3000
Wire Wire Line
	5400 3000 5950 3000
Connection ~ 5950 3000
Wire Wire Line
	5950 3000 6500 3000
Connection ~ 5950 3800
Wire Wire Line
	5950 3800 6500 3800
Connection ~ 5400 3800
Wire Wire Line
	5400 3800 5950 3800
Wire Wire Line
	2250 3000 2650 3000
Wire Wire Line
	2250 3350 2000 3350
Wire Wire Line
	2250 3450 2000 3450
Text HLabel 2000 3350 0    50   Input ~ 0
V+
Text HLabel 2000 3450 0    50   Input ~ 0
V-
$Comp
L Device:R_Small R?
U 1 1 5F7EAB27
P 2650 3250
AR Path="/5F7EAB27" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB27" Ref="R12"  Part="1" 
AR Path="/5F1D49F5/5F7EAB27" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB27" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB27" Ref="R12"  Part="1" 
AR Path="/5F7B83A2/5F7EAB27" Ref="R?"  Part="1" 
F 0 "R12" H 2700 3200 39  0000 L CNN
F 1 "120" H 2700 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2650 3250 50  0001 C CNN
F 3 "~" H 2650 3250 50  0001 C CNN
	1    2650 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 3350 2650 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB28
P 3200 3250
AR Path="/5F7EAB28" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB28" Ref="R13"  Part="1" 
AR Path="/5F1D49F5/5F7EAB28" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB28" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB28" Ref="R13"  Part="1" 
AR Path="/5F7B83A2/5F7EAB28" Ref="R?"  Part="1" 
F 0 "R13" H 3250 3200 39  0000 L CNN
F 1 "120" H 3250 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3200 3250 50  0001 C CNN
F 3 "~" H 3200 3250 50  0001 C CNN
	1    3200 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 3350 3200 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB29
P 3750 3250
AR Path="/5F7EAB29" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB29" Ref="R14"  Part="1" 
AR Path="/5F1D49F5/5F7EAB29" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB29" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB29" Ref="R14"  Part="1" 
AR Path="/5F7B83A2/5F7EAB29" Ref="R?"  Part="1" 
F 0 "R14" H 3800 3200 39  0000 L CNN
F 1 "120" H 3800 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3750 3250 50  0001 C CNN
F 3 "~" H 3750 3250 50  0001 C CNN
	1    3750 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3350 3750 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB2A
P 4300 3250
AR Path="/5F7EAB2A" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB2A" Ref="R15"  Part="1" 
AR Path="/5F1D49F5/5F7EAB2A" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB2A" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB2A" Ref="R15"  Part="1" 
AR Path="/5F7B83A2/5F7EAB2A" Ref="R?"  Part="1" 
F 0 "R15" H 4350 3200 39  0000 L CNN
F 1 "120" H 4350 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4300 3250 50  0001 C CNN
F 3 "~" H 4300 3250 50  0001 C CNN
	1    4300 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 3350 4300 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB2B
P 4850 3250
AR Path="/5F7EAB2B" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB2B" Ref="R16"  Part="1" 
AR Path="/5F1D49F5/5F7EAB2B" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB2B" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB2B" Ref="R16"  Part="1" 
AR Path="/5F7B83A2/5F7EAB2B" Ref="R?"  Part="1" 
F 0 "R16" H 4900 3200 39  0000 L CNN
F 1 "120" H 4900 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4850 3250 50  0001 C CNN
F 3 "~" H 4850 3250 50  0001 C CNN
	1    4850 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4850 3350 4850 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB2C
P 5400 3250
AR Path="/5F7EAB2C" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB2C" Ref="R17"  Part="1" 
AR Path="/5F1D49F5/5F7EAB2C" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB2C" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB2C" Ref="R17"  Part="1" 
AR Path="/5F7B83A2/5F7EAB2C" Ref="R?"  Part="1" 
F 0 "R17" H 5450 3200 39  0000 L CNN
F 1 "120" H 5450 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5400 3250 50  0001 C CNN
F 3 "~" H 5400 3250 50  0001 C CNN
	1    5400 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 3350 5400 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB2D
P 5950 3250
AR Path="/5F7EAB2D" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB2D" Ref="R18"  Part="1" 
AR Path="/5F1D49F5/5F7EAB2D" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB2D" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB2D" Ref="R18"  Part="1" 
AR Path="/5F7B83A2/5F7EAB2D" Ref="R?"  Part="1" 
F 0 "R18" H 6000 3200 39  0000 L CNN
F 1 "120" H 6000 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5950 3250 50  0001 C CNN
F 3 "~" H 5950 3250 50  0001 C CNN
	1    5950 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 3350 5950 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB2E
P 6500 3250
AR Path="/5F7EAB2E" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB2E" Ref="R19"  Part="1" 
AR Path="/5F1D49F5/5F7EAB2E" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB2E" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB2E" Ref="R19"  Part="1" 
AR Path="/5F7B83A2/5F7EAB2E" Ref="R?"  Part="1" 
F 0 "R19" H 6550 3200 39  0000 L CNN
F 1 "120" H 6550 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6500 3250 50  0001 C CNN
F 3 "~" H 6500 3250 50  0001 C CNN
	1    6500 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 3350 6500 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB2F
P 7050 3250
AR Path="/5F7EAB2F" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB2F" Ref="R20"  Part="1" 
AR Path="/5F1D49F5/5F7EAB2F" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB2F" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB2F" Ref="R20"  Part="1" 
AR Path="/5F7B83A2/5F7EAB2F" Ref="R?"  Part="1" 
F 0 "R20" H 7100 3200 39  0000 L CNN
F 1 "120" H 7100 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7050 3250 50  0001 C CNN
F 3 "~" H 7050 3250 50  0001 C CNN
	1    7050 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7050 3350 7050 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB30
P 7600 3250
AR Path="/5F7EAB30" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB30" Ref="R21"  Part="1" 
AR Path="/5F1D49F5/5F7EAB30" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB30" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB30" Ref="R21"  Part="1" 
AR Path="/5F7B83A2/5F7EAB30" Ref="R?"  Part="1" 
F 0 "R21" H 7650 3200 39  0000 L CNN
F 1 "120" H 7650 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7600 3250 50  0001 C CNN
F 3 "~" H 7600 3250 50  0001 C CNN
	1    7600 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7600 3350 7600 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB31
P 8150 3250
AR Path="/5F7EAB31" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB31" Ref="R22"  Part="1" 
AR Path="/5F1D49F5/5F7EAB31" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB31" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB31" Ref="R22"  Part="1" 
AR Path="/5F7B83A2/5F7EAB31" Ref="R?"  Part="1" 
F 0 "R22" H 8200 3200 39  0000 L CNN
F 1 "120" H 8200 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8150 3250 50  0001 C CNN
F 3 "~" H 8150 3250 50  0001 C CNN
	1    8150 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8150 3350 8150 3450
$Comp
L Device:R_Small R?
U 1 1 5F7EAB32
P 8700 3250
AR Path="/5F7EAB32" Ref="R?"  Part="1" 
AR Path="/5F1D940F/5F7EAB32" Ref="R23"  Part="1" 
AR Path="/5F1D49F5/5F7EAB32" Ref="R?"  Part="1" 
AR Path="/5F202C6C/5F7EAB32" Ref="R?"  Part="1" 
AR Path="/5F7B4C62/5F7EAB32" Ref="R23"  Part="1" 
AR Path="/5F7B83A2/5F7EAB32" Ref="R?"  Part="1" 
F 0 "R23" H 8750 3200 39  0000 L CNN
F 1 "120" H 8750 3250 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8700 3250 50  0001 C CNN
F 3 "~" H 8700 3250 50  0001 C CNN
	1    8700 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8700 3350 8700 3450
Wire Wire Line
	2250 3450 2250 3800
Wire Wire Line
	2250 3000 2250 3350
$EndSCHEMATC
