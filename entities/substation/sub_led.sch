EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Substation"
Date "2020-06-08"
Rev "1.3"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2750 6750 0    39   ~ 0
LED_R1
Text Label 2750 6850 0    39   ~ 0
LED_R2
Text Label 2750 6950 0    39   ~ 0
LED_R3
Text Label 2750 7050 0    39   ~ 0
LED_R4
Text Label 2750 7150 0    39   ~ 0
LED_R5
Text Label 2750 7250 0    39   ~ 0
LED_R6
Text HLabel 1500 6750 0    39   Input ~ 0
~EN_1
Text HLabel 1500 6850 0    39   Input ~ 0
~EN_2
Text HLabel 1500 6950 0    39   Input ~ 0
~EN_3
Text HLabel 1500 7050 0    39   Input ~ 0
~EN_4
Text HLabel 1500 7150 0    39   Input ~ 0
~EN_5
Text HLabel 1500 7250 0    39   Input ~ 0
~EN_6
Text Label 1700 6750 0    39   ~ 0
LED_G1
Text Label 1700 6850 0    39   ~ 0
LED_G2
Text Label 1700 6950 0    39   ~ 0
LED_G3
Text Label 1700 7050 0    39   ~ 0
LED_G4
Text Label 1700 7150 0    39   ~ 0
LED_G5
Text Label 1700 7250 0    39   ~ 0
LED_G6
Text HLabel 2550 6750 0    39   Input ~ 0
~IRQ_1
Text HLabel 2550 6850 0    39   Input ~ 0
~IRQ_2
Text HLabel 2550 6950 0    39   Input ~ 0
~IRQ_3
Text HLabel 2550 7050 0    39   Input ~ 0
~IRQ_4
Text HLabel 2550 7150 0    39   Input ~ 0
~IRQ_5
Text HLabel 2550 7250 0    39   Input ~ 0
~IRQ_6
$Comp
L power:GND #PWR038
U 1 1 5ED9B7BA
P 4100 2750
AR Path="/5ED8FE6A/5ED9B7BA" Ref="#PWR038"  Part="1" 
AR Path="/5EDBB486/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5ED9B7BA" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5ED9B7BA" Ref="#PWR?"  Part="1" 
F 0 "#PWR038" H 4100 2500 50  0001 C CNN
F 1 "GND" H 4105 2577 39  0000 C CNN
F 2 "" H 4100 2750 50  0001 C CNN
F 3 "" H 4100 2750 50  0001 C CNN
	1    4100 2750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4100 2750 4100 2650
Wire Wire Line
	4100 2350 4100 2450
$Comp
L power:GND #PWR034
U 1 1 5F21C9FD
P 3900 2750
AR Path="/5ED8FE6A/5F21C9FD" Ref="#PWR034"  Part="1" 
AR Path="/5EDBB486/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F21C9FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F21C9FD" Ref="#PWR?"  Part="1" 
F 0 "#PWR034" H 3900 2500 50  0001 C CNN
F 1 "GND" H 3905 2577 39  0000 C CNN
F 2 "" H 3900 2750 50  0001 C CNN
F 3 "" H 3900 2750 50  0001 C CNN
	1    3900 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1950 3900 2050
Wire Wire Line
	3900 2450 3900 2350
Wire Wire Line
	3900 2750 3900 2650
$Comp
L Device:Q_PMOS_GSD Q7
U 1 1 5F48FFA4
P 3800 1750
F 0 "Q7" H 3750 1550 39  0000 L CNN
F 1 "Si2301BDS" H 3550 1600 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4000 1850 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 3800 1750 50  0001 C CNN
	1    3800 1750
	1    0    0    1   
$EndComp
Wire Wire Line
	3150 1750 3450 1750
Wire Wire Line
	3450 1750 3600 1750
Connection ~ 3450 1750
$Comp
L Device:R_Small R8
U 1 1 5EDB8114
P 3450 1600
AR Path="/5ED8FE6A/5EDB8114" Ref="R8"  Part="1" 
AR Path="/5EDBB486/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5EDB8114" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5EDB8114" Ref="R?"  Part="1" 
F 0 "R8" H 3509 1646 39  0000 L CNN
F 1 "220k" H 3509 1555 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3450 1600 50  0001 C CNN
F 3 "~" H 3450 1600 50  0001 C CNN
	1    3450 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3450 1700 3450 1750
$Comp
L power:+3.3V #PWR031
U 1 1 5F4A4370
P 3450 1450
F 0 "#PWR031" H 3450 1300 50  0001 C CNN
F 1 "+3.3V" H 3465 1615 39  0000 C CNN
F 2 "" H 3450 1450 50  0001 C CNN
F 3 "" H 3450 1450 50  0001 C CNN
	1    3450 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1450 3450 1500
Text Label 3150 1750 0    39   ~ 0
LED_R1
$Comp
L power:+3.3V #PWR033
U 1 1 5F4845F6
P 3900 1450
F 0 "#PWR033" H 3900 1300 50  0001 C CNN
F 1 "+3.3V" H 3915 1615 39  0000 C CNN
F 2 "" H 3900 1450 50  0001 C CNN
F 3 "" H 3900 1450 50  0001 C CNN
	1    3900 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1450 3900 1550
$Comp
L power:+3.3V #PWR037
U 1 1 5F4A85CD
P 4100 1450
F 0 "#PWR037" H 4100 1300 50  0001 C CNN
F 1 "+3.3V" H 4115 1615 39  0000 C CNN
F 2 "" H 4100 1450 50  0001 C CNN
F 3 "" H 4100 1450 50  0001 C CNN
	1    4100 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 5F4B951C
P 3900 2550
AR Path="/5ED8FE6A/5F4B951C" Ref="R10"  Part="1" 
AR Path="/5EDBB486/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F4B951C" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F4B951C" Ref="R?"  Part="1" 
F 0 "R10" H 3959 2596 39  0000 L CNN
F 1 "140" H 3959 2505 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3900 2550 50  0001 C CNN
F 3 "~" H 3900 2550 50  0001 C CNN
	1    3900 2550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R12
U 1 1 5F4BAAF9
P 4100 2550
AR Path="/5ED8FE6A/5F4BAAF9" Ref="R12"  Part="1" 
AR Path="/5EDBB486/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F4BAAF9" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F4BAAF9" Ref="R?"  Part="1" 
F 0 "R12" H 4159 2596 39  0000 L CNN
F 1 "130" H 4159 2505 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4100 2550 50  0001 C CNN
F 3 "~" H 4100 2550 50  0001 C CNN
	1    4100 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q9
U 1 1 5F4D857F
P 4200 1750
F 0 "Q9" H 4150 1550 39  0000 L CNN
F 1 "Si2301BDS" H 3900 1600 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4400 1850 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 4200 1750 50  0001 C CNN
	1    4200 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 1450 4100 1550
Wire Wire Line
	4100 1950 4100 2050
Wire Wire Line
	4850 1750 4550 1750
Wire Wire Line
	4550 1750 4400 1750
Connection ~ 4550 1750
$Comp
L Device:R_Small R14
U 1 1 5F4EF6D0
P 4550 1600
AR Path="/5ED8FE6A/5F4EF6D0" Ref="R14"  Part="1" 
AR Path="/5EDBB486/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F4EF6D0" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F4EF6D0" Ref="R?"  Part="1" 
F 0 "R14" H 4609 1646 39  0000 L CNN
F 1 "220k" H 4609 1555 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4550 1600 50  0001 C CNN
F 3 "~" H 4550 1600 50  0001 C CNN
	1    4550 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1700 4550 1750
$Comp
L power:+3.3V #PWR041
U 1 1 5F4EF6D7
P 4550 1450
F 0 "#PWR041" H 4550 1300 50  0001 C CNN
F 1 "+3.3V" H 4565 1615 39  0000 C CNN
F 2 "" H 4550 1450 50  0001 C CNN
F 3 "" H 4550 1450 50  0001 C CNN
	1    4550 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4550 1450 4550 1500
Text Label 4850 1750 2    39   ~ 0
LED_G1
Wire Wire Line
	6400 1950 6400 2050
$Comp
L Device:Q_PMOS_GSD Q11
U 1 1 5F50DC64
P 6300 1750
F 0 "Q11" H 6250 1550 39  0000 L CNN
F 1 "Si2301BDS" H 6050 1600 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6500 1850 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 6300 1750 50  0001 C CNN
	1    6300 1750
	1    0    0    1   
$EndComp
Wire Wire Line
	5650 1750 5950 1750
Wire Wire Line
	5950 1750 6100 1750
Connection ~ 5950 1750
$Comp
L Device:R_Small R16
U 1 1 5F50DC6D
P 5950 1600
AR Path="/5ED8FE6A/5F50DC6D" Ref="R16"  Part="1" 
AR Path="/5EDBB486/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F50DC6D" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F50DC6D" Ref="R?"  Part="1" 
F 0 "R16" H 6009 1646 39  0000 L CNN
F 1 "220k" H 6009 1555 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5950 1600 50  0001 C CNN
F 3 "~" H 5950 1600 50  0001 C CNN
	1    5950 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5950 1700 5950 1750
$Comp
L power:+3.3V #PWR043
U 1 1 5F50DC74
P 5950 1450
F 0 "#PWR043" H 5950 1300 50  0001 C CNN
F 1 "+3.3V" H 5965 1615 39  0000 C CNN
F 2 "" H 5950 1450 50  0001 C CNN
F 3 "" H 5950 1450 50  0001 C CNN
	1    5950 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1450 5950 1500
Text Label 5650 1750 0    39   ~ 0
LED_R2
$Comp
L power:+3.3V #PWR045
U 1 1 5F50DC7C
P 6400 1450
F 0 "#PWR045" H 6400 1300 50  0001 C CNN
F 1 "+3.3V" H 6415 1615 39  0000 C CNN
F 2 "" H 6400 1450 50  0001 C CNN
F 3 "" H 6400 1450 50  0001 C CNN
	1    6400 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 1450 6400 1550
$Comp
L power:+3.3V #PWR049
U 1 1 5F50DC83
P 6600 1450
F 0 "#PWR049" H 6600 1300 50  0001 C CNN
F 1 "+3.3V" H 6615 1615 39  0000 C CNN
F 2 "" H 6600 1450 50  0001 C CNN
F 3 "" H 6600 1450 50  0001 C CNN
	1    6600 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q13
U 1 1 5F50DC95
P 6700 1750
F 0 "Q13" H 6600 1550 39  0000 L CNN
F 1 "Si2301BDS" H 6400 1600 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6900 1850 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 6700 1750 50  0001 C CNN
	1    6700 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 1450 6600 1550
Wire Wire Line
	6600 1950 6600 2050
Wire Wire Line
	7350 1750 7050 1750
Wire Wire Line
	7050 1750 6900 1750
Connection ~ 7050 1750
$Comp
L Device:R_Small R22
U 1 1 5F50DCA0
P 7050 1600
AR Path="/5ED8FE6A/5F50DCA0" Ref="R22"  Part="1" 
AR Path="/5EDBB486/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F50DCA0" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F50DCA0" Ref="R?"  Part="1" 
F 0 "R22" H 7109 1646 39  0000 L CNN
F 1 "220k" H 7109 1555 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7050 1600 50  0001 C CNN
F 3 "~" H 7050 1600 50  0001 C CNN
	1    7050 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1700 7050 1750
$Comp
L power:+3.3V #PWR053
U 1 1 5F50DCA7
P 7050 1450
F 0 "#PWR053" H 7050 1300 50  0001 C CNN
F 1 "+3.3V" H 7065 1615 39  0000 C CNN
F 2 "" H 7050 1450 50  0001 C CNN
F 3 "" H 7050 1450 50  0001 C CNN
	1    7050 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7050 1450 7050 1500
Text Label 7350 1750 2    39   ~ 0
LED_G2
Wire Wire Line
	8900 1950 8900 2050
$Comp
L Device:Q_PMOS_GSD Q15
U 1 1 5F51532D
P 8800 1750
F 0 "Q15" H 8750 1550 39  0000 L CNN
F 1 "Si2301BDS" H 8550 1600 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9000 1850 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 8800 1750 50  0001 C CNN
	1    8800 1750
	1    0    0    1   
$EndComp
Wire Wire Line
	8150 1750 8450 1750
Wire Wire Line
	8450 1750 8600 1750
Connection ~ 8450 1750
$Comp
L Device:R_Small R24
U 1 1 5F515336
P 8450 1600
AR Path="/5ED8FE6A/5F515336" Ref="R24"  Part="1" 
AR Path="/5EDBB486/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F515336" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F515336" Ref="R?"  Part="1" 
AR Path="/5F515336" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F515336" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F515336" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F515336" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F515336" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F515336" Ref="R?"  Part="1" 
F 0 "R24" H 8509 1646 39  0000 L CNN
F 1 "220k" H 8509 1555 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8450 1600 50  0001 C CNN
F 3 "~" H 8450 1600 50  0001 C CNN
	1    8450 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8450 1700 8450 1750
$Comp
L power:+3.3V #PWR055
U 1 1 5F51533D
P 8450 1450
F 0 "#PWR055" H 8450 1300 50  0001 C CNN
F 1 "+3.3V" H 8465 1615 39  0000 C CNN
F 2 "" H 8450 1450 50  0001 C CNN
F 3 "" H 8450 1450 50  0001 C CNN
	1    8450 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 1450 8450 1500
Text Label 8150 1750 0    39   ~ 0
LED_R3
$Comp
L power:+3.3V #PWR057
U 1 1 5F515345
P 8900 1450
F 0 "#PWR057" H 8900 1300 50  0001 C CNN
F 1 "+3.3V" H 8915 1615 39  0000 C CNN
F 2 "" H 8900 1450 50  0001 C CNN
F 3 "" H 8900 1450 50  0001 C CNN
	1    8900 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 1450 8900 1550
$Comp
L power:+3.3V #PWR061
U 1 1 5F51534C
P 9100 1450
F 0 "#PWR061" H 9100 1300 50  0001 C CNN
F 1 "+3.3V" H 9115 1615 39  0000 C CNN
F 2 "" H 9100 1450 50  0001 C CNN
F 3 "" H 9100 1450 50  0001 C CNN
	1    9100 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q17
U 1 1 5F51535E
P 9200 1750
F 0 "Q17" H 9100 1550 39  0000 L CNN
F 1 "Si2301BDS" H 8900 1600 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9400 1850 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 9200 1750 50  0001 C CNN
	1    9200 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	9100 1450 9100 1550
Wire Wire Line
	9100 1950 9100 2050
Wire Wire Line
	9850 1750 9550 1750
Wire Wire Line
	9550 1750 9400 1750
Connection ~ 9550 1750
$Comp
L Device:R_Small R30
U 1 1 5F515369
P 9550 1600
AR Path="/5ED8FE6A/5F515369" Ref="R30"  Part="1" 
AR Path="/5EDBB486/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F515369" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F515369" Ref="R?"  Part="1" 
AR Path="/5F515369" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F515369" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F515369" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F515369" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F515369" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F515369" Ref="R?"  Part="1" 
F 0 "R30" H 9609 1646 39  0000 L CNN
F 1 "220k" H 9609 1555 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9550 1600 50  0001 C CNN
F 3 "~" H 9550 1600 50  0001 C CNN
	1    9550 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 1700 9550 1750
$Comp
L power:+3.3V #PWR065
U 1 1 5F515370
P 9550 1450
F 0 "#PWR065" H 9550 1300 50  0001 C CNN
F 1 "+3.3V" H 9565 1615 39  0000 C CNN
F 2 "" H 9550 1450 50  0001 C CNN
F 3 "" H 9550 1450 50  0001 C CNN
	1    9550 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9550 1450 9550 1500
Text Label 9850 1750 2    39   ~ 0
LED_G3
Wire Wire Line
	6400 4450 6400 4550
$Comp
L Device:Q_PMOS_GSD Q12
U 1 1 5F530044
P 6300 4250
F 0 "Q12" H 6250 4050 39  0000 L CNN
F 1 "Si2301BDS" H 6050 4100 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6500 4350 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 6300 4250 50  0001 C CNN
	1    6300 4250
	1    0    0    1   
$EndComp
Wire Wire Line
	5650 4250 5950 4250
Wire Wire Line
	5950 4250 6100 4250
Connection ~ 5950 4250
$Comp
L Device:R_Small R17
U 1 1 5F53004D
P 5950 4100
AR Path="/5ED8FE6A/5F53004D" Ref="R17"  Part="1" 
AR Path="/5EDBB486/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F53004D" Ref="R?"  Part="1" 
AR Path="/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F53004D" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F53004D" Ref="R?"  Part="1" 
F 0 "R17" H 6009 4146 39  0000 L CNN
F 1 "220k" H 6009 4055 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5950 4100 50  0001 C CNN
F 3 "~" H 5950 4100 50  0001 C CNN
	1    5950 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5950 4200 5950 4250
$Comp
L power:+3.3V #PWR044
U 1 1 5F530054
P 5950 3950
F 0 "#PWR044" H 5950 3800 50  0001 C CNN
F 1 "+3.3V" H 5965 4115 39  0000 C CNN
F 2 "" H 5950 3950 50  0001 C CNN
F 3 "" H 5950 3950 50  0001 C CNN
	1    5950 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3950 5950 4000
Text Label 5650 4250 0    39   ~ 0
LED_R5
$Comp
L power:+3.3V #PWR047
U 1 1 5F53005C
P 6400 3950
F 0 "#PWR047" H 6400 3800 50  0001 C CNN
F 1 "+3.3V" H 6415 4115 39  0000 C CNN
F 2 "" H 6400 3950 50  0001 C CNN
F 3 "" H 6400 3950 50  0001 C CNN
	1    6400 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3950 6400 4050
$Comp
L power:+3.3V #PWR051
U 1 1 5F530063
P 6600 3950
F 0 "#PWR051" H 6600 3800 50  0001 C CNN
F 1 "+3.3V" H 6615 4115 39  0000 C CNN
F 2 "" H 6600 3950 50  0001 C CNN
F 3 "" H 6600 3950 50  0001 C CNN
	1    6600 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q14
U 1 1 5F530075
P 6700 4250
F 0 "Q14" H 6600 4050 39  0000 L CNN
F 1 "Si2301BDS" H 6400 4100 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6900 4350 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 6700 4250 50  0001 C CNN
	1    6700 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 3950 6600 4050
Wire Wire Line
	6600 4450 6600 4550
Wire Wire Line
	7350 4250 7050 4250
Wire Wire Line
	7050 4250 6900 4250
Connection ~ 7050 4250
$Comp
L Device:R_Small R23
U 1 1 5F530080
P 7050 4100
AR Path="/5ED8FE6A/5F530080" Ref="R23"  Part="1" 
AR Path="/5EDBB486/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F530080" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F530080" Ref="R?"  Part="1" 
AR Path="/5F530080" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F530080" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F530080" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F530080" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F530080" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F530080" Ref="R?"  Part="1" 
F 0 "R23" H 7109 4146 39  0000 L CNN
F 1 "220k" H 7109 4055 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7050 4100 50  0001 C CNN
F 3 "~" H 7050 4100 50  0001 C CNN
	1    7050 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4200 7050 4250
$Comp
L power:+3.3V #PWR054
U 1 1 5F530087
P 7050 3950
F 0 "#PWR054" H 7050 3800 50  0001 C CNN
F 1 "+3.3V" H 7065 4115 39  0000 C CNN
F 2 "" H 7050 3950 50  0001 C CNN
F 3 "" H 7050 3950 50  0001 C CNN
	1    7050 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7050 3950 7050 4000
Text Label 7350 4250 2    39   ~ 0
LED_G5
Wire Wire Line
	8900 4450 8900 4550
$Comp
L Device:Q_PMOS_GSD Q16
U 1 1 5F537CA7
P 8800 4250
F 0 "Q16" H 8750 4050 39  0000 L CNN
F 1 "Si2301BDS" H 8550 4100 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9000 4350 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 8800 4250 50  0001 C CNN
	1    8800 4250
	1    0    0    1   
$EndComp
Wire Wire Line
	8150 4250 8450 4250
Wire Wire Line
	8450 4250 8600 4250
Connection ~ 8450 4250
$Comp
L Device:R_Small R25
U 1 1 5F537CB0
P 8450 4100
AR Path="/5ED8FE6A/5F537CB0" Ref="R25"  Part="1" 
AR Path="/5EDBB486/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F537CB0" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F537CB0" Ref="R?"  Part="1" 
F 0 "R25" H 8509 4146 39  0000 L CNN
F 1 "220k" H 8509 4055 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8450 4100 50  0001 C CNN
F 3 "~" H 8450 4100 50  0001 C CNN
	1    8450 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8450 4200 8450 4250
$Comp
L power:+3.3V #PWR056
U 1 1 5F537CB7
P 8450 3950
F 0 "#PWR056" H 8450 3800 50  0001 C CNN
F 1 "+3.3V" H 8465 4115 39  0000 C CNN
F 2 "" H 8450 3950 50  0001 C CNN
F 3 "" H 8450 3950 50  0001 C CNN
	1    8450 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3950 8450 4000
Text Label 8150 4250 0    39   ~ 0
LED_R6
$Comp
L power:+3.3V #PWR059
U 1 1 5F537CBF
P 8900 3950
F 0 "#PWR059" H 8900 3800 50  0001 C CNN
F 1 "+3.3V" H 8915 4115 39  0000 C CNN
F 2 "" H 8900 3950 50  0001 C CNN
F 3 "" H 8900 3950 50  0001 C CNN
	1    8900 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 3950 8900 4050
$Comp
L power:+3.3V #PWR063
U 1 1 5F537CC6
P 9100 3950
F 0 "#PWR063" H 9100 3800 50  0001 C CNN
F 1 "+3.3V" H 9115 4115 39  0000 C CNN
F 2 "" H 9100 3950 50  0001 C CNN
F 3 "" H 9100 3950 50  0001 C CNN
	1    9100 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q18
U 1 1 5F537CD8
P 9200 4250
F 0 "Q18" H 9100 4050 39  0000 L CNN
F 1 "Si2301BDS" H 8900 4100 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9400 4350 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 9200 4250 50  0001 C CNN
	1    9200 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	9100 3950 9100 4050
Wire Wire Line
	9100 4450 9100 4550
Wire Wire Line
	9850 4250 9550 4250
Wire Wire Line
	9550 4250 9400 4250
Connection ~ 9550 4250
$Comp
L Device:R_Small R31
U 1 1 5F537CE3
P 9550 4100
AR Path="/5ED8FE6A/5F537CE3" Ref="R31"  Part="1" 
AR Path="/5EDBB486/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F537CE3" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F537CE3" Ref="R?"  Part="1" 
F 0 "R31" H 9609 4146 39  0000 L CNN
F 1 "220k" H 9609 4055 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9550 4100 50  0001 C CNN
F 3 "~" H 9550 4100 50  0001 C CNN
	1    9550 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 4200 9550 4250
$Comp
L power:+3.3V #PWR066
U 1 1 5F537CEA
P 9550 3950
F 0 "#PWR066" H 9550 3800 50  0001 C CNN
F 1 "+3.3V" H 9565 4115 39  0000 C CNN
F 2 "" H 9550 3950 50  0001 C CNN
F 3 "" H 9550 3950 50  0001 C CNN
	1    9550 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9550 3950 9550 4000
Text Label 9850 4250 2    39   ~ 0
LED_G6
Wire Wire Line
	3900 4450 3900 4550
$Comp
L Device:Q_PMOS_GSD Q8
U 1 1 5F53FD37
P 3800 4250
F 0 "Q8" H 3750 4050 39  0000 L CNN
F 1 "Si2301BDS" H 3550 4100 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4000 4350 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 3800 4250 50  0001 C CNN
	1    3800 4250
	1    0    0    1   
$EndComp
Wire Wire Line
	3150 4250 3450 4250
Wire Wire Line
	3450 4250 3600 4250
Connection ~ 3450 4250
$Comp
L Device:R_Small R9
U 1 1 5F53FD40
P 3450 4100
AR Path="/5ED8FE6A/5F53FD40" Ref="R9"  Part="1" 
AR Path="/5EDBB486/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F53FD40" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F53FD40" Ref="R?"  Part="1" 
F 0 "R9" H 3509 4146 39  0000 L CNN
F 1 "220k" H 3509 4055 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3450 4100 50  0001 C CNN
F 3 "~" H 3450 4100 50  0001 C CNN
	1    3450 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3450 4200 3450 4250
$Comp
L power:+3.3V #PWR032
U 1 1 5F53FD47
P 3450 3950
F 0 "#PWR032" H 3450 3800 50  0001 C CNN
F 1 "+3.3V" H 3465 4115 39  0000 C CNN
F 2 "" H 3450 3950 50  0001 C CNN
F 3 "" H 3450 3950 50  0001 C CNN
	1    3450 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3950 3450 4000
Text Label 3150 4250 0    39   ~ 0
LED_R4
$Comp
L power:+3.3V #PWR035
U 1 1 5F53FD4F
P 3900 3950
F 0 "#PWR035" H 3900 3800 50  0001 C CNN
F 1 "+3.3V" H 3915 4115 39  0000 C CNN
F 2 "" H 3900 3950 50  0001 C CNN
F 3 "" H 3900 3950 50  0001 C CNN
	1    3900 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3950 3900 4050
$Comp
L power:+3.3V #PWR039
U 1 1 5F53FD56
P 4100 3950
F 0 "#PWR039" H 4100 3800 50  0001 C CNN
F 1 "+3.3V" H 4115 4115 39  0000 C CNN
F 2 "" H 4100 3950 50  0001 C CNN
F 3 "" H 4100 3950 50  0001 C CNN
	1    4100 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q10
U 1 1 5F53FD68
P 4200 4250
F 0 "Q10" H 4100 4050 39  0000 L CNN
F 1 "Si2301BDS" H 3900 4100 39  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4400 4350 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/427/si2301bds-1765637.pdf" H 4200 4250 50  0001 C CNN
	1    4200 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 3950 4100 4050
Wire Wire Line
	4100 4450 4100 4550
Wire Wire Line
	4850 4250 4550 4250
Wire Wire Line
	4550 4250 4400 4250
Connection ~ 4550 4250
$Comp
L Device:R_Small R15
U 1 1 5F53FD73
P 4550 4100
AR Path="/5ED8FE6A/5F53FD73" Ref="R15"  Part="1" 
AR Path="/5EDBB486/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F53FD73" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F53FD73" Ref="R?"  Part="1" 
F 0 "R15" H 4609 4146 39  0000 L CNN
F 1 "220k" H 4609 4055 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4550 4100 50  0001 C CNN
F 3 "~" H 4550 4100 50  0001 C CNN
	1    4550 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4200 4550 4250
$Comp
L power:+3.3V #PWR042
U 1 1 5F53FD7A
P 4550 3950
F 0 "#PWR042" H 4550 3800 50  0001 C CNN
F 1 "+3.3V" H 4565 4115 39  0000 C CNN
F 2 "" H 4550 3950 50  0001 C CNN
F 3 "" H 4550 3950 50  0001 C CNN
	1    4550 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4550 3950 4550 4000
Text Label 4850 4250 2    39   ~ 0
LED_G4
$Comp
L Device:LED D1
U 1 1 5F65BAE3
P 3900 2200
F 0 "D1" V 3900 2350 39  0000 R CNN
F 1 "WP513IDT" V 3850 2550 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 3900 2200 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513IDT-79442.pdf" H 3900 2200 50  0001 C CNN
	1    3900 2200
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D3
U 1 1 5F65DE8B
P 4100 2200
F 0 "D3" V 4100 2100 39  0000 R CNN
F 1 "WP513GDT" V 4050 2100 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 4100 2200 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513GDT-78019.pdf" H 4100 2200 50  0001 C CNN
	1    4100 2200
	0    -1   -1   0   
$EndComp
Text Notes 4350 2600 0    50   ~ 0
If = 10 mA
$Comp
L power:GND #PWR050
U 1 1 5F69C8FB
P 6600 2750
AR Path="/5ED8FE6A/5F69C8FB" Ref="#PWR050"  Part="1" 
AR Path="/5EDBB486/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F69C8FB" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F69C8FB" Ref="#PWR?"  Part="1" 
F 0 "#PWR050" H 6600 2500 50  0001 C CNN
F 1 "GND" H 6605 2577 39  0000 C CNN
F 2 "" H 6600 2750 50  0001 C CNN
F 3 "" H 6600 2750 50  0001 C CNN
	1    6600 2750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6600 2750 6600 2650
Wire Wire Line
	6600 2350 6600 2450
$Comp
L power:GND #PWR046
U 1 1 5F69C903
P 6400 2750
AR Path="/5ED8FE6A/5F69C903" Ref="#PWR046"  Part="1" 
AR Path="/5EDBB486/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F69C903" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F69C903" Ref="#PWR?"  Part="1" 
F 0 "#PWR046" H 6400 2500 50  0001 C CNN
F 1 "GND" H 6405 2577 39  0000 C CNN
F 2 "" H 6400 2750 50  0001 C CNN
F 3 "" H 6400 2750 50  0001 C CNN
	1    6400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2450 6400 2350
Wire Wire Line
	6400 2750 6400 2650
$Comp
L Device:R_Small R18
U 1 1 5F69C90B
P 6400 2550
AR Path="/5ED8FE6A/5F69C90B" Ref="R18"  Part="1" 
AR Path="/5EDBB486/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F69C90B" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F69C90B" Ref="R?"  Part="1" 
F 0 "R18" H 6459 2596 39  0000 L CNN
F 1 "140" H 6459 2505 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6400 2550 50  0001 C CNN
F 3 "~" H 6400 2550 50  0001 C CNN
	1    6400 2550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R20
U 1 1 5F69C911
P 6600 2550
AR Path="/5ED8FE6A/5F69C911" Ref="R20"  Part="1" 
AR Path="/5EDBB486/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F69C911" Ref="R?"  Part="1" 
AR Path="/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F69C911" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F69C911" Ref="R?"  Part="1" 
F 0 "R20" H 6659 2596 39  0000 L CNN
F 1 "130" H 6659 2505 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6600 2550 50  0001 C CNN
F 3 "~" H 6600 2550 50  0001 C CNN
	1    6600 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 5F69C917
P 6400 2200
F 0 "D5" V 6400 2350 39  0000 R CNN
F 1 "WP513IDT" V 6350 2550 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 6400 2200 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513IDT-79442.pdf" H 6400 2200 50  0001 C CNN
	1    6400 2200
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 5F69C91D
P 6600 2200
F 0 "D7" V 6600 2100 39  0000 R CNN
F 1 "WP513GDT" V 6550 2100 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 6600 2200 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513GDT-78019.pdf" H 6600 2200 50  0001 C CNN
	1    6600 2200
	0    -1   -1   0   
$EndComp
Text Notes 6850 2600 0    50   ~ 0
If = 10 mA
$Comp
L power:GND #PWR062
U 1 1 5F6A386C
P 9100 2750
AR Path="/5ED8FE6A/5F6A386C" Ref="#PWR062"  Part="1" 
AR Path="/5EDBB486/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6A386C" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6A386C" Ref="#PWR?"  Part="1" 
F 0 "#PWR062" H 9100 2500 50  0001 C CNN
F 1 "GND" H 9105 2577 39  0000 C CNN
F 2 "" H 9100 2750 50  0001 C CNN
F 3 "" H 9100 2750 50  0001 C CNN
	1    9100 2750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9100 2750 9100 2650
Wire Wire Line
	9100 2350 9100 2450
$Comp
L power:GND #PWR058
U 1 1 5F6A3874
P 8900 2750
AR Path="/5ED8FE6A/5F6A3874" Ref="#PWR058"  Part="1" 
AR Path="/5EDBB486/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6A3874" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6A3874" Ref="#PWR?"  Part="1" 
F 0 "#PWR058" H 8900 2500 50  0001 C CNN
F 1 "GND" H 8905 2577 39  0000 C CNN
F 2 "" H 8900 2750 50  0001 C CNN
F 3 "" H 8900 2750 50  0001 C CNN
	1    8900 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2450 8900 2350
Wire Wire Line
	8900 2750 8900 2650
$Comp
L Device:R_Small R26
U 1 1 5F6A387C
P 8900 2550
AR Path="/5ED8FE6A/5F6A387C" Ref="R26"  Part="1" 
AR Path="/5EDBB486/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6A387C" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6A387C" Ref="R?"  Part="1" 
F 0 "R26" H 8959 2596 39  0000 L CNN
F 1 "140" H 8959 2505 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8900 2550 50  0001 C CNN
F 3 "~" H 8900 2550 50  0001 C CNN
	1    8900 2550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R28
U 1 1 5F6A3882
P 9100 2550
AR Path="/5ED8FE6A/5F6A3882" Ref="R28"  Part="1" 
AR Path="/5EDBB486/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6A3882" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6A3882" Ref="R?"  Part="1" 
F 0 "R28" H 9159 2596 39  0000 L CNN
F 1 "130" H 9159 2505 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9100 2550 50  0001 C CNN
F 3 "~" H 9100 2550 50  0001 C CNN
	1    9100 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D9
U 1 1 5F6A3888
P 8900 2200
F 0 "D9" V 8900 2350 39  0000 R CNN
F 1 "WP513IDT" V 8850 2550 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 8900 2200 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513IDT-79442.pdf" H 8900 2200 50  0001 C CNN
	1    8900 2200
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D11
U 1 1 5F6A388E
P 9100 2200
F 0 "D11" V 9100 2100 39  0000 R CNN
F 1 "WP513GDT" V 9050 2100 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 9100 2200 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513GDT-78019.pdf" H 9100 2200 50  0001 C CNN
	1    9100 2200
	0    -1   -1   0   
$EndComp
Text Notes 9350 2600 0    50   ~ 0
If = 10 mA
$Comp
L power:GND #PWR040
U 1 1 5F6AAC46
P 4100 5250
AR Path="/5ED8FE6A/5F6AAC46" Ref="#PWR040"  Part="1" 
AR Path="/5EDBB486/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6AAC46" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6AAC46" Ref="#PWR?"  Part="1" 
F 0 "#PWR040" H 4100 5000 50  0001 C CNN
F 1 "GND" H 4105 5077 39  0000 C CNN
F 2 "" H 4100 5250 50  0001 C CNN
F 3 "" H 4100 5250 50  0001 C CNN
	1    4100 5250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4100 5250 4100 5150
Wire Wire Line
	4100 4850 4100 4950
$Comp
L power:GND #PWR036
U 1 1 5F6AAC4E
P 3900 5250
AR Path="/5ED8FE6A/5F6AAC4E" Ref="#PWR036"  Part="1" 
AR Path="/5EDBB486/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6AAC4E" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6AAC4E" Ref="#PWR?"  Part="1" 
F 0 "#PWR036" H 3900 5000 50  0001 C CNN
F 1 "GND" H 3905 5077 39  0000 C CNN
F 2 "" H 3900 5250 50  0001 C CNN
F 3 "" H 3900 5250 50  0001 C CNN
	1    3900 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4950 3900 4850
Wire Wire Line
	3900 5250 3900 5150
$Comp
L Device:R_Small R11
U 1 1 5F6AAC56
P 3900 5050
AR Path="/5ED8FE6A/5F6AAC56" Ref="R11"  Part="1" 
AR Path="/5EDBB486/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6AAC56" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6AAC56" Ref="R?"  Part="1" 
F 0 "R11" H 3959 5096 39  0000 L CNN
F 1 "140" H 3959 5005 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3900 5050 50  0001 C CNN
F 3 "~" H 3900 5050 50  0001 C CNN
	1    3900 5050
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R13
U 1 1 5F6AAC5C
P 4100 5050
AR Path="/5ED8FE6A/5F6AAC5C" Ref="R13"  Part="1" 
AR Path="/5EDBB486/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6AAC5C" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6AAC5C" Ref="R?"  Part="1" 
F 0 "R13" H 4159 5096 39  0000 L CNN
F 1 "130" H 4159 5005 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4100 5050 50  0001 C CNN
F 3 "~" H 4100 5050 50  0001 C CNN
	1    4100 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5F6AAC62
P 3900 4700
F 0 "D2" V 3900 4850 39  0000 R CNN
F 1 "WP513IDT" V 3850 5050 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 3900 4700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513IDT-79442.pdf" H 3900 4700 50  0001 C CNN
	1    3900 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D4
U 1 1 5F6AAC68
P 4100 4700
F 0 "D4" V 4100 4600 39  0000 R CNN
F 1 "WP513GDT" V 4050 4600 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 4100 4700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513GDT-78019.pdf" H 4100 4700 50  0001 C CNN
	1    4100 4700
	0    -1   -1   0   
$EndComp
Text Notes 4350 5100 0    50   ~ 0
If = 10 mA
$Comp
L power:GND #PWR052
U 1 1 5F6B15D9
P 6600 5250
AR Path="/5ED8FE6A/5F6B15D9" Ref="#PWR052"  Part="1" 
AR Path="/5EDBB486/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6B15D9" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6B15D9" Ref="#PWR?"  Part="1" 
F 0 "#PWR052" H 6600 5000 50  0001 C CNN
F 1 "GND" H 6605 5077 39  0000 C CNN
F 2 "" H 6600 5250 50  0001 C CNN
F 3 "" H 6600 5250 50  0001 C CNN
	1    6600 5250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6600 5250 6600 5150
Wire Wire Line
	6600 4850 6600 4950
$Comp
L power:GND #PWR048
U 1 1 5F6B15E1
P 6400 5250
AR Path="/5ED8FE6A/5F6B15E1" Ref="#PWR048"  Part="1" 
AR Path="/5EDBB486/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6B15E1" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6B15E1" Ref="#PWR?"  Part="1" 
F 0 "#PWR048" H 6400 5000 50  0001 C CNN
F 1 "GND" H 6405 5077 39  0000 C CNN
F 2 "" H 6400 5250 50  0001 C CNN
F 3 "" H 6400 5250 50  0001 C CNN
	1    6400 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4950 6400 4850
Wire Wire Line
	6400 5250 6400 5150
$Comp
L Device:R_Small R19
U 1 1 5F6B15E9
P 6400 5050
AR Path="/5ED8FE6A/5F6B15E9" Ref="R19"  Part="1" 
AR Path="/5EDBB486/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6B15E9" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6B15E9" Ref="R?"  Part="1" 
F 0 "R19" H 6459 5096 39  0000 L CNN
F 1 "140" H 6459 5005 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6400 5050 50  0001 C CNN
F 3 "~" H 6400 5050 50  0001 C CNN
	1    6400 5050
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R21
U 1 1 5F6B15EF
P 6600 5050
AR Path="/5ED8FE6A/5F6B15EF" Ref="R21"  Part="1" 
AR Path="/5EDBB486/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6B15EF" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6B15EF" Ref="R?"  Part="1" 
F 0 "R21" H 6659 5096 39  0000 L CNN
F 1 "130" H 6659 5005 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6600 5050 50  0001 C CNN
F 3 "~" H 6600 5050 50  0001 C CNN
	1    6600 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D6
U 1 1 5F6B15F5
P 6400 4700
F 0 "D6" V 6400 4850 39  0000 R CNN
F 1 "WP513IDT" V 6350 5050 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 6400 4700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513IDT-79442.pdf" H 6400 4700 50  0001 C CNN
	1    6400 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D8
U 1 1 5F6B15FB
P 6600 4700
F 0 "D8" V 6600 4600 39  0000 R CNN
F 1 "WP513GDT" V 6550 4600 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 6600 4700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513GDT-78019.pdf" H 6600 4700 50  0001 C CNN
	1    6600 4700
	0    -1   -1   0   
$EndComp
Text Notes 6850 5100 0    50   ~ 0
If = 10 mA
$Comp
L power:GND #PWR064
U 1 1 5F6B6893
P 9100 5250
AR Path="/5ED8FE6A/5F6B6893" Ref="#PWR064"  Part="1" 
AR Path="/5EDBB486/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6B6893" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6B6893" Ref="#PWR?"  Part="1" 
F 0 "#PWR064" H 9100 5000 50  0001 C CNN
F 1 "GND" H 9105 5077 39  0000 C CNN
F 2 "" H 9100 5250 50  0001 C CNN
F 3 "" H 9100 5250 50  0001 C CNN
	1    9100 5250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9100 5250 9100 5150
Wire Wire Line
	9100 4850 9100 4950
$Comp
L power:GND #PWR060
U 1 1 5F6B689B
P 8900 5250
AR Path="/5ED8FE6A/5F6B689B" Ref="#PWR060"  Part="1" 
AR Path="/5EDBB486/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBA0E/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDBBFB6/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDBC559/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDBCAF9/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD0A1/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDBF4CB/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDBFAC2/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDC00C9/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDC06CE/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDC0CB7/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EDC12A6/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EE2BC4E/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C3CA/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C7D7/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EE2C9E2/5F6B689B" Ref="#PWR?"  Part="1" 
AR Path="/5EE2CBEC/5F6B689B" Ref="#PWR?"  Part="1" 
F 0 "#PWR060" H 8900 5000 50  0001 C CNN
F 1 "GND" H 8905 5077 39  0000 C CNN
F 2 "" H 8900 5250 50  0001 C CNN
F 3 "" H 8900 5250 50  0001 C CNN
	1    8900 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 4950 8900 4850
Wire Wire Line
	8900 5250 8900 5150
$Comp
L Device:R_Small R27
U 1 1 5F6B68A3
P 8900 5050
AR Path="/5ED8FE6A/5F6B68A3" Ref="R27"  Part="1" 
AR Path="/5EDBB486/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6B68A3" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6B68A3" Ref="R?"  Part="1" 
F 0 "R27" H 8959 5096 39  0000 L CNN
F 1 "140" H 8959 5005 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8900 5050 50  0001 C CNN
F 3 "~" H 8900 5050 50  0001 C CNN
	1    8900 5050
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R29
U 1 1 5F6B68A9
P 9100 5050
AR Path="/5ED8FE6A/5F6B68A9" Ref="R29"  Part="1" 
AR Path="/5EDBB486/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDBBA0E/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDBBFB6/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDBC559/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDBCAF9/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDBD0A1/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDBF4CB/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDBFAC2/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDC00C9/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDC06CE/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDC0CB7/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EDC12A6/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EE2BC4E/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EE2C3CA/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EE2C7D7/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EE2C9E2/5F6B68A9" Ref="R?"  Part="1" 
AR Path="/5EE2CBEC/5F6B68A9" Ref="R?"  Part="1" 
F 0 "R29" H 9159 5096 39  0000 L CNN
F 1 "130" H 9159 5005 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9100 5050 50  0001 C CNN
F 3 "~" H 9100 5050 50  0001 C CNN
	1    9100 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D10
U 1 1 5F6B68AF
P 8900 4700
F 0 "D10" V 8900 4850 39  0000 R CNN
F 1 "WP513IDT" V 8850 5050 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 8900 4700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513IDT-79442.pdf" H 8900 4700 50  0001 C CNN
	1    8900 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D12
U 1 1 5F6B68B5
P 9100 4700
F 0 "D12" V 9100 4600 39  0000 R CNN
F 1 "WP513GDT" V 9050 4600 39  0000 R CNN
F 2 "LEGOS:LED_Rectangular_W5.0mm_H2.5mm" H 9100 4700 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/216/WP513GDT-78019.pdf" H 9100 4700 50  0001 C CNN
	1    9100 4700
	0    -1   -1   0   
$EndComp
Text Notes 9350 5100 0    50   ~ 0
If = 10 mA
Wire Wire Line
	2550 7250 2750 7250
Wire Wire Line
	2550 7150 2750 7150
Wire Wire Line
	2550 7050 2750 7050
Wire Wire Line
	2550 6950 2750 6950
Wire Wire Line
	2550 6850 2750 6850
Wire Wire Line
	2550 6750 2750 6750
Wire Wire Line
	1500 7250 1700 7250
Wire Wire Line
	1500 7150 1700 7150
Wire Wire Line
	1500 7050 1700 7050
Wire Wire Line
	1500 6950 1700 6950
Wire Wire Line
	1500 6850 1700 6850
Wire Wire Line
	1500 6750 1700 6750
$Bitmap
Pos 2150 6350
Scale 1.000000
Data
89 50 4E 47 0D 0A 1A 0A 00 00 00 0D 49 48 44 52 00 00 02 19 00 00 00 79 08 03 00 00 00 A3 AF C3 
CA 00 00 00 03 73 42 49 54 08 08 08 DB E1 4F E0 00 00 00 6C 50 4C 54 45 C0 DC C0 FF FF FF FF 00 
00 00 C0 40 FF FB F0 A0 40 00 00 40 80 E0 C0 80 00 00 40 A6 CA F0 E0 A0 40 00 00 00 40 A0 C0 00 
00 80 80 00 00 60 C0 C0 E0 E0 80 40 20 00 00 60 C0 A0 A0 40 C0 60 00 A0 40 80 A0 40 40 A0 C0 C0 
00 60 80 60 60 40 60 60 00 40 40 80 A0 60 00 40 40 40 C0 C0 80 60 00 80 40 00 40 00 40 40 40 00 
80 60 40 00 D6 50 9A 95 00 00 09 9F 49 44 41 54 78 9C ED 9D E9 82 A3 36 10 84 87 C4 36 63 7C CD 
10 36 F7 9D F7 7F C7 A0 D6 6D 63 68 70 4B B4 98 AE 1F BB 18 43 31 40 21 09 AC 0F BD BD 89 44 C3 
AA 68 45 ED F7 1D B1 BE A7 16 F1 FE 72 91 24 43 92 31 2C 49 86 24 63 58 92 0C 49 C6 B0 24 19 92 
8C 61 49 32 24 19 C3 92 64 64 4D C6 6E 7F 40 CC 62 A1 F1 64 D4 EF C7 AA 6A 4E BD D4 44 65 A6 CF 
E3 8E 63 5F C2 FA A7 FE 50 D4 A7 8B 72 DC 5D C7 CD AA 42 92 A1 F7 6B 72 5F B6 96 8C 0B C4 E3 A6 
76 42 4D 7E 7C 8E EE CA 78 32 2E 47 33 71 3A B5 D5 86 92 81 38 30 8F AA CD F5 C6 52 D8 64 EC AE 
FD 89 6C F4 9E 7C 7C 8E 9D 4E 64 32 2E 3F BC 1F 37 96 8C 89 03 F3 A8 8D 24 E3 DC 17 19 2D CC 1D 
2F FF B0 C9 78 53 2E DB 4A 46 D5 A9 FF 6A 5D 5F F6 39 39 A9 82 16 6A 9A 7E 2F BB B6 EB 67 77 FD 
37 DD B9 9F D5 2F D9 E9 1A A8 B3 0B C3 D2 5D DB E8 D5 D7 16 36 19 5D 3F E5 4E 63 77 19 C9 3A 36 
19 C7 BA 3F 12 1B 4B 86 2A 54 D5 A4 BA 76 E0 5A FA 76 84 79 BB 1F 55 0E D4 75 05 C9 E8 A7 E0 EA 
D2 65 46 07 CD AE 9B FA E7 6C BE 9C 3E 2A E9 85 6D 81 AA BD 72 85 E5 0B C9 B0 AD 59 75 FC 7A 9B 
8D 25 A3 3F 5E BB EB CD 4E C0 AE ED F6 D1 41 83 64 1C CC A1 75 C9 30 A5 B1 5A A4 B3 F9 59 5B C8 
32 03 1A A0 3E 19 14 B5 C9 51 F9 6D 2C 19 7D F9 00 D5 02 A4 BF 81 4B E0 E3 F3 66 0E 5A 6B 0F 5D 
A7 2F B3 5B 90 0C 88 13 7C 2E 2D 19 F0 B7 42 2B B4 AA 5C 7B E3 89 E3 D8 E6 A2 64 F4 07 F2 A7 6D 
25 A3 BF E8 5D 12 D4 C7 53 F0 79 2C 19 66 A1 BA C0 32 43 57 84 A6 16 81 E9 E7 8E 63 9B 8B 93 B1 
BB FE BC A9 64 A8 42 35 2A 05 FB 33 EE 1B 67 3E 19 8F B5 C9 35 8E 4D 49 C9 D8 E9 3B 09 1D 92 B1 
22 63 4E 32 54 33 7E 43 C9 D0 CF 33 E0 D6 BE 3E 54 75 AB 8F 9F 6B 81 FA 93 0F 8D CC D6 D6 34 6A 
56 A3 5B A0 B7 02 93 61 9E 65 A8 1B AD F7 5F 5E 28 33 EC 33 50 5B FC 8E 5A 81 CA 48 06 EC D7 CD 
ED E3 C1 1C AB E8 AE 15 76 F8 10 DE 98 76 6A 09 5D 84 D8 E7 CC 85 24 E3 89 9A B1 F3 29 BF 9B 4C 
A9 1B 2D 73 B9 68 D9 2F 6A 61 33 EB C1 71 F9 1F 33 28 49 C6 3A 92 DF 5A 25 19 C3 92 64 E4 4F 46 
19 92 64 48 32 86 95 13 5F 58 26 F6 C9 58 FB 00 A5 13 75 D6 C4 6F 13 92 64 70 F3 E3 22 49 06 37 
3F 2E 92 64 70 F3 E3 22 49 06 37 3F 2E 92 64 E4 F4 E3 F0 7B 08 56 92 0C 62 BF B1 93 6F 3A E8 3C 
CE 7F EC 58 3B F8 9C 14 11 2C BA EC 61 93 D1 98 1E 7F 08 C7 CC 7E AE 33 22 92 61 49 E4 B7 DB EB 
9F 19 C7 4E 8D EE 22 EA E0 1D A7 92 93 01 9D 0C AE A8 53 99 D7 6F B7 D7 70 82 66 62 30 0C 4B 22 
BF FA F2 AB FB 99 FD 4E 16 1E 50 7D E1 20 04 1F BF 0D F8 C4 88 41 29 C9 D0 07 A7 19 EB 18 EC 1D 
B3 FA 39 04 A6 C5 32 2C 89 FC BA B6 76 5D 80 EF 64 CF B9 FA 85 FA B9 59 99 C9 D0 BF BA 8F F7 F2 
73 8E 39 FD 5C 97 54 D5 11 11 C7 B0 A4 F1 EB 9B 10 7A 39 D7 0F 07 3A EF 28 C6 E4 77 0B 36 36 41 
5F 5A DD 85 D2 76 21 EE 57 32 F0 89 EF E7 13 62 27 76 66 44 A6 58 9E C5 6E 4C 59 B4 B9 93 A1 F3 
3C D6 2B 23 70 CC E9 E7 CE 9A 21 3D 10 0C 4B 1A 3F E8 DC 0B 61 38 54 0E 39 31 8C 89 29 0D 4C 41 
79 F2 FB DE FD 71 F6 6B EA A5 42 3A 25 FA 63 60 66 48 A6 38 9E C5 6D 0C FA F8 4B 32 B4 1C E8 60 
19 A0 69 86 25 89 1F 94 35 B0 57 8E 11 00 44 40 F7 76 7D B7 30 8A DE 84 CA 06 B4 39 BE FD 69 BA 
04 BB 64 78 3A 25 E8 F4 E7 67 86 64 8A E5 59 62 BE 45 CA 0C 23 7F 59 5D 8E 48 86 25 89 1F EC 0A 
2C 08 8C 80 45 4E 74 6B C1 44 C2 9D B4 8F 4F 65 DE 2F F7 DB DB 5F 9A 5C 72 C9 88 19 04 B3 86 9F 
19 91 29 96 67 31 1B F3 DD 8D 69 34 AB 9D 81 02 74 B3 FA F9 76 81 ED 75 3C C9 B0 24 F1 D3 3D 84 
4F BA 62 F0 89 0F 93 11 3C CC 50 35 C2 C7 DF C7 E6 BC DB DF 6A 4B F3 E0 92 11 90 29 77 3C CB 3A 
C9 E0 7B 6F D2 05 78 3E 8E 61 49 E1 67 02 55 EB 9E E0 BE 90 09 93 11 EC 2D DC A4 EC CF DD AD 6A 
DA C6 46 A0 8E EE 83 A2 DA E4 1A D4 26 D7 F0 DE 38 E4 59 D6 A9 4D E0 80 21 DF 01 90 D7 4F 3F 79 
30 A4 07 8A 61 49 E1 67 EE B2 D4 69 83 C2 C6 20 27 26 19 BA AA 31 E1 39 D8 D7 90 34 FF FC 7B EC 
3F FE 67 AF 74 7D CD 47 74 8A 3D CD 61 0B D4 91 29 21 CF A2 37 06 78 7D F6 67 A0 1D EA 95 32 E0 
98 D9 CF DE EE 61 19 96 14 7E 16 01 EF FF 87 B3 E9 90 93 D6 5A DE 6C A9 6F BF D2 E9 D3 41 34 77 
35 70 13 1A D2 29 AE 00 08 EF 5A 3D 99 12 F0 2C EA 0B F5 74 F5 DC 64 4F C6 0C 47 F1 1B 12 AE EA 
64 24 49 46 1E 3F 16 AF C4 98 25 49 06 37 3F 2E 92 64 70 F3 E3 22 49 06 37 3F 2E CA 89 2F 88 0A 
13 75 D6 C4 6F 13 92 64 70 F3 E3 22 49 06 37 3F 2E 92 64 70 F3 E3 22 49 06 37 3F 2E 92 64 E4 F4 
13 DE E4 0B FB A5 E4 4D B2 0A 9B 0C 6A DE 64 EA DD 91 78 3F 5A DE C4 BB D9 21 4B A6 86 2E C9 C8 
9B 64 15 32 19 D4 BC 89 3A 3E 24 C9 A0 E5 4D 22 37 DB 39 62 A2 06 C8 C8 9B 64 15 2E 19 D4 7D BA 
54 D2 46 5F 51 8D F6 A3 E5 4D 22 B7 79 C9 C8 C1 9B 64 15 2E 19 D4 BC 49 35 F5 F2 72 AC 1F 2D 6F 
12 BB CD 4A 46 5A DE C4 8F 7E 62 86 46 09 37 91 68 3C 14 5C 32 A8 FB 8E 57 64 C9 20 E5 4D 62 B7 
59 C9 48 CA 9B DC 8D 7E B2 BF DF 44 92 CE 1F 85 27 83 96 37 89 DD DC 60 14 A7 C7 36 E3 BD 5F 62 
DE C4 8F 7E 62 5E 66 1F 6C 22 D5 DB A8 0B 4F 06 2D 6F 12 BB CD 29 33 92 F2 26 F7 A3 9F F4 0B DD 
6D 62 BD 64 50 F3 26 15 7D 3B 83 82 37 89 DD E6 24 23 07 6F E2 46 3F 81 64 44 9B 58 2F 19 E4 F7 
26 54 C9 20 E6 4D 22 B7 19 C9 48 CD 9B 3C 0C 8D 72 B7 89 F5 92 41 CE 9B 90 25 83 96 37 89 DC 66 
24 23 35 6F E2 47 3F B1 43 A3 C4 9B 58 31 19 D4 BC 49 63 0B DF D7 FD 68 79 93 C0 6D 46 32 52 F3 
26 C1 E8 27 F6 1E 35 DA C4 9A C9 98 E1 28 7E 43 A2 E1 4D 32 3E 00 93 64 E4 F1 23 7A E4 20 C9 F8 
C2 7E A3 92 64 7C 61 3F 2E 92 64 70 F3 E3 A2 9C F8 82 A8 30 51 67 4D FC 36 21 49 06 37 3F 2E 92 
64 70 F3 E3 22 49 06 37 3F 2E 92 64 70 F3 E3 22 49 06 37 3F 2E 92 64 AC E4 37 40 12 F0 12 3A 19 
64 7C 88 16 15 BF B2 98 0F 41 38 CE E2 57 FC 6A 38 6D 25 19 64 7C 88 11 11 BF F2 02 1F 82 71 C4 
F3 2B E1 6A 93 C2 F5 8D 5B 5B C8 64 90 F1 21 5A 54 7D C4 5E E0 43 30 8E 78 7E 25 5C 6D 52 9B 4A 
06 59 1F 2C 2D 22 7E E5 05 3E 04 E5 88 E6 57 A2 D5 86 98 10 78 B7 6F A7 EB 1B D3 39 28 1E AC 24 
19 36 B2 58 EB 24 83 A8 2F FA 0B 7C 08 CA 11 CD AF 44 AB 0D 33 21 8A 22 32 BB AC F7 3E 1A AC 24 
1D 36 B2 58 45 27 63 39 1F 82 73 44 F3 2B D1 6A 03 4C 88 AB 36 3D 97 12 0F 56 92 AE D3 DE 62 15 
9D 8C E5 7C 08 CE 11 CD AF 44 AB 3D 61 42 A0 15 0F D5 89 4F C6 1D 48 22 C9 A0 E2 57 96 F3 21 38 
47 34 BF 12 AD F6 84 09 81 7A 25 2E 33 EE 41 12 49 06 D9 BD C9 52 3E 04 E9 88 E7 57 C2 D5 9E 30 
21 9E 2D 08 CA 0C 0F 92 48 32 B4 88 F8 95 A5 7C 08 D2 11 CF AF 84 AB 3D 61 42 60 16 B4 45 FD E8 
45 01 48 52 6A 32 08 F9 10 2D 2A 7E 65 19 1F 82 74 9C C3 AF F8 D5 9E 30 21 70 87 DA EA 04 9C 34 
CC 16 81 24 55 99 C9 98 E3 F8 D5 FD D6 7F 29 0A 89 24 19 E4 7E 92 8C 67 8E 5F DD 4F 92 F1 CC 51 
FC 36 21 49 06 37 3F 2E CA 89 2F 88 0A 13 75 D6 C4 6F 13 92 64 70 F3 E3 22 49 06 37 3F 2E 92 64 
70 F3 E3 22 49 06 37 3F 2E 92 64 70 F3 E3 22 49 06 37 BF 40 AB 92 07 E8 64 10 F3 26 54 7E D4 BC 
C9 42 BF 31 34 05 FA 76 2D EA E1 59 42 32 A8 79 13 22 3F 6A DE 64 B1 DF 08 9A 02 3F E2 BB F7 89 
63 B5 3E 79 80 4C 06 31 6F 42 E5 47 CD 9B 2C F6 7B 8E A6 E0 D0 89 07 15 93 0C EA 3E 5D 34 7E D4 
BC C9 72 BF 08 4D 71 10 49 15 57 08 8F 70 89 EF E4 63 D1 14 F3 A6 58 06 4C 4A D9 C9 20 E6 4D 96 
FB 85 68 8A 87 48 2A 3F D2 4D 35 08 97 B8 09 87 A6 D8 B7 4B AF CF A4 14 9D 0C 6A DE 64 B9 5F 80 
A6 04 10 49 15 D6 2F 43 70 89 9D 70 68 8A 6B 8F AC CF A4 14 9D 0C 6A DE 64 B9 5F 80 A6 04 10 49 
15 96 19 43 70 89 9D 70 68 8A 23 11 D6 67 52 CA 4E 06 31 6F F2 C2 F8 26 1E 4D 09 20 92 2A 6C 67 
0C C1 25 3E 19 B7 68 21 0E 4C 4A D1 C9 20 E7 4D 16 FB 05 68 4A 00 91 C0 37 AE 2D 3A 00 97 F8 DA 
E4 6C 17 7A A8 4D D6 62 52 CA 4E 06 35 6F B2 D8 2F 44 53 3C 44 02 96 7B 33 68 E2 10 5C E2 26 1C 
9A 62 5B A0 EB 33 29 D8 E7 19 C4 BC 09 99 1F 35 6F B2 D0 2F 42 53 EA B8 C9 DA 58 CB 47 B8 C4 9F 
EB 26 BE 6B 65 C0 A4 C8 EF 26 DC FC B8 48 92 C1 CD 8F 8B 24 19 DC FC B8 48 92 C1 CD 8F 8B 24 19 
DC FC B8 28 27 BE 20 2A 4A FF 03 FD C0 C0 68 28 30 7D B2 00 00 00 00 49 45 4E 44 AE 42 60 82 
EndData
$EndBitmap
$EndSCHEMATC
