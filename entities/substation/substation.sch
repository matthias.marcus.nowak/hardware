EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Substation"
Date "2020-10-03"
Rev "1.3"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR010
U 1 1 5E680CF4
P 4350 5925
F 0 "#PWR010" H 4350 5675 50  0001 C CNN
F 1 "GND" H 4355 5752 50  0000 C CNN
F 2 "" H 4350 5925 50  0001 C CNN
F 3 "" H 4350 5925 50  0001 C CNN
	1    4350 5925
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C4
U 1 1 5E8CA2CF
P 4350 2925
F 0 "C4" H 4500 2875 39  0000 R CNN
F 1 "10u" H 4500 2975 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4350 2925 50  0001 C CNN
F 3 "~" H 4350 2925 50  0001 C CNN
	1    4350 2925
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5E8CB08F
P 4100 2925
F 0 "C3" H 4050 2875 39  0000 R CNN
F 1 "100n" H 4100 2975 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4100 2925 50  0001 C CNN
F 3 "~" H 4100 2925 50  0001 C CNN
	1    4100 2925
	1    0    0    1   
$EndComp
Wire Wire Line
	4350 3025 4350 3125
Wire Wire Line
	4100 3025 4100 3125
$Comp
L power:GND #PWR07
U 1 1 5E8F3CB2
P 4100 2825
F 0 "#PWR07" H 4100 2575 50  0001 C CNN
F 1 "GND" H 4105 2652 39  0000 C CNN
F 2 "" H 4100 2825 50  0001 C CNN
F 3 "" H 4100 2825 50  0001 C CNN
	1    4100 2825
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5E8F4451
P 4350 2825
F 0 "#PWR09" H 4350 2575 50  0001 C CNN
F 1 "GND" H 4355 2652 39  0000 C CNN
F 2 "" H 4350 2825 50  0001 C CNN
F 3 "" H 4350 2825 50  0001 C CNN
	1    4350 2825
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5E8F840A
P 3000 3725
F 0 "#PWR01" H 3000 3475 50  0001 C CNN
F 1 "GND" H 3005 3552 39  0000 C CNN
F 2 "" H 3000 3725 50  0001 C CNN
F 3 "" H 3000 3725 50  0001 C CNN
	1    3000 3725
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5E8F73C1
P 3350 3525
F 0 "C1" H 3300 3475 39  0000 R CNN
F 1 "220n" H 3300 3575 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 3525 50  0001 C CNN
F 3 "~" H 3350 3525 50  0001 C CNN
	1    3350 3525
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5E8F6D03
P 3350 3075
F 0 "R1" H 3400 3025 39  0000 L CNN
F 1 "10k" H 3400 3075 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3350 3075 50  0001 C CNN
F 3 "~" H 3350 3075 50  0001 C CNN
	1    3350 3075
	-1   0    0    1   
$EndComp
Text Label 3650 3325 2    39   ~ 0
EN
Text Notes 5450 3325 0    28   ~ 0
UP during boot
Text Notes 5450 3925 0    28   ~ 0
DOWN during boot
Text Label 5050 3425 0    39   ~ 0
TXD
Text Label 5050 3625 0    39   ~ 0
RXD
Wire Wire Line
	4950 3625 5050 3625
Text Notes 5450 3525 0    28   ~ 0
DOWN during prog
Text Notes 5450 4225 0    28   ~ 0
UP during boot for log
Text Notes 3600 5075 1    28   ~ 0
Internally connected to the flash
Text Notes 5450 3825 0    28   ~ 0
UP during boot
Wire Wire Line
	4950 3325 5050 3325
Wire Wire Line
	3350 3325 3350 3425
Wire Wire Line
	3900 2825 3900 3125
Wire Wire Line
	4350 3125 4100 3125
Connection ~ 4100 3125
Wire Wire Line
	4100 3125 3900 3125
Wire Wire Line
	5050 3425 4950 3425
Text Notes 3700 3625 2    28   ~ 0
only input
Text Notes 3700 3525 2    28   ~ 0
only input
NoConn ~ 3750 5025
NoConn ~ 3750 4925
NoConn ~ 3750 4825
NoConn ~ 3750 4725
NoConn ~ 3750 4625
NoConn ~ 3750 4525
Text Notes 5350 5525 0    28   ~ 0
only input
Text Notes 5350 5625 0    28   ~ 0
only input
Text Label 7050 4325 2    39   ~ 0
F1
Text Notes 4950 3325 0    39   ~ 0
IO0
$Comp
L power:+5V #PWR04
U 1 1 5F0D3FB3
P 7050 1525
F 0 "#PWR04" H 7050 1375 50  0001 C CNN
F 1 "+5V" V 7050 1725 39  0000 C CNN
F 2 "" H 7050 1525 50  0001 C CNN
F 3 "" H 7050 1525 50  0001 C CNN
	1    7050 1525
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR011
U 1 1 5F0D86A3
P 7050 1775
AR Path="/5F0D86A3" Ref="#PWR011"  Part="1" 
AR Path="/5E872B89/5F0D86A3" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F0D86A3" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F0D86A3" Ref="#PWR?"  Part="1" 
F 0 "#PWR011" H 7050 1625 50  0001 C CNN
F 1 "+3.3V" V 7050 1975 39  0000 C CNN
F 2 "" H 7050 1775 50  0001 C CNN
F 3 "" H 7050 1775 50  0001 C CNN
	1    7050 1775
	0    -1   1    0   
$EndComp
$Sheet
S 7150 4225 800  750 
U 5ED64706
F0 "Fault" 39
F1 "sub_fault.sch" 39
F2 "F1" I L 7150 4325 39 
F3 "F2" I L 7150 4425 39 
F4 "F3" I L 7150 4525 39 
F5 "F4" I L 7150 4625 39 
F6 "F5" I L 7150 4725 39 
F7 "F6" I L 7150 4825 39 
$EndSheet
Wire Wire Line
	5050 5125 4950 5125
Wire Wire Line
	5050 4925 4950 4925
Wire Wire Line
	4950 4525 5050 4525
Wire Wire Line
	4950 4625 5050 4625
Wire Wire Line
	4950 4825 5050 4825
$Comp
L power:+3.3V #PWR06
U 1 1 5EE49B63
P 3900 2825
AR Path="/5EE49B63" Ref="#PWR06"  Part="1" 
AR Path="/5E872B89/5EE49B63" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5EE49B63" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE49B63" Ref="#PWR?"  Part="1" 
F 0 "#PWR06" H 3900 2675 50  0001 C CNN
F 1 "+3.3V" H 3915 2998 39  0000 C CNN
F 2 "" H 3900 2825 50  0001 C CNN
F 3 "" H 3900 2825 50  0001 C CNN
	1    3900 2825
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR02
U 1 1 5EE4CE76
P 3350 2825
AR Path="/5EE4CE76" Ref="#PWR02"  Part="1" 
AR Path="/5E872B89/5EE4CE76" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5EE4CE76" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE4CE76" Ref="#PWR?"  Part="1" 
F 0 "#PWR02" H 3350 2675 50  0001 C CNN
F 1 "+3.3V" H 3365 2998 39  0000 C CNN
F 2 "" H 3350 2825 50  0001 C CNN
F 3 "" H 3350 2825 50  0001 C CNN
	1    3350 2825
	-1   0    0    -1  
$EndComp
Text Label 5050 4625 0    39   ~ 0
~EN_2
Text Label 5050 4825 0    39   ~ 0
~EN_3
Text Label 5050 5325 0    39   ~ 0
~EN_5
Text Label 5050 5025 0    39   ~ 0
~EN_6
Connection ~ 4350 3125
$Comp
L RF_Module:ESP32-WROOM-32D U2
U 1 1 5ED27F09
P 4350 4525
F 0 "U2" H 3750 4925 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 3400 4825 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 4350 3025 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 4050 4575 50  0001 C CNN
	1    4350 4525
	1    0    0    -1  
$EndComp
Text Notes 5350 5025 0    28   ~ 0
DAC
Text Notes 5350 5125 0    28   ~ 0
DAC
Connection ~ 3350 3325
Wire Wire Line
	7150 4325 7050 4325
Text Label 7050 4425 2    39   ~ 0
F2
Text Label 7050 4525 2    39   ~ 0
F3
Text Label 7050 4625 2    39   ~ 0
F4
Text Label 7050 4725 2    39   ~ 0
F5
Text Label 7050 4825 2    39   ~ 0
F6
Wire Wire Line
	7050 4825 7150 4825
Wire Wire Line
	7150 4725 7050 4725
Wire Wire Line
	7050 4625 7150 4625
Wire Wire Line
	7150 4525 7050 4525
Wire Wire Line
	7050 4425 7150 4425
NoConn ~ 3750 3525
Text Label 5050 4725 0    39   ~ 0
~IRQ_2
Text Label 5050 5625 0    39   ~ 0
~IRQ_4
Wire Wire Line
	4950 5225 5050 5225
Wire Wire Line
	5050 5325 4950 5325
Wire Wire Line
	5050 5425 4950 5425
Wire Wire Line
	5050 5625 4950 5625
Wire Wire Line
	5050 5025 4950 5025
Text Label 5050 3325 0    39   ~ 0
IO0
Wire Wire Line
	7050 2325 7150 2325
Wire Wire Line
	7050 2425 7150 2425
Wire Wire Line
	7050 2525 7150 2525
Wire Wire Line
	7050 2625 7100 2625
Wire Wire Line
	7100 2625 7100 2725
Connection ~ 7100 2625
Wire Wire Line
	7100 2625 7150 2625
Wire Wire Line
	7100 2725 7150 2725
Text Label 7050 2425 2    39   ~ 0
TXD
Text Label 7050 2525 2    39   ~ 0
RXD
Text Label 7050 2625 2    39   ~ 0
IO0
Text Label 7050 2325 2    39   ~ 0
EN
Text Label 8050 3225 0    39   ~ 0
~EN_1
Text Label 8050 3325 0    39   ~ 0
~EN_2
Text Label 8050 3425 0    39   ~ 0
~EN_3
Text Label 8050 3525 0    39   ~ 0
~EN_4
Text Label 8050 3625 0    39   ~ 0
~EN_5
Text Label 8050 3725 0    39   ~ 0
~EN_6
Text Label 8050 2425 0    39   ~ 0
~IRQ_1
Text Label 8050 2525 0    39   ~ 0
~IRQ_2
Text Label 8050 2625 0    39   ~ 0
~IRQ_3
Text Label 8050 2725 0    39   ~ 0
~IRQ_4
Text Label 8050 2825 0    39   ~ 0
~IRQ_5
Text Label 8050 2925 0    39   ~ 0
~IRQ_6
Wire Wire Line
	7950 3225 8650 3225
Wire Wire Line
	7950 3325 8650 3325
Wire Wire Line
	7950 3425 8650 3425
Wire Wire Line
	7950 3525 8650 3525
Wire Wire Line
	7950 3625 8650 3625
Wire Wire Line
	7950 3725 8650 3725
$Sheet
S 8650 2225 700  1650
U 5ED8FE6A
F0 "Status" 39
F1 "sub_led.sch" 39
F2 "~EN_1" I L 8650 3225 50 
F3 "~EN_2" I L 8650 3325 50 
F4 "~EN_3" I L 8650 3425 50 
F5 "~EN_4" I L 8650 3525 50 
F6 "~EN_5" I L 8650 3625 50 
F7 "~EN_6" I L 8650 3725 50 
F8 "~IRQ_1" I L 8650 2425 50 
F9 "~IRQ_2" I L 8650 2525 50 
F10 "~IRQ_3" I L 8650 2625 50 
F11 "~IRQ_4" I L 8650 2725 50 
F12 "~IRQ_5" I L 8650 2825 50 
F13 "~IRQ_6" I L 8650 2925 50 
$EndSheet
Wire Wire Line
	7950 2925 8650 2925
Wire Wire Line
	7950 2825 8650 2825
Wire Wire Line
	7950 2725 8650 2725
Wire Wire Line
	7950 2625 8650 2625
Wire Wire Line
	7950 2525 8650 2525
Wire Wire Line
	7950 2425 8650 2425
Text Label 5050 4425 0    39   ~ 0
F1
Wire Wire Line
	5050 4425 4950 4425
Text Label 5050 3925 0    39   ~ 0
F4
Wire Wire Line
	5050 3525 4950 3525
Text Label 5050 4125 0    39   ~ 0
F5
Wire Wire Line
	5050 4125 4950 4125
Text Label 5050 3725 0    39   ~ 0
F2
Wire Wire Line
	5050 3725 4950 3725
Text Label 5050 5225 0    39   ~ 0
F6
Wire Wire Line
	5050 4325 4950 4325
$Comp
L Switch:SW_Push SW1
U 1 1 5F5D1AE0
P 3000 3525
F 0 "SW1" V 3050 3825 39  0000 R CNN
F 1 "PTS815" V 3000 3925 39  0000 R CNN
F 2 "LEGOS:PTS815" H 3000 3725 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/60/pts815-1535658.pdf" H 3000 3725 50  0001 C CNN
	1    3000 3525
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F5D647C
P 3350 3725
F 0 "#PWR03" H 3350 3475 50  0001 C CNN
F 1 "GND" H 3355 3552 39  0000 C CNN
F 2 "" H 3350 3725 50  0001 C CNN
F 3 "" H 3350 3725 50  0001 C CNN
	1    3350 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3625 3350 3725
Wire Wire Line
	3000 3325 3350 3325
Wire Wire Line
	3350 2825 3350 2975
Wire Wire Line
	3350 3175 3350 3325
$Sheet
S 7150 2225 800  1650
U 5F2C5EA5
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "EN" O L 7150 2325 50 
F3 "TXD" O L 7150 2425 50 
F4 "RXD" I L 7150 2525 50 
F5 "IO0" B L 7150 2625 50 
F6 "ID" B L 7150 2725 50 
F7 "~IRQ_1" I R 7950 2425 50 
F8 "~IRQ_2" I R 7950 2525 50 
F9 "~IRQ_3" I R 7950 2625 50 
F10 "~IRQ_4" I R 7950 2725 50 
F11 "~IRQ_5" I R 7950 2825 50 
F12 "~IRQ_6" I R 7950 2925 50 
F13 "~EN_6" O R 7950 3725 50 
F14 "~EN_5" O R 7950 3625 50 
F15 "~EN_4" O R 7950 3525 50 
F16 "~EN_3" O R 7950 3425 50 
F17 "~EN_2" O R 7950 3325 50 
F18 "~EN_1" O R 7950 3225 50 
$EndSheet
Text Label 5050 4025 0    39   ~ 0
~EN_4
Text Label 5050 4525 0    39   ~ 0
~IRQ_1
Wire Wire Line
	5050 3925 4950 3925
Text Label 5050 3525 0    39   ~ 0
F3
Wire Wire Line
	5050 4025 4950 4025
Wire Wire Line
	5050 4725 4950 4725
NoConn ~ 3750 3625
Wire Wire Line
	3750 3325 3350 3325
$Comp
L Power_Supervisor:LM809 U3
U 1 1 5F39BC44
P 2150 3325
F 0 "U3" H 1921 3363 39  0000 R CNN
F 1 "TLV803E" H 1921 3288 39  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2450 3425 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv803e.pdf" H 2450 3425 50  0001 C CNN
	1    2150 3325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR074
U 1 1 5F39CE3F
P 2150 3725
F 0 "#PWR074" H 2150 3475 50  0001 C CNN
F 1 "GND" H 2155 3552 39  0000 C CNN
F 2 "" H 2150 3725 50  0001 C CNN
F 3 "" H 2150 3725 50  0001 C CNN
	1    2150 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3325 2450 3325
Connection ~ 3000 3325
$Comp
L power:+3.3V #PWR073
U 1 1 5F39F111
P 2150 2825
AR Path="/5F39F111" Ref="#PWR073"  Part="1" 
AR Path="/5E872B89/5F39F111" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F39F111" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F39F111" Ref="#PWR?"  Part="1" 
F 0 "#PWR073" H 2150 2675 50  0001 C CNN
F 1 "+3.3V" H 2165 2998 39  0000 C CNN
F 2 "" H 2150 2825 50  0001 C CNN
F 3 "" H 2150 2825 50  0001 C CNN
	1    2150 2825
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2150 2825 2150 2925
$Sheet
S 7150 1425 800  450 
U 5F5A3EFF
F0 "Auto Power" 39
F1 "sub_power_auto.sch" 39
F2 "VIN" I L 7150 1525 50 
F3 "VOUT" O L 7150 1775 50 
$EndSheet
Wire Wire Line
	7150 1525 7050 1525
Wire Wire Line
	7150 1775 7050 1775
$Sheet
S 8650 1425 700  450 
U 5F55DF71
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
Text Label 5050 4325 0    39   ~ 0
~EN_1
Text Label 5050 5125 0    39   ~ 0
~IRQ_6
Text Label 5050 5425 0    39   ~ 0
~IRQ_5
Text Label 5050 4925 0    39   ~ 0
~IRQ_3
$EndSCHEMATC
