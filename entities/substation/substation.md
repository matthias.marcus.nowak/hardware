# Substation
The Substation is a key element for the automation of a power grid. It is able to detect the connected branches, detect the fault status from the respective branch telemetry and eventually trip the branch reclosers. The circuitry includes a sub-block with status LED indicatorsa and a sub-block for fault animations based on burning resistors.

## Schematic
[<img src="docs/substation_sch.png"  width="812" height="500">](docs/substation_sch-main.png)

[<img src="docs/substation_sch-sub_connectors.png"  width="145" height="100">](docs/substation_sch-sub_connectors.png)
&nbsp;
[<img src="docs/substation_sch-sub_led.png"  width="145" height="100">](docs/substation_sch-sub_led.png)
&nbsp;
[<img src="docs/substation_sch-sub_fault.png"  width="145" height="100">](docs/substation_sch-sub_fault.png)
&nbsp;
[<img src="docs/substation_sch-sub_power_auto.png"  width="145" height="100">](docs/substation_sch-sub_power_auto.png)
&nbsp;
[<img src="docs/substation_sch-sub_mechanical.png"  width="145" height="100">](docs/substation_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/substation_pcb.png"  width="532" height="500">](docs/substation_pcb-brd.png)

[<img src="docs/substation_pcb-F_Cu.png"  width="145" height="100">](docs/substation_pcb-F_Cu.png)
&nbsp;
[<img src="docs/substation_pcb-In1_Cu.png"  width="145" height="100">](docs/substation_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/substation_pcb-In2_Cu.png"  width="145" height="100">](docs/substation_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/substation_pcb-B_Cu.png"  width="145" height="100">](docs/substation_pcb-B_Cu.png)

## Media

[<img src="docs/substation_3d-top.png"  width="100" height="100">](docs/substation_3d-top.png)
&nbsp;
[<img src="docs/substation_3d-bottom.png"  width="100" height="100">](docs/substation_3d-bottom.png)
&nbsp;
[<img src="docs/substation_brd.png"  width="137" height="100">](docs/substation_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/substation/substation.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/substation/substation.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/substation/substation.md)