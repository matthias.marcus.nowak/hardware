# Electric Car station
The Electric Car Station provides a charging point for electric vehicles. The power absorbed from the grid is determined by the charging pattern, stored in a RFID tag inside the car. The circuitry includes docking connectors for RC522 standard RFID reader modules, a virtual load sub-block, a telemetry sub-block for monitoring the self power consumption in the 3.3 V bus and a sub-block for programmable LEDs animations.

## Schematic
[<img src="docs/ec_station_sch.png"  width="1014" height="500">](docs/ec_station_sch-main.png)

[<img src="docs/ec_station_sch-sub_connectors.png"  width="145" height="100">](docs/ec_station_sch-sub_connectors.png)
&nbsp;
[<img src="docs/ec_station_sch-sub_virtual_load.png"  width="145" height="100">](docs/ec_station_sch-sub_virtual_load.png)
&nbsp;
[<img src="docs/ec_station_sch-sub_led_charging-CAR1.png"  width="145" height="100">](docs/ec_station_sch-sub_led_charging-CAR1.png)
&nbsp;
[<img src="docs/ec_station_sch-sub_led_charging-CAR2.png"  width="145" height="100">](docs/ec_station_sch-sub_led_charging-CAR2.png)
&nbsp;
[<img src="docs/ec_station_sch-sub_telemetry.png"  width="145" height="100">](docs/ec_station_sch-sub_telemetry.png)
&nbsp;
[<img src="docs/ec_station_sch-sub_mechanical.png"  width="145" height="100">](docs/ec_station_sch-sub_mechanical.png)

## Printed Circuit Board

[<img src="docs/ec_station_pcb.png"  width="531" height="500">](docs/ec_station_pcb-brd.png)

[<img src="docs/ec_station_pcb-F_Cu.png"  width="145" height="100">](docs/ec_station_pcb-F_Cu.png)
&nbsp;
[<img src="docs/ec_station_pcb-In1_Cu.png"  width="145" height="100">](docs/ec_station_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/ec_station_pcb-In2_Cu.png"  width="145" height="100">](docs/ec_station_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/ec_station_pcb-B_Cu.png"  width="145" height="100">](docs/ec_station_pcb-B_Cu.png)

## Media

[<img src="docs/ec_station_3d-top.png"  width="96" height="100">](docs/ec_station_3d-top.png)
&nbsp;
[<img src="docs/ec_station_3d-bottom.png"  width="96" height="100">](docs/ec_station_3d-bottom.png)
&nbsp;
[<img src="docs/ec_station_brd.png"  width="147" height="100">](docs/ec_station_brd.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/ec_station/ec_station.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/ec_station/ec_station.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/ec_station/ec_station.md)

