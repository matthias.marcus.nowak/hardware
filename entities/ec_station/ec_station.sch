EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title "Electric Car Station"
Date "2020-10-02"
Rev "1.1"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D. Wienands, S. Melcher, L. Jeske, D. Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR07
U 1 1 5E680CF4
P 3600 5600
F 0 "#PWR07" H 3600 5350 50  0001 C CNN
F 1 "GND" H 3605 5427 39  0000 C CNN
F 2 "" H 3600 5600 50  0001 C CNN
F 3 "" H 3600 5600 50  0001 C CNN
	1    3600 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C3
U 1 1 5E8CA2CF
P 3600 2600
F 0 "C3" H 3750 2550 39  0000 R CNN
F 1 "10u" H 3750 2650 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3600 2600 50  0001 C CNN
F 3 "~" H 3600 2600 50  0001 C CNN
	1    3600 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5E8CB08F
P 3350 2600
F 0 "C2" H 3300 2550 39  0000 R CNN
F 1 "100n" H 3350 2650 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 2600 50  0001 C CNN
F 3 "~" H 3350 2600 50  0001 C CNN
	1    3350 2600
	1    0    0    1   
$EndComp
Wire Wire Line
	3600 2700 3600 2800
Wire Wire Line
	3350 2700 3350 2800
$Comp
L power:GND #PWR05
U 1 1 5E8F3CB2
P 3350 2500
F 0 "#PWR05" H 3350 2250 50  0001 C CNN
F 1 "GND" H 3355 2327 39  0000 C CNN
F 2 "" H 3350 2500 50  0001 C CNN
F 3 "" H 3350 2500 50  0001 C CNN
	1    3350 2500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5E8F4451
P 3600 2500
F 0 "#PWR06" H 3600 2250 50  0001 C CNN
F 1 "GND" H 3605 2327 39  0000 C CNN
F 2 "" H 3600 2500 50  0001 C CNN
F 3 "" H 3600 2500 50  0001 C CNN
	1    3600 2500
	-1   0    0    1   
$EndComp
Connection ~ 2500 3000
$Comp
L Device:C_Small C1
U 1 1 5E8F73C1
P 2500 3200
F 0 "C1" H 2450 3150 39  0000 R CNN
F 1 "10u" H 2450 3250 39  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2500 3200 50  0001 C CNN
F 3 "~" H 2500 3200 50  0001 C CNN
	1    2500 3200
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5E8F6D03
P 2500 2750
F 0 "R1" H 2550 2700 39  0000 L CNN
F 1 "10k" H 2550 2750 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2500 2750 50  0001 C CNN
F 3 "~" H 2500 2750 50  0001 C CNN
	1    2500 2750
	-1   0    0    1   
$EndComp
NoConn ~ 3000 4200
NoConn ~ 3000 4300
NoConn ~ 3000 4400
NoConn ~ 3000 4500
NoConn ~ 3000 4600
NoConn ~ 3000 4700
Text Label 2650 3000 2    39   ~ 0
EN
Text Label 6900 3600 2    39   ~ 0
EN
Text Label 6900 3700 2    39   ~ 0
TXD
Text Label 6900 3800 2    39   ~ 0
RXD
Text Label 4400 3000 0    39   ~ 0
ID
Text Notes 4650 3000 0    28   ~ 0
UP during boot
Text Notes 4650 3600 0    28   ~ 0
DOWN during boot
Text Label 4400 3100 0    39   ~ 0
TXD
Text Label 4400 3300 0    39   ~ 0
RXD
Text Notes 4650 3200 0    28   ~ 0
DOWN during prog
Text Notes 4650 3900 0    28   ~ 0
UP during boot for log
Text Notes 2900 4800 1    28   ~ 0
Internally connected to the flash
Text Notes 4650 3500 0    28   ~ 0
UP during boot
Text Notes 4200 3000 0    39   ~ 0
IO0
Wire Wire Line
	2500 3000 2500 3100
NoConn ~ 3000 3200
NoConn ~ 3000 3300
Wire Wire Line
	2500 3000 3000 3000
Wire Wire Line
	3150 2500 3150 2800
Wire Wire Line
	2500 2500 2500 2650
Wire Wire Line
	2500 2850 2500 3000
Wire Wire Line
	3600 2800 3350 2800
Connection ~ 3350 2800
Wire Wire Line
	3350 2800 3150 2800
$Comp
L LEGOS:VBUS #PWR08
U 1 1 5ED32C52
P 6750 4900
F 0 "#PWR08" H 6750 4750 50  0001 C CNN
F 1 "VBUS" H 6765 5073 39  0000 C CNN
F 2 "" H 6750 4900 50  0001 C CNN
F 3 "" H 6750 4900 50  0001 C CNN
	1    6750 4900
	1    0    0    -1  
$EndComp
Text Notes 4200 4700 0    50   ~ 0
DAC
Text Label 6900 3900 2    39   ~ 0
ID
Text Notes 2950 3300 2    28   ~ 0
only input
Text Notes 2950 3200 2    28   ~ 0
only input
Text Notes 4400 5200 0    28   ~ 0
only input
Text Notes 4600 5300 0    28   ~ 0
only input
Text Notes 4200 4800 0    50   ~ 0
DAC
Wire Wire Line
	4200 3000 4400 3000
Wire Wire Line
	4200 3100 4400 3100
Wire Wire Line
	4200 3300 4400 3300
Text Label 9400 3600 2    39   ~ 0
PWM_1G
Text Label 9400 3700 2    39   ~ 0
PWM_1R
Text Label 9400 4200 2    39   ~ 0
PWM_2R
Text Label 9400 4100 2    39   ~ 0
PWM_2G
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5EDE8FA3
P 8375 2150
F 0 "J2" H 8455 2142 50  0000 L CNN
F 1 "RFID_car2" H 8455 2051 50  0000 L CNN
F 2 "LEGOS:RFC522" H 8375 2150 50  0001 C CNN
F 3 "~" H 8375 2150 50  0001 C CNN
	1    8375 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5EDE6EF3
P 7200 2150
F 0 "J1" H 7280 2142 50  0000 L CNN
F 1 "RFID_car1" H 7280 2051 50  0000 L CNN
F 2 "LEGOS:RFC522" H 7200 2150 50  0001 C CNN
F 3 "~" H 7200 2150 50  0001 C CNN
	1    7200 2150
	1    0    0    -1  
$EndComp
Text Label 7000 1850 2    39   ~ 0
~CS1
$Comp
L power:GND #PWR010
U 1 1 5EDED9DB
P 6800 2350
F 0 "#PWR010" H 6800 2100 50  0001 C CNN
F 1 "GND" V 6805 2177 39  0000 C CNN
F 2 "" H 6800 2350 50  0001 C CNN
F 3 "" H 6800 2350 50  0001 C CNN
	1    6800 2350
	0    1    1    0   
$EndComp
Text Label 7000 2450 2    39   ~ 0
~RST
Wire Wire Line
	7975 2350 8175 2350
Wire Wire Line
	6800 2350 7000 2350
Text Label 8175 1850 2    39   ~ 0
~CS2
$Comp
L power:GND #PWR013
U 1 1 5EDF291C
P 7975 2350
F 0 "#PWR013" H 7975 2100 50  0001 C CNN
F 1 "GND" V 7980 2177 39  0000 C CNN
F 2 "" H 7975 2350 50  0001 C CNN
F 3 "" H 7975 2350 50  0001 C CNN
	1    7975 2350
	0    1    1    0   
$EndComp
Text Label 8175 2450 2    39   ~ 0
~RST
Wire Wire Line
	4400 4800 4200 4800
Text Label 4400 3700 0    39   ~ 0
~CS1
Wire Wire Line
	4200 3800 4400 3800
Wire Wire Line
	4400 4900 4200 4900
Text Label 4400 3500 0    39   ~ 0
~CS2
Wire Wire Line
	4200 3500 4400 3500
Wire Wire Line
	4400 3900 4200 3900
$Comp
L Device:R_Small R2
U 1 1 5EDF935E
P 7000 1650
F 0 "R2" H 7050 1600 39  0000 L CNN
F 1 "10k" H 7050 1650 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7000 1650 50  0001 C CNN
F 3 "~" H 7000 1650 50  0001 C CNN
	1    7000 1650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5EDFB248
P 8175 1650
F 0 "R3" H 8225 1600 39  0000 L CNN
F 1 "10k" H 8225 1650 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8175 1650 50  0001 C CNN
F 3 "~" H 8175 1650 50  0001 C CNN
	1    8175 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	8175 1850 8175 1750
Wire Wire Line
	7000 1850 7000 1750
Connection ~ 3600 2800
$Comp
L RF_Module:ESP32-WROOM-32D U1
U 1 1 5ED28AD7
P 3600 4200
F 0 "U1" H 3000 4650 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 2650 4550 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 3600 2700 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 3300 4250 50  0001 C CNN
	1    3600 4200
	1    0    0    -1  
$EndComp
Wire Notes Line
	8900 1250 6100 1250
Wire Notes Line
	6100 1250 6100 2900
Wire Notes Line
	6100 2900 8900 2900
Wire Notes Line
	8900 1250 8900 2900
Text Notes 7500 1200 0    50   ~ 0
RFID
Wire Wire Line
	4400 4400 4200 4400
Wire Wire Line
	4200 4300 4400 4300
$Sheet
S 9500 3500 600  300 
U 5EDC7347
F0 "CAR1" 39
F1 "sub_led_charging.sch" 39
F2 "LED_G" I L 9500 3600 39 
F3 "LED_R" I L 9500 3700 39 
$EndSheet
$Sheet
S 9500 4000 600  300 
U 5EF91F99
F0 "CAR2" 39
F1 "sub_led_charging.sch" 39
F2 "LED_G" I L 9500 4100 39 
F3 "LED_R" I L 9500 4200 39 
$EndSheet
Text GLabel 4400 4300 2    39   Input ~ 0
SDA
Text GLabel 4400 4400 2    39   Input ~ 0
SCL
Wire Wire Line
	6900 3600 7000 3600
Wire Wire Line
	6900 3700 7000 3700
Wire Wire Line
	6900 3800 7000 3800
Wire Wire Line
	6900 3900 6950 3900
Wire Wire Line
	6900 4500 7000 4500
Text Label 6900 4500 2    39   ~ 0
LOAD
Wire Wire Line
	7000 5050 6750 5050
Wire Wire Line
	7000 5250 6750 5250
Wire Wire Line
	7000 4900 6750 4900
Wire Wire Line
	6750 4900 6750 5050
Connection ~ 6750 4900
$Comp
L power:+3.3V #PWR09
U 1 1 5F116946
P 6750 5250
AR Path="/5F116946" Ref="#PWR09"  Part="1" 
AR Path="/5E872B89/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F116946" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F116946" Ref="#PWR?"  Part="1" 
F 0 "#PWR09" H 6750 5100 50  0001 C CNN
F 1 "+3.3V" H 6765 5423 39  0000 C CNN
F 2 "" H 6750 5250 50  0001 C CNN
F 3 "" H 6750 5250 50  0001 C CNN
	1    6750 5250
	1    0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 5F11784E
P 3150 2500
AR Path="/5F11784E" Ref="#PWR04"  Part="1" 
AR Path="/5E872B89/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F11784E" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F11784E" Ref="#PWR?"  Part="1" 
F 0 "#PWR04" H 3150 2350 50  0001 C CNN
F 1 "+3.3V" H 3165 2673 39  0000 C CNN
F 2 "" H 3150 2500 50  0001 C CNN
F 3 "" H 3150 2500 50  0001 C CNN
	1    3150 2500
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR02
U 1 1 5F11A48F
P 2500 2500
AR Path="/5F11A48F" Ref="#PWR02"  Part="1" 
AR Path="/5E872B89/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F11A48F" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F11A48F" Ref="#PWR?"  Part="1" 
F 0 "#PWR02" H 2500 2350 50  0001 C CNN
F 1 "+3.3V" H 2515 2673 39  0000 C CNN
F 2 "" H 2500 2500 50  0001 C CNN
F 3 "" H 2500 2500 50  0001 C CNN
	1    2500 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9400 3600 9500 3600
Wire Wire Line
	9400 3700 9500 3700
Wire Wire Line
	9400 4100 9500 4100
Wire Wire Line
	9400 4200 9500 4200
Text GLabel 7000 2050 0    39   Input ~ 0
MOSI
Text GLabel 7000 2150 0    39   Input ~ 0
MISO
Text GLabel 7000 1950 0    39   Input ~ 0
CLK
Text GLabel 4400 4800 2    39   Input ~ 0
MISO
Text GLabel 4400 4900 2    39   Input ~ 0
MOSI
Text GLabel 4400 3800 2    39   Input ~ 0
CLK
$Comp
L power:+3.3V #PWR012
U 1 1 5F0F4709
P 7000 2550
AR Path="/5F0F4709" Ref="#PWR012"  Part="1" 
AR Path="/5E872B89/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F0F4709" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F0F4709" Ref="#PWR?"  Part="1" 
F 0 "#PWR012" H 7000 2400 50  0001 C CNN
F 1 "+3.3V" V 7000 2775 39  0000 C CNN
F 2 "" H 7000 2550 50  0001 C CNN
F 3 "" H 7000 2550 50  0001 C CNN
	1    7000 2550
	0    -1   1    0   
$EndComp
$Comp
L power:+3.3V #PWR015
U 1 1 5F0F63C6
P 8175 2550
AR Path="/5F0F63C6" Ref="#PWR015"  Part="1" 
AR Path="/5E872B89/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F0F63C6" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F0F63C6" Ref="#PWR?"  Part="1" 
F 0 "#PWR015" H 8175 2400 50  0001 C CNN
F 1 "+3.3V" V 8175 2775 39  0000 C CNN
F 2 "" H 8175 2550 50  0001 C CNN
F 3 "" H 8175 2550 50  0001 C CNN
	1    8175 2550
	0    -1   1    0   
$EndComp
$Comp
L power:+3.3V #PWR011
U 1 1 5F0F6B45
P 7000 1550
AR Path="/5F0F6B45" Ref="#PWR011"  Part="1" 
AR Path="/5E872B89/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F0F6B45" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F0F6B45" Ref="#PWR?"  Part="1" 
F 0 "#PWR011" H 7000 1400 50  0001 C CNN
F 1 "+3.3V" H 7015 1723 39  0000 C CNN
F 2 "" H 7000 1550 50  0001 C CNN
F 3 "" H 7000 1550 50  0001 C CNN
	1    7000 1550
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR014
U 1 1 5F0F7BFF
P 8175 1550
AR Path="/5F0F7BFF" Ref="#PWR014"  Part="1" 
AR Path="/5E872B89/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F0F7BFF" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F0F7BFF" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 8175 1400 50  0001 C CNN
F 1 "+3.3V" H 8190 1723 39  0000 C CNN
F 2 "" H 8175 1550 50  0001 C CNN
F 3 "" H 8175 1550 50  0001 C CNN
	1    8175 1550
	-1   0    0    -1  
$EndComp
Text Label 7000 2250 2    39   ~ 0
IRQ1
Text Label 8175 2250 2    39   ~ 0
IRQ2
Text GLabel 8175 2050 0    39   Input ~ 0
MOSI
Text GLabel 8175 2150 0    39   Input ~ 0
MISO
Text GLabel 8175 1950 0    39   Input ~ 0
CLK
Text Label 4400 3400 0    39   ~ 0
IRQ2
$Sheet
S 7000 3500 600  650 
U 5ED76C75
F0 "Connectors" 39
F1 "sub_connectors.sch" 39
F2 "EN" I L 7000 3600 39 
F3 "TXD" O L 7000 3700 39 
F4 "RXD" I L 7000 3800 39 
F5 "IO0" B L 7000 3900 39 
F6 "ID" B L 7000 4000 39 
$EndSheet
$Sheet
S 7000 4400 600  200 
U 5ED37053
F0 "Virtual Load" 39
F1 "sub_virtual_load.sch" 39
F2 "IN" I L 7000 4500 39 
$EndSheet
$Sheet
S 7000 4800 600  600 
U 5F10E493
F0 "Telemetry" 39
F1 "sub_telemetry.sch" 39
F2 "V+" I L 7000 5050 39 
F3 "V-" I L 7000 5250 39 
F4 "~IRQ" I R 7600 4900 39 
F5 "VCOM" I L 7000 4900 39 
F6 "A1" I R 7600 5050 39 
F7 "A0" I R 7600 5250 39 
$EndSheet
Wire Wire Line
	6950 3900 6950 4000
Wire Wire Line
	6950 4000 7000 4000
Connection ~ 6950 3900
Wire Wire Line
	6950 3900 7000 3900
$Comp
L Switch:SW_Push SW1
U 1 1 5F317784
P 2100 3200
F 0 "SW1" V 2050 3000 39  0000 L CNN
F 1 "SW_Push" V 2100 2850 39  0000 L CNN
F 2 "LEGOS:PTS815" H 2100 3400 50  0001 C CNN
F 3 "~" H 2100 3400 50  0001 C CNN
	1    2100 3200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5F31A7B3
P 2100 3400
F 0 "#PWR01" H 2100 3150 50  0001 C CNN
F 1 "GND" H 2105 3227 39  0000 C CNN
F 2 "" H 2100 3400 50  0001 C CNN
F 3 "" H 2100 3400 50  0001 C CNN
	1    2100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3000 2500 3000
Wire Wire Line
	2500 3300 2500 3400
$Comp
L power:GND #PWR03
U 1 1 5E8F840A
P 2500 3400
F 0 "#PWR03" H 2500 3150 50  0001 C CNN
F 1 "GND" H 2505 3227 39  0000 C CNN
F 2 "" H 2500 3400 50  0001 C CNN
F 3 "" H 2500 3400 50  0001 C CNN
	1    2500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5100 4200 5100
Wire Wire Line
	4200 5000 4400 5000
Wire Wire Line
	4400 4200 4200 4200
NoConn ~ 4200 5200
Wire Wire Line
	4200 4100 4400 4100
Text Label 4400 4000 0    39   ~ 0
PWM_2G
Wire Wire Line
	4400 4000 4200 4000
Text Label 4400 4100 0    39   ~ 0
PWM_2R
Wire Wire Line
	4200 5300 4400 5300
Text Label 4400 5300 0    39   ~ 0
IRQ1
$Comp
L Power_Supervisor:LM809 U4
U 1 1 5F3A530C
P 1300 3000
F 0 "U4" H 1071 3038 39  0000 R CNN
F 1 "TLV803E" H 1071 2963 39  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1600 3100 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv803e.pdf" H 1600 3100 50  0001 C CNN
	1    1300 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F3A5BBD
P 1300 3400
F 0 "#PWR0102" H 1300 3150 50  0001 C CNN
F 1 "GND" H 1305 3227 39  0000 C CNN
F 2 "" H 1300 3400 50  0001 C CNN
F 3 "" H 1300 3400 50  0001 C CNN
	1    1300 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0103
U 1 1 5F3A602F
P 1300 2500
AR Path="/5F3A602F" Ref="#PWR0103"  Part="1" 
AR Path="/5E872B89/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F3A602F" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F3A602F" Ref="#PWR?"  Part="1" 
F 0 "#PWR0103" H 1300 2350 50  0001 C CNN
F 1 "+3.3V" H 1315 2673 39  0000 C CNN
F 2 "" H 1300 2500 50  0001 C CNN
F 3 "" H 1300 2500 50  0001 C CNN
	1    1300 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1300 2500 1300 2600
Wire Wire Line
	2100 3000 1600 3000
Connection ~ 2100 3000
$Comp
L power:GND #PWR0104
U 1 1 5F4112AD
P 7700 5250
F 0 "#PWR0104" H 7700 5000 50  0001 C CNN
F 1 "GND" V 7700 5050 39  0000 C CNN
F 2 "" H 7700 5250 50  0001 C CNN
F 3 "" H 7700 5250 50  0001 C CNN
	1    7700 5250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7700 5250 7600 5250
Wire Wire Line
	7600 5050 7700 5050
Wire Wire Line
	7700 5050 7700 5250
Connection ~ 7700 5250
Text Label 7700 4900 0    39   ~ 0
~IRQ
Wire Wire Line
	7700 4900 7600 4900
Text Label 4400 4200 0    39   ~ 0
~IRQ
Wire Wire Line
	4400 3400 4200 3400
$Sheet
S 9500 4500 600  300 
U 5F54F38E
F0 "Mechanical" 39
F1 "sub_mechanical.sch" 39
$EndSheet
Text Label 4400 3900 0    39   ~ 0
~RST
Wire Wire Line
	4400 3700 4200 3700
Text Label 4400 4700 0    39   ~ 0
LOAD
Wire Wire Line
	4200 4700 4400 4700
$Comp
L Device:R_Small R21
U 1 1 5F59863B
P 7650 1650
F 0 "R21" H 7700 1600 39  0000 L CNN
F 1 "10k" H 7700 1650 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7650 1650 50  0001 C CNN
F 3 "~" H 7650 1650 50  0001 C CNN
	1    7650 1650
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR0115
U 1 1 5F598641
P 7650 1550
AR Path="/5F598641" Ref="#PWR0115"  Part="1" 
AR Path="/5E872B89/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5EE71F43/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5EE65594/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F06E731/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F06EB4F/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F0706C1/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F070B09/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F070F02/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5EE404E6/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5ED37053/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5EF4D338/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5ED6690B/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5ED76C75/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5EF58462/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5EFD3723/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F128218/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5ECB7EC7/5F128218/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F0E69AF/5F598641" Ref="#PWR?"  Part="1" 
AR Path="/5F10E493/5F598641" Ref="#PWR?"  Part="1" 
F 0 "#PWR0115" H 7650 1400 50  0001 C CNN
F 1 "+3.3V" H 7665 1723 39  0000 C CNN
F 2 "" H 7650 1550 50  0001 C CNN
F 3 "" H 7650 1550 50  0001 C CNN
	1    7650 1550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7650 1850 7650 1750
Text Label 7650 1850 2    39   ~ 0
~RST
Text Label 4400 5100 0    39   ~ 0
PWM_1R
Text Label 4400 5000 0    39   ~ 0
PWM_1G
$EndSCHEMATC
