EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 7
Title "Sub Connectors"
Date "2020-07-02"
Rev "1.0"
Comp "Institute for Automation of Complex Power Systems - E.ON ERC - RWTH Aachen University"
Comment1 "Designer: D.Wienands, S.Melcher, L.Jeske, D.Moll"
Comment2 "Supervisor: C. Guarnieri Calò Carducci"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J3
U 1 1 5F2C644D
P 4500 3450
F 0 "J3" H 4550 3767 39  0000 C CNN
F 1 "PROG" H 4550 3676 39  0000 C CNN
F 2 "LEGOS:IDC-Header_2x03_P1.27mm_Vertical" H 4500 3450 50  0001 C CNN
F 3 "https://www.mouser.de/datasheet/2/18/20021511-1362852.pdf" H 4500 3450 50  0001 C CNN
F 4 "20021511-00006T4LF" H 4500 3450 50  0001 C CNN "Mfr"
	1    4500 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3350 4150 3350
Wire Wire Line
	4300 3450 4150 3450
Wire Wire Line
	4300 3550 4150 3550
Wire Wire Line
	4800 3550 4950 3550
Wire Wire Line
	4800 3450 5150 3450
$Comp
L power:GND #PWR029
U 1 1 5F2C6458
P 5150 3450
F 0 "#PWR029" H 5150 3200 50  0001 C CNN
F 1 "GND" H 5155 3277 39  0000 C CNN
F 2 "" H 5150 3450 50  0001 C CNN
F 3 "" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    -1  
$EndComp
Text HLabel 4150 3350 0    39   Output ~ 0
EN
Text HLabel 4150 3450 0    39   Output ~ 0
TXD
Text HLabel 4150 3550 0    39   Input ~ 0
RXD
Text HLabel 4950 3550 2    39   BiDi ~ 0
IO0
Wire Notes Line
	3750 2850 5450 2850
Wire Notes Line
	5450 2850 5450 3950
Wire Notes Line
	5450 3950 3750 3950
Wire Notes Line
	3750 3950 3750 2850
Text Notes 3750 4050 0    50   ~ 0
MCU Programming
Wire Notes Line
	5850 2850 7850 2850
Wire Notes Line
	7850 2850 7850 3950
Wire Notes Line
	7850 3950 5850 3950
Wire Notes Line
	5850 3950 5850 2850
Text Notes 5850 4050 0    50   ~ 0
Main Socket\n
$Comp
L Device:R_Small R12
U 1 1 5F2C646C
P 7500 3550
F 0 "R12" H 7550 3500 39  0000 L CNN
F 1 "4.7k" H 7550 3550 39  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7500 3550 50  0001 C CNN
F 3 "~" H 7500 3550 50  0001 C CNN
	1    7500 3550
	1    0    0    -1  
$EndComp
Text HLabel 7700 3750 2    39   BiDi ~ 0
ID
$Comp
L Diode:BAT60A D3
U 1 1 5F2C6473
P 4950 3350
F 0 "D3" H 4950 3543 39  0000 C CNN
F 1 "BAT60A" H 4950 3468 39  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 4950 3175 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/Infineon-BAT60ASERIES-DS-v01_01-en.pdf?fileId=db3a304313d846880113def70c9304a9" H 4950 3350 50  0001 C CNN
	1    4950 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 3350 5150 3350
Wire Wire Line
	6150 3150 6300 3150
Wire Wire Line
	6150 3250 6300 3250
Wire Wire Line
	6150 3350 6300 3350
Wire Wire Line
	6150 3450 6300 3450
Wire Wire Line
	6150 3550 6300 3550
Wire Wire Line
	6150 3650 6300 3650
$Comp
L LEGOS:VBUS #PWR030
U 1 1 5F2C6486
P 6300 3750
F 0 "#PWR030" H 6300 3600 50  0001 C CNN
F 1 "VBUS" V 6300 3900 39  0000 L CNN
F 2 "" H 6300 3750 50  0001 C CNN
F 3 "" H 6300 3750 50  0001 C CNN
	1    6300 3750
	0    -1   1    0   
$EndComp
$Comp
L power:+5V #PWR031
U 1 1 5F2C648C
P 6300 3850
F 0 "#PWR031" H 6300 3700 50  0001 C CNN
F 1 "+5V" V 6300 4050 39  0000 C CNN
F 2 "" H 6300 3850 50  0001 C CNN
F 3 "" H 6300 3850 50  0001 C CNN
	1    6300 3850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5F2C6492
P 6800 3850
F 0 "#PWR032" H 6800 3600 50  0001 C CNN
F 1 "GND" V 6800 3650 39  0000 C CNN
F 2 "" H 6800 3850 50  0001 C CNN
F 3 "" H 6800 3850 50  0001 C CNN
	1    6800 3850
	0    -1   1    0   
$EndComp
Wire Wire Line
	6950 3150 6800 3150
Wire Wire Line
	6950 3250 6800 3250
Wire Wire Line
	6950 3450 6800 3450
Wire Wire Line
	6950 3350 6800 3350
Wire Wire Line
	6950 3550 6800 3550
Wire Wire Line
	6950 3650 6800 3650
Wire Wire Line
	6800 3750 7500 3750
Wire Wire Line
	7500 3650 7500 3750
Connection ~ 7500 3750
Wire Wire Line
	7500 3750 7700 3750
$Comp
L power:+3.3V #PWR033
U 1 1 5F2C64A2
P 7500 3450
F 0 "#PWR033" H 7500 3300 50  0001 C CNN
F 1 "+3.3V" H 7515 3615 39  0000 C CNN
F 2 "" H 7500 3450 50  0001 C CNN
F 3 "" H 7500 3450 50  0001 C CNN
	1    7500 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR028
U 1 1 5F2C64A8
P 5150 3350
F 0 "#PWR028" H 5150 3200 50  0001 C CNN
F 1 "+3.3V" H 5165 3515 39  0000 C CNN
F 2 "" H 5150 3350 50  0001 C CNN
F 3 "" H 5150 3350 50  0001 C CNN
	1    5150 3350
	1    0    0    -1  
$EndComp
Text HLabel 6950 3150 2    39   Input ~ 0
~IRQ_1
Text HLabel 6950 3250 2    39   Input ~ 0
~IRQ_2
Text HLabel 6950 3350 2    39   Input ~ 0
~IRQ_3
Text HLabel 6950 3450 2    39   Input ~ 0
~IRQ_4
Text HLabel 6950 3550 2    39   Input ~ 0
~IRQ_5
Text HLabel 6950 3650 2    39   Input ~ 0
~IRQ_6
Text HLabel 6150 3650 0    39   Output ~ 0
~EN_6
Text HLabel 6150 3550 0    39   Output ~ 0
~EN_5
Text HLabel 6150 3450 0    39   Output ~ 0
~EN_4
Text HLabel 6150 3350 0    39   Output ~ 0
~EN_3
Text HLabel 6150 3250 0    39   Output ~ 0
~EN_2
Text HLabel 6150 3150 0    39   Output ~ 0
~EN_1
$Comp
L LEGOS:PogoConn J4
U 1 1 5F4EF680
P 6600 3450
F 0 "J4" H 6650 3967 50  0000 C CNN
F 1 "PogoConn" H 6650 3876 50  0000 C CNN
F 2 "LEGOS:PogoConn_Standard" H 6600 3450 50  0001 C CNN
F 3 "~" H 6600 3450 50  0001 C CNN
	1    6600 3450
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
