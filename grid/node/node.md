# Node
The node is a passive device for power distribution that allows the interconnection of up to six branches on the edge and an entity on top. It allows the design of compact triangular grid meshes with self-mapping capabilities: each port can be identified by a 64-bit unique ID accessible via 1-wire. Branches are connected via ultra-low profile pin headers, whereas entities via a custom designed spring-loaded connector.

## Schematic
[<img src="docs/node_sch.png"  width="748" height="500">](docs/node_sch-main.png)

## Printed Circuit Board

[<img src="docs/node_pcb.png"  width="532" height="500">](docs/node_pcb-brd.png)

[<img src="docs/node_pcb-F_Cu.png"  width="145" height="100">](docs/node_pcb-F_Cu.png)
&nbsp;
[<img src="docs/node_pcb-In1_Cu.png"  width="145" height="100">](docs/node_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/node_pcb-In2_Cu.png"  width="145" height="100">](docs/node_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/node_pcb-B_Cu.png"  width="145" height="100">](docs/node_pcb-B_Cu.png)

## Media

[<img src="docs/node_3d-top.png"  width="100" height="100">](docs/node_3d-top.png)
&nbsp;
[<img src="docs/node_3d-bottom.png"  width="100" height="100">](docs/node_3d-bottom.png)
&nbsp;
[<img src="docs/node_brd.png"  width="110" height="100">](docs/node_brd.png)


## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/node/node.md)
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/grid/node/node.md)