%% Positions
p = [...
     11, 12, pi/2-pi/12-0/3*pi;
     12, 12, pi/2-pi/12-1/3*pi;
     13, 12, pi/2-pi/12-2/3*pi;
     14, 12, pi/2-pi/12-3/3*pi;
     15, 12, pi/2-pi/12-4/3*pi;
     16, 12, pi/2-pi/12-5/3*pi;
      6, 12, pi/2+pi/12-0/3*pi;
      1, 12, pi/2+pi/12-1/3*pi;
      2, 12, pi/2+pi/12-2/3*pi;
      3, 12, pi/2+pi/12-3/3*pi;
      4, 12, pi/2+pi/12-4/3*pi;
      5, 12, pi/2+pi/12-5/3*pi;
      7,  0, 0;
      8,4.5, pi/6+0/3*2*pi;
      8,4.5, pi/6+1/3*2*pi;
      8,4.5, pi/6+2/3*2*pi;
      9,  7, pi/2-pi/9+0/3*2*pi;
      9,  7, pi/2-pi/9+1/3*2*pi;
      9,  7, pi/2-pi/9+2/3*2*pi;
     10,  7, pi/2+pi/9+0/3*2*pi;
     10,  7, pi/2+pi/9+1/3*2*pi;
     10,  7, pi/2+pi/9+2/3*2*pi];

v = [ 8,6.43, pi/6+0/3*2*pi;
      8,6.43, pi/6+1/3*2*pi;
      8,6.43, pi/6+2/3*2*pi;
      9,8.93, pi/2-pi/9+0/3*2*pi;
      9,8.93, pi/2-pi/9+1/3*2*pi;
      9,8.93, pi/2-pi/9+2/3*2*pi;
     10,8.93, pi/2+pi/9+0/3*2*pi;
     10,8.93, pi/2+pi/9+1/3*2*pi;
     10,8.93, pi/2+pi/9+2/3*2*pi];

%% Connector  
clc
for i = 1:length(p)
    fprintf('  (pad %d thru_hole circle (at %.5f %.5f 0) (size 1.83 1.83) (drill 0.55) (layers *.Cu *.Mask))\n',p(i,1),p(i,2)*cos(p(i,3)),-p(i,2)*sin(p(i,3)));
end

for i = 1:length(p)
    fprintf('  (model ${KIPRJMOD}/../../libs/3D/mill-max-0908-9-15-20-75-14-11-0.step\n');
    fprintf('    (offset (xyz %.5f %.5f 0))\n',p(i,2)*cos(p(i,3)),p(i,2)*sin(p(i,3)));
    fprintf('    (scale (xyz 1 1 1))\n');
    fprintf('    (rotate (xyz 0 -90 0))\n');
    fprintf('  )\n');
end

%% Target
clc
for i = 1:length(v)
    fprintf('  (pad %d thru_hole circle (at %.5f %.5f 0) (size 0.6 0.6) (drill 0.25) (layers *.Cu))\n',v(i,1),v(i,2)*cos(v(i,3)),-v(i,2)*sin(v(i,3)));
end

for i = 1:length(p)
    fprintf('  (pad %d smd circle (at %.5f %.5f 0) (size 3.6 3.6) (layers F.Cu F.Paste F.Mask))\n',p(i,1),p(i,2)*cos(p(i,3)),-p(i,2)*sin(p(i,3)));
end

for i = 1:length(p)
    fprintf('  (model ${KIPRJMOD}/../../libs/3D/s70-332002045.stp\n');
    fprintf('    (offset (xyz %.5f %.5f 0))\n',p(i,2)*cos(p(i,3)),p(i,2)*sin(p(i,3)));
    fprintf('    (scale (xyz 1 1 1))\n');
    fprintf('    (rotate (xyz -90 0 0))\n');
    fprintf('  )\n');
end