%% Make Polygon

R = 11;                             % Radius (mm)
N = 40;                             % Number of sides
C = [150 100];                      % Center (mm)
P = 0;                              % Phase  (rad)
clc

i = (0:N-1)'/N;
p = R*[cos(2*pi*i+P) sin(2*pi*i+P)] + C;

fprintf('(xy %.2f %.2f) ', p') 



