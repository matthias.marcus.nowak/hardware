# Branch
The branch is an active device for power distribution that allows to measure and control the power flowing among the entities. The device is based on the Espressif ESP32 dual-core MCU with embedded Wi-Fi and BLE. The MCU controls the power flow by means of two bi-directional back-to-back switches. A third switch, connected between the center of the bus and ground, allows the emulation of grid faults. The flow in the two halves of the bus is monitored by two independent monitoring units, placed respectively upstream and downstream of the fault switch in proximity of the node interconnection. A magnetic Unipolar Hall effect switch IC with dual outputs allows for indipendent external triggering of the switches.

## Schematic
[<img src="docs/branch_sch.png"  width="867" height="500">](docs/branch_sch-main.png)

[<img src="docs/branch_sch-sub_bus.png"  width="145" height="100">](docs/branch_sch-sub_bus.png)
&nbsp;
[<img src="docs/branch_sch-sub_led.png"  width="145" height="100">](docs/branch_sch-sub_led.png)


## Printed Circuit Board

[<img src="docs/branch_pcb.png"  width="997" height="500">](docs/branch_pcb-brd.png)

[<img src="docs/branch_pcb-F_Cu.png"  width="145" height="100">](docs/branch_pcb-F_Cu.png)
&nbsp;
[<img src="docs/branch_pcb-In1_Cu.png"  width="145" height="100">](docs/branch_pcb-In1_Cu.png)
&nbsp;
[<img src="docs/branch_pcb-In2_Cu.png"  width="145" height="100">](docs/branch_pcb-In2_Cu.png)
&nbsp;
[<img src="docs/branch_pcb-B_Cu.png"  width="145" height="100">](docs/branch_pcb-B_Cu.png)

## Media

[<img src="docs/branch_3d-top.png"  width="208" height="100">](docs/branch_3d-top.png)
&nbsp;
[<img src="docs/branch_3d-bottom.png"  width="208" height="100">](docs/branch_3d-bottom.png)
&nbsp;
[<img src="docs/branch_brd-mcu.png"  width="196" height="100">](docs/branch_brd-mcu.png)
&nbsp;
[<img src="docs/branch_brd-mcu1.png"  width="133" height="100">](docs/branch_brd-mcu1.png)
&nbsp;
[<img src="docs/branch_brd-conn.png"  width="197" height="100">](docs/branch_brd-conn.png)
&nbsp;
[<img src="docs/branch_brd-led.png"  width="133" height="100">](docs/branch_brd-led.png)


## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/branch/branch.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/branch/branch.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/grid/branch/branch.md)