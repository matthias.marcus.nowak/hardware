%% Fit antenna path

x = [150.85,151.45,nan,152.5,nan,157.515,158.485]';
y = [ 89.6,89.125, nan,88.95,nan, 89.81,89.81]';
[xData, yData] = prepareCurveData( x, y );
fit1 = fit( xData, yData, 'pchipinterp', 'Normalize', 'on' );

x1 = [150.85:0.05:152.5,152.7:0.2:157.515];
y1 = fit1(x1);
plot(x,y,x1,y1)


clc

%% Make path segment
w = [0.25,linspace(0.25,0.52,20)];
w = [w,0.52*ones(1,numel(x1)-numel(w))];
for i=1:numel(x1)-1
fprintf('(segment (start %.6f %.6f) (end %.6f %.6f) (width %.2f) (layer B.Cu) (net 108))\n',x1(i),y1(i),x1(i+1),y1(i+1),w(i))
end


%% Make upper vias 
clc
xv1= 151.4:0.73:158;
yv1= fit1(xv1)-0.8;

for i=1:numel(xv1)
fprintf('(via (at %.6f %.6f) (size 0.6) (drill 0.25) (layers F.Cu B.Cu) (net 4))\n',xv1(i),yv1(i))
end

%% Make lower vias 
clc
xv1= 153.1:0.72:156.725;
yv1= fit1(xv1)+0.8;

for i=1:numel(xv1)
fprintf('(via (at %.6f %.6f) (size 0.6) (drill 0.25) (layers F.Cu B.Cu) (net 4))\n',xv1(i),yv1(i))
end

